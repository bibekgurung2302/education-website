<!DOCTYPE html>
<html lang="en-US">

<head>
    <script>
        console.log(window.location.pathname);
        console.log(window.innerWidth);
        if (window.location.pathname == '/theme/educleverv2/' && document.body.clientWidth <= 480) {
            window.location = "/theme/educleverv2/m-2/";
        }
    </script>
    <meta charset="UTF-8">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="pingback" href="https://educlever.beplusthemes.com/elementary/xmlrpc.php">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Edusite &#8211; Educations WordPress Themes</title>
    <link rel="stylesheet" id="siteground-optimizer-combined-css-2174cc4d0fa8c301895f83b250d4fc29" href="https://educlever.beplusthemes.com/elementary/wp-content/uploads/siteground-optimizer-assets/siteground-optimizer-combined-css-2174cc4d0fa8c301895f83b250d4fc29.css" media="all" />
    <style type="text/css" data-type="vc_shortcodes-custom-css">
        .vc_custom_1571044605598 {
            padding-top: 0px !important;
            padding-right: 0px !important;
            padding-bottom: 20px !important;
            padding-left: 0px !important;
        }

        .vc_custom_1571044588966 {
            padding-top: 100px !important;
            padding-bottom: 60px !important;
            background-image: url(https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/bg-box.jpg?id=2124) !important;
            background-position: center !important;
            background-repeat: no-repeat !important;
            background-size: cover !important;
        }

        .vc_custom_1570700709491 {
            padding-top: 68px !important;
            padding-bottom: 78px !important;
            background-image: url(https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/bg-courses.jpg?id=2130) !important;
            background-position: center !important;
            background-repeat: no-repeat !important;
            background-size: cover !important;
        }

        .vc_custom_1571036268766 {
            margin-bottom: 50px !important;
        }

        .vc_custom_1570848944239 {
            padding-top: 80px !important;
            padding-bottom: 90px !important;
            background-image: url(https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/bg-ga.jpg?id=2154) !important;
            background-position: center !important;
            background-repeat: no-repeat !important;
            background-size: cover !important;
        }

        .vc_custom_1571036132907 {
            padding-top: 90px !important;
            padding-bottom: 100px !important;
        }

        .vc_custom_1571019447485 {
            padding-top: 90px !important;
            padding-bottom: 90px !important;
        }

        .vc_custom_1571036734601 {
            padding-top: 45px !important;
        }

        .vc_custom_1571106895956 {
            padding-top: 90px !important;
            padding-bottom: 90px !important;
        }

        .vc_custom_1571040125175 {
            padding-top: 50px !important;
        }

        .vc_custom_1570680255431 {
            margin-top: -30px !important;
            background-image: url(https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/bg-head.png?id=2105) !important;
            background-position: center !important;
            background-repeat: no-repeat !important;
            background-size: contain !important;
        }

        .vc_custom_1570680026205 {
            margin-bottom: 0px !important;
        }

        .vc_custom_1571040098524 {
            padding-right: 20px !important;
            padding-left: 20px !important;
        }

        .vc_custom_1571040109805 {
            padding-top: 0px !important;
            background-position: center !important;
            background-repeat: no-repeat !important;
            background-size: cover !important;
        }

        .vc_custom_1571037604522 {
            margin-top: 0px !important;
            padding-bottom: 15px !important;
        }

        .vc_custom_1571037623409 {
            margin-top: 0px !important;
            padding-bottom: 15px !important;
        }

        .vc_custom_1571038736641 {
            margin-right: 20px !important;
            margin-bottom: 39px !important;
        }

        .vc_custom_1571038442961 {
            margin-bottom: 39px !important;
        }

        .vc_custom_1571019571504 {
            padding-top: 30px !important;
            padding-bottom: 40px !important;
        }

        .vc_custom_1571039722443 {
            padding-top: 0px !important;
            padding-right: 15px !important;
            padding-left: 15px !important;
            background-color: #ffffff !important;
        }

        .vc_custom_1571039770884 {
            padding-top: 0px !important;
            padding-right: 15px !important;
            padding-left: 15px !important;
            background-color: #ffffff !important;
        }

        .vc_custom_1571039790266 {
            padding-top: 0px !important;
            padding-right: 15px !important;
            padding-left: 15px !important;
            background-color: #ffffff !important;
        }

        .vc_custom_1570697346908 {
            margin-top: -100px !important;
            margin-bottom: 30px !important;
        }

        .vc_custom_1570695770539 {
            margin-bottom: 0px !important;
        }

        .vc_custom_1570696091689 {
            margin-bottom: 35px !important;
        }

        .vc_custom_1570697363224 {
            margin-top: -100px !important;
            margin-bottom: 30px !important;
        }

        .vc_custom_1570695785652 {
            margin-bottom: 0px !important;
        }

        .vc_custom_1570696009229 {
            margin-bottom: 35px !important;
        }

        .vc_custom_1570697384251 {
            margin-top: -100px !important;
            margin-bottom: 30px !important;
        }

        .vc_custom_1570695849235 {
            margin-bottom: 0px !important;
        }

        .vc_custom_1570696071017 {
            margin-bottom: 35px !important;
        }

        .vc_custom_1570699963406 {
            padding-right: 3% !important;
            padding-left: 3% !important;
        }

        .vc_custom_1571036423161 {
            margin-bottom: 0px !important;
        }

        .vc_custom_1571036293049 {
            margin-top: 0px !important;
        }

        .vc_custom_1571026279747 {
            margin-bottom: 0px !important;
        }

        .vc_custom_1570785702603 {
            margin-bottom: 0px !important;
        }

        .vc_custom_1570785714033 {
            margin-top: 0px !important;
        }

        .vc_custom_1571036241875 {
            margin-bottom: 0px !important;
        }

        .vc_custom_1571036202902 {
            margin-bottom: 0px !important;
        }

        .vc_custom_1571036184421 {
            margin-top: 0px !important;
        }

        .vc_custom_1570848931278 {
            margin-bottom: 0px !important;
        }

        .vc_custom_1571039860669 {
            padding-right: 50px !important;
        }

        .vc_custom_1571039876369 {
            padding-left: 50px !important;
        }

        .vc_custom_1571036119126 {
            margin-top: 0px !important;
        }

        .vc_custom_1570852780910 {
            margin-right: 0px !important;
            margin-left: 0px !important;
        }

        .vc_custom_1570853767504 {
            padding-top: 50px !important;
            padding-bottom: 50px !important;
            background-image: url(https://educlever.beplusthemes.com/elementary/wp-content/uploads/2017/06/edu_event4.jpg?id=239) !important;
            border-radius: 5px !important;
        }

        .vc_custom_1570850984616 {
            margin-bottom: 0px !important;
        }

        .vc_custom_1571036070400 {
            padding-top: 0px !important;
        }

        .vc_custom_1570854746021 {
            margin-bottom: 20px !important;
        }

        .vc_custom_1571027281014 {
            padding-right: 0px !important;
            padding-left: 0px !important;
        }

        .vc_custom_1571033769049 {
            margin-bottom: 0px !important;
        }

        .vc_custom_1571033796248 {
            margin-top: 0px !important;
        }

        .vc_custom_1551495466118 {
            margin-bottom: 20px !important;
        }

        .vc_custom_1571018890427 {
            margin-bottom: 0px !important;
        }
    </style>
    <meta name='robots' content='max-image-preview:large' />
    <link rel='dns-prefetch' href='//fonts.googleapis.com' />
    <link rel="alternate" type="application/rss+xml" title="Educlever &raquo; Feed" href="https://educlever.beplusthemes.com/elementary/feed/" />
    <link rel="alternate" type="application/rss+xml" title="Educlever &raquo; Comments Feed" href="https://educlever.beplusthemes.com/elementary/comments/feed/" />
    <link rel="alternate" type="text/calendar" title="Educlever &raquo; iCal Feed" href="https://educlever.beplusthemes.com/elementary/events/?ical=1" />
    <style id='global-styles-inline-css' type='text/css'>
        body {
            --wp--preset--color--black: #000000;
            --wp--preset--color--cyan-bluish-gray: #abb8c3;
            --wp--preset--color--white: #ffffff;
            --wp--preset--color--pale-pink: #f78da7;
            --wp--preset--color--vivid-red: #cf2e2e;
            --wp--preset--color--luminous-vivid-orange: #ff6900;
            --wp--preset--color--luminous-vivid-amber: #fcb900;
            --wp--preset--color--light-green-cyan: #7bdcb5;
            --wp--preset--color--vivid-green-cyan: #00d084;
            --wp--preset--color--pale-cyan-blue: #8ed1fc;
            --wp--preset--color--vivid-cyan-blue: #0693e3;
            --wp--preset--color--vivid-purple: #9b51e0;
            --wp--preset--gradient--vivid-cyan-blue-to-vivid-purple: linear-gradient(135deg, rgba(6, 147, 227, 1) 0%, rgb(155, 81, 224) 100%);
            --wp--preset--gradient--light-green-cyan-to-vivid-green-cyan: linear-gradient(135deg, rgb(122, 220, 180) 0%, rgb(0, 208, 130) 100%);
            --wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange: linear-gradient(135deg, rgba(252, 185, 0, 1) 0%, rgba(255, 105, 0, 1) 100%);
            --wp--preset--gradient--luminous-vivid-orange-to-vivid-red: linear-gradient(135deg, rgba(255, 105, 0, 1) 0%, rgb(207, 46, 46) 100%);
            --wp--preset--gradient--very-light-gray-to-cyan-bluish-gray: linear-gradient(135deg, rgb(238, 238, 238) 0%, rgb(169, 184, 195) 100%);
            --wp--preset--gradient--cool-to-warm-spectrum: linear-gradient(135deg, rgb(74, 234, 220) 0%, rgb(151, 120, 209) 20%, rgb(207, 42, 186) 40%, rgb(238, 44, 130) 60%, rgb(251, 105, 98) 80%, rgb(254, 248, 76) 100%);
            --wp--preset--gradient--blush-light-purple: linear-gradient(135deg, rgb(255, 206, 236) 0%, rgb(152, 150, 240) 100%);
            --wp--preset--gradient--blush-bordeaux: linear-gradient(135deg, rgb(254, 205, 165) 0%, rgb(254, 45, 45) 50%, rgb(107, 0, 62) 100%);
            --wp--preset--gradient--luminous-dusk: linear-gradient(135deg, rgb(255, 203, 112) 0%, rgb(199, 81, 192) 50%, rgb(65, 88, 208) 100%);
            --wp--preset--gradient--pale-ocean: linear-gradient(135deg, rgb(255, 245, 203) 0%, rgb(182, 227, 212) 50%, rgb(51, 167, 181) 100%);
            --wp--preset--gradient--electric-grass: linear-gradient(135deg, rgb(202, 248, 128) 0%, rgb(113, 206, 126) 100%);
            --wp--preset--gradient--midnight: linear-gradient(135deg, rgb(2, 3, 129) 0%, rgb(40, 116, 252) 100%);
            --wp--preset--duotone--dark-grayscale: url('#wp-duotone-dark-grayscale');
            --wp--preset--duotone--grayscale: url('#wp-duotone-grayscale');
            --wp--preset--duotone--purple-yellow: url('#wp-duotone-purple-yellow');
            --wp--preset--duotone--blue-red: url('#wp-duotone-blue-red');
            --wp--preset--duotone--midnight: url('#wp-duotone-midnight');
            --wp--preset--duotone--magenta-yellow: url('#wp-duotone-magenta-yellow');
            --wp--preset--duotone--purple-green: url('#wp-duotone-purple-green');
            --wp--preset--duotone--blue-orange: url('#wp-duotone-blue-orange');
            --wp--preset--font-size--small: 13px;
            --wp--preset--font-size--medium: 20px;
            --wp--preset--font-size--large: 36px;
            --wp--preset--font-size--x-large: 42px;
        }

        .has-black-color {
            color: var(--wp--preset--color--black) !important;
        }

        .has-cyan-bluish-gray-color {
            color: var(--wp--preset--color--cyan-bluish-gray) !important;
        }

        .has-white-color {
            color: var(--wp--preset--color--white) !important;
        }

        .has-pale-pink-color {
            color: var(--wp--preset--color--pale-pink) !important;
        }

        .has-vivid-red-color {
            color: var(--wp--preset--color--vivid-red) !important;
        }

        .has-luminous-vivid-orange-color {
            color: var(--wp--preset--color--luminous-vivid-orange) !important;
        }

        .has-luminous-vivid-amber-color {
            color: var(--wp--preset--color--luminous-vivid-amber) !important;
        }

        .has-light-green-cyan-color {
            color: var(--wp--preset--color--light-green-cyan) !important;
        }

        .has-vivid-green-cyan-color {
            color: var(--wp--preset--color--vivid-green-cyan) !important;
        }

        .has-pale-cyan-blue-color {
            color: var(--wp--preset--color--pale-cyan-blue) !important;
        }

        .has-vivid-cyan-blue-color {
            color: var(--wp--preset--color--vivid-cyan-blue) !important;
        }

        .has-vivid-purple-color {
            color: var(--wp--preset--color--vivid-purple) !important;
        }

        .has-black-background-color {
            background-color: var(--wp--preset--color--black) !important;
        }

        .has-cyan-bluish-gray-background-color {
            background-color: var(--wp--preset--color--cyan-bluish-gray) !important;
        }

        .has-white-background-color {
            background-color: var(--wp--preset--color--white) !important;
        }

        .has-pale-pink-background-color {
            background-color: var(--wp--preset--color--pale-pink) !important;
        }

        .has-vivid-red-background-color {
            background-color: var(--wp--preset--color--vivid-red) !important;
        }

        .has-luminous-vivid-orange-background-color {
            background-color: var(--wp--preset--color--luminous-vivid-orange) !important;
        }

        .has-luminous-vivid-amber-background-color {
            background-color: var(--wp--preset--color--luminous-vivid-amber) !important;
        }

        .has-light-green-cyan-background-color {
            background-color: var(--wp--preset--color--light-green-cyan) !important;
        }

        .has-vivid-green-cyan-background-color {
            background-color: var(--wp--preset--color--vivid-green-cyan) !important;
        }

        .has-pale-cyan-blue-background-color {
            background-color: var(--wp--preset--color--pale-cyan-blue) !important;
        }

        .has-vivid-cyan-blue-background-color {
            background-color: var(--wp--preset--color--vivid-cyan-blue) !important;
        }

        .has-vivid-purple-background-color {
            background-color: var(--wp--preset--color--vivid-purple) !important;
        }

        .has-black-border-color {
            border-color: var(--wp--preset--color--black) !important;
        }

        .has-cyan-bluish-gray-border-color {
            border-color: var(--wp--preset--color--cyan-bluish-gray) !important;
        }

        .has-white-border-color {
            border-color: var(--wp--preset--color--white) !important;
        }

        .has-pale-pink-border-color {
            border-color: var(--wp--preset--color--pale-pink) !important;
        }

        .has-vivid-red-border-color {
            border-color: var(--wp--preset--color--vivid-red) !important;
        }

        .has-luminous-vivid-orange-border-color {
            border-color: var(--wp--preset--color--luminous-vivid-orange) !important;
        }

        .has-luminous-vivid-amber-border-color {
            border-color: var(--wp--preset--color--luminous-vivid-amber) !important;
        }

        .has-light-green-cyan-border-color {
            border-color: var(--wp--preset--color--light-green-cyan) !important;
        }

        .has-vivid-green-cyan-border-color {
            border-color: var(--wp--preset--color--vivid-green-cyan) !important;
        }

        .has-pale-cyan-blue-border-color {
            border-color: var(--wp--preset--color--pale-cyan-blue) !important;
        }

        .has-vivid-cyan-blue-border-color {
            border-color: var(--wp--preset--color--vivid-cyan-blue) !important;
        }

        .has-vivid-purple-border-color {
            border-color: var(--wp--preset--color--vivid-purple) !important;
        }

        .has-vivid-cyan-blue-to-vivid-purple-gradient-background {
            background: var(--wp--preset--gradient--vivid-cyan-blue-to-vivid-purple) !important;
        }

        .has-light-green-cyan-to-vivid-green-cyan-gradient-background {
            background: var(--wp--preset--gradient--light-green-cyan-to-vivid-green-cyan) !important;
        }

        .has-luminous-vivid-amber-to-luminous-vivid-orange-gradient-background {
            background: var(--wp--preset--gradient--luminous-vivid-amber-to-luminous-vivid-orange) !important;
        }

        .has-luminous-vivid-orange-to-vivid-red-gradient-background {
            background: var(--wp--preset--gradient--luminous-vivid-orange-to-vivid-red) !important;
        }

        .has-very-light-gray-to-cyan-bluish-gray-gradient-background {
            background: var(--wp--preset--gradient--very-light-gray-to-cyan-bluish-gray) !important;
        }

        .has-cool-to-warm-spectrum-gradient-background {
            background: var(--wp--preset--gradient--cool-to-warm-spectrum) !important;
        }

        .has-blush-light-purple-gradient-background {
            background: var(--wp--preset--gradient--blush-light-purple) !important;
        }

        .has-blush-bordeaux-gradient-background {
            background: var(--wp--preset--gradient--blush-bordeaux) !important;
        }

        .has-luminous-dusk-gradient-background {
            background: var(--wp--preset--gradient--luminous-dusk) !important;
        }

        .has-pale-ocean-gradient-background {
            background: var(--wp--preset--gradient--pale-ocean) !important;
        }

        .has-electric-grass-gradient-background {
            background: var(--wp--preset--gradient--electric-grass) !important;
        }

        .has-midnight-gradient-background {
            background: var(--wp--preset--gradient--midnight) !important;
        }

        .has-small-font-size {
            font-size: var(--wp--preset--font-size--small) !important;
        }

        .has-medium-font-size {
            font-size: var(--wp--preset--font-size--medium) !important;
        }

        .has-large-font-size {
            font-size: var(--wp--preset--font-size--large) !important;
        }

        .has-x-large-font-size {
            font-size: var(--wp--preset--font-size--x-large) !important;
        }
    </style>
    <link rel='stylesheet' id='tp-open-sans-css' href='https://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700%2C800&#038;ver=6.0.2' type='text/css' media='all' />
    <link rel='stylesheet' id='tp-raleway-css' href='https://fonts.googleapis.com/css?family=Raleway%3A100%2C200%2C300%2C400%2C500%2C600%2C700%2C800%2C900&#038;ver=6.0.2' type='text/css' media='all' />
    <link rel='stylesheet' id='tp-droid-serif-css' href='https://fonts.googleapis.com/css?family=Droid+Serif%3A400%2C700&#038;ver=6.0.2' type='text/css' media='all' />
    <style id='rs-plugin-settings-inline-css' type='text/css'>
        #rs-demo-id {}
    </style>
    <link rel='stylesheet' id='woocommerce-smallscreen-css' href='https://educlever.beplusthemes.com/elementary/wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen.min.css' type='text/css' media='only screen and (max-width: 768px)' />
    <style id='woocommerce-inline-inline-css' type='text/css'>
        .woocommerce form .form-row .required {
            visibility: visible;
        }
    </style>
    <!--[if lt IE 9]><link rel='stylesheet' id='vc_lte_ie9-css'  href='https://educlever.beplusthemes.com/elementary/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css' type='text/css' media='screen' /> <![endif]-->
    <link rel='stylesheet' id='fw-googleFonts-css' href='//fonts.googleapis.com/css?family=Lato%3A900%2C700%2Cregular%2C300%2C100%7CPoppins%3A700%2C800&#038;subset=latin%2Clatin-ext&#038;ver=6.0.2' type='text/css' media='all' />
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-includes/js/jquery/jquery.min.js' id='jquery-core-js'></script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-includes/js/jquery/jquery-migrate.min.js' id='jquery-migrate-js'></script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/plugins/bears-loading/assets/fakeLoader.min.js' id='bears-fakeLoader-js'></script>
    <script type='text/javascript' id='bears-loading-js-extra'>
        /* <![CDATA[ */
        var option_loading = {
            "skin": "spinner5",
            "color": "#559d2d",
            "image": ""
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/uploads/siteground-optimizer-assets/bears-loading.min.js' id='bears-loading-js'></script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/plugins/revslider/public/assets/js/revolution.tools.min.js' id='tp-tools-js'></script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/plugins/revslider/public/assets/js/rs6.min.js' id='revmin-js'></script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min.js' id='jquery-blockui-js'></script>
    <script type='text/javascript' id='wc-add-to-cart-js-extra'>
        /* <![CDATA[ */
        var wc_add_to_cart_params = {
            "ajax_url": "\/elementary\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/elementary\/?wc-ajax=%%endpoint%%",
            "i18n_view_cart": "View cart",
            "cart_url": "https:\/\/educlever.beplusthemes.com\/elementary\/cart\/",
            "is_cart": "",
            "cart_redirect_after_add": "no"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min.js' id='wc-add-to-cart-js'></script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/uploads/siteground-optimizer-assets/vc_woocommerce-add-to-cart-js.min.js' id='vc_woocommerce-add-to-cart-js-js'></script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/themes/educlever/assets/jquery-countdown/jquery.plugin.min.js' id='jquery-plugin-js'></script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/themes/educlever/assets/jquery-countdown/jquery.countdown.min.js' id='jquery-countdown-js'></script>
    <link rel="https://api.w.org/" href="https://educlever.beplusthemes.com/elementary/wp-json/" />
    <link rel="alternate" type="application/json" href="https://educlever.beplusthemes.com/elementary/wp-json/wp/v2/pages/8" />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://educlever.beplusthemes.com/elementary/xmlrpc.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://educlever.beplusthemes.com/elementary/wp-includes/wlwmanifest.xml" />
    <meta name="generator" content="WordPress 6.0.2" />
    <meta name="generator" content="WooCommerce 6.8.2" />
    <link rel="canonical" href="https://educlever.beplusthemes.com/elementary/" />
    <link rel='shortlink' href='https://educlever.beplusthemes.com/elementary/' />
    <link rel="alternate" type="application/json+oembed" href="https://educlever.beplusthemes.com/elementary/wp-json/oembed/1.0/embed?url=https%3A%2F%2Feduclever.beplusthemes.com%2Felementary%2F" />
    <link rel="alternate" type="text/xml+oembed" href="https://educlever.beplusthemes.com/elementary/wp-json/oembed/1.0/embed?url=https%3A%2F%2Feduclever.beplusthemes.com%2Felementary%2F&#038;format=xml" />
    <style id="learn-press-custom-css">
        :root {
            --lp-primary-color: #ffb606;
            --lp-secondary-color: #442e66;
        }
    </style>
    <meta name="tec-api-version" content="v1">
    <meta name="tec-api-origin" content="https://educlever.beplusthemes.com/elementary">
    <link rel="alternate" href="https://educlever.beplusthemes.com/elementary/wp-json/tribe/events/v1/" /> <!-- Chrome, Firefox OS and Opera -->
    <meta name="theme-color" content="#559d2d"> <!-- Windows Phone -->
    <meta name="msapplication-navbutton-color" content="#559d2d"> <!-- iOS Safari -->
    <meta name="apple-mobile-web-app-status-bar-style" content="#559d2d"> <noscript>
        <style>
            .woocommerce-product-gallery {
                opacity: 1 !important;
            }
        </style>
    </noscript>
    <meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress." />
    <meta name="generator" content="Powered by Slider Revolution 6.1.3 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
    <link rel="icon" href="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2018/11/cropped-favicon-32x32.png" sizes="32x32" />
    <link rel="icon" href="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2018/11/cropped-favicon-192x192.png" sizes="192x192" />
    <link rel="apple-touch-icon" href="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2018/11/cropped-favicon-180x180.png" />
    <meta name="msapplication-TileImage" content="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2018/11/cropped-favicon-270x270.png" />
    <script type="text/javascript">
        function setREVStartSize(t) {
            try {
                var h, e = document.getElementById(t.c).parentNode.offsetWidth;
                if (e = 0 === e || isNaN(e) ? window.innerWidth : e, t.tabw = void 0 === t.tabw ? 0 : parseInt(t.tabw), t.thumbw = void 0 === t.thumbw ? 0 : parseInt(t.thumbw), t.tabh = void 0 === t.tabh ? 0 : parseInt(t.tabh), t.thumbh = void 0 === t.thumbh ? 0 : parseInt(t.thumbh), t.tabhide = void 0 === t.tabhide ? 0 : parseInt(t.tabhide), t.thumbhide = void 0 === t.thumbhide ? 0 : parseInt(t.thumbhide), t.mh = void 0 === t.mh || "" == t.mh || "auto" === t.mh ? 0 : parseInt(t.mh, 0), "fullscreen" === t.layout || "fullscreen" === t.l) h = Math.max(t.mh, window.innerHeight);
                else {
                    for (var i in t.gw = Array.isArray(t.gw) ? t.gw : [t.gw], t.rl) void 0 !== t.gw[i] && 0 !== t.gw[i] || (t.gw[i] = t.gw[i - 1]);
                    for (var i in t.gh = void 0 === t.el || "" === t.el || Array.isArray(t.el) && 0 == t.el.length ? t.gh : t.el, t.gh = Array.isArray(t.gh) ? t.gh : [t.gh], t.rl) void 0 !== t.gh[i] && 0 !== t.gh[i] || (t.gh[i] = t.gh[i - 1]);
                    var r, a = new Array(t.rl.length),
                        n = 0;
                    for (var i in t.tabw = t.tabhide >= e ? 0 : t.tabw, t.thumbw = t.thumbhide >= e ? 0 : t.thumbw, t.tabh = t.tabhide >= e ? 0 : t.tabh, t.thumbh = t.thumbhide >= e ? 0 : t.thumbh, t.rl) a[i] = t.rl[i] < window.innerWidth ? 0 : t.rl[i];
                    for (var i in r = a[0], a) r > a[i] && 0 < a[i] && (r = a[i], n = i);
                    var d = e > t.gw[n] + t.tabw + t.thumbw ? 1 : (e - (t.tabw + t.thumbw)) / t.gw[n];
                    h = t.gh[n] * d + (t.tabh + t.thumbh)
                }
                void 0 === window.rs_init_css && (window.rs_init_css = document.head.appendChild(document.createElement("style"))), document.getElementById(t.c).height = h, window.rs_init_css.innerHTML += "#" + t.c + "_wrapper { height: " + h + "px }"
            } catch (t) {
                console.log("Failure at Presize of Slider:" + t)
            }
        };
    </script>
    <style type="text/css" id="wp-custom-css">
        .bt-header-top-bar:before {
            content: "";
            height: 45px;
            background-color: #559d2d;
            width: calc(50% - 35px);
            position: absolute;
            left: 0;
            top: 0;
            border-radius: 0 30px 30px 0px;
        }

        .bt-header-top-bar {
            position: relative;
            padding: 0;
        }

        .welcome ul.menu {
            display: inline-block;
            background: #488c23;
            padding: 0 30px;
            border-radius: 30px;
        }

        .welcome:before {
            content: "Welcome to our Elementary School!";
            display: inline-block;
            float: left;
        }

        .pi-info a,
        .pi-info span {
            color: #444 !important;
        }

        .pi-info i.fa {
            color: #559d2d;
        }

        #page .wpb_theme_custom_element.wpb_featured_box.box-footer-elementary .vc-custom-inner-wrap .featured-box-alignment .entry-box-wrap .featured-box-title {
            font-size: 18px;
            line-height: 24px;
            font-weight: 700;
            margin-bottom: 5px;
        }

        #page .wpb_theme_custom_element.wpb_featured_box.box-footer-elementary .vc-custom-inner-wrap .featured-box-alignment .entry-box-wrap .featured-box-text {
            font-size: 15px;
        }

        #page .wpb_theme_custom_element.wpb_featured_box.box-footer-elementary .vc-custom-inner-wrap .featured-box-alignment .icon-wrap .type-icon ._icon {
            font-size: 24px
        }

        ul.bt-link li {
            margin-bottom: 10px;
        }

        .search-bt .notification-center-item a {
            display: inline-block !important;
            width: 50px;
            height: 50px;
            border-radius: 50%;
            background: #559d2d;
            color: #fff !important;
            text-align: center;
            line-height: 47px !important;
            padding-left: 2px;
        }

        body.is-header-sticky .bt-header-container.fw-sticky-header .bt-site-navigation>ul.bt-nav-menu>li.menu-item.menu-item-custom-type-notification_center .notification-center-icon .notification-center-item a {
            color: #fff !important;
        }

        .img-svg {
            width: 130px;
            margin: 0 auto;
        }

        #page .wpb_theme_custom_element.wpb_featured_box.box-provide.c1 .vc-custom-inner-wrap .featured-box-alignment .icon-wrap {
            background-image: url("https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/bg-box-provide1.png");
        }

        #page .wpb_theme_custom_element.wpb_featured_box.box-provide.c2 .vc-custom-inner-wrap .featured-box-alignment .icon-wrap {
            background-image: url("https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/bg-box-provide2.png");
        }

        #page .wpb_theme_custom_element.wpb_featured_box.box-provide.c3 .vc-custom-inner-wrap .featured-box-alignment .icon-wrap {
            background-image: url("https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/bg-box-provide3.png");
        }

        #page .wpb_theme_custom_element.wpb_featured_box.box-provide.c4 .vc-custom-inner-wrap .featured-box-alignment .icon-wrap {
            background-image: url("https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/bg-box-provide4.png");
        }

        #page .wpb_theme_custom_element.wpb_featured_box.box-provide.c5 .vc-custom-inner-wrap .featured-box-alignment .icon-wrap {
            background-image: url("https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/bg-box-provide5.png");
        }

        #page .wpb_theme_custom_element.wpb_featured_box.box-provide.c6 .vc-custom-inner-wrap .featured-box-alignment .icon-wrap {
            background-image: url("https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/bg-box-provide6.png");
        }

        #page .wpb_theme_custom_element.wpb_featured_box.box-provide .vc-custom-inner-wrap .featured-box-alignment .icon-wrap {
            display: inline-block;
            margin-bottom: 20px;
            width: 178px;
            height: 150px;
            line-height: 160px;
            text-align: center;
            background-size: cover;
        }

        #page .wpb_theme_custom_element.wpb_featured_box.box-provide .vc-custom-inner-wrap .featured-box-alignment .icon-wrap .type-image {
            float: right;
            margin-right: 37px;
        }

        #page .wpb_theme_custom_element.wpb_featured_box.box-provide .vc-custom-inner-wrap .featured-box-alignment .entry-box-wrap .featured-box-title {
            font-weight: bold;
        }

        #page .wpb_theme_custom_element.wpb_base_review .owl-carousel.review .owl-item .item.layout-default article .bt-content .bt-excerpt {
            font-size: 20px;
            line-height: 30px;
        }

        #page .wpb_theme_custom_element.wpb_base_review .owl-carousel.review .owl-item .item.layout-default article .bt-info-review .bt-name-position .bt-title {
            font-size: 25px;
            line-height: 30px;
        }

        #page .wpb_theme_custom_element.wpb_base_review .owl-carousel.review .owl-item .item.layout-default article .bt-info-review .bt-name-position .bt-position {
            color: #e0e0e0;
            font-size: 16px;
        }

        #page .wpb_theme_custom_element.wpb_base_review .owl-carousel.review .owl-dots .owl-dot.active {
            background: #559d2d;
        }

        #page .wpb_theme_custom_element.wpb_posts_slider_2 .vc-custom-inner-wrap .item .item-inner.posts_slider_2_template_default .post-caption .post-date {
            border-radius: 25px;
            border: 3px solid #fff;
            line-height: 39px;
        }

        .bt-copyright a {
            font-weight: 700;
        }

        .inline-c {
            text-align: right;
        }

        @media only screen and (max-width: 400px) {
            .vc_custom_1571038736641 {
                margin-right: 0px !important;
            }
        }

        @media only screen and (max-width: 991px) {
            .vc_custom_1571040125175.vc_column-gap-35 {
                margin-left: 0 !important;
                margin-right: 0 !important;
                padding: 0 !important;
            }

            .vc_custom_1571037604522,
            .vc_custom_1571037623409 {
                text-align: center !important;
            }

            .inline-c {
                text-align: center;
            }

            .box1,
            .box2,
            .box3 {
                margin-bottom: 130px;
            }

            .vc_custom_1571039860669 {
                padding-right: 15px !important;
            }

            .vc_custom_1571039876369 {
                padding-left: 15px !important;
            }

            .vc_custom_1571040098524 {
                padding-right: 0px !important;
                padding-left: 0px !important;
            }

            #page .vc_row.vc_column-gap-30 {
                margin-left: 0px;
                margin-right: 0px;
                padding: 0 !important;
            }

            footer#colophon {
                margin-top: 0px;
            }
        }

        .bt-header-top-bar-mobi *:not(.fa) {
            color: #333 !important;
        }

        footer#colophon {
            margin-top: 120px;
        }

        .bt-faq .vc_toggle_title {
            padding: 7px 10px;
            margin-bottom: 10px;
            border-radius: 7px;
            background: #559d2d;
            padding-left: 36px !important;
        }

        .bt-faq h4 {
            font-size: 18px;
            font-weight: 500;
            color: #fff;
        }

        .bt-faq i.vc_toggle_icon {
            font-style: normal;
            background: transparent;
            color: #fff;
        }

        .bt-faq .vc_toggle_title:hover .vc_toggle_icon {
            background: transparent;
        }

        .bt-faq.c1 i.vc_toggle_icon:before {
            font-family: FontAwesome;
            content: "\f0a4";
        }

        .bt-faq.c2 i.vc_toggle_icon:before {
            font-family: FontAwesome;
            content: "\f209";
        }

        .bt-faq.c3 i.vc_toggle_icon:before {
            font-family: FontAwesome;
            content: "\f2a9";
        }

        .bt-faq.c4 i.vc_toggle_icon:before {
            font-family: FontAwesome;
            content: "\f17d";
        }

        .bt-faq i.vc_toggle_icon:before {
            background: transparent;
            width: 0;
            height: 0;
            position: absolute;
            top: -3px;
            font-size: 20px;
        }

        .bt-faq i.vc_toggle_icon:after {
            background: transparent;
            width: 0;
            height: 0;
        }

        .bears-course-content .course-title {
            font-size: 25px;
            line-height: 28px;
        }

        .course-meta {
            display: none;
        }

        .bears-course-content {
            clear: both;
        }

        .educlever-ralated-course {
            padding: 0 50px;
        }

        .educlever-ralated-course .course-content {
            padding: 20px 35px;
        }

        .educlever-ralated-course .educlever-course-grid ul.educlever-carousel-wrapper li.lp_course .course-item .bears-course-content {
            padding: 0px;
            width: 100%;
        }
    </style> <noscript>
        <style>
            .wpb_animate_when_almost_visible {
                opacity: 1;
            }
        </style>
    </noscript>
    <style id="btfb-footer-builder-css">
        .vc_custom_1570504247525 {
            padding-top: 120px !important;
            padding-bottom: 90px !important;
            background-color: #f6f6f6 !important;
        }

        .vc_custom_1570434838537 {
            margin-top: 0px !important;
        }

        .vc_custom_1570434874623 {
            margin-top: 0px !important;
        }

        .vc_custom_1571043193316 {
            background-image: url(https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/bg-call.png?id=2086) !important;
            background-position: center !important;
            background-repeat: no-repeat !important;
            background-size: cover !important;
        }

        .vc_custom_1570503794602 {
            margin-bottom: 0px !important;
        }

        .vc_custom_1570504234953 {
            margin-top: 5px !important;
            margin-bottom: 10px !important;
        }

        .vc_custom_1570503802993 {
            margin-top: 0px !important;
        }

        .call-footer .vc_column-inner {
            position: absolute;
            width: 410px;
            height: 235px;
            right: 0;
            z-index: 9;
        }

        .call-footer {
            position: absolute;
            top: -120px;
            right: 0;
        }
    </style>
</head>

<body data-rsssl=1 class="home page-template page-template-visual-builder-template page-template-visual-builder-template-php page page-id-8 theme-educlever loading woocommerce-no-js tribe-no-js bt-full wpb-js-composer js-comp-ver-6.0.5 vc_responsive" itemscope="itemscope" itemtype="http://schema.org/WebPage">
    <div id="page" class="site">
        <header class="bt-header-mobi header-mobi bt-logo-no-retina fw-menu-position-right fw-no-absolute-header" itemscope="itemscope" itemtype="http://schema.org/WPHeader">
            <!-- Header top bar -->
            <div class="bt-header-top-bar-mobi">
                <div class="container">
                    <div class="row">
                        <div class="header-top-sidebar-item col-md-12 col-sm-12 col-sx-12 fw-sidebar-content-align-center">
                            <aside id="nav_menu-7" class="widget widget_nav_menu">
                                <div class="menu-top-info-container">
                                    <ul id="menu-top-info" class="menu">
                                        <li id="menu-item-1942" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-icon menu-item-1942" style=""><a href="tel:123456789"><i class="fa fa-phone"></i><span>123-4567-890</span></a></li>
                                        <li id="menu-item-1943" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-icon menu-item-1943" style=""><a href="/cdn-cgi/l/email-protection#066463677475726e636b6346616b676f6a2865696b"><i class="fa fa-envelope-o"></i><span><span class="__cf_email__" data-cfemail="98fafdf9eaebecf0fdf5fdd8fff5f9f1f4b6fbf7f5">[email&#160;protected]</span></span></a></li>
                                    </ul>
                                </div>
                            </aside>
                        </div>
                    </div>
                </div>
            </div> <!-- Header main menu -->
            <div class="bt-header-mobi-main">
                <div class="container">
                    <div class="bt-container-logo bt-vertical-align-middle">
                        <div class="fw-wrap-logo"> <a href="https://educlever.beplusthemes.com/elementary/" class="fw-site-logo"> <img src="//educlever.beplusthemes.com/elementary/wp-content/uploads/2018/09/logo-1.png" alt="Educlever" class="main-logo" /> </a></div>
                    </div>
                    <!--
 -->
                    <div class="bt-container-menu bt-vertical-align-middle">
                        <div class="bt-nav-wrap" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement" role="navigation">
                            <nav id="bt-menu-mobi-menu" class="bt-site-navigation mobi-menu-navigation">
                                <ul id="menu-menu-mobile" class="bt-nav-menu">
                                    <li id="menu-item-172" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-172 menu-item-custom-type-notification_center menu-item-hidden-title-yes" style=""><a href="#"><span>Notifi</span></a>
                                        <div class="notification-center-icon">
                                            <div class="notification-center-item"><a href="#notification-search" class="" data-notification="notification-search"><span class='icon-type-v2 '><i class='fa fa-search'></i></span></a></div>
                                        </div>
                                    </li>
                                    <li id="menu-item-173" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-icon menu-item-173 menu-item-custom-type-off-cavans-menu menu-item-hidden-title-yes edu-menu-mobile" style=""><a href="#"><i class="fa fa-reorder"></i><span>Canvas</span></a>
                                        <div class="menu-item-custom-wrap off-canvas-menu-wrap"> <span class="off-canvas-menu-closed"><i class="ion-ios-close-empty"></i></span>
                                            <div class="off-canvas-menu-container">
                                                <div class="menu-canvas-container">
                                                    <ul id="menu-canvas" class="menu">
                                                        <li id="menu-item-789" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-8 current_page_item menu-item-789" style=""><a href="https://educlever.beplusthemes.com/elementary/"><span>HOME</span></a></li>
                                                        <li id="menu-item-793" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-793" style=""><a href="https://educlever.beplusthemes.com/elementary/our-courses/"><span>COURSES</span></a>
                                                            <ul class="sub-menu" style="">
                                                                <li id="menu-item-801" class="menu-item menu-item-type-post_type menu-item-object-lp_course menu-item-801" style=""><a href="https://educlever.beplusthemes.com/elementary/course/introduction-learn-lms-plugin/"><span>COURSES SINGLE</span></a></li>
                                                                <li id="menu-item-802" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-802" style=""><a href="https://educlever.beplusthemes.com/elementary/our-courses/"><span>COURSES ARCHIVE</span></a></li>
                                                                <li id="menu-item-791" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-791" style=""><a href="https://educlever.beplusthemes.com/elementary/become-a-teacher/"><span>BECOME A TEACHER</span></a></li>
                                                            </ul>
                                                        </li>
                                                        <li id="menu-item-2010" class="menu-item menu-item-type-post_type_archive menu-item-object-tribe_events menu-item-has-children menu-item-2010" style=""><a href="https://educlever.beplusthemes.com/elementary/events/"><span>EVENTS</span></a>
                                                            <ul class="sub-menu" style="">
                                                                <li id="menu-item-2008" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2008" style=""><a href="https://educlever.beplusthemes.com/elementary/events-list/"><span>EVENTS LIST</span></a></li>
                                                                <li id="menu-item-2009" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2009" style=""><a href="https://educlever.beplusthemes.com/elementary/events-grid/"><span>EVENTS GRID</span></a></li>
                                                            </ul>
                                                        </li>
                                                        <li id="menu-item-806" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-806" style=""><a href="#!"><span>PAGES</span></a>
                                                            <ul class="sub-menu" style="">
                                                                <li id="menu-item-790" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-790" style=""><a href="https://educlever.beplusthemes.com/elementary/about/"><span>ABOUT</span></a></li>
                                                                <li id="menu-item-795" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-795" style=""><a href="https://educlever.beplusthemes.com/elementary/forums/"><span>FORUMS</span></a></li>
                                                                <li id="menu-item-807" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-807" style=""><a href="https://educlever.beplusthemes.com/elementary/topics/"><span>TOPICS</span></a></li>
                                                                <li id="menu-item-799" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-799" style=""><a href="https://educlever.beplusthemes.com/elementary/our-teacher/"><span>OUR TEACHER</span></a></li>
                                                            </ul>
                                                        </li>
                                                        <li id="menu-item-797" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-797" style=""><a href="https://educlever.beplusthemes.com/elementary/our-blog/"><span>BLOG</span></a>
                                                            <ul class="sub-menu" style="">
                                                                <li id="menu-item-798" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-798" style=""><a href="https://educlever.beplusthemes.com/elementary/our-blog-2/"><span>OUR BLOG 2</span></a></li>
                                                            </ul>
                                                        </li>
                                                        <li id="menu-item-792" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-792" style=""><a href="https://educlever.beplusthemes.com/elementary/contact/"><span>CONTACT</span></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <header class="bt-header header-1   fw-menu-position-right bt-logo-no-retina" itemscope="itemscope" itemtype="http://schema.org/WPHeader">
            <!-- Header top bar -->
            <div class="bt-header-top-bar">
                <div class="container">
                    <div class="row">
                        <div class="header-top-sidebar-item col-md-6 col-sm-12 col-sx-12 fw-sidebar-content-align-right welcome">
                            <aside id="nav_menu-4" class="widget widget_nav_menu">
                                <div class="menu-register-menu-container">
                                    <ul id="menu-register-menu" class="menu">
                                        <li id="menu-item-823" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-icon menu-item-823 menu-item-custom-type- bt-profile display-login" style=""><a href="https://educlever.beplusthemes.com/elementary/profile/"><i class="fa fa-user"></i><span>Profile</span></a></li>
                                        <li id="menu-item-2015" class="lrh-login-registration-popup menu-item menu-item-type-lrh-custom menu-item-object- menu-item-2015" style=""><a><span>Login | Registration Popup Link</span></a></li>
                                    </ul>
                                </div>
                            </aside>
                        </div>
                        <div class="header-top-sidebar-item col-md-6 col-sm-12 col-sx-12 fw-sidebar-content-align-right pi-info">
                            <aside id="nav_menu-7" class="widget widget_nav_menu">
                                <div class="menu-top-info-container">
                                    <ul id="menu-top-info-1" class="menu">
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-icon menu-item-1942" style=""><a href="tel:123456789"><i class="fa fa-phone"></i><span>123-4567-890</span></a></li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-icon menu-item-1943" style=""><a href="/cdn-cgi/l/email-protection#670502061514130f020a0227000a060e0b4904080a"><i class="fa fa-envelope-o"></i><span><span class="__cf_email__" data-cfemail="bddfd8dccfcec9d5d8d0d8fddad0dcd4d193ded2d0">[email&#160;protected]</span></span></a></li>
                                    </ul>
                                </div>
                            </aside>
                        </div>
                    </div>
                </div>
            </div> <!-- Header main menu -->
            <div class="bt-header-main">
                <div class="bt-header-container fw-no-absolute-header fw-sticky-header">
                    <div class="container">
                        <div class="bt-container-logo bt-vertical-align-middle">
                            <div class="fw-wrap-logo"> <a href="https://educlever.beplusthemes.com/elementary/" class="fw-site-logo"> <img src="//educlever.beplusthemes.com/elementary/wp-content/uploads/2018/09/logo-1.png" alt="Educlever" class="main-logo" /> </a></div>
                        </div>
                        <!--
 -->
                        <div class="bt-container-menu bt-vertical-align-middle">
                            <div class="bt-nav-wrap" itemscope="itemscope" itemtype="http://schema.org/SiteNavigationElement" role="navigation">
                                <nav id="bt-menu-primary" class="bt-site-navigation primary-navigation">
                                    <ul id="menu-main-menu" class="bt-nav-menu">
                                        <li id="menu-item-651" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-8 current_page_item menu-item-651" style=""><a href="https://educlever.beplusthemes.com/elementary/"><span>Home</span></a></li>
                                        <li id="menu-item-555" class="mega-bt menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-has-mega-menu menu-item-555" style=""><a href="https://educlever.beplusthemes.com/elementary/our-courses/"><span>Courses</span></a>
                                            <div class="mega-menu">
                                                <ul class="sub-menu mega-menu-row fw-sub-menu-position-center" style="background: url(//educlever.beplusthemes.com/elementary/wp-content/uploads/2019/06/bg-topic.jpg) no-repeat center center / cover">
                                                    <li id="menu-item-1786" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children mega-menu-col menu-item-1786" style="width: 190px"><a href="#!"><span>ABOUT COURSES</span></a>
                                                        <ul class="sub-menu" style="">
                                                            <li id="menu-item-1802" class="menu-item menu-item-type-post_type menu-item-object-lp_course menu-item-1802" style=""><a href="https://educlever.beplusthemes.com/elementary/course/javascript-online-course/"><span>Free Access Type</span></a></li>
                                                            <li id="menu-item-1803" class="menu-item menu-item-type-post_type menu-item-object-lp_course menu-item-1803" style=""><a href="https://educlever.beplusthemes.com/elementary/course/visual-studio-online/"><span>Other Free Type</span></a></li>
                                                            <li id="menu-item-1804" class="menu-item menu-item-type-post_type menu-item-object-lp_course menu-item-1804" style=""><a href="https://educlever.beplusthemes.com/elementary/course/chemestry-online-course/"><span>Paid Type</span></a></li>
                                                            <li id="menu-item-1805" class="menu-item menu-item-type-post_type menu-item-object-lp_course menu-item-1805" style=""><a href="https://educlever.beplusthemes.com/elementary/course/html5-css3-essentials/"><span>Other Paid Type</span></a></li>
                                                            <li id="menu-item-746" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-746" style=""><a href="https://educlever.beplusthemes.com/elementary/our-courses/"><span>Courses Archive</span></a></li>
                                                            <li id="menu-item-745" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-745" style=""><a href="/profile/Luis+Figo/"><span>Instructor Profile</span></a></li>
                                                        </ul>
                                                    </li>
                                                    <li id="menu-item-1791" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children mega-menu-col menu-item-1791" style="width: 190px"><a href="#!"><span>KEY FEATURED</span></a>
                                                        <ul class="sub-menu" style="">
                                                            <li id="menu-item-1814" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1814" style=""><a href="https://educlever.beplusthemes.com/elementary/our-teacher/"><span>Our Teacher</span></a></li>
                                                            <li id="menu-item-1813" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1813" style=""><a href="https://educlever.beplusthemes.com/elementary/forums/"><span>Forums</span></a></li>
                                                            <li id="menu-item-1815" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-1815" style=""><a href="https://educlever.beplusthemes.com/elementary/topics/"><span>Topics</span></a></li>
                                                            <li id="menu-item-650" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-650" style=""><a href="https://educlever.beplusthemes.com/elementary/become-a-teacher/"><span>Become A Teacher</span></a></li>
                                                        </ul>
                                                    </li>
                                                    <li id="menu-item-1787" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children mega-menu-col menu-item-1787" style="width: 360px"><a href="#!"><span>FEATURED COURSES</span></a>
                                                        <ul class="sub-menu" style="">
                                                            <li id="menu-item-1789" class="bt-ft-courses menu-item menu-item-type-custom menu-item-object-custom menu-item-1789 title-hidden mega-menu-item-custom-type-sidebar" style=""><a href="#!"><span>single courses</span></a>
                                                                <div class="menu-item-custom-wrap sidebar-container">
                                                                    <aside id="media_image-2" class="widget widget_media_image"><a href="#!"><img width="700" height="868" src="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2018/11/we.jpg" class="image wp-image-594  attachment-full size-full" alt="" loading="lazy" style="max-width: 100%; height: auto;" srcset="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2018/11/we.jpg 700w, https://educlever.beplusthemes.com/elementary/wp-content/uploads/2018/11/we-242x300.jpg 242w, https://educlever.beplusthemes.com/elementary/wp-content/uploads/2018/11/we-600x744.jpg 600w" sizes="(max-width: 700px) 100vw, 700px" /></a></aside>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li id="menu-item-1788" class="menu-item menu-item-type-custom menu-item-object-custom mega-menu-col menu-item-1788 title-hidden" style="width: 230px"><a href="#!"><span>b</span></a>
                                                        <div>
                                                            <div class="bt-edu-mega"><img src="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2018/11/we.jpg" alt=""></div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li id="menu-item-641" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-641" style=""><a href="#!"><span>Pages</span></a>
                                            <ul class="sub-menu" style="">
                                                <li id="menu-item-24" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-24" style=""><a href="https://educlever.beplusthemes.com/elementary/about/"><span>About</span></a></li>
                                                <li id="menu-item-645" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-645" style=""><a href="https://educlever.beplusthemes.com/elementary/our-teacher/"><span>Our Teacher</span></a></li>
                                                <li id="menu-item-556" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-556" style=""><a href="https://educlever.beplusthemes.com/elementary/forums/"><span>Forums</span></a></li>
                                                <li id="menu-item-649" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-649" style=""><a href="https://educlever.beplusthemes.com/elementary/topics/"><span>Topics</span></a></li>
                                            </ul>
                                        </li>
                                        <li id="menu-item-2001" class="menu-item menu-item-type-post_type_archive menu-item-object-tribe_events menu-item-has-children menu-item-2001" style=""><a href="https://educlever.beplusthemes.com/elementary/events/"><span>Events</span></a>
                                            <ul class="sub-menu" style="">
                                                <li id="menu-item-2006" class="menu-item menu-item-type-post_type_archive menu-item-object-tribe_events menu-item-2006" style=""><a href="https://educlever.beplusthemes.com/elementary/events/"><span>Events Calendar</span></a></li>
                                                <li id="menu-item-2007" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2007" style=""><a href="https://educlever.beplusthemes.com/elementary/events-list/"><span>Events List</span></a></li>
                                                <li id="menu-item-642" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-642" style=""><a href="https://educlever.beplusthemes.com/elementary/event-grid/"><span>Event Grid</span></a></li>
                                            </ul>
                                        </li>
                                        <li id="menu-item-22" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-22" style=""><a href="https://educlever.beplusthemes.com/elementary/our-blog/"><span>Blog</span></a></li>
                                        <li id="menu-item-21" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-21" style=""><a href="https://educlever.beplusthemes.com/elementary/contact/"><span>Contact</span></a></li>
                                        <li id="menu-item-60" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-60 menu-item-custom-type-notification_center menu-item-hidden-title-yes search-bt" style=""><a href="#"><span>Notifi</span></a>
                                            <div class="notification-center-icon">
                                                <div class="notification-center-item"><a href="#notification-search" class="" data-notification="notification-search"><span class='icon-type-v2 '><i class='fa fa-search'></i></span></a></div>
                                            </div>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <div id="main" class="site-main">
            <div class="container">
                <div class="container">
                    <div class="vc_row wpb_row vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
                                    <script type="text/javascript" src="//educlever.beplusthemes.com/elementary/wp-content/plugins/revslider/public/assets/js/revolution.tools.min.js?rev=6.1.3"></script>
                                    <script type="text/javascript" src="//educlever.beplusthemes.com/elementary/wp-content/plugins/revslider/public/assets/js/rs6.min.js?rev=6.1.3"></script> <!-- START Home REVOLUTION SLIDER 6.1.3 -->
                                    <p class="rs-p-wp-fix"></p>
                                    <rs-module-wrap id="rev_slider_1_1_wrapper" data-source="gallery" style="background:transparent;padding:0;margin:0px auto;margin-top:0;margin-bottom:0;">
                                        <rs-module id="rev_slider_1_1" style="display:none;" data-version="6.1.3">
                                            <rs-slides>
                                                <rs-slide data-key="rs-1" data-title="Slide" data-anim="ei:d;eo:d;s:600;r:0;t:fade;sl:d;"> <img src="//educlever.beplusthemes.com/elementary/wp-content/plugins/revslider/public/assets/assets/dummy.png" title="slider" width="1920" height="800" data-lazyload="//educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/slider.jpg" class="rev-slidebg" data-no-retina>
                                                    <!--
 -->
                                                    <rs-layer id="slider-1-slide-1-layer-1" data-type="image" data-rsp_ch="on" data-xy="x:l,l,l,c;xo:137px,137px,137px,0;yo:150px,150px,150px,134px;" data-text="l:22;a:inherit;" data-dim="w:['512px','512px','512px','512px'];h:['63px','63px','63px','63px'];" data-frame_0="o:1;" data-frame_1="st:20;sp:3000;" data-frame_999="o:0;tp:600;st:8900;sR:8680;" data-loop_0="xR:10;yR:10;oX:50;oY:50;" data-loop_999="xR:10;yR:10;crd:t;sp:7000;yys:t;yyf:t;" style="z-index:11;font-family:Roboto;"><img src="//educlever.beplusthemes.com/elementary/wp-content/plugins/revslider/public/assets/assets/dummy.png" width="512" height="63" data-lazyload="//educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/ccd.png" data-no-retina> </rs-layer>
                                                    <!--
 -->
                                                    <rs-layer id="slider-1-slide-1-layer-2" data-type="text" data-rsp_ch="on" data-xy="x:l,l,l,c;xo:216px,216px,216px,0;yo:161px,161px,161px,145px;" data-text="s:22;l:38;a:inherit;" data-frame_0="y:-100%;o:1;tp:600;" data-frame_0_mask="u:t;" data-frame_1="tp:600;sp:1500;sR:10;" data-frame_1_mask="u:t;" data-frame_999="o:0;tp:600;st:w;sR:7490;" data-loop_0="xR:10;yR:10;oX:50;oY:50;" data-loop_999="xR:10;yR:10;crd:t;sp:7000;yys:t;yyf:t;" style="z-index:12;font-family:Lato;">Hundreds of successful Student Here </rs-layer>
                                                    <!--
 -->
                                                    <rs-layer id="slider-1-slide-1-layer-3" data-type="text" data-rsp_ch="on" data-xy="x:l,l,l,c;xo:120px,120px,120px,0;yo:226px,226px,226px,228px;" data-text="s:55,55,55,45;l:65,65,65,50;ls:0px,0px,0px,0;fw:900,900,900,700;a:inherit;" data-frame_0="y:-100%;o:1;tp:600;" data-frame_0_mask="u:t;" data-frame_1="tp:600;st:610;sp:1500;sR:610;" data-frame_1_mask="u:t;" data-frame_999="o:0;tp:600;st:w;sR:6890;" style="z-index:9;font-family:Lato;">Enjoy Learning Centre </rs-layer>
                                                    <!--
 -->
                                                    <rs-layer id="slider-1-slide-1-layer-4" data-type="text" data-rsp_ch="on" data-xy="x:l,l,l,c;xo:161px,161px,161px,0;yo:298px,298px,298px,318px;" data-text="w:normal;s:18;l:28;a:center;" data-dim="w:456px;" data-frame_0="y:100%;tp:600;" data-frame_0_mask="u:t;y:100%;" data-frame_1="tp:600;e:Power2.easeInOut;st:1140;sp:2000;sR:1140;" data-frame_1_mask="u:t;" data-frame_999="o:0;tp:600;st:w;sR:5860;" style="z-index:10;font-family:Lato;">Education in its general sense is a form of learning in
                                                        the knowledge, skills, and habits </rs-layer>
                                                    <!--
 -->
                                                    <rs-layer id="slider-1-slide-1-layer-7" data-type="text" data-rsp_ch="on" data-xy="x:l,l,l,c;xo:295px,295px,295px,0;yo:402px,402px,402px,420px;" data-text="l:22;a:inherit;" data-frame_0="x:-50;" data-frame_1="sp:1000;" data-frame_999="o:0;tp:600;st:w;sR:8000;" style="z-index:8;font-family:Open Sans;">
                                                        <div class="vc_btn3-container  animated-dashes-edu edu2"> <a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-round vc_btn3-style-custom vc_btn3-color-grey" href="#!" title="">GET INVOLVED </a></div>
                                                    </rs-layer>
                                                    <!--
-->
                                                </rs-slide>
                                            </rs-slides>
                                            <rs-progress class="rs-bottom" style="visibility: hidden !important;"></rs-progress>
                                        </rs-module>
                                        <script type="text/javascript">
                                            setREVStartSize({
                                                c: 'rev_slider_1_1',
                                                rl: [1240, 1024, 768, 480],
                                                el: [600, 600, 600, 600],
                                                gw: [1240, 1024, 778, 600],
                                                gh: [600, 600, 600, 600],
                                                layout: 'fullwidth',
                                                mh: "0"
                                            });
                                            var revapi1,
                                                tpj;
                                            jQuery(function() {
                                                tpj = jQuery;
                                                if (tpj("#rev_slider_1_1").revolution == undefined) {
                                                    revslider_showDoubleJqueryError("#rev_slider_1_1");
                                                } else {
                                                    revapi1 = tpj("#rev_slider_1_1").show().revolution({
                                                        jsFileLocation: "//educlever.beplusthemes.com/elementary/wp-content/plugins/revslider/public/assets/js/",
                                                        sliderLayout: "fullwidth",
                                                        visibilityLevels: "1240,1024,768,480",
                                                        gridwidth: "1240,1024,778,600",
                                                        gridheight: "600,600,600,600",
                                                        minHeight: "",
                                                        lazyType: "smart",
                                                        editorheight: "600,600,600,600",
                                                        responsiveLevels: "1240,1024,768,480",
                                                        disableProgressBar: "on",
                                                        navigation: {
                                                            onHoverStop: false
                                                        },
                                                        fallbacks: {
                                                            allowHTML5AutoPlayOnAndroid: true
                                                        },
                                                    });
                                                }
                                            });
                                        </script>
                                        <script>
                                            var htmlDivCss = unescape("%23rev_slider_1_1%20.zeus.tparrows.tp-leftarrow%3Abefore%7B%0A%09font-family%3A%20FontAwesome%20%21important%3B%0A%20%20%20%20content%3A%20%22%5Cf104%22%20%21important%3B%0A%20%20%20%20%20%20font-size%3A%2030px%3B%0A%7D%0A%23rev_slider_1_1%20.zeus.tparrows.tp-rightarrow%3Abefore%7B%0A%09font-family%3A%20FontAwesome%20%21important%3B%0A%20%20%20%20content%3A%20%22%5Cf105%22%20%21important%3B%0A%20%20%20%20%20%20font-size%3A%2030px%3B%0A%7D");
                                            var htmlDiv = document.getElementById('rs-plugin-settings-inline-css');
                                            if (htmlDiv) {
                                                htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                            } else {
                                                var htmlDiv = document.createElement('div');
                                                htmlDiv.innerHTML = '<style>' + htmlDivCss + '</style>';
                                                document.getElementsByTagName('head')[0].appendChild(htmlDiv.childNodes[0]);
                                            }
                                        </script>
                                    </rs-module-wrap> <!-- END REVOLUTION SLIDER -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <section class="vc_section vc_custom_1571044605598">
                    <div class="container">
                        <div class="vc_row wpb_row vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-offset-3 vc_col-lg-6 vc_col-has-fill">
                                <div class="vc_column-inner vc_custom_1570680255431">
                                    <div class="wpb_wrapper">
                                        <div class="vc_empty_space" style="height: 60px"><span class="vc_empty_space_inner"></span></div>
                                        <h4 style="font-size: 22px;color: #666666;line-height: 32px;text-align: center" class="vc_custom_heading head-color">Join our Journey of dicovery</h4>
                                        <h3 style="font-size: 40px;color: #333333;line-height: 45px;text-align: center" class="vc_custom_heading head-color vc_custom_1570680026205">WELCOME TO THE <b>BRIGHT FUTURE</b> OF KIDS</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vc_row wpb_row vc_row-fluid vc_custom_1571040125175 vc_column-gap-35">
                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6 vc_col-xs-12">
                            <div class="vc_column-inner vc_custom_1571040098524">
                                <div class="wpb_wrapper">
                                    <div class="vc_empty_space" style="height: 5px"><span class="vc_empty_space_inner"></span></div>
                                    <h3 style="font-size: 19px;color: #666666;line-height: 32px;text-align: right" class="vc_custom_heading vc_custom_1571037604522" data-animate="fadeInLeft" data-duration="1.6" data-delay="0.2">Voluptas sit aspernatur aut odit aut fugit, sed quias consequuntur magni dolores eos qui ratione volust luptatem sequi nesciunt voluptas sit aspernatur aui luptatem sequi nesciunt.</h3>
                                    <h3 style="font-size: 19px;color: #666666;line-height: 32px;text-align: right" class="vc_custom_heading vc_custom_1571037623409" data-animate="fadeInLeft" data-duration="1.6" data-delay="0.2">Voluptas sit aspernatur aut odit aut fugit, sed quias luptatem sequi nesciunt voluptas sit aspernatur aui luptatem sequi nesciunt.</h3>
                                    <div class="vc_empty_space" style="height: 30px"><span class="vc_empty_space_inner"></span></div>
                                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                        <div class="inline-c wpb_column vc_column_container vc_col-sm-12">
                                            <div class="vc_column-inner">
                                                <div class="wpb_wrapper">
                                                    <div class="vc_btn3-container  animated-dashes-edu vc_btn3-inline vc_custom_1571038736641" data-animate="fadeInLeft" data-duration="1.6" data-delay="0.2"> <a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-round vc_btn3-style-custom vc_btn3-color-grey" href="#!" title="">REGISTRATION</a></div>
                                                    <div class="vc_btn3-container  animated-dashes-edu edu2 vc_btn3-inline vc_custom_1571038442961" data-animate="fadeInLeft" data-duration="1.6" data-delay="0.2"> <a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-round vc_btn3-style-custom vc_btn3-color-grey" href="#!" title="">VIEW LOCATION</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div data-animate="fadeInRight" data-duration="1.6" data-delay=".3" class="bt-video-edu wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6 vc_col-xs-12 vc_col-has-fill">
                            <div class="vc_column-inner vc_custom_1571040109805">
                                <div class="wpb_wrapper">
                                    <div class="wpb_single_image wpb_content_element vc_align_left">
                                        <figure class="wpb_wrapper vc_figure">
                                            <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="568" height="431" src="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/img-about-1.jpg" class="vc_single_image-img attachment-full" alt="" loading="lazy" srcset="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/img-about-1.jpg 568w, https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/img-about-1-300x228.jpg 300w" sizes="(max-width: 568px) 100vw, 568px" /></div>
                                        </figure>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section data-vc-full-width="true" data-vc-full-width-init="false" class="vc_section vc_custom_1571044588966 vc_section-has-fill">
                    <div class="vc_row wpb_row vc_row-fluid bt-dashes vc_custom_1571019571504 vc_column-gap-30">
                        <div data-animate="fadeInUp" data-duration="1.6" data-delay=".2" class="box1 wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4 vc_col-has-fill">
                            <div class="vc_column-inner vc_custom_1571039722443">
                                <div class="wpb_wrapper">
                                    <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1570697346908">
                                        <figure class="wpb_wrapper vc_figure"> <a href="#!" target="_self" class="vc_single_image-wrapper vc_box_circle  vc_box_border_grey"><img width="800" height="800" src="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/box1.jpg" class="vc_single_image-img attachment-full" alt="" loading="lazy" srcset="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/box1.jpg 800w, https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/box1-150x150.jpg 150w, https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/box1-300x300.jpg 300w, https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/box1-768x768.jpg 768w, https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/box1-600x600.jpg 600w, https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/box1-100x100.jpg 100w" sizes="(max-width: 800px) 100vw, 800px" /></a></figure>
                                    </div>
                                    <h1 style="font-size: 22px;color: #333333;line-height: 24px;text-align: center" class="vc_custom_heading">STRATEGIES</h1>
                                    <div class="wpb_text_column wpb_content_element  vc_custom_1570695770539">
                                        <div class="wpb_wrapper">
                                            <p style="text-align: center;">Lorem ipsum dolor sit amet, conse conse adipiscing elit.</p>
                                        </div>
                                    </div>
                                    <div class="vc_btn3-container  bt-service-but vc_btn3-center vc_custom_1570696091689"> <a style="background-color:rgb(255,255,255);background-color:rgba(255,255,255,0.01); color:#2e91fc;" class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-custom vc_btn3-icon-right" href="#!" title="">READING CONTINUE <i class="vc_btn3-icon fa fa-angle-double-right"></i></a></div>
                                </div>
                            </div>
                        </div>
                        <div data-animate="fadeInUp" data-duration="1.6" data-delay=".35" class="box2 wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4 vc_col-has-fill">
                            <div class="vc_column-inner vc_custom_1571039770884">
                                <div class="wpb_wrapper">
                                    <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1570697363224">
                                        <figure class="wpb_wrapper vc_figure"> <a href="#!" target="_self" class="vc_single_image-wrapper vc_box_circle  vc_box_border_grey"><img width="800" height="800" src="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/box2.jpg" class="vc_single_image-img attachment-large" alt="" loading="lazy" srcset="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/box2.jpg 800w, https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/box2-150x150.jpg 150w, https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/box2-300x300.jpg 300w, https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/box2-768x768.jpg 768w, https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/box2-600x600.jpg 600w, https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/box2-100x100.jpg 100w" sizes="(max-width: 800px) 100vw, 800px" /></a></figure>
                                    </div>
                                    <h1 style="font-size: 22px;color: #333333;line-height: 24px;text-align: center" class="vc_custom_heading">MOTIVATION</h1>
                                    <div class="wpb_text_column wpb_content_element  vc_custom_1570695785652">
                                        <div class="wpb_wrapper">
                                            <p style="text-align: center;">Lorem ipsum dolor sit amet, conse conse adipiscing elit.</p>
                                        </div>
                                    </div>
                                    <div class="vc_btn3-container  bt-service-but vc_btn3-center vc_custom_1570696009229"> <a style="background-color:rgb(255,255,255);background-color:rgba(255,255,255,0.01); color:#2e91fc;" class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-custom vc_btn3-icon-right" href="#!" title="">READING CONTINUE <i class="vc_btn3-icon fa fa-angle-double-right"></i></a></div>
                                </div>
                            </div>
                        </div>
                        <div data-animate="fadeInUp" data-duration="1.6" data-delay=".5" class="box3 wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4 vc_col-has-fill">
                            <div class="vc_column-inner vc_custom_1571039790266">
                                <div class="wpb_wrapper">
                                    <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1570697384251">
                                        <figure class="wpb_wrapper vc_figure"> <a href="#!" target="_self" class="vc_single_image-wrapper vc_box_circle  vc_box_border_grey"><img width="800" height="800" src="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/box3.jpg" class="vc_single_image-img attachment-large" alt="" loading="lazy" srcset="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/box3.jpg 800w, https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/box3-150x150.jpg 150w, https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/box3-300x300.jpg 300w, https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/box3-768x768.jpg 768w, https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/box3-600x600.jpg 600w, https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/box3-100x100.jpg 100w" sizes="(max-width: 800px) 100vw, 800px" /></a></figure>
                                    </div>
                                    <h1 style="font-size: 22px;color: #333333;line-height: 24px;text-align: center" class="vc_custom_heading">REGISTRATION</h1>
                                    <div class="wpb_text_column wpb_content_element  vc_custom_1570695849235">
                                        <div class="wpb_wrapper">
                                            <p style="text-align: center;">Lorem ipsum dolor sit amet, conse conse adipiscing elit.</p>
                                        </div>
                                    </div>
                                    <div class="vc_btn3-container  bt-service-but vc_btn3-center vc_custom_1570696071017"> <a style="background-color:rgb(255,255,255);background-color:rgba(255,255,255,0.01); color:#2e91fc;" class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-custom vc_btn3-icon-right" href="#!" title="">READING CONTINUE <i class="vc_btn3-icon fa fa-angle-double-right"></i></a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vc_row wpb_row vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-offset-2 vc_col-lg-8">
                            <div class="vc_column-inner vc_custom_1570699963406">
                                <div class="wpb_wrapper">
                                    <div class="vc_empty_space" style="height: 20px"><span class="vc_empty_space_inner"></span></div>
                                    <div class="wpb_single_image wpb_content_element vc_align_center">
                                        <figure class="wpb_wrapper vc_figure">
                                            <div class="vc_single_image-wrapper   vc_box_border_grey"><img width="149" height="151" src="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/loa.png" class="vc_single_image-img attachment-full" alt="" loading="lazy" srcset="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/loa.png 149w, https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/loa-100x100.png 100w" sizes="(max-width: 149px) 100vw, 149px" /></div>
                                        </figure>
                                    </div>
                                    <h3 style="font-size: 30px;color: #333333;line-height: 40px;text-align: center" class="vc_custom_heading head-color">Please Enter Email and <b>Subscribe Now</b> in Order to Get the Latest News or Request a Quote</h3>
                                    <div class="vc_empty_space" style="height: 32px"><span class="vc_empty_space_inner"></span></div>
                                    <div class="wpb_widgetised_column wpb_content_element newsleter-tary">
                                        <div class="wpb_wrapper">
                                            <aside id="newsletterwidgetminimal-4" class="widget widget_newsletterwidgetminimal">
                                                <div class="tnp tnp-widget-minimal">
                                                    <form class="tnp-form" action="https://educlever.beplusthemes.com/elementary/?na=s" method="post"><input type="hidden" name="nr" value="widget-minimal" /><input class="tnp-email" type="email" required name="ne" value="" placeholder="Email"><input class="tnp-submit" type="submit" value="SUBSCRIBE"></form>
                                                </div>
                                            </aside>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div class="vc_row-full-width vc_clearfix"></div>
                <section data-vc-full-width="true" data-vc-full-width-init="false" class="vc_section vc_custom_1570700709491 vc_section-has-fill">
                    <div class="vc_row wpb_row vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-offset-3 vc_col-lg-6 vc_col-md-offset-2 vc_col-md-8">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="vc_icon_element vc_icon_element-outer vc_custom_1571036423161 vc_icon_element-align-center">
                                        <div class="vc_icon_element-inner vc_icon_element-color-custom vc_icon_element-size-md vc_icon_element-style- vc_icon_element-background-color-grey" style=""> <span class="vc_icon_element-icon fa fa-spinner" style="color:#559d2d !important"></span></div>
                                    </div>
                                    <h1 style="font-size: 40px;color: #ffffff;line-height: 44px;text-align: center" class="vc_custom_heading edu-title vc_custom_1571036293049">POPULAR COURSES</h1>
                                    <h4 style="font-size: 18px;color: #ffffff;line-height: 24px;text-align: center" class="vc_custom_heading">Lorem ipsum dolor sit amet, conse ct amet, conse adipiscing</h4>
                                    <div class="vc_empty_space  edu-space" style="height: 40px"><span class="vc_empty_space_inner"></span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vc_row wpb_row vc_row-fluid">
                        <div data-animate="fadeInUp" data-duration="1.6" data-delay=".3" class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <style type="text/css">
                                        a.eg-henryharrison-element-1,
                                        a.eg-henryharrison-element-2 {
                                            -webkit-transition: all .4s linear;
                                            -moz-transition: all .4s linear;
                                            -o-transition: all .4s linear;
                                            -ms-transition: all .4s linear;
                                            transition: all .4s linear
                                        }

                                        .eg-jimmy-carter-element-11 i:before {
                                            margin-left: 0px;
                                            margin-right: 0px
                                        }

                                        .eg-harding-element-17 {
                                            letter-spacing: 1px
                                        }

                                        .eg-harding-wrapper .esg-entry-media {
                                            overflow: hidden;
                                            box-sizing: border-box;
                                            -webkit-box-sizing: border-box;
                                            -moz-box-sizing: border-box;
                                            padding: 30px 30px 0px 30px
                                        }

                                        .eg-harding-wrapper .esg-media-poster {
                                            overflow: hidden;
                                            border-radius: 50%;
                                            -webkit-border-radius: 50%;
                                            -moz-border-radius: 50%
                                        }

                                        .eg-ulysses-s-grant-wrapper .esg-entry-media {
                                            overflow: hidden;
                                            box-sizing: border-box;
                                            -webkit-box-sizing: border-box;
                                            -moz-box-sizing: border-box;
                                            padding: 30px 30px 0px 30px
                                        }

                                        .eg-ulysses-s-grant-wrapper .esg-media-poster {
                                            overflow: hidden;
                                            border-radius: 50%;
                                            -webkit-border-radius: 50%;
                                            -moz-border-radius: 50%
                                        }

                                        .eg-richard-nixon-wrapper .esg-entry-media {
                                            overflow: hidden;
                                            box-sizing: border-box;
                                            -webkit-box-sizing: border-box;
                                            -moz-box-sizing: border-box;
                                            padding: 30px 30px 0px 30px
                                        }

                                        .eg-richard-nixon-wrapper .esg-media-poster {
                                            overflow: hidden;
                                            border-radius: 50%;
                                            -webkit-border-radius: 50%;
                                            -moz-border-radius: 50%
                                        }

                                        .eg-herbert-hoover-wrapper .esg-media-poster {
                                            filter: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/></filter></svg>#grayscale");
                                            filter: gray;
                                            -webkit-filter: grayscale(100%)
                                        }

                                        .eg-herbert-hoover-wrapper:hover .esg-media-poster {
                                            filter: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='1 0 0 0 0,0 1 0 0 0,0 0 1 0 0,0 0 0 1 0'/></filter></svg>#grayscale");
                                            -webkit-filter: grayscale(0%)
                                        }

                                        .eg-lyndon-johnson-wrapper .esg-media-poster {
                                            filter: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/></filter></svg>#grayscale");
                                            filter: gray;
                                            -webkit-filter: grayscale(100%)
                                        }

                                        .eg-lyndon-johnson-wrapper:hover .esg-media-poster {
                                            filter: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='1 0 0 0 0,0 1 0 0 0,0 0 1 0 0,0 0 0 1 0'/></filter></svg>#grayscale");
                                            -webkit-filter: grayscale(0%)
                                        }

                                        .esg-overlay.eg-ronald-reagan-container {
                                            background: -moz-linear-gradient(top, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            background: -webkit-gradient(linear, left top, left bottom, color-stop(50%, rgba(0, 0, 0, 0)), color-stop(99%, rgba(0, 0, 0, 0.83)), color-stop(100%, rgba(0, 0, 0, 0.85)));
                                            background: -webkit-linear-gradient(top, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            background: -o-linear-gradient(top, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            background: -ms-linear-gradient(top, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00000000', endColorstr='#d9000000', GradientType=0)
                                        }

                                        .eg-georgebush-wrapper .esg-entry-cover {
                                            background: -moz-linear-gradient(top, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            background: -webkit-gradient(linear, left top, left bottom, color-stop(50%, rgba(0, 0, 0, 0)), color-stop(99%, rgba(0, 0, 0, 0.83)), color-stop(100%, rgba(0, 0, 0, 0.85)));
                                            background: -webkit-linear-gradient(top, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            background: -o-linear-gradient(top, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            background: -ms-linear-gradient(top, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00000000', endColorstr='#d9000000', GradientType=0)
                                        }

                                        .eg-jefferson-wrapper {
                                            -webkit-border-radius: 5px !important;
                                            -moz-border-radius: 5px !important;
                                            border-radius: 5px !important;
                                            -webkit-mask-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAA5JREFUeNpiYGBgAAgwAAAEAAGbA+oJAAAAAElFTkSuQmCC) !important
                                        }

                                        .eg-monroe-element-1 {
                                            text-shadow: 0px 1px 3px rgba(0, 0, 0, 0.1)
                                        }

                                        .eg-lyndon-johnson-wrapper .esg-entry-cover {
                                            background: -moz-radial-gradient(center, ellipse cover, rgba(0, 0, 0, 0.35) 0%, rgba(18, 18, 18, 0) 96%, rgba(19, 19, 19, 0) 100%);
                                            background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%, rgba(0, 0, 0, 0.35)), color-stop(96%, rgba(18, 18, 18, 0)), color-stop(100%, rgba(19, 19, 19, 0)));
                                            background: -webkit-radial-gradient(center, ellipse cover, rgba(0, 0, 0, 0.35) 0%, rgba(18, 18, 18, 0) 96%, rgba(19, 19, 19, 0) 100%);
                                            background: -o-radial-gradient(center, ellipse cover, rgba(0, 0, 0, 0.35) 0%, rgba(18, 18, 18, 0) 96%, rgba(19, 19, 19, 0) 100%);
                                            background: -ms-radial-gradient(center, ellipse cover, rgba(0, 0, 0, 0.35) 0%, rgba(18, 18, 18, 0) 96%, rgba(19, 19, 19, 0) 100%);
                                            background: radial-gradient(ellipse at center, rgba(0, 0, 0, 0.35) 0%, rgba(18, 18, 18, 0) 96%, rgba(19, 19, 19, 0) 100%);
                                            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#59000000', endColorstr='#00131313', GradientType=1)
                                        }

                                        .eg-wilbert-wrapper .esg-entry-cover {
                                            background: -moz-radial-gradient(center, ellipse cover, rgba(0, 0, 0, 0.35) 0%, rgba(18, 18, 18, 0) 96%, rgba(19, 19, 19, 0) 100%);
                                            background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%, rgba(0, 0, 0, 0.35)), color-stop(96%, rgba(18, 18, 18, 0)), color-stop(100%, rgba(19, 19, 19, 0)));
                                            background: -webkit-radial-gradient(center, ellipse cover, rgba(0, 0, 0, 0.35) 0%, rgba(18, 18, 18, 0) 96%, rgba(19, 19, 19, 0) 100%);
                                            background: -o-radial-gradient(center, ellipse cover, rgba(0, 0, 0, 0.35) 0%, rgba(18, 18, 18, 0) 96%, rgba(19, 19, 19, 0) 100%);
                                            background: -ms-radial-gradient(center, ellipse cover, rgba(0, 0, 0, 0.35) 0%, rgba(18, 18, 18, 0) 96%, rgba(19, 19, 19, 0) 100%);
                                            background: radial-gradient(ellipse at center, rgba(0, 0, 0, 0.35) 0%, rgba(18, 18, 18, 0) 96%, rgba(19, 19, 19, 0) 100%);
                                            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#59000000', endColorstr='#00131313', GradientType=1)
                                        }

                                        .eg-wilbert-wrapper .esg-media-poster {
                                            -webkit-transition: 0.4s ease-in-out;
                                            -moz-transition: 0.4s ease-in-out;
                                            -o-transition: 0.4s ease-in-out;
                                            transition: 0.4s ease-in-out;
                                            filter: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/></filter></svg>#grayscale");
                                            filter: gray;
                                            -webkit-filter: grayscale(100%)
                                        }

                                        .eg-wilbert-wrapper:hover .esg-media-poster {
                                            filter: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='1 0 0 0 0,0 1 0 0 0,0 0 1 0 0,0 0 0 1 0'/></filter></svg>#grayscale");
                                            -webkit-filter: grayscale(0%)
                                        }

                                        .eg-phillie-element-3:after {
                                            content: " ";
                                            width: 0px;
                                            height: 0px;
                                            border-style: solid;
                                            border-width: 5px 5px 0 5px;
                                            border-color: #000 transparent transparent transparent;
                                            left: 50%;
                                            margin-left: -5px;
                                            bottom: -5px;
                                            position: absolute
                                        }

                                        .eg-howardtaft-wrapper .esg-media-poster {
                                            filter: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='1 0 0 0 0,0 1 0 0 0,0 0 1 0 0,0 0 0 1 0'/></filter></svg>#grayscale");
                                            -webkit-filter: grayscale(0%)
                                        }

                                        .eg-howardtaft-wrapper:hover .esg-media-poster {
                                            filter: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/></filter></svg>#grayscale");
                                            filter: gray;
                                            -webkit-filter: grayscale(100%)
                                        }

                                        .myportfolio-container .added_to_cart.wc-forward {
                                            font-family: "Open Sans";
                                            font-size: 13px;
                                            color: #fff;
                                            margin-top: 10px
                                        }

                                        .esgbox-title.esgbox-title-outside-wrap {
                                            font-size: 15px;
                                            font-weight: 700;
                                            text-align: center
                                        }

                                        .esgbox-title.esgbox-title-inside-wrap {
                                            padding-bottom: 10px;
                                            font-size: 15px;
                                            font-weight: 700;
                                            text-align: center
                                        }

                                        .esg-content.eg-twitterstream-element-33-a {
                                            display: inline-block
                                        }

                                        .eg-twitterstream-element-35 {
                                            word-break: break-all
                                        }

                                        .esg-overlay.eg-twitterstream-container {
                                            background: -moz-linear-gradient(top, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            background: -webkit-gradient(linear, left top, left bottom, color-stop(50%, rgba(0, 0, 0, 0)), color-stop(99%, rgba(0, 0, 0, 0.83)), color-stop(100%, rgba(0, 0, 0, 0.85)));
                                            background: -webkit-linear-gradient(top, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            background: -o-linear-gradient(top, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            background: -ms-linear-gradient(top, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00000000', endColorstr='#d9000000', GradientType=0)
                                        }

                                        .esg-content.eg-facebookstream-element-33-a {
                                            display: inline-block
                                        }

                                        .eg-facebookstream-element-0 {
                                            word-break: break-all
                                        }

                                        .esg-overlay.eg-flickrstream-container {
                                            background: -moz-linear-gradient(top, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            background: -webkit-gradient(linear, left top, left bottom, color-stop(50%, rgba(0, 0, 0, 0)), color-stop(99%, rgba(0, 0, 0, 0.83)), color-stop(100%, rgba(0, 0, 0, 0.85)));
                                            background: -webkit-linear-gradient(top, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            background: -o-linear-gradient(top, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            background: -ms-linear-gradient(top, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00000000', endColorstr='#d9000000', GradientType=0)
                                        }

                                        a.eg-henryharrison-element-1,
                                        a.eg-henryharrison-element-2 {
                                            -webkit-transition: all .4s linear;
                                            -moz-transition: all .4s linear;
                                            -o-transition: all .4s linear;
                                            -ms-transition: all .4s linear;
                                            transition: all .4s linear
                                        }

                                        .eg-jimmy-carter-element-11 i:before {
                                            margin-left: 0px;
                                            margin-right: 0px
                                        }

                                        .eg-harding-element-17 {
                                            letter-spacing: 1px
                                        }

                                        .eg-harding-wrapper .esg-entry-media {
                                            overflow: hidden;
                                            box-sizing: border-box;
                                            -webkit-box-sizing: border-box;
                                            -moz-box-sizing: border-box;
                                            padding: 30px 30px 0px 30px
                                        }

                                        .eg-harding-wrapper .esg-media-poster {
                                            overflow: hidden;
                                            border-radius: 50%;
                                            -webkit-border-radius: 50%;
                                            -moz-border-radius: 50%
                                        }

                                        .eg-ulysses-s-grant-wrapper .esg-entry-media {
                                            overflow: hidden;
                                            box-sizing: border-box;
                                            -webkit-box-sizing: border-box;
                                            -moz-box-sizing: border-box;
                                            padding: 30px 30px 0px 30px
                                        }

                                        .eg-ulysses-s-grant-wrapper .esg-media-poster {
                                            overflow: hidden;
                                            border-radius: 50%;
                                            -webkit-border-radius: 50%;
                                            -moz-border-radius: 50%
                                        }

                                        .eg-richard-nixon-wrapper .esg-entry-media {
                                            overflow: hidden;
                                            box-sizing: border-box;
                                            -webkit-box-sizing: border-box;
                                            -moz-box-sizing: border-box;
                                            padding: 30px 30px 0px 30px
                                        }

                                        .eg-richard-nixon-wrapper .esg-media-poster {
                                            overflow: hidden;
                                            border-radius: 50%;
                                            -webkit-border-radius: 50%;
                                            -moz-border-radius: 50%
                                        }

                                        .eg-herbert-hoover-wrapper .esg-media-poster {
                                            filter: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/></filter></svg>#grayscale");
                                            filter: gray;
                                            -webkit-filter: grayscale(100%)
                                        }

                                        .eg-herbert-hoover-wrapper:hover .esg-media-poster {
                                            filter: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='1 0 0 0 0,0 1 0 0 0,0 0 1 0 0,0 0 0 1 0'/></filter></svg>#grayscale");
                                            -webkit-filter: grayscale(0%)
                                        }

                                        .eg-lyndon-johnson-wrapper .esg-media-poster {
                                            filter: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/></filter></svg>#grayscale");
                                            filter: gray;
                                            -webkit-filter: grayscale(100%)
                                        }

                                        .eg-lyndon-johnson-wrapper:hover .esg-media-poster {
                                            filter: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='1 0 0 0 0,0 1 0 0 0,0 0 1 0 0,0 0 0 1 0'/></filter></svg>#grayscale");
                                            -webkit-filter: grayscale(0%)
                                        }

                                        .esg-overlay.eg-ronald-reagan-container {
                                            background: -moz-linear-gradient(top, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            background: -webkit-gradient(linear, left top, left bottom, color-stop(50%, rgba(0, 0, 0, 0)), color-stop(99%, rgba(0, 0, 0, 0.83)), color-stop(100%, rgba(0, 0, 0, 0.85)));
                                            background: -webkit-linear-gradient(top, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            background: -o-linear-gradient(top, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            background: -ms-linear-gradient(top, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00000000', endColorstr='#d9000000', GradientType=0)
                                        }

                                        .eg-georgebush-wrapper .esg-entry-cover {
                                            background: -moz-linear-gradient(top, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            background: -webkit-gradient(linear, left top, left bottom, color-stop(50%, rgba(0, 0, 0, 0)), color-stop(99%, rgba(0, 0, 0, 0.83)), color-stop(100%, rgba(0, 0, 0, 0.85)));
                                            background: -webkit-linear-gradient(top, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            background: -o-linear-gradient(top, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            background: -ms-linear-gradient(top, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00000000', endColorstr='#d9000000', GradientType=0)
                                        }

                                        .eg-jefferson-wrapper {
                                            -webkit-border-radius: 5px !important;
                                            -moz-border-radius: 5px !important;
                                            border-radius: 5px !important;
                                            -webkit-mask-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAIAAACQd1PeAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAA5JREFUeNpiYGBgAAgwAAAEAAGbA+oJAAAAAElFTkSuQmCC) !important
                                        }

                                        .eg-monroe-element-1 {
                                            text-shadow: 0px 1px 3px rgba(0, 0, 0, 0.1)
                                        }

                                        .eg-lyndon-johnson-wrapper .esg-entry-cover {
                                            background: -moz-radial-gradient(center, ellipse cover, rgba(0, 0, 0, 0.35) 0%, rgba(18, 18, 18, 0) 96%, rgba(19, 19, 19, 0) 100%);
                                            background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%, rgba(0, 0, 0, 0.35)), color-stop(96%, rgba(18, 18, 18, 0)), color-stop(100%, rgba(19, 19, 19, 0)));
                                            background: -webkit-radial-gradient(center, ellipse cover, rgba(0, 0, 0, 0.35) 0%, rgba(18, 18, 18, 0) 96%, rgba(19, 19, 19, 0) 100%);
                                            background: -o-radial-gradient(center, ellipse cover, rgba(0, 0, 0, 0.35) 0%, rgba(18, 18, 18, 0) 96%, rgba(19, 19, 19, 0) 100%);
                                            background: -ms-radial-gradient(center, ellipse cover, rgba(0, 0, 0, 0.35) 0%, rgba(18, 18, 18, 0) 96%, rgba(19, 19, 19, 0) 100%);
                                            background: radial-gradient(ellipse at center, rgba(0, 0, 0, 0.35) 0%, rgba(18, 18, 18, 0) 96%, rgba(19, 19, 19, 0) 100%);
                                            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#59000000', endColorstr='#00131313', GradientType=1)
                                        }

                                        .eg-wilbert-wrapper .esg-entry-cover {
                                            background: -moz-radial-gradient(center, ellipse cover, rgba(0, 0, 0, 0.35) 0%, rgba(18, 18, 18, 0) 96%, rgba(19, 19, 19, 0) 100%);
                                            background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%, rgba(0, 0, 0, 0.35)), color-stop(96%, rgba(18, 18, 18, 0)), color-stop(100%, rgba(19, 19, 19, 0)));
                                            background: -webkit-radial-gradient(center, ellipse cover, rgba(0, 0, 0, 0.35) 0%, rgba(18, 18, 18, 0) 96%, rgba(19, 19, 19, 0) 100%);
                                            background: -o-radial-gradient(center, ellipse cover, rgba(0, 0, 0, 0.35) 0%, rgba(18, 18, 18, 0) 96%, rgba(19, 19, 19, 0) 100%);
                                            background: -ms-radial-gradient(center, ellipse cover, rgba(0, 0, 0, 0.35) 0%, rgba(18, 18, 18, 0) 96%, rgba(19, 19, 19, 0) 100%);
                                            background: radial-gradient(ellipse at center, rgba(0, 0, 0, 0.35) 0%, rgba(18, 18, 18, 0) 96%, rgba(19, 19, 19, 0) 100%);
                                            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#59000000', endColorstr='#00131313', GradientType=1)
                                        }

                                        .eg-wilbert-wrapper .esg-media-poster {
                                            -webkit-transition: 0.4s ease-in-out;
                                            -moz-transition: 0.4s ease-in-out;
                                            -o-transition: 0.4s ease-in-out;
                                            transition: 0.4s ease-in-out;
                                            filter: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/></filter></svg>#grayscale");
                                            filter: gray;
                                            -webkit-filter: grayscale(100%)
                                        }

                                        .eg-wilbert-wrapper:hover .esg-media-poster {
                                            filter: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='1 0 0 0 0,0 1 0 0 0,0 0 1 0 0,0 0 0 1 0'/></filter></svg>#grayscale");
                                            -webkit-filter: grayscale(0%)
                                        }

                                        .eg-phillie-element-3:after {
                                            content: " ";
                                            width: 0px;
                                            height: 0px;
                                            border-style: solid;
                                            border-width: 5px 5px 0 5px;
                                            border-color: #000 transparent transparent transparent;
                                            left: 50%;
                                            margin-left: -5px;
                                            bottom: -5px;
                                            position: absolute
                                        }

                                        .eg-howardtaft-wrapper .esg-media-poster {
                                            filter: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='1 0 0 0 0,0 1 0 0 0,0 0 1 0 0,0 0 0 1 0'/></filter></svg>#grayscale");
                                            -webkit-filter: grayscale(0%)
                                        }

                                        .eg-howardtaft-wrapper:hover .esg-media-poster {
                                            filter: url("data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg'><filter id='grayscale'><feColorMatrix type='matrix' values='0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0.3333 0.3333 0.3333 0 0 0 0 0 1 0'/></filter></svg>#grayscale");
                                            filter: gray;
                                            -webkit-filter: grayscale(100%)
                                        }

                                        .myportfolio-container .added_to_cart.wc-forward {
                                            font-family: "Open Sans";
                                            font-size: 13px;
                                            color: #fff;
                                            margin-top: 10px
                                        }

                                        .esgbox-title.esgbox-title-outside-wrap {
                                            font-size: 15px;
                                            font-weight: 700;
                                            text-align: center
                                        }

                                        .esgbox-title.esgbox-title-inside-wrap {
                                            padding-bottom: 10px;
                                            font-size: 15px;
                                            font-weight: 700;
                                            text-align: center
                                        }

                                        .esg-content.eg-twitterstream-element-33-a {
                                            display: inline-block
                                        }

                                        .eg-twitterstream-element-35 {
                                            word-break: break-all
                                        }

                                        .esg-overlay.eg-twitterstream-container {
                                            background: -moz-linear-gradient(top, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            background: -webkit-gradient(linear, left top, left bottom, color-stop(50%, rgba(0, 0, 0, 0)), color-stop(99%, rgba(0, 0, 0, 0.83)), color-stop(100%, rgba(0, 0, 0, 0.85)));
                                            background: -webkit-linear-gradient(top, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            background: -o-linear-gradient(top, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            background: -ms-linear-gradient(top, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00000000', endColorstr='#d9000000', GradientType=0)
                                        }

                                        .esg-content.eg-facebookstream-element-33-a {
                                            display: inline-block
                                        }

                                        .eg-facebookstream-element-0 {
                                            word-break: break-all
                                        }

                                        .esg-overlay.eg-flickrstream-container {
                                            background: -moz-linear-gradient(top, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            background: -webkit-gradient(linear, left top, left bottom, color-stop(50%, rgba(0, 0, 0, 0)), color-stop(99%, rgba(0, 0, 0, 0.83)), color-stop(100%, rgba(0, 0, 0, 0.85)));
                                            background: -webkit-linear-gradient(top, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            background: -o-linear-gradient(top, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            background: -ms-linear-gradient(top, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            background: linear-gradient(to bottom, rgba(0, 0, 0, 0) 50%, rgba(0, 0, 0, 0.83) 99%, rgba(0, 0, 0, 0.85) 100%);
                                            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00000000', endColorstr='#d9000000', GradientType=0)
                                        }
                                    </style> <!-- CACHE CREATED FOR: 4 -->
                                    <style type="text/css">
                                        .courses-skin-1 {
                                            text-align: center !important
                                        }

                                        .courses-skin-1 .navigationbuttons,
                                        .courses-skin-1 .esg-pagination,
                                        .courses-skin-1 .esg-filters {
                                            border-radius: 40px !important;
                                            line-height: 70px !important;
                                            padding: 0 30px;
                                            text-align: center !important;
                                            display: inline-block;
                                            background: #fff;
                                            -webkit-box-shadow: 0px 0px 30px 0px rgba(0, 0, 0, 0.1);
                                            -moz-box-shadow: 0px 0px 30px 0px rgba(0, 0, 0, 0.1);
                                            box-shadow: 0px 0px 30px 0px rgba(0, 0, 0, 0.1)
                                        }

                                        .courses-skin-1 .esg-filterbutton,
                                        .courses-skin-1 .esg-navigationbutton,
                                        .courses-skin-1 .esg-sortbutton,
                                        .courses-skin-1 .esg-cartbutton {
                                            color: #000;
                                            cursor: pointer;
                                            position: relative;
                                            z-index: 2;
                                            padding: 0px 25px;
                                            border: none;
                                            line-height: 70px;
                                            font-size: 17px;
                                            font-weight: 700;
                                            display: inline-block;
                                            background: #fff
                                        }

                                        .courses-skin-1 .esg-navigationbutton {
                                            padding: 2px 12px
                                        }

                                        .courses-skin-1 .esg-navigationbutton * {
                                            color: #000
                                        }

                                        .courses-skin-1 .esg-pagination-button:last-child {
                                            margin-right: 0
                                        }

                                        .courses-skin-1 .esg-sortbutton-wrapper,
                                        .courses-skin-1 .esg-cartbutton-wrapper {
                                            display: inline-block
                                        }

                                        .courses-skin-1 .esg-sortbutton-order,
                                        .courses-skin-1 .esg-cartbutton-order {
                                            display: inline-block;
                                            vertical-align: top;
                                            border: none;
                                            width: 40px;
                                            line-height: 40px;
                                            border-radius: 5px;
                                            -moz-border-radius: 5px;
                                            -webkit-border-radius: 5px;
                                            font-size: 12px;
                                            font-weight: 700;
                                            color: #999;
                                            cursor: pointer;
                                            background: #eee;
                                            background: #fff;
                                            margin-left: 5px
                                        }

                                        .courses-skin-1 .esg-cartbutton {
                                            color: #fff;
                                            cursor: default !important
                                        }

                                        .courses-skin-1 .esg-cartbutton .esgicon-basket {
                                            color: #fff;
                                            font-size: 15px;
                                            line-height: 15px;
                                            margin-right: 10px
                                        }

                                        .courses-skin-1 .esg-cartbutton-wrapper {
                                            cursor: default !important
                                        }

                                        .courses-skin-1 .esg-sortbutton,
                                        .courses-skin-1 .esg-cartbutton {
                                            display: inline-block;
                                            position: relative;
                                            cursor: pointer;
                                            margin-right: 0px;
                                            border-radius: 5px;
                                            -moz-border-radius: 5px;
                                            -webkit-border-radius: 5px
                                        }

                                        .courses-skin-1 .esg-navigationbutton:hover,
                                        .courses-skin-1 .esg-filterbutton:hover,
                                        .courses-skin-1 .esg-sortbutton:hover,
                                        .courses-skin-1 .esg-sortbutton-order:hover,
                                        .courses-skin-1 .esg-cartbutton-order:hover,
                                        .courses-skin-1 .esg-filterbutton.selected {
                                            border-color: none;
                                            color: #fff;
                                            background: #91c73d
                                        }

                                        .courses-skin-1 .esg-navigationbutton:hover * {
                                            color: #333
                                        }

                                        .courses-skin-1 .esg-sortbutton-order.tp-desc:hover {
                                            color: #333
                                        }

                                        .courses-skin-1 .esg-filter-checked {
                                            padding: 1px 3px;
                                            color: #cbcbcb;
                                            background: #cbcbcb;
                                            margin-left: 7px;
                                            font-size: 9px;
                                            font-weight: 300;
                                            line-height: 9px;
                                            vertical-align: middle
                                        }

                                        .courses-skin-1 .esg-filterbutton.selected .esg-filter-checked,
                                        .courses-skin-1 .esg-filterbutton:hover .esg-filter-checked {
                                            padding: 1px 3px 1px 3px;
                                            color: #fff;
                                            background: #000;
                                            margin-left: 7px;
                                            font-size: 9px;
                                            font-weight: 300;
                                            line-height: 9px;
                                            vertical-align: middle
                                        }

                                        @media only screen and (max-width:768px) {

                                            .courses-skin-1 .navigationbuttons,
                                            .courses-skin-1 .esg-pagination,
                                            .courses-skin-1 .esg-filters {
                                                padding: 0;
                                                text-align: center !important;
                                                display: inline-block;
                                                background: transparent;
                                                -webkit-box-shadow: none;
                                                -moz-box-shadow: none;
                                                box-shadow: none
                                            }

                                            .courses-skin-1 .esg-filterbutton,
                                            .courses-skin-1 .esg-navigationbutton,
                                            .courses-skin-1 .esg-sortbutton,
                                            .courses-skin-1 .esg-cartbutton {
                                                line-height: 40px;
                                                padding: 0 15px
                                            }
                                        }
                                    </style>
                                    <style type="text/css">
                                        .eg-courses-skin-3-element-25 {
                                            font-size: 16px;
                                            line-height: 22px;
                                            color: #ffffff;
                                            font-weight: 400;
                                            display: inline-block;
                                            float: none;
                                            clear: none;
                                            margin: 0px 0px 0px 5px;
                                            padding: 17px 17px 17px 17px;
                                            border-radius: 50% 50% 50% 50%;
                                            background: rgba(0, 0, 0, 0.50);
                                            position: relative;
                                            z-index: 2 !important;
                                            border-top-width: 0px;
                                            border-right-width: 0px;
                                            border-bottom-width: 0px;
                                            border-left-width: 0px;
                                            border-color: #ffffff;
                                            border-style: solid
                                        }

                                        .eg-courses-skin-3-element-34 {
                                            font-size: 24px;
                                            line-height: 30px;
                                            color: #333333;
                                            font-weight: 700;
                                            padding: 0px 0px 0px 0px;
                                            border-radius: 0px 0px 0px 0px;
                                            background: rgba(255, 255, 255, 0);
                                            z-index: 2 !important;
                                            display: block;
                                            font-family: "Raleway"
                                        }

                                        .eg-courses-skin-3-element-35 {
                                            font-size: 16px;
                                            line-height: 24px;
                                            color: #666666;
                                            font-weight: 400;
                                            display: inline-block;
                                            float: none;
                                            clear: both;
                                            margin: 10px 0px 20px 0px;
                                            padding: 0px 0px 0px 0px;
                                            border-radius: 0px 0px 0px 0px;
                                            background: rgba(255, 255, 255, 1);
                                            position: relative;
                                            z-index: 2 !important;
                                            font-family: "Raleway"
                                        }

                                        .eg-courses-skin-3-element-6 {
                                            font-size: 15px;
                                            line-height: 22px;
                                            color: #555555;
                                            font-weight: 400;
                                            display: inline-block;
                                            float: none;
                                            clear: both;
                                            margin: 5px 0px 0px 0px;
                                            padding: 0px 0px 0px 0px;
                                            border-radius: 0px 0px 0px 0px;
                                            background: transparent;
                                            position: relative;
                                            z-index: 2 !important;
                                            font-family: "Open Sans";
                                            text-transform: capitalize
                                        }
                                    </style>
                                    <style type="text/css">
                                        .eg-courses-skin-3-element-25:hover {
                                            font-size: 16px;
                                            line-height: 22px;
                                            color: #ffffff;
                                            font-weight: 400;
                                            border-radius: 50px 50px 50px 50px;
                                            background: rgba(0, 0, 0, 0.75);
                                            border-top-width: 0px;
                                            border-right-width: 0px;
                                            border-bottom-width: 0px;
                                            border-left-width: 0px;
                                            border-color: #ffffff;
                                            border-style: solid
                                        }

                                        .eg-courses-skin-3-element-34:hover {
                                            font-size: 24px;
                                            line-height: 30px;
                                            color: #95c847;
                                            font-weight: 700;
                                            border-radius: 0px 0px 0px 0px;
                                            background: rgba(255, 255, 255, 0)
                                        }
                                    </style>
                                    <style type="text/css">
                                        .eg-courses-skin-3-element-34-a {
                                            display: inline-block;
                                            float: none;
                                            clear: both;
                                            margin: 0px 0px 10px 0px;
                                            position: relative
                                        }
                                    </style>
                                    <style type="text/css">
                                        .eg-courses-skin-3-container {
                                            background: transparent
                                        }
                                    </style>
                                    <style type="text/css">
                                        .eg-courses-skin-3-content {
                                            background: #ffffff;
                                            padding: 30px 30px 30px 30px;
                                            border-width: 0px 1px 1px 1px;
                                            border-radius: 0px 0px 0px 0px;
                                            border-color: #e5e5e5;
                                            border-style: solid;
                                            text-align: left
                                        }
                                    </style>
                                    <style type="text/css">
                                        .esg-grid .mainul li.eg-courses-skin-3-wrapper {
                                            background: #ffffff;
                                            padding: 0px 0px 0px 0px;
                                            border-width: 0px 0px 0px 0px;
                                            border-radius: 0px 0px 0px 0px;
                                            border-color: transparent;
                                            border-style: none
                                        }
                                    </style>
                                    <style type="text/css">
                                        .esg-grid .mainul li.eg-courses-skin-3-wrapper .esg-media-poster {
                                            background-size: cover;
                                            background-position: center center;
                                            background-repeat: no-repeat
                                        }
                                    </style> <!-- THE ESSENTIAL GRID 2.3.2 POST -->
                                    <article class="myportfolio-container courses-skin-1 source_type_post" id="courses-grid-2">
                                        <div id="esg-grid-4-1" class="esg-grid" style="background: transparent;padding: 0px 0px 0px 0px ; box-sizing:border-box; -moz-box-sizing:border-box; -webkit-box-sizing:border-box; display:none">
                                            <ul>
                                                <li id="eg-4-post-id-30" data-skin="courses-skin-3" class="filterall filter-frontend filter-it-software filter-articles filter-photoshop filter-websites eg-courses-skin-3-wrapper eg-post-id-30" data-date="1536673974" data-anime="esg-item-shift" data-anime-shift="up" data-anime-shift-amount="10">
                                                    <div class="esg-media-cover-wrapper">
                                                        <div class="esg-entry-media"><img src="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2018/10/course_huge3.jpg" alt="" width="1200" height="800"></div>
                                                        <div class="esg-entry-cover">
                                                            <div class="esg-overlay esg-transition eg-courses-skin-3-container" data-delay="0" data-duration="default" data-transition="esg-fade"></div>
                                                            <div class="esg-center eg-post-30 esg-click-to-play-video eg-courses-skin-3-element-25 esg-transition" data-delay="0.1" data-duration="default" data-transition="esg-fade"><i class="eg-icon-play"></i></div>
                                                        </div>
                                                        <div class="esg-entry-content eg-courses-skin-3-content">
                                                            <div class="esg-content eg-post-30 eg-courses-skin-3-element-34-a"><a class="eg-courses-skin-3-element-34 eg-post-30" href="https://educlever.beplusthemes.com/elementary/course/javascript-online-course/" target="_self">JavaScript Online Course</a></div>
                                                            <div class="esg-content eg-courses-skin-3-element-3 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                            <div class="esg-content eg-post-30 eg-courses-skin-3-nostyle-element-38">
                                                                <div class="courses-les-skin3"><span><i class="fa fa-align-left" aria-hidden="true"></i> 4 Lesson</span><span><i class="fa fa-clock-o" aria-hidden="true"></i> 12 week</span></div>
                                                            </div>
                                                            <div class="esg-content eg-courses-skin-3-element-37 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                            <div class="esg-content eg-post-30 eg-courses-skin-3-element-35">Lorem Ipsu is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's...</div>
                                                            <div class="esg-content eg-post-30 eg-courses-skin-3-nostyle-element-36">
                                                                <div class="ess-courses-meta3">
                                                                    <div class="count-user"><i class="fa fa-users" aria-hidden="true"></i>124 Buyers</div>
                                                                    <div class="count-comment"><i class="fa fa-comment"></i> 0 Comment</div>
                                                                </div>
                                                            </div>
                                                            <div class="esg-content eg-post-30 eg-courses-skin-3-element-6">
                                                                <div class="ess-coursrs-price_skin3">PRICE <span> <span class="price">Free</span></span></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li id="eg-4-post-id-387" data-skin="courses-skin-3" class="filterall filter-frontend filter-technology filter-graphic-design filter-photography filter-websites eg-courses-skin-3-wrapper eg-post-id-387" data-date="1539401723" data-anime="esg-item-shift" data-anime-shift="up" data-anime-shift-amount="10">
                                                    <div class="esg-media-cover-wrapper">
                                                        <div class="esg-entry-media"><img src="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2018/10/course_huge.jpg" alt="" width="1200" height="800"></div>
                                                        <div class="esg-entry-cover">
                                                            <div class="esg-overlay esg-transition eg-courses-skin-3-container" data-delay="0" data-duration="default" data-transition="esg-fade"></div>
                                                            <div class="esg-center eg-post-387 esg-click-to-play-video eg-courses-skin-3-element-25 esg-transition" data-delay="0.1" data-duration="default" data-transition="esg-fade"><i class="eg-icon-play"></i></div>
                                                        </div>
                                                        <div class="esg-entry-content eg-courses-skin-3-content">
                                                            <div class="esg-content eg-post-387 eg-courses-skin-3-element-34-a"><a class="eg-courses-skin-3-element-34 eg-post-387" href="https://educlever.beplusthemes.com/elementary/course/chemestry-online-course/" target="_self">Chemestry Online Course</a></div>
                                                            <div class="esg-content eg-courses-skin-3-element-3 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                            <div class="esg-content eg-post-387 eg-courses-skin-3-nostyle-element-38">
                                                                <div class="courses-les-skin3"><span><i class="fa fa-align-left" aria-hidden="true"></i> 2 Lesson</span><span><i class="fa fa-clock-o" aria-hidden="true"></i> 12 week</span></div>
                                                            </div>
                                                            <div class="esg-content eg-courses-skin-3-element-37 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                            <div class="esg-content eg-post-387 eg-courses-skin-3-element-35">Lorem Ipsu is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's...</div>
                                                            <div class="esg-content eg-post-387 eg-courses-skin-3-nostyle-element-36">
                                                                <div class="ess-courses-meta3">
                                                                    <div class="count-user"><i class="fa fa-users" aria-hidden="true"></i>123 Buyers</div>
                                                                    <div class="count-comment"><i class="fa fa-comment"></i> 0 Comment</div>
                                                                </div>
                                                            </div>
                                                            <div class="esg-content eg-post-387 eg-courses-skin-3-element-6">
                                                                <div class="ess-coursrs-price_skin3">PRICE <span> <span class="price has-origin">$69.00</span></span></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li id="eg-4-post-id-389" data-skin="courses-skin-3" class="filterall filter-backend filter-technology filter-articles filter-graphic-design filter-photoshop eg-courses-skin-3-wrapper eg-post-id-389" data-date="1539401797" data-anime="esg-item-shift" data-anime-shift="up" data-anime-shift-amount="10">
                                                    <div class="esg-media-cover-wrapper">
                                                        <div class="esg-entry-media"><img src="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2018/10/course_supersize.jpg" alt="" width="1200" height="800"></div>
                                                        <div class="esg-entry-cover">
                                                            <div class="esg-overlay esg-transition eg-courses-skin-3-container" data-delay="0" data-duration="default" data-transition="esg-fade"></div>
                                                            <div class="esg-center eg-post-389 esg-click-to-play-video eg-courses-skin-3-element-25 esg-transition" data-delay="0.1" data-duration="default" data-transition="esg-fade"><i class="eg-icon-play"></i></div>
                                                        </div>
                                                        <div class="esg-entry-content eg-courses-skin-3-content">
                                                            <div class="esg-content eg-post-389 eg-courses-skin-3-element-34-a"><a class="eg-courses-skin-3-element-34 eg-post-389" href="https://educlever.beplusthemes.com/elementary/course/html5-css3-essentials/" target="_self">HTML5/CSS3 Essentials</a></div>
                                                            <div class="esg-content eg-courses-skin-3-element-3 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                            <div class="esg-content eg-post-389 eg-courses-skin-3-nostyle-element-38">
                                                                <div class="courses-les-skin3"><span><i class="fa fa-align-left" aria-hidden="true"></i> 4 Lesson</span><span><i class="fa fa-clock-o" aria-hidden="true"></i> 12 week</span></div>
                                                            </div>
                                                            <div class="esg-content eg-courses-skin-3-element-37 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                            <div class="esg-content eg-post-389 eg-courses-skin-3-element-35">Lorem Ipsu is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's...</div>
                                                            <div class="esg-content eg-post-389 eg-courses-skin-3-nostyle-element-36">
                                                                <div class="ess-courses-meta3">
                                                                    <div class="count-user"><i class="fa fa-users" aria-hidden="true"></i>123 Buyers</div>
                                                                    <div class="count-comment"><i class="fa fa-comment"></i> 0 Comment</div>
                                                                </div>
                                                            </div>
                                                            <div class="esg-content eg-post-389 eg-courses-skin-3-element-6">
                                                                <div class="ess-coursrs-price_skin3">PRICE <span> <span class="price has-origin">$69.00</span></span></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li id="eg-4-post-id-391" data-skin="courses-skin-3" class="filterall filter-photography filter-technology filter-articles filter-graphic-design filter-websites eg-courses-skin-3-wrapper eg-post-id-391" data-date="1539401914" data-anime="esg-item-shift" data-anime-shift="up" data-anime-shift-amount="10">
                                                    <div class="esg-media-cover-wrapper">
                                                        <div class="esg-entry-media"><img src="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2018/10/course_huge1.jpg" alt="" width="1200" height="800"></div>
                                                        <div class="esg-entry-cover">
                                                            <div class="esg-overlay esg-transition eg-courses-skin-3-container" data-delay="0" data-duration="default" data-transition="esg-fade"></div>
                                                            <div class="esg-center eg-post-391 esg-click-to-play-video eg-courses-skin-3-element-25 esg-transition" data-delay="0.1" data-duration="default" data-transition="esg-fade"><i class="eg-icon-play"></i></div>
                                                        </div>
                                                        <div class="esg-entry-content eg-courses-skin-3-content">
                                                            <div class="esg-content eg-post-391 eg-courses-skin-3-element-34-a"><a class="eg-courses-skin-3-element-34 eg-post-391" href="https://educlever.beplusthemes.com/elementary/course/visual-studio-online/" target="_self">Visual Studio Online Course</a></div>
                                                            <div class="esg-content eg-courses-skin-3-element-3 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                            <div class="esg-content eg-post-391 eg-courses-skin-3-nostyle-element-38">
                                                                <div class="courses-les-skin3"><span><i class="fa fa-align-left" aria-hidden="true"></i> 2 Lesson</span><span><i class="fa fa-clock-o" aria-hidden="true"></i> 11 week</span></div>
                                                            </div>
                                                            <div class="esg-content eg-courses-skin-3-element-37 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                            <div class="esg-content eg-post-391 eg-courses-skin-3-element-35">Lorem Ipsu is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's...</div>
                                                            <div class="esg-content eg-post-391 eg-courses-skin-3-nostyle-element-36">
                                                                <div class="ess-courses-meta3">
                                                                    <div class="count-user"><i class="fa fa-users" aria-hidden="true"></i>125 Buyers</div>
                                                                    <div class="count-comment"><i class="fa fa-comment"></i> 0 Comment</div>
                                                                </div>
                                                            </div>
                                                            <div class="esg-content eg-post-391 eg-courses-skin-3-element-6">
                                                                <div class="ess-coursrs-price_skin3">PRICE <span> <span class="price">Free</span></span></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li id="eg-4-post-id-393" data-skin="courses-skin-3" class="filterall filter-it-software filter-photography filter-photography filter-websites eg-courses-skin-3-wrapper eg-post-id-393" data-date="1539402148" data-anime="esg-item-shift" data-anime-shift="up" data-anime-shift-amount="10">
                                                    <div class="esg-media-cover-wrapper">
                                                        <div class="esg-entry-media"><img src="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2018/09/edu-post6-1.jpg" alt="" width="1200" height="800"></div>
                                                        <div class="esg-entry-cover">
                                                            <div class="esg-overlay esg-transition eg-courses-skin-3-container" data-delay="0" data-duration="default" data-transition="esg-fade"></div>
                                                            <div class="esg-center eg-post-393 esg-click-to-play-video eg-courses-skin-3-element-25 esg-transition" data-delay="0.1" data-duration="default" data-transition="esg-fade"><i class="eg-icon-play"></i></div>
                                                        </div>
                                                        <div class="esg-entry-content eg-courses-skin-3-content">
                                                            <div class="esg-content eg-post-393 eg-courses-skin-3-element-34-a"><a class="eg-courses-skin-3-element-34 eg-post-393" href="https://educlever.beplusthemes.com/elementary/course/logo-designing-classes/" target="_self">Logo Designing Classes</a></div>
                                                            <div class="esg-content eg-courses-skin-3-element-3 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                            <div class="esg-content eg-post-393 eg-courses-skin-3-nostyle-element-38">
                                                                <div class="courses-les-skin3"><span><i class="fa fa-align-left" aria-hidden="true"></i> 3 Lesson</span><span><i class="fa fa-clock-o" aria-hidden="true"></i> 11 week</span></div>
                                                            </div>
                                                            <div class="esg-content eg-courses-skin-3-element-37 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                            <div class="esg-content eg-post-393 eg-courses-skin-3-element-35">Lorem Ipsu is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's...</div>
                                                            <div class="esg-content eg-post-393 eg-courses-skin-3-nostyle-element-36">
                                                                <div class="ess-courses-meta3">
                                                                    <div class="count-user"><i class="fa fa-users" aria-hidden="true"></i>124 Buyers</div>
                                                                    <div class="count-comment"><i class="fa fa-comment"></i> 0 Comment</div>
                                                                </div>
                                                            </div>
                                                            <div class="esg-content eg-post-393 eg-courses-skin-3-element-6">
                                                                <div class="ess-coursrs-price_skin3">PRICE <span> <span class="price">Free</span></span></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li id="eg-4-post-id-395" data-skin="courses-skin-3" class="filterall filter-frontend filter-photography filter-technology filter-articles filter-photography filter-websites eg-courses-skin-3-wrapper eg-post-id-395" data-date="1539402258" data-anime="esg-item-shift" data-anime-shift="up" data-anime-shift-amount="10">
                                                    <div class="esg-media-cover-wrapper">
                                                        <div class="esg-entry-media"><img src="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2018/10/course_huge2.jpg" alt="" width="1200" height="800"></div>
                                                        <div class="esg-entry-cover">
                                                            <div class="esg-overlay esg-transition eg-courses-skin-3-container" data-delay="0" data-duration="default" data-transition="esg-fade"></div>
                                                            <div class="esg-center eg-post-395 esg-click-to-play-video eg-courses-skin-3-element-25 esg-transition" data-delay="0.1" data-duration="default" data-transition="esg-fade"><i class="eg-icon-play"></i></div>
                                                        </div>
                                                        <div class="esg-entry-content eg-courses-skin-3-content">
                                                            <div class="esg-content eg-post-395 eg-courses-skin-3-element-34-a"><a class="eg-courses-skin-3-element-34 eg-post-395" href="https://educlever.beplusthemes.com/elementary/course/print-designing-teaching/" target="_self">Print Designing Teaching</a></div>
                                                            <div class="esg-content eg-courses-skin-3-element-3 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                            <div class="esg-content eg-post-395 eg-courses-skin-3-nostyle-element-38">
                                                                <div class="courses-les-skin3"><span><i class="fa fa-align-left" aria-hidden="true"></i> 3 Lesson</span><span><i class="fa fa-clock-o" aria-hidden="true"></i> 10 week</span></div>
                                                            </div>
                                                            <div class="esg-content eg-courses-skin-3-element-37 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                            <div class="esg-content eg-post-395 eg-courses-skin-3-element-35">Lorem Ipsu is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's...</div>
                                                            <div class="esg-content eg-post-395 eg-courses-skin-3-nostyle-element-36">
                                                                <div class="ess-courses-meta3">
                                                                    <div class="count-user"><i class="fa fa-users" aria-hidden="true"></i>100 Buyers</div>
                                                                    <div class="count-comment"><i class="fa fa-comment"></i> 0 Comment</div>
                                                                </div>
                                                            </div>
                                                            <div class="esg-content eg-post-395 eg-courses-skin-3-element-6">
                                                                <div class="ess-coursrs-price_skin3">PRICE <span> <span class="price has-origin">$69.00</span></span></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li id="eg-4-post-id-397" data-skin="courses-skin-3" class="filterall filter-frontend filter-it-software filter-photography filter-graphic-design filter-photography filter-photoshop eg-courses-skin-3-wrapper eg-post-id-397" data-date="1539402470" data-anime="esg-item-shift" data-anime-shift="up" data-anime-shift-amount="10">
                                                    <div class="esg-media-cover-wrapper">
                                                        <div class="esg-entry-media"><img src="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2018/09/edu-post5-1.jpg" alt="" width="1200" height="800"></div>
                                                        <div class="esg-entry-cover">
                                                            <div class="esg-overlay esg-transition eg-courses-skin-3-container" data-delay="0" data-duration="default" data-transition="esg-fade"></div>
                                                            <div class="esg-center eg-post-397 esg-click-to-play-video eg-courses-skin-3-element-25 esg-transition" data-delay="0.1" data-duration="default" data-transition="esg-fade"><i class="eg-icon-play"></i></div>
                                                        </div>
                                                        <div class="esg-entry-content eg-courses-skin-3-content">
                                                            <div class="esg-content eg-post-397 eg-courses-skin-3-element-34-a"><a class="eg-courses-skin-3-element-34 eg-post-397" href="https://educlever.beplusthemes.com/elementary/course/learn-web-graphic-designing/" target="_self">Learn Web Graphic Designing</a></div>
                                                            <div class="esg-content eg-courses-skin-3-element-3 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                            <div class="esg-content eg-post-397 eg-courses-skin-3-nostyle-element-38">
                                                                <div class="courses-les-skin3"><span><i class="fa fa-align-left" aria-hidden="true"></i> 5 Lesson</span><span><i class="fa fa-clock-o" aria-hidden="true"></i> 10 week</span></div>
                                                            </div>
                                                            <div class="esg-content eg-courses-skin-3-element-37 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                            <div class="esg-content eg-post-397 eg-courses-skin-3-element-35">Lorem Ipsu is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's...</div>
                                                            <div class="esg-content eg-post-397 eg-courses-skin-3-nostyle-element-36">
                                                                <div class="ess-courses-meta3">
                                                                    <div class="count-user"><i class="fa fa-users" aria-hidden="true"></i>101 Buyers</div>
                                                                    <div class="count-comment"><i class="fa fa-comment"></i> 0 Comment</div>
                                                                </div>
                                                            </div>
                                                            <div class="esg-content eg-post-397 eg-courses-skin-3-element-6">
                                                                <div class="ess-coursrs-price_skin3">PRICE <span> <span class="price">Free</span></span></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li id="eg-4-post-id-399" data-skin="courses-skin-3" class="filterall filter-backend filter-frontend filter-articles filter-photography filter-photoshop eg-courses-skin-3-wrapper eg-post-id-399" data-date="1539402592" data-anime="esg-item-shift" data-anime-shift="up" data-anime-shift-amount="10">
                                                    <div class="esg-media-cover-wrapper">
                                                        <div class="esg-entry-media"><img src="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2018/10/course_huge4.jpg" alt="" width="1200" height="800"></div>
                                                        <div class="esg-entry-cover">
                                                            <div class="esg-overlay esg-transition eg-courses-skin-3-container" data-delay="0" data-duration="default" data-transition="esg-fade"></div>
                                                            <div class="esg-center eg-post-399 esg-click-to-play-video eg-courses-skin-3-element-25 esg-transition" data-delay="0.1" data-duration="default" data-transition="esg-fade"><i class="eg-icon-play"></i></div>
                                                        </div>
                                                        <div class="esg-entry-content eg-courses-skin-3-content">
                                                            <div class="esg-content eg-post-399 eg-courses-skin-3-element-34-a"><a class="eg-courses-skin-3-element-34 eg-post-399" href="https://educlever.beplusthemes.com/elementary/course/introduction-learn-lms-plugin/" target="_self">Introduction Learn LMS Plugin</a></div>
                                                            <div class="esg-content eg-courses-skin-3-element-3 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                            <div class="esg-content eg-post-399 eg-courses-skin-3-nostyle-element-38">
                                                                <div class="courses-les-skin3"><span><i class="fa fa-align-left" aria-hidden="true"></i> 3 Lesson</span><span><i class="fa fa-clock-o" aria-hidden="true"></i> 10 week</span></div>
                                                            </div>
                                                            <div class="esg-content eg-courses-skin-3-element-37 esg-none esg-clear" style="height: 5px; visibility: hidden;"></div>
                                                            <div class="esg-content eg-post-399 eg-courses-skin-3-element-35">Lorem Ipsu is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's...</div>
                                                            <div class="esg-content eg-post-399 eg-courses-skin-3-nostyle-element-36">
                                                                <div class="ess-courses-meta3">
                                                                    <div class="count-user"><i class="fa fa-users" aria-hidden="true"></i>101 Buyers</div>
                                                                    <div class="count-comment"><i class="fa fa-comment"></i> 0 Comment</div>
                                                                </div>
                                                            </div>
                                                            <div class="esg-content eg-post-399 eg-courses-skin-3-element-6">
                                                                <div class="ess-coursrs-price_skin3">PRICE <span> <span class="price has-origin">$69.00</span></span></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </article>
                                    <div class="clear"></div>
                                    <script type="text/javascript">
                                        var essapi_4;
                                        jQuery(document).ready(function() {
                                            essapi_4 = jQuery("#esg-grid-4-1").tpessential({
                                                gridID: 4,
                                                layout: "masonry",
                                                forceFullWidth: "off",
                                                lazyLoad: "off",
                                                row: 1,
                                                apiName: "essapi_4",
                                                loadMoreAjaxToken: "22f73ff103",
                                                loadMoreAjaxUrl: "https://educlever.beplusthemes.com/elementary/wp-admin/admin-ajax.php",
                                                loadMoreAjaxAction: "Essential_Grid_Front_request_ajax",
                                                ajaxContentTarget: "ess-grid-ajax-container-",
                                                ajaxScrollToOffset: "0",
                                                ajaxCloseButton: "off",
                                                ajaxContentSliding: "on",
                                                ajaxScrollToOnLoad: "on",
                                                ajaxCallbackArgument: "off",
                                                ajaxNavButton: "off",
                                                ajaxCloseType: "type1",
                                                ajaxCloseInner: "false",
                                                ajaxCloseStyle: "light",
                                                ajaxClosePosition: "tr",
                                                space: 25,
                                                pageAnimation: "fade",
                                                startAnimation: "none",
                                                startAnimationSpeed: 1000,
                                                startAnimationDelay: 100,
                                                startAnimationType: "item",
                                                animationType: "item",
                                                paginationScrollToTop: "off",
                                                paginationAutoplay: "off",
                                                spinner: "spinner0",
                                                lightBoxMode: "single",
                                                lightboxHash: "group",
                                                lightboxPostMinWid: "75%",
                                                lightboxPostMaxWid: "75%",
                                                lightboxSpinner: "off",
                                                lightBoxFeaturedImg: "off",
                                                lightBoxPostTitle: "off",
                                                lightBoxPostTitleTag: "h2",
                                                lightboxMargin: "0|0|0|0",
                                                lbContentPadding: "0|0|0|0",
                                                lbContentOverflow: "auto",
                                                animSpeed: 1000,
                                                delayBasic: 1,
                                                mainhoverdelay: 1,
                                                filterType: "single",
                                                showDropFilter: "hover",
                                                filterGroupClass: "esg-fgc-4",
                                                filterNoMatch: "No Items for the Selected Filter",
                                                filterDeepLink: "off",
                                                hideMarkups: "on",
                                                inViewport: true,
                                                viewportBuffer: 20,
                                                youtubeNoCookie: "false",
                                                convertFilterMobile: false,
                                                paginationSwipe: "off",
                                                paginationDragVer: "on",
                                                pageSwipeThrottle: 30,
                                                googleFonts: ['Open+Sans:300,400,600,700,800', 'Raleway:100,200,300,400,500,600,700,800,900', 'Droid+Serif:400,700'],
                                                hideBlankItemsAt: "1",
                                                responsiveEntries: [{
                                                        width: 1400,
                                                        amount: 3,
                                                        mmheight: 0
                                                    },
                                                    {
                                                        width: 1170,
                                                        amount: 3,
                                                        mmheight: 0
                                                    },
                                                    {
                                                        width: 1024,
                                                        amount: 3,
                                                        mmheight: 0
                                                    },
                                                    {
                                                        width: 960,
                                                        amount: 2,
                                                        mmheight: 0
                                                    },
                                                    {
                                                        width: 778,
                                                        amount: 2,
                                                        mmheight: 0
                                                    },
                                                    {
                                                        width: 640,
                                                        amount: 2,
                                                        mmheight: 0
                                                    },
                                                    {
                                                        width: 480,
                                                        amount: 1,
                                                        mmheight: 0
                                                    }
                                                ]
                                            });
                                            var arrows = true,
                                                lightboxOptions = {
                                                    margin: [0, 0, 0, 0],
                                                    buttons: ["share", "thumbs", "close"],
                                                    infobar: true,
                                                    loop: true,
                                                    slideShow: {
                                                        "autoStart": false,
                                                        "speed": 3000
                                                    },
                                                    animationEffect: "fade",
                                                    animationDuration: 500,
                                                    beforeShow: function(a, c) {
                                                        if (!arrows) {
                                                            jQuery("body").addClass("esgbox-hidearrows");
                                                        }
                                                        var i = 0,
                                                            multiple = false;
                                                        a = a.slides;
                                                        for (var b in a) {
                                                            i++;
                                                            if (i > 1) {
                                                                multiple = true;
                                                                break;
                                                            }
                                                        }
                                                        if (!multiple) jQuery("body").addClass("esgbox-single");
                                                        if (c.type === "image") jQuery(".esgbox-button--zoom").show();
                                                    },
                                                    beforeLoad: function(a, b) {
                                                        jQuery("body").removeClass("esg-four-by-three");
                                                        if (b.opts.$orig.data("ratio") === "4:3") jQuery("body").addClass("esg-four-by-three");
                                                    },
                                                    afterLoad: function() {
                                                        jQuery(window).trigger("resize.esglb");
                                                    },
                                                    afterClose: function() {
                                                        jQuery("body").removeClass("esgbox-hidearrows esgbox-single");
                                                    },
                                                    transitionEffect: "fade",
                                                    transitionDuration: 500,
                                                    hash: "group",
                                                    arrows: true,
                                                    wheel: false,
                                                };
                                            jQuery("#esg-grid-4-1").data("lightboxsettings", lightboxOptions);
                                        });
                                    </script>
                                    <div class="vc_empty_space" style="height: 20px"><span class="vc_empty_space_inner"></span></div>
                                    <div class="vc_btn3-container  animated-dashes-edu vc_btn3-center vc_custom_1571026279747" data-animate="fadeInLeft" data-duration="1.6" data-delay="0.2"> <a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-round vc_btn3-style-custom vc_btn3-color-grey" href="#!" title="">MORE COURSES</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div class="vc_row-full-width vc_clearfix"></div>
                <section class="vc_section vc_custom_1571036268766">
                    <div class="vc_row wpb_row vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="wpb_single_image wpb_content_element vc_align_center   img-svg">
                                        <figure class="wpb_wrapper vc_figure">
                                            <div class="vc_single_image-wrapper   vc_box_border_grey"><img src="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/megaphone.svg" class="vc_single_image-img attachment-full" alt="" loading="lazy" /></div>
                                        </figure>
                                    </div>
                                    <h1 style="font-size: 40px;color: #333333;line-height: 44px;text-align: center" class="vc_custom_heading edu-title">Education School Special Event Program</h1>
                                    <h3 style="font-size: 20px;color: #666666;line-height: 32px;text-align: center" class="vc_custom_heading">Voluptas sit aspernatur aut odit aut fugit, sed quias consequuntur magni dolores eos qui ratione volust luptatem sequi nesciunt.</h3>
                                    <div class="vc_empty_space" style="height: 10px"><span class="vc_empty_space_inner"></span></div>
                                    <h1 style="font-size: 20px;color: #666666;line-height: 30px;text-align: center" class="vc_custom_heading vc_custom_1570785702603">NEXT EVENTS START IN</h1>
                                    <h3 style="font-size: 20px;color: #559d2d;line-height: 30px;text-align: center" class="vc_custom_heading vc_custom_1570785714033">20-July-2016 To 25-July-2016</h3>
                                    <div class="vc_empty_space" style="height: 32px"><span class="vc_empty_space_inner"></span></div>
                                    <div class="vc_btn3-container  animated-dashes-edu edu2 vc_btn3-center vc_custom_1571036241875" data-animate="fadeInLeft" data-duration="1.6" data-delay="0.2"> <a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-round vc_btn3-style-custom vc_btn3-color-grey" href="#!" title="">JOIN EVENTS</a></div>
                                    <div class="vc_empty_space" style="height: 32px"><span class="vc_empty_space_inner"></span></div>
                                </div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="wpb_theme_custom_element wpb_events_grid">
                                        <div class="vc-custom-inner-wrap">
                                            <div class="item-inner col-xs-12 col-sm-6 col-md-6 col-lg-6 layout-style1">
                                                <div class="event-featured-image-wrap">
                                                    <div class="event-thumbnail" style="background: url(https://educlever.beplusthemes.com/elementary/wp-content/uploads/2017/06/edu_event6-614x346.jpg) center center / cover, #9E9E9E;"></div>
                                                    <div class="venue-empty"><i class="fa fa-map-marker" aria-hidden="true"> </i> <span class="tribe-address"> <span class="tribe-street-address">Phillip Street</span> <br> <span class="tribe-locality">Jersey City</span><span class="tribe-delimiter">,</span> <abbr class="tribe-region tribe-events-abbr" title="New Jersey">NJ</abbr> <span class="tribe-country-name">United States</span> </span></div>
                                                </div><a href="https://educlever.beplusthemes.com/elementary/event/light-box-paper-cut-dioramas/" class="title-link" title="Light Box Paper Cut Dioramas">
                                                    <div class="title">Light Box Paper Cut Dioramas</div>
                                                </a>
                                            </div>
                                            <div class="item-inner col-xs-12 col-sm-6 col-md-6 col-lg-6 layout-style1">
                                                <div class="event-featured-image-wrap">
                                                    <div class="event-thumbnail" style="background: url(https://educlever.beplusthemes.com/elementary/wp-content/uploads/2017/06/edu_event3-614x346.jpg) center center / cover, #9E9E9E;"></div>
                                                    <div class="venue-empty"><i class="fa fa-map-marker" aria-hidden="true"> </i> <span class="tribe-address"> <span class="tribe-street-address">Phillip Street</span> <br> <span class="tribe-locality">Jersey City</span><span class="tribe-delimiter">,</span> <abbr class="tribe-region tribe-events-abbr" title="New Jersey">NJ</abbr> <span class="tribe-country-name">United States</span> </span></div>
                                                </div><a href="https://educlever.beplusthemes.com/elementary/event/summer-school-2018/" class="title-link" title="Summer School 2018">
                                                    <div class="title">Summer School 2018</div>
                                                </a>
                                            </div>
                                            <div class="item-inner col-xs-12 col-sm-6 col-md-6 col-lg-6 layout-style1">
                                                <div class="event-featured-image-wrap">
                                                    <div class="event-thumbnail" style="background: url(https://educlever.beplusthemes.com/elementary/wp-content/uploads/2017/06/edu_event2-614x346.jpg) center center / cover, #9E9E9E;"></div>
                                                    <div class="venue-empty"><i class="fa fa-map-marker" aria-hidden="true"> </i> <span class="tribe-address"> <span class="tribe-street-address">Phillip Street</span> <br> <span class="tribe-locality">Jersey City</span><span class="tribe-delimiter">,</span> <abbr class="tribe-region tribe-events-abbr" title="New Jersey">NJ</abbr> <span class="tribe-country-name">United States</span> </span></div>
                                                </div><a href="https://educlever.beplusthemes.com/elementary/event/eduma-autumn-2017/" class="title-link" title="Eduma Autumn 2017">
                                                    <div class="title">Eduma Autumn 2017</div>
                                                </a>
                                            </div>
                                            <div class="item-inner col-xs-12 col-sm-6 col-md-6 col-lg-6 layout-style1">
                                                <div class="event-featured-image-wrap">
                                                    <div class="event-thumbnail" style="background: url(https://educlever.beplusthemes.com/elementary/wp-content/uploads/2017/06/edu_event1-614x346.jpg) center center / cover, #9E9E9E;"></div>
                                                    <div class="venue-empty"><i class="fa fa-map-marker" aria-hidden="true"> </i> <span class="tribe-address"> <span class="tribe-street-address">Sanford Street</span> <br> <span class="tribe-locality">Pretoria</span><span class="tribe-delimiter">,</span> <abbr class="tribe-region tribe-events-abbr" title="Gauteng">Gauteng</abbr> <span class="tribe-country-name">South Africa</span> </span></div>
                                                </div><a href="https://educlever.beplusthemes.com/elementary/event/build-education-website/" class="title-link" title="Build Education Website">
                                                    <div class="title">Build Education Website</div>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section data-vc-full-width="true" data-vc-full-width-init="false" class="vc_section vc_custom_1570848944239 vc_section-has-fill">
                    <div class="vc_row wpb_row vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-offset-3 vc_col-lg-6 vc_col-md-offset-2 vc_col-md-8">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="vc_icon_element vc_icon_element-outer vc_custom_1571036202902 vc_icon_element-align-center">
                                        <div class="vc_icon_element-inner vc_icon_element-color-custom vc_icon_element-size-md vc_icon_element-style- vc_icon_element-background-color-grey" style=""> <span class="vc_icon_element-icon fa fa-spinner" style="color:#559d2d !important"></span></div>
                                    </div>
                                    <h1 style="font-size: 40px;color: #333333;line-height: 44px;text-align: center" class="vc_custom_heading edu-title vc_custom_1571036184421">What We Provide?</h1>
                                    <h4 style="font-size: 18px;color: #666666;line-height: 24px;text-align: center" class="vc_custom_heading">Lorem ipsum dolor sit amet, conse ct amet, conse adipiscing</h4>
                                    <div class="vc_empty_space  edu-space" style="height: 30px"><span class="vc_empty_space_inner"></span></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vc_row wpb_row vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-4">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="wpb_theme_custom_element wpb_featured_box box-provide c1">
                                        <div class="vc-custom-inner-wrap">
                                            <div class="featured-box-alignment alignment-center content-alignment-center">
                                                <div class="icon-wrap">
                                                    <div class="type-image" style="width: 70px"><img src="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/maternity.svg" alt="#"></div>
                                                </div>
                                                <div class="entry-box-wrap">
                                                    <h4 class="featured-box-title" style="color: #333333;">Friendly Environment</h4>
                                                    <div class="featured-box-text" style="color: #666666;">Lorem ipsum dolor sit amet, conse ilesy conse adipiscing elit dolor sit.</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-4">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="wpb_theme_custom_element wpb_featured_box box-provide c2">
                                        <div class="vc-custom-inner-wrap">
                                            <div class="featured-box-alignment alignment-center content-alignment-center">
                                                <div class="icon-wrap">
                                                    <div class="type-image" style="width: 70px"><img src="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/book.svg" alt="#"></div>
                                                </div>
                                                <div class="entry-box-wrap">
                                                    <h4 class="featured-box-title" style="color: #333333;">Activity Rooms</h4>
                                                    <div class="featured-box-text" style="color: #666666;">Lorem ipsum dolor sit amet, conse ilesy conse adipiscing elit dolor sit.</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-4">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="wpb_theme_custom_element wpb_featured_box box-provide c3">
                                        <div class="vc-custom-inner-wrap">
                                            <div class="featured-box-alignment alignment-center content-alignment-center">
                                                <div class="icon-wrap">
                                                    <div class="type-image" style="width: 70px"><img src="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/audiobook.svg" alt="#"></div>
                                                </div>
                                                <div class="entry-box-wrap">
                                                    <h4 class="featured-box-title" style="color: #333333;">Art Music Group</h4>
                                                    <div class="featured-box-text" style="color: #666666;">Lorem ipsum dolor sit amet, conse ilesy conse adipiscing elit dolor sit.</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vc_row wpb_row vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-4">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="wpb_theme_custom_element wpb_featured_box box-provide c4">
                                        <div class="vc-custom-inner-wrap">
                                            <div class="featured-box-alignment alignment-center content-alignment-center">
                                                <div class="icon-wrap">
                                                    <div class="type-image" style="width: 70px"><img src="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/graduation-hat.svg" alt="#"></div>
                                                </div>
                                                <div class="entry-box-wrap">
                                                    <h4 class="featured-box-title" style="color: #333333;">Full Day Sessions</h4>
                                                    <div class="featured-box-text" style="color: #666666;">Lorem ipsum dolor sit amet, conse ilesy conse adipiscing elit dolor sit.</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-4">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="wpb_theme_custom_element wpb_featured_box box-provide c5">
                                        <div class="vc-custom-inner-wrap">
                                            <div class="featured-box-alignment alignment-center content-alignment-center">
                                                <div class="icon-wrap">
                                                    <div class="type-image" style="width: 70px"><img src="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/options.svg" alt="#"></div>
                                                </div>
                                                <div class="entry-box-wrap">
                                                    <h4 class="featured-box-title" style="color: #333333;">Play Games Areas</h4>
                                                    <div class="featured-box-text" style="color: #666666;">Lorem ipsum dolor sit amet, conse ilesy conse adipiscing elit dolor sit.</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-4">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="wpb_theme_custom_element wpb_featured_box box-provide c6">
                                        <div class="vc-custom-inner-wrap">
                                            <div class="featured-box-alignment alignment-center content-alignment-center">
                                                <div class="icon-wrap">
                                                    <div class="type-image" style="width: 70px"><img src="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/creativity.svg" alt="#"></div>
                                                </div>
                                                <div class="entry-box-wrap">
                                                    <h4 class="featured-box-title" style="color: #333333;">Experts Staff</h4>
                                                    <div class="featured-box-text" style="color: #666666;">Lorem ipsum dolor sit amet, conse ilesy conse adipiscing elit dolor sit.</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="vc_row wpb_row vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class="vc_empty_space" style="height: 20px"><span class="vc_empty_space_inner"></span></div>
                                        <div class="vc_btn3-container  animated-dashes-edu edu2 vc_btn3-center vc_custom_1570848931278" data-animate="fadeInLeft" data-duration="1.6" data-delay="0.2"> <a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-round vc_btn3-style-custom vc_btn3-color-grey" href="#!" title="">VIEW LOCATION</a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div class="vc_row-full-width vc_clearfix"></div>
                <section class="vc_section vc_custom_1571036132907">
                    <div class="vc_row wpb_row vc_row-fluid">
                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                            <div class="vc_column-inner vc_custom_1571039860669">
                                <div class="wpb_wrapper">
                                    <h1 style="font-size: 40px;color: #333333;line-height: 44px;text-align: left" class="vc_custom_heading edu-title vc_custom_1571036119126">Our Activities</h1>
                                    <h4 style="font-size: 18px;color: #666666;line-height: 24px;text-align: left" class="vc_custom_heading">Children develop skills in five main areas of development:</h4>
                                    <div class="vc_empty_space" style="height: 32px"><span class="vc_empty_space_inner"></span></div>
                                    <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1570852780910">
                                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-has-fill">
                                            <div class="vc_column-inner vc_custom_1570853767504">
                                                <div class="wpb_wrapper">
                                                    <div class="wpb_theme_custom_element wpb_liquid_button text-center">
                                                        <div class="vc-custom-inner-wrap"> <a class="liquid-button-link item" href="#"> <svg class="liquid-button-svg" data-width="100" data-height="100" data-color1="#559d2d" data-color2="#ffffff" data-color3="#002859" data-liquid-button=""></svg>
                                                                <div class="liquid-button-text" style="color: #559d2d">
                                                                    <p>►</p>
                                                                </div>
                                                            </a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="vc_empty_space" style="height: 15px"><span class="vc_empty_space_inner"></span></div>
                                </div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6">
                            <div class="vc_column-inner vc_custom_1571039876369">
                                <div class="wpb_wrapper">
                                    <div class="vc_toggle vc_toggle_round vc_toggle_color_green  vc_toggle_size_md vc_toggle_active  bt-faq c1">
                                        <div class="vc_toggle_title">
                                            <h4>Table/Floor Toys</h4><i class="vc_toggle_icon"></i>
                                        </div>
                                        <div class="vc_toggle_content">
                                            <p>Voluptas sit aspernatur aut odit aut fugit, sed quias consequuntur magni dolores eos qui ratione volust luptatem sequi nesciunt voluptas sit aspernatur aut consequuntur magni &#8230;</p>
                                        </div>
                                    </div>
                                    <div class="vc_toggle vc_toggle_round vc_toggle_color_green  vc_toggle_size_md   bt-faq c2">
                                        <div class="vc_toggle_title">
                                            <h4>Sand Play</h4><i class="vc_toggle_icon"></i>
                                        </div>
                                        <div class="vc_toggle_content">
                                            <p>Voluptas sit aspernatur aut odit aut fugit, sed quias consequuntur magni dolores eos qui ratione volust luptatem sequi nesciunt voluptas sit aspernatur aut consequuntur magni dolores eos qui ratione volust luptatem sequi nesciunt.</p>
                                        </div>
                                    </div>
                                    <div class="vc_toggle vc_toggle_round vc_toggle_color_green  vc_toggle_size_md   bt-faq c3">
                                        <div class="vc_toggle_title">
                                            <h4>Building Blocks</h4><i class="vc_toggle_icon"></i>
                                        </div>
                                        <div class="vc_toggle_content">
                                            <p>Voluptas sit aspernatur aut odit aut fugit, sed quias consequuntur magni dolores eos qui ratione volust luptatem sequi nesciunt voluptas sit aspernatur aut consequuntur magni dolores eos qui ratione volust luptatem sequi nesciunt.</p>
                                        </div>
                                    </div>
                                    <div class="vc_toggle vc_toggle_round vc_toggle_color_green  vc_toggle_size_md   bt-faq c4">
                                        <div class="vc_toggle_title">
                                            <h4>Outdoor Games</h4><i class="vc_toggle_icon"></i>
                                        </div>
                                        <div class="vc_toggle_content">
                                            <p>Voluptas sit aspernatur aut odit aut fugit, sed quias consequuntur magni dolores eos qui ratione volust luptatem sequi nesciunt voluptas sit aspernatur aut consequuntur magni dolores eos qui ratione volust luptatem sequi nesciunt.</p>
                                            <p>Outdoor Games</p>
                                        </div>
                                    </div>
                                    <div class="vc_empty_space" style="height: 25px"><span class="vc_empty_space_inner"></span></div>
                                    <div class="vc_btn3-container  animated-dashes-edu edu2 vc_btn3-left vc_custom_1570850984616" data-animate="fadeInLeft" data-duration="1.6" data-delay="0.2"> <a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-round vc_btn3-style-custom vc_btn3-color-grey" href="#!" title="">CONTACT US</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-parallax="1.5" data-vc-parallax-image="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/bg-tes.jpg" class="vc_row wpb_row vc_row-fluid vc_custom_1571019447485 vc_row-has-fill vc_general vc_parallax vc_parallax-content-moving">
                    <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-offset-1 vc_col-lg-10">
                        <div class="vc_column-inner vc_custom_1571036070400">
                            <div class="wpb_wrapper">
                                <div class="wpb_single_image wpb_content_element vc_align_center  vc_custom_1570854746021  img-svg">
                                    <figure class="wpb_wrapper vc_figure">
                                        <div class="vc_single_image-wrapper   vc_box_border_grey"><img src="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/left-quote.svg" class="vc_single_image-img attachment-full" alt="" loading="lazy" /></div>
                                    </figure>
                                </div>
                                <div class="wpb_theme_custom_element wpb_base_review bt-tes bt-outer-visible">
                                    <div class="vc-custom-inner-wrap">
                                        <div class="owl-carousel review default" data-bears-owl-carousel='eyJpdGVtcyI6MSwibWFyZ2luIjowLCJsb29wIjowLCJjZW50ZXIiOjAsInN0YWdlUGFkZGluZyI6MCwic3RhcnRQb3NpdGlvbiI6MCwibmF2IjowLCJkb3RzIjoxLCJzbGlkZUJ5IjoxLCJhdXRvcGxheSI6MCwiYXV0b3BsYXlIb3ZlclBhdXNlIjowLCJhdXRvcGxheVRpbWVvdXQiOjUwMDAsInNtYXJ0U3BlZWQiOjI1MCwicmVzcG9uc2l2ZSI6eyIwIjp7Iml0ZW1zIjoxLCJzdGFnZVBhZGRpbmciOjB9LCI0ODAiOnsiaXRlbXMiOjEsInN0YWdlUGFkZGluZyI6MH0sIjc2OCI6eyJpdGVtcyI6MSwic3RhZ2VQYWRkaW5nIjowfSwiMTAwMCI6eyJpdGVtcyI6MSwic3RhZ2VQYWRkaW5nIjowfX19'>
                                            <div class="item layout-default">
                                                <article class="educlever-review">
                                                    <div class="bt-content">
                                                        <div class="bt-excerpt">The others comfortable these days are all happy and free listen to a story now the world do not move to the beat of just one drum what might be right for you may not be story of a man</div>
                                                    </div>
                                                    <div class="bt-info-review">
                                                        <div class="bt-thumb"><img src="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2018/09/logo-review.jpg" alt="Dolphin Jannies" /></div>
                                                        <div class="bt-name-position">
                                                            <h3 class="bt-title">Dolphin Jannies</h3>
                                                            <div class="bt-position">Graphic Designer</div>
                                                        </div>
                                                    </div>
                                                </article>
                                            </div>
                                            <div class="item layout-default">
                                                <article class="educlever-review">
                                                    <div class="bt-content">
                                                        <div class="bt-excerpt">The others comfortable these days are all happy and free listen to a story now the world do not move to the beat of just one drum what might be right for you may not be story of a man</div>
                                                    </div>
                                                    <div class="bt-info-review">
                                                        <div class="bt-thumb"><img src="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2018/09/logo-review.jpg" alt="Dolphin Jannies" /></div>
                                                        <div class="bt-name-position">
                                                            <h3 class="bt-title">Dolphin Jannies</h3>
                                                            <div class="bt-position">Graphic Designer</div>
                                                        </div>
                                                    </div>
                                                </article>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="vc_row-full-width vc_clearfix"></div>
                <div class="vc_row wpb_row vc_row-fluid vc_custom_1571036734601">
                    <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-offset-3 vc_col-lg-6 vc_col-md-offset-2 vc_col-md-8">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div class="vc_icon_element vc_icon_element-outer vc_custom_1571033769049 vc_icon_element-align-center">
                                    <div class="vc_icon_element-inner vc_icon_element-color-custom vc_icon_element-size-md vc_icon_element-style- vc_icon_element-background-color-grey" style=""> <span class="vc_icon_element-icon fa fa-spinner" style="color:#559d2d !important"></span></div>
                                </div>
                                <h1 style="font-size: 40px;color: #333333;line-height: 44px;text-align: center" class="vc_custom_heading edu-title vc_custom_1571033796248">Recent Articles</h1>
                                <h4 style="font-size: 18px;color: #666666;line-height: 24px;text-align: center" class="vc_custom_heading">Lorem ipsum dolor sit amet, conse ct amet, conse adipiscing</h4>
                            </div>
                        </div>
                    </div>
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                        <div class="vc_column-inner vc_custom_1571027281014">
                            <div class="wpb_wrapper">
                                <div class="wpb_theme_custom_element wpb_posts_slider_2 vc_custom_1551495466118">
                                    <div class="vc-custom-inner-wrap">
                                        <div class="owl-carousel" data-bears-owl-carousel='eyJpdGVtcyI6MywibWFyZ2luIjowLCJsb29wIjoxLCJjZW50ZXIiOjAsInN0YWdlUGFkZGluZyI6MCwic3RhcnRQb3NpdGlvbiI6MCwibmF2IjowLCJkb3RzIjowLCJzbGlkZUJ5IjoxLCJhdXRvcGxheSI6MCwiYXV0b3BsYXlIb3ZlclBhdXNlIjowLCJhdXRvcGxheVRpbWVvdXQiOjUwMDAsInNtYXJ0U3BlZWQiOjI1MCwicmVzcG9uc2l2ZSI6eyIwIjp7Iml0ZW1zIjoxLCJzdGFnZVBhZGRpbmciOjB9LCI0ODAiOnsiaXRlbXMiOjEsInN0YWdlUGFkZGluZyI6MH0sIjc2OCI6eyJpdGVtcyI6Miwic3RhZ2VQYWRkaW5nIjowfSwiMTAwMCI6eyJpdGVtcyI6Mywic3RhZ2VQYWRkaW5nIjowfX19'>
                                            <div class="item post_recent">
                                                <div class="item-inner posts_slider_2_template_default">
                                                    <div class="post-thumbnail"><img width="614" height="346" src="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2018/09/edu-post1-1-614x346.jpg" class="attachment-educlever-image-medium size-educlever-image-medium" alt="" loading="lazy" srcset="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2018/09/edu-post1-1-614x346.jpg 614w, https://educlever.beplusthemes.com/elementary/wp-content/uploads/2018/09/edu-post1-1-295x166.jpg 295w" sizes="(max-width: 614px) 100vw, 614px" /></div>
                                                    <div class="post-caption">
                                                        <div class="post-date">18 Sep, 2018</div><a class="post-title-link" href="https://educlever.beplusthemes.com/elementary/seo-for-your-wp-website/">
                                                            <h2 class="post-title" title="SEO for your WP website">SEO for your WP website</h2>
                                                        </a>
                                                        <div class="post-excerpt">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book...</div>
                                                        <div class="post-author"><span class="edu-meta-top">Posted By: </span><span class="edu-meta-bot">Keny White</span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item post_recent">
                                                <div class="item-inner posts_slider_2_template_default">
                                                    <div class="post-thumbnail"><img width="614" height="346" src="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2018/09/edu-post2-1-614x346.jpg" class="attachment-educlever-image-medium size-educlever-image-medium" alt="" loading="lazy" srcset="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2018/09/edu-post2-1-614x346.jpg 614w, https://educlever.beplusthemes.com/elementary/wp-content/uploads/2018/09/edu-post2-1-295x166.jpg 295w" sizes="(max-width: 614px) 100vw, 614px" /></div>
                                                    <div class="post-caption">
                                                        <div class="post-date">18 Sep, 2018</div><a class="post-title-link" href="https://educlever.beplusthemes.com/elementary/expenses-you-arent-thinking/">
                                                            <h2 class="post-title" title="Expenses You Arent Thinking">Expenses You Arent Thinking</h2>
                                                        </a>
                                                        <div class="post-excerpt">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book...</div>
                                                        <div class="post-author"><span class="edu-meta-top">Posted By: </span><span class="edu-meta-bot">Keny White</span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="item post_recent">
                                                <div class="item-inner posts_slider_2_template_default">
                                                    <div class="post-thumbnail"><img width="614" height="346" src="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2018/09/edu-post3-1-614x346.jpg" class="attachment-educlever-image-medium size-educlever-image-medium" alt="" loading="lazy" srcset="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2018/09/edu-post3-1-614x346.jpg 614w, https://educlever.beplusthemes.com/elementary/wp-content/uploads/2018/09/edu-post3-1-295x166.jpg 295w" sizes="(max-width: 614px) 100vw, 614px" /></div>
                                                    <div class="post-caption">
                                                        <div class="post-date">18 Sep, 2018</div><a class="post-title-link" href="https://educlever.beplusthemes.com/elementary/design-tool-you-want-to-learn/">
                                                            <h2 class="post-title" title="Design Tool You Want To Learn">Design Tool You Want To Learn</h2>
                                                        </a>
                                                        <div class="post-excerpt">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book...</div>
                                                        <div class="post-author"><span class="edu-meta-top">Posted By: </span><span class="edu-meta-bot">Keny White</span></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="vc_empty_space" style="height: 12px"><span class="vc_empty_space_inner"></span></div>
                                <div class="vc_btn3-container  animated-dashes-edu edu2 vc_btn3-center vc_custom_1571018890427" data-animate="fadeInLeft" data-duration="1.6" data-delay="0.2"> <a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-round vc_btn3-style-custom vc_btn3-color-grey" href="#!" title="">View All Posts</a></div>
                            </div>
                        </div>
                    </div>
                </div>
                <section data-vc-full-width="true" data-vc-full-width-init="false" class="vc_section vc_custom_1571106895956">
                    <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true" class="vc_row wpb_row vc_row-fluid vc_row-no-padding">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class="wpb_theme_custom_element wpb_posts_grid_resizable">
                                        <div class="vc-custom-inner-wrap">
                                            <div class="masonry-hybrid-wrap" data-bears-masonryhybrid="{&quot;col&quot;:6,&quot;space&quot;:0,&quot;responsive&quot;:{&quot;860&quot;:{&quot;col&quot;:&quot;3&quot;},&quot;577&quot;:{&quot;col&quot;:&quot;2&quot;}}}" data-bears-masonryhybrid-resize="{&quot;celHeight&quot;:220,&quot;grid_name&quot;:&quot;posts_grid_resizable_1571019605225-6c395902-ce18&quot;,&quot;__sizeMap&quot;:&quot;&quot;,&quot;resize&quot;:false}" data-bears-lightgallery="{&quot;selector&quot;:&quot;.zoom-item&quot;,&quot;thumbnail&quot;:true}">
                                                <div class="grid-sizer"></div>
                                                <div class="gutter-sizer"></div>
                                                <div class="grid-item item-skin-image_gallery-default">
                                                    <div class="grid-item-inner">
                                                        <div class="image-item" style="background: url(https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/box1-768x768.jpg) center center / cover, #333"><a href="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/box1.jpg" class="zoom-item" title=""><span class="ion-ios-plus-empty"></span><img src="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/box1-768x768.jpg" /></a></div>
                                                    </div>
                                                </div>
                                                <div class="grid-item item-skin-image_gallery-default">
                                                    <div class="grid-item-inner">
                                                        <div class="image-item" style="background: url(https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/box3-768x768.jpg) center center / cover, #333"><a href="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/box3.jpg" class="zoom-item" title=""><span class="ion-ios-plus-empty"></span><img src="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/box3-768x768.jpg" /></a></div>
                                                    </div>
                                                </div>
                                                <div class="grid-item item-skin-image_gallery-default">
                                                    <div class="grid-item-inner">
                                                        <div class="image-item" style="background: url(https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/box2-768x768.jpg) center center / cover, #333"><a href="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/box2.jpg" class="zoom-item" title=""><span class="ion-ios-plus-empty"></span><img src="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2019/10/box2-768x768.jpg" /></a></div>
                                                    </div>
                                                </div>
                                                <div class="grid-item item-skin-image_gallery-default">
                                                    <div class="grid-item-inner">
                                                        <div class="image-item" style="background: url(https://educlever.beplusthemes.com/elementary/wp-content/uploads/2018/10/course_supersize-768x512.jpg) center center / cover, #333"><a href="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2018/10/course_supersize-1024x683.jpg" class="zoom-item" title=""><span class="ion-ios-plus-empty"></span><img src="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2018/10/course_supersize-768x512.jpg" /></a></div>
                                                    </div>
                                                </div>
                                                <div class="grid-item item-skin-image_gallery-default">
                                                    <div class="grid-item-inner">
                                                        <div class="image-item" style="background: url(https://educlever.beplusthemes.com/elementary/wp-content/uploads/2018/10/course_huge4-768x512.jpg) center center / cover, #333"><a href="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2018/10/course_huge4-1024x683.jpg" class="zoom-item" title=""><span class="ion-ios-plus-empty"></span><img src="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2018/10/course_huge4-768x512.jpg" /></a></div>
                                                    </div>
                                                </div>
                                                <div class="grid-item item-skin-image_gallery-default">
                                                    <div class="grid-item-inner">
                                                        <div class="image-item" style="background: url(https://educlever.beplusthemes.com/elementary/wp-content/uploads/2017/06/edu_event6-768x512.jpg) center center / cover, #333"><a href="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2017/06/edu_event6-1024x683.jpg" class="zoom-item" title=""><span class="ion-ios-plus-empty"></span><img src="https://educlever.beplusthemes.com/elementary/wp-content/uploads/2017/06/edu_event6-768x512.jpg" /></a></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vc_row-full-width vc_clearfix"></div>
                </section>
                <div class="vc_row-full-width vc_clearfix"></div>
            </div>
        </div><!-- /.site-main -->
        <!-- Footer -->
        <footer id="colophon" class="site-footer bt-footer ">
            <div class="bt-footer-custom-layout footer-elementary post-2070 footer-builder type-footer-builder status-publish hentry">
                <div class="bt-inner">
                    <section data-vc-full-width="true" data-vc-full-width-init="false" class="vc_section section-visible vc_custom_1570504247525 vc_section-has-fill">
                        <div class="container">
                            <div class="vc_row wpb_row vc_row-fluid footer-sidebar-item vc_column-gap-30">
                                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-4 vc_col-md-4">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <h2 style="font-size: 22px;color: #559d2d;line-height: 38px;text-align: left" class="vc_custom_heading vc_custom_1570434838537">INFORAMTION</h2>
                                            <div class="vc_empty_space" style="height: 5px"><span class="vc_empty_space_inner"></span></div>
                                            <div class="wpb_text_column wpb_content_element ">
                                                <div class="wpb_wrapper">
                                                    <p>Lorem ipsum dolor sit amet consetuer ing elit diam uismod tincidunt Lorem ipsum li dolor t diam uismod tincidunt tincidunt yu dolor t diam uismod.</p>
                                                </div>
                                            </div>
                                            <div class="vc_icon_element vc_icon_element-outer bt-icon vc_icon_element-align-left vc_icon_element-have-style">
                                                <div class="vc_icon_element-inner vc_icon_element-color-custom vc_icon_element-have-style-inner vc_icon_element-size-sm vc_icon_element-style-rounded vc_icon_element-background vc_icon_element-background-color-custom" style="background-color:#5472d2"> <span class="vc_icon_element-icon fa fa-facebook" style="color:#ffffff !important"></span></div>
                                            </div>
                                            <div class="vc_icon_element vc_icon_element-outer bt-icon vc_icon_element-align-left vc_icon_element-have-style">
                                                <div class="vc_icon_element-inner vc_icon_element-color-custom vc_icon_element-have-style-inner vc_icon_element-size-sm vc_icon_element-style-rounded vc_icon_element-background vc_icon_element-background-color-custom" style="background-color:#dd4b39"> <span class="vc_icon_element-icon fa fa-google-plus" style="color:#ffffff !important"></span></div>
                                            </div>
                                            <div class="vc_icon_element vc_icon_element-outer bt-icon vc_icon_element-align-left vc_icon_element-have-style">
                                                <div class="vc_icon_element-inner vc_icon_element-color-custom vc_icon_element-have-style-inner vc_icon_element-size-sm vc_icon_element-style-rounded vc_icon_element-background vc_icon_element-background-color-custom" style="background-color:#55acee"> <span class="vc_icon_element-icon fa fa-twitter" style="color:#ffffff !important"></span></div>
                                            </div>
                                            <div class="vc_icon_element vc_icon_element-outer bt-icon vc_icon_element-align-left vc_icon_element-have-style">
                                                <div class="vc_icon_element-inner vc_icon_element-color-custom vc_icon_element-have-style-inner vc_icon_element-size-sm vc_icon_element-style-rounded vc_icon_element-background vc_icon_element-background-color-custom" style="background-color:#2e6caa"> <span class="vc_icon_element-icon fa fa-linkedin" style="color:#ffffff !important"></span></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-4 vc_col-md-4">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <h2 style="font-size: 22px;color: #559d2d;line-height: 38px;text-align: left" class="vc_custom_heading vc_custom_1570434874623">QUCIK LINKS</h2>
                                            <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                                <div class="wpb_column vc_column_container vc_col-sm-6">
                                                    <div class="vc_column-inner">
                                                        <div class="wpb_wrapper">
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <ul class="bt-link">
                                                                        <li><a href="#!"><i class="fa fa-spinner" aria-hidden="true"></i> Extended care</a></li>
                                                                        <li><a href="#!"><i class="fa fa-spinner" aria-hidden="true"></i> Handbook</a></li>
                                                                        <li><a href="#!"><i class="fa fa-spinner" aria-hidden="true"></i> About us</a></li>
                                                                        <li><a href="#!"><i class="fa fa-spinner" aria-hidden="true"></i> Instagram</a></li>
                                                                        <li><a href="#!"><i class="fa fa-spinner" aria-hidden="true"></i> Brochure</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="wpb_column vc_column_container vc_col-sm-6">
                                                    <div class="vc_column-inner">
                                                        <div class="wpb_wrapper">
                                                            <div class="wpb_text_column wpb_content_element ">
                                                                <div class="wpb_wrapper">
                                                                    <ul class="bt-link">
                                                                        <li><a href="#!"><i class="fa fa-spinner" aria-hidden="true"></i> Classroom</a></li>
                                                                        <li><a href="#!"><i class="fa fa-spinner" aria-hidden="true"></i> Events</a></li>
                                                                        <li><a href="#!"><i class="fa fa-spinner" aria-hidden="true"></i> News</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-4 vc_col-md-4">
                                    <div class="vc_column-inner ">
                                        <div class="wpb_wrapper">
                                            <div class="wpb_theme_custom_element wpb_featured_box box-footer-elementary">
                                                <div class="vc-custom-inner-wrap">
                                                    <div class="featured-box-alignment alignment-left vertical-alignment-middle">
                                                        <div class="icon-wrap">
                                                            <div class="type-icon graphic-shape-circle" style="width: 65px;height: 65px;background-color: #ffffff"><span style="color: #559d2d" class="_icon fa fa-graduation-cap"></span></div>
                                                        </div>
                                                        <div class="entry-box-wrap">
                                                            <h4 class="featured-box-title" style="color: #2c2c2c;">PROGRAMS</h4>
                                                            <div class="featured-box-text" style="color: #666666;">Develop fine and gross skills</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wpb_theme_custom_element wpb_featured_box box-footer-elementary">
                                                <div class="vc-custom-inner-wrap">
                                                    <div class="featured-box-alignment alignment-left vertical-alignment-middle">
                                                        <div class="icon-wrap">
                                                            <div class="type-icon graphic-shape-circle" style="width: 65px;height: 65px;background-color: #ffffff"><span style="color: #559d2d" class="_icon fa fa-bell"></span></div>
                                                        </div>
                                                        <div class="entry-box-wrap">
                                                            <h4 class="featured-box-title" style="color: #2c2c2c;">ONLINE ED</h4>
                                                            <div class="featured-box-text" style="color: #666666;">Processes and maintenance</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="wpb_theme_custom_element wpb_featured_box box-footer-elementary">
                                                <div class="vc-custom-inner-wrap">
                                                    <div class="featured-box-alignment alignment-left vertical-alignment-middle">
                                                        <div class="icon-wrap">
                                                            <div class="type-icon graphic-shape-circle" style="width: 65px;height: 65px;background-color: #ffffff"><span style="color: #559d2d" class="_icon fa fa-trophy"></span></div>
                                                        </div>
                                                        <div class="entry-box-wrap">
                                                            <h4 class="featured-box-title" style="color: #2c2c2c;">AWARDS</h4>
                                                            <div class="featured-box-text" style="color: #666666;">Develop fine and gross skills</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="container">
                            <div class="vc_row wpb_row vc_row-fluid">
                                <div class="call-footer wpb_column vc_column_container vc_col-sm-12 vc_hidden-sm vc_hidden-xs vc_col-has-fill">
                                    <div class="vc_column-inner vc_custom_1571043193316">
                                        <div class="wpb_wrapper">
                                            <h3 style="font-size: 16px;color: #ffffff;line-height: 22px;text-align: center" class="vc_custom_heading vc_custom_1570503794602">Join Our Journey of Discovery</h3>
                                            <h1 style="font-size: 22px;color: #ffd200;line-height: 30px;text-align: center" class="vc_custom_heading vc_custom_1570504234953">Enroll Your Child in a Class?</h1>
                                            <h2 style="font-size: 16px;color: #ffffff;line-height: 22px;text-align: center" class="vc_custom_heading vc_custom_1570503802993">Call Us 1 800 456 78 90</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <div class="vc_row-full-width vc_clearfix"></div>
                </div>
            </div>
            <div class="bt-footer-bar bt-copyright-center">
                <div class="container">
                    <div class="bt-copyright">© All right reserved 2022 <a href="#">Educlever</a> —&nbsp; Trusted by <a href="https://wimgo.com/" target="_blank" rel="noopener">Wimgo</a></div>
                </div>
            </div>
        </footer>
    </div><!-- /#page -->
    <style>
        /* LRM */
        body.logged-in .lrm-hide-if-logged-in {
            display: none !important;
        }

        body.logged-in [class*='lrm-hide-if-logged-in'] {
            display: none !important;
        }

        body:not(.logged-in) .lrm-show-if-logged-in {
            display: none !important;
        }

        body:not(.logged-in) [class*='lrm-show-if-logged-in'] {
            display: none !important;
        }
    </style>
    <!--
-->
    <div class="lrm-main lrm-font-svg lrm-user-modal lrm-btn-style--default" style="visibility: hidden;">
        <!--<div class="lrm-user-modal" style="visibility: hidden;"> this is the entire modal form, including the background -->
        <div class="lrm-user-modal-container">
            <!-- this is the container wrapper -->
            <div class="lrm-user-modal-container-inner">
                <!-- this is the container wrapper -->
                <ul class="lrm-switcher -is-not-login-only">
                    <li><a href="#0" class="lrm-switch-to-link lrm-switch-to--login lrm-ficon-login "> Sign in </a></li>
                    <li><a href="#0" class="lrm-switch-to-link lrm-switch-to--register lrm-ficon-register "> New account </a></li>
                </ul>
                <div class="lrm-signin-section ">
                    <!-- log in form -->
                    <form class="lrm-form js-lrm-form" action="#0" data-action="login">
                        <div class="lrm-fieldset-wrap">
                            <div class="lrm-integrations lrm-integrations--login"></div>
                            <p class="lrm-form-message lrm-form-message--init"></p>
                            <div class="fieldset"> <label class="image-replace lrm-email lrm-ficon-mail" title="Email or Username"></label> <input name="username" class="full-width has-padding has-border" type="text" aria-label="Email or Username" placeholder="Email or Username" required value="" autocomplete="username" data-autofocus="1"> <span class="lrm-error-message"></span></div>
                            <div class="fieldset"> <label class="image-replace lrm-password lrm-ficon-key" title="Password"></label> <input name="password" class="full-width has-padding has-border" type="password" aria-label="Password" placeholder="Password" required value=""> <span class="lrm-error-message"></span> <span class="hide-password lrm-ficon-eye" data-show="Show" data-hide="Hide" aria-label="Show"></span></div>
                            <div class="fieldset"> <label class="lrm-nice-checkbox__label lrm-remember-me-checkbox">Remember me <input type="checkbox" class="lrm-nice-checkbox lrm-remember-me" name="remember-me" checked>
                                    <div class="lrm-nice-checkbox__indicator"></div>
                                </label></div>
                            <div class="lrm-integrations lrm-integrations--login lrm-integrations-before-btn"></div>
                            <div class="lrm-integrations-otp"></div>
                        </div>
                        <div class="fieldset fieldset--submit fieldset--default"> <button class="full-width has-padding" type="submit"> Log in </button></div>
                        <div class="lrm-fieldset-wrap">
                            <div class="lrm-integrations lrm-integrations--login">
                                <div class="lrh-placeholder-social-login">
                                    <div class="__or"> <span class="line"></span> <span class="text">or</span> <span class="line"></span></div>
                                    <ul>
                                        <li> <a class="login-social-btn login-with-facebook" href="#!"> <span class="icon"> <i class="fa fa-facebook" aria-hidden="true"></i> </span> <span class="text"> Login with Facebook </span> </a></li>
                                        <li> <a class="login-social-btn login-with-google" href="#!"> <span class="icon"> <i class="fa fa-google-plus" aria-hidden="true"></i> </span> <span class="text"> Login with Google </span> </a></li>
                                    </ul>
                                </div>
                            </div>
                        </div> <input type="hidden" name="redirect_to" value=""> <input type="hidden" name="lrm_action" value="login"> <input type="hidden" name="wp-submit" value="1"> <!-- Fix for Eduma WP theme--> <input type="hidden" name="lp-ajax" value="login"> <input type="hidden" id="security-login" name="security-login" value="c0d0ea3776" /><input type="hidden" name="_wp_http_referer" value="/elementary/" /> <!-- For Invisible Recaptcha plugin --> <span class="wpcf7-submit" style="display: none;"></span>
                    </form>
                    <p class="lrm-form-bottom-message"><a href="#0" class="lrm-switch-to--reset-password">Forgot your password?</a></p> <!-- <a href="#0" class="lrm-close-form">Close</a> -->
                </div> <!-- lrm-login -->
                <div class="lrm-signup-section ">
                    <!-- sign up form -->
                    <form class="lrm-form js-lrm-form" action="#0" data-action="registration" data-lpignore="true">
                        <div class="lrm-fieldset-wrap lrm-form-message-wrap">
                            <p class="lrm-form-message lrm-form-message--init"></p>
                        </div>
                        <div class="lrm-fieldset-wrap">
                            <div class="lrm-integrations lrm-integrations--register"></div>
                            <div class="fieldset fieldset--username"> <label class="image-replace lrm-username lrm-ficon-user" for="signup-username" title="Username*"></label> <input name="username" class="full-width has-padding has-border" id="signup-username" type="text" placeholder="Username*" required aria-label="Username*" autocomplete="off" data-lpignore="true"> <span class="lrm-error-message"></span></div>
                            <div class="fieldset fieldset--login"> <label class="image-replace lrm-username lrm-ficon-user" for="signup-login" title="Login"></label> <input name="login" class="full-width has-padding has-border" id="signup-login" type="text" autocomplete="off" data-lpignore="true"></div>
                            <div class="clearfix lrm-row">
                                <div class="lrm-col-half-width lrm-col-first fieldset--first-name lrm-col"> <label class="image-replace lrm-username lrm-ficon-user" for="signup-first-name" title="First name*"></label> <input name="first-name" class="full-width has-padding has-border" id="signup-first-name" type="text" placeholder="First name*" required aria-label="First name*" autocomplete="off" data-lpignore="true"> <span class="lrm-error-message"></span></div>
                                <div class="lrm-col-half-width lrm-col-last fieldset--last-name lrm-col"> <label class="image-replace lrm-username lrm-ficon-user" for="signup-last-name" title="Last name"></label> <input name="last-name" class="full-width has-padding has-border" id="signup-last-name" type="text" placeholder="Last name" aria-label="Last name" autocomplete="off" data-lpignore="true"> <span class="lrm-error-message"></span></div>
                            </div>
                            <div class="fieldset fieldset--email"> <label class="image-replace lrm-email lrm-ficon-mail" for="signup-email" title="Email*"></label> <input name="email" class="full-width has-padding has-border" id="signup-email" type="email" placeholder="Email*" required autocomplete="off" aria-label="Email*"> <span class="lrm-error-message"></span></div>
                            <div class="lrm-integrations lrm-integrations--register"></div>
                            <div class="fieldset fieldset--terms"> <label class="lrm-nice-checkbox__label lrm-accept-terms-checkbox">I agree with the <a>Terms</a>. <i>Edit this in Settings =&gt; Ajax Login Modal =&gt; Expressions tab =&gt; Registration section</i> <input type="checkbox" class="lrm-nice-checkbox lrm-accept-terms" name="registration_terms" value="yes"> <span class="lrm-error-message"></span>
                                    <div class="lrm-nice-checkbox__indicator"></div>
                                </label></div>
                            <div class="lrm-integrations lrm-integrations--register lrm-info lrm-info--register"></div>
                        </div>
                        <div class="fieldset fieldset--submit fieldset--default"> <button class="full-width has-padding" type="submit"> Create account </button></div>
                        <div class="lrm-fieldset-wrap">
                            <div class="lrm-integrations lrm-integrations--register"></div>
                        </div> <input type="hidden" name="redirect_to" value=""> <input type="hidden" name="lrm_action" value="signup"> <input type="hidden" name="wp-submit" value="1"> <!-- Fix for Eduma WP theme--> <input type="hidden" name="is_popup_register" value="1"> <input type="hidden" id="security-signup" name="security-signup" value="1269c480f9" /><input type="hidden" name="_wp_http_referer" value="/elementary/" /> <!-- For Invisible Recaptcha plugin --> <span class="wpcf7-submit" style="display: none;"></span>
                    </form> <!-- <a href="#0" class="lrm-close-form">Close</a> -->
                </div> <!-- lrm-signup -->
                <div class="lrm-reset-password-section ">
                    <!-- reset password form -->
                    <form class="lrm-form js-lrm-form" action="#0" data-action="lost-password">
                        <div class="lrm-fieldset-wrap">
                            <p class="lrm-form-message">Lost your password? Please enter your email address. You will receive mail with link to set new password.</p>
                            <div class="fieldset"> <label class="image-replace lrm-email lrm-ficon-mail" title="Email or Username"></label> <input class="full-width has-padding has-border" name="user_login" type="text" required placeholder="Email or Username" data-autofocus="1" aria-label="Email or Username"> <span class="lrm-error-message"></span></div>
                            <div class="lrm-integrations lrm-integrations--reset-pass"></div> <input type="hidden" name="lrm_action" value="lostpassword"> <input type="hidden" name="wp-submit" value="1"> <input type="hidden" id="security-lostpassword" name="security-lostpassword" value="6f336716d7" /><input type="hidden" name="_wp_http_referer" value="/elementary/" />
                        </div>
                        <div class="fieldset fieldset--submit fieldset--default"> <button class="full-width has-padding" type="submit"> Reset password </button></div> <!-- For Invisible Recaptcha plugin --> <span class="wpcf7-submit" style="display: none;"></span>
                    </form>
                    <p class="lrm-form-bottom-message"><a href="#0" class="lrm-switch-to--login">Back to login</a></p>
                </div> <!-- lrm-reset-password -->
            </div> <!-- lrm-user-modal-container --> <a href="#0" class="lrm-close-form" title="close"> <span class="lrm-ficon-close"></span> </a>
        </div> <!-- lrm-user-modal-container -->
    </div> <!-- lrm-user-modal -->
    <script type="text/javascript">
        var ajaxRevslider;
        jQuery(document).ready(function() {
            // CUSTOM AJAX CONTENT LOADING FUNCTION
            ajaxRevslider = function(obj) {
                // obj.type : Post Type
                // obj.id : ID of Content to Load
                // obj.aspectratio : The Aspect Ratio of the Container / Media
                // obj.selector : The Container Selector where the Content of Ajax will be injected. It is done via the Essential Grid on Return of Content 
                var content = '';
                var data = {
                    action: 'revslider_ajax_call_front',
                    client_action: 'get_slider_html',
                    token: '6659057c4b',
                    type: obj.type,
                    id: obj.id,
                    aspectratio: obj.aspectratio
                };
                // SYNC AJAX REQUEST
                jQuery.ajax({
                    type: 'post',
                    url: 'https://educlever.beplusthemes.com/elementary/wp-admin/admin-ajax.php',
                    dataType: 'json',
                    data: data,
                    async: false,
                    success: function(ret, textStatus, XMLHttpRequest) {
                        if (ret.success == true)
                            content = ret.data;
                    },
                    error: function(e) {
                        console.log(e);
                    }
                });
                // FIRST RETURN THE CONTENT WHEN IT IS LOADED !!
                return content;
            };
            // CUSTOM AJAX FUNCTION TO REMOVE THE SLIDER
            var ajaxRemoveRevslider = function(obj) {
                return jQuery(obj.selector + ' .rev_slider').revkill();
            }; // EXTEND THE AJAX CONTENT LOADING TYPES WITH TYPE AND FUNCTION 
            if (jQuery.fn.tpessential !== undefined)
                if (typeof(jQuery.fn.tpessential.defaults) !== 'undefined')
                    jQuery.fn.tpessential.defaults.ajaxTypes.push({
                        type: 'revslider',
                        func: ajaxRevslider,
                        killfunc: ajaxRemoveRevslider,
                        openAnimationSpeed: 0.3
                    });
            // type:  Name of the Post to load via Ajax into the Essential Grid Ajax Container
            // func: the Function Name which is Called once the Item with the Post Type has been clicked
            // killfunc: function to kill in case the Ajax Window going to be removed (before Remove function !
            // openAnimationSpeed: how quick the Ajax Content window should be animated (default is 0.3) 
        });
    </script>
    <div class="bears-purchaseref-wrap "> <a class="link-purchase-iu" target="_blank" title="purchase theme" href="https://1.envato.market/gXGLO"> <img src="https://educlever.beplusthemes.com/elementary/wp-content/plugins/tbaffiliate/img/cart-icon.png"> <span>Buy</span> EDUCLEVER <span>on</span> <img alt="purchase theme" src="https://educlever.beplusthemes.com/elementary/wp-content/plugins/tbaffiliate/img/envato-label.png"> </a></div>
    <script>
        (function(body) {
            'use strict';
            body.className = body.className.replace(/\btribe-no-js\b/, 'tribe-js');
        })(document.body);
    </script>
    <div class="notification-wrap dark" style=""> <a href="#" class="close-notification"> <span class="ion-ios-close-empty"></span> </a>
        <div class="notification-inner">
            <div class="notification-heading-tabs"></div>
            <div class="notification-content-tabs">
                <div class="notification-content-tabs-inner">
                    <div id="notification-slider-panel" class="owl-carousel" data-bears-owl-carousel="{&quot;items&quot;:1,&quot;loop&quot;:false,&quot;center&quot;:false,&quot;margin&quot;:30,&quot;URLhashListener&quot;:true,&quot;URLhashSelector&quot;:&quot;.notification-heading-tabs .nav-tab-item&quot;,&quot;autoplayHoverPause&quot;:true,&quot;nav&quot;:false,&quot;dots&quot;:false}">
                        <div class="item" data-hash="notification-search">
                            <div class="item-inner tab-container-search">
                                <form class="custom-search-form" role="search" method="get" action="https://educlever.beplusthemes.com/elementary">
                                    <div class="btp-search"> <input class="search-field" data-search-ajax-result="" placeholder="Type to search..." value="" name="s" type="search"> <button type="submit" class="search-submit"><span class="ion-ios-search"></span></button></div>
                                </form>
                                <div id="notification-search-ajax-result"></div>
                            </div>
                        </div>
                        <div class="item" data-hash="notification-cart">
                            <div class="item-inner tab-container-cart">
                                <div id="notification-mini-cart"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        /* <![CDATA[ */
        var tribe_l10n_datatables = {
            "aria": {
                "sort_ascending": ": activate to sort column ascending",
                "sort_descending": ": activate to sort column descending"
            },
            "length_menu": "Show _MENU_ entries",
            "empty_table": "No data available in table",
            "info": "Showing _START_ to _END_ of _TOTAL_ entries",
            "info_empty": "Showing 0 to 0 of 0 entries",
            "info_filtered": "(filtered from _MAX_ total entries)",
            "zero_records": "No matching records found",
            "search": "Search:",
            "all_selected_text": "All items on this page were selected. ",
            "select_all_link": "Select all pages",
            "clear_selection": "Clear Selection.",
            "pagination": {
                "all": "All",
                "next": "Next",
                "previous": "Previous"
            },
            "select": {
                "rows": {
                    "0": "",
                    "_": ": Selected %d rows",
                    "1": ": Selected 1 row"
                }
            },
            "datepicker": {
                "dayNames": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
                "dayNamesShort": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
                "dayNamesMin": ["S", "M", "T", "W", "T", "F", "S"],
                "monthNames": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                "monthNamesShort": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
                "monthNamesMin": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
                "nextText": "Next",
                "prevText": "Prev",
                "currentText": "Today",
                "closeText": "Done",
                "today": "Today",
                "clear": "Clear"
            }
        }; /* ]]> */
    </script>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400%7CLato:400%2C900%2C700%7COpen+Sans:400" rel="stylesheet" property="stylesheet" media="all" type="text/css">
    <script type="text/javascript">
        (function() {
            var c = document.body.className;
            c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
            document.body.className = c;
        })();
    </script>
    <script type="text/javascript">
        if (typeof revslider_showDoubleJqueryError === "undefined") {
            function revslider_showDoubleJqueryError(sliderID) {
                var err = "<div class='rs_error_message_box'>";
                err += "<div class='rs_error_message_oops'>Oops...</div>";
                err += "<div class='rs_error_message_content'>";
                err += "You have some jquery.js library include that comes after the Slider Revolution files js inclusion.<br>";
                err += "To fix this, you can:<br>&nbsp;&nbsp;&nbsp; 1. Set 'Module General Options' ->  'jQuery & OutPut Filters' -> 'Put JS to Body' to on";
                err += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jQuery.js inclusion and remove it";
                err += "</div>";
                err += "</div>";
                jQuery(sliderID).show().html(err);
            }
        }
    </script>
    <script type='text/javascript' id='lrm-modal-js-extra'>
        /* <![CDATA[ */
        var LRM = {
            "home_url_arr": {
                "scheme": "https",
                "host": "educlever.beplusthemes.com",
                "path": "\/elementary"
            },
            "home_url": "https:\/\/educlever.beplusthemes.com\/elementary",
            "password_zxcvbn_js_src": "https:\/\/educlever.beplusthemes.com\/elementary\/wp-includes\/js\/zxcvbn.min.js",
            "allow_weak_password": "",
            "password_strength_lib": null,
            "redirect_url": "",
            "ajax_url": "https:\/\/educlever.beplusthemes.com\/elementary\/?lrm=1",
            "is_user_logged_in": "",
            "reload_after_login": null,
            "selectors_mapping": {
                "login": "a[href*='wp-login']",
                "register": ""
            },
            "is_customize_preview": "",
            "l10n": {
                "password_is_good": "Good Password",
                "password_is_strong": "Strong Password",
                "password_is_short": "Too Short Password",
                "password_is_bad": "Bad Password",
                "passwords_is_mismatch": "Passwords is mismatch!",
                "passwords_is_weak": "Error: Your password is very weak!"
            }
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/uploads/siteground-optimizer-assets/lrm-modal.min.js' id='lrm-modal-js'></script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-includes/js/dist/vendor/regenerator-runtime.min.js' id='regenerator-runtime-js'></script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-includes/js/dist/vendor/wp-polyfill.min.js' id='wp-polyfill-js'></script>
    <script type='text/javascript' id='contact-form-7-js-extra'>
        /* <![CDATA[ */
        var wpcf7 = {
            "api": {
                "root": "https:\/\/educlever.beplusthemes.com\/elementary\/wp-json\/",
                "namespace": "contact-form-7\/v1"
            }
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/uploads/siteground-optimizer-assets/contact-form-7.min.js' id='contact-form-7-js'></script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min.js' id='js-cookie-js'></script>
    <script type='text/javascript' id='woocommerce-js-extra'>
        /* <![CDATA[ */
        var woocommerce_params = {
            "ajax_url": "\/elementary\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/elementary\/?wc-ajax=%%endpoint%%"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min.js' id='woocommerce-js'></script>
    <script type='text/javascript' id='wc-cart-fragments-js-extra'>
        /* <![CDATA[ */
        var wc_cart_fragments_params = {
            "ajax_url": "\/elementary\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/elementary\/?wc-ajax=%%endpoint%%",
            "cart_hash_key": "wc_cart_hash_610b3036c4b581e41fa0376c678deb30",
            "fragment_name": "wc_fragments_610b3036c4b581e41fa0376c678deb30",
            "request_timeout": "5000"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min.js' id='wc-cart-fragments-js'></script>
    <script type='text/javascript' id='LRH_js-js-extra'>
        /* <![CDATA[ */
        var LRH_php_value = {
            "ajax_url": "https:\/\/educlever.beplusthemes.com\/elementary\/wp-admin\/admin-ajax.php",
            "nav_item_template": "<div class=\"lrh-nav-item-container \">\n<div class=\"__inner\">\n<div class=\"lrh-not-login\">\n <span class=\"lrm-login\">\n <a href=\"#login\"> <span class=\"text\">Login<\/span> <\/a> <\/span>\n <span class=\"lrm-register\">\n <a href=\"#register\"> <span class=\"text\">Register<\/span> <\/a> <\/span>\n <\/div>\n <\/div>\n<\/div>\n",
            "js_language": []
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/uploads/siteground-optimizer-assets/LRH_js.min.js' id='LRH_js-js'></script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-includes/js/jquery/ui/core.min.js' id='jquery-ui-core-js'></script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-includes/js/jquery/ui/mouse.min.js' id='jquery-ui-mouse-js'></script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-includes/js/jquery/ui/resizable.min.js' id='jquery-ui-resizable-js'></script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/uploads/siteground-optimizer-assets/bootstrap.min.js' id='bootstrap-js'></script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/themes/educlever/assets/js/lazysizes.min.js' id='lazysizes-js'></script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/themes/educlever/assets/js/jquery.stellar.min.js' id='stellar-js'></script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/plugins/js_composer/assets/lib/bower/isotope/dist/isotope.pkgd.min.js' id='isotope-js'></script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/themes/educlever/assets/js/jquery.mousewheel.min.js' id='mousewheel-js'></script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/themes/educlever/assets/js/froogaloop2.min.js' id='froogaloop2-js'></script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/themes/educlever/assets/lightGallery/js/lightgallery.min.js' id='lightGallery-js'></script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/themes/educlever/assets/lightGallery/js/lg-zoom.min.js' id='lg-zoom-js'></script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/themes/educlever/assets/lightGallery/js/lg-autoplay.min.js' id='lg-autoplay-js'></script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/themes/educlever/assets/lightGallery/js/lg-thumbnail.min.js' id='lg-thumbnail-js'></script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/themes/educlever/assets/lightGallery/js/lg-video.min.js' id='lg-video-js'></script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/themes/educlever/assets/owl.carousel/owl.carousel.min.js' id='owl.carousel-js'></script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/themes/educlever/assets/js/tilt.jquery.min.js' id='tilt-js'></script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/themes/educlever/assets/sweetalert/dist/sweetalert.min.js' id='sweetalert-js'></script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/themes/educlever/assets/js/progressbar.min.js' id='progressbarjs-js'></script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/uploads/siteground-optimizer-assets/waypoints.min.js' id='waypoints-js'></script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/themes/educlever/assets/js/jquery.counterup.min.js' id='counterup-js'></script>
    <script type='text/javascript' id='educlever-theme-script-js-extra'>
        /* <![CDATA[ */
        var BtPhpVars = {
            "ajax_url": "https:\/\/educlever.beplusthemes.com\/elementary\/wp-admin\/admin-ajax.php",
            "template_directory": "https:\/\/educlever.beplusthemes.com\/elementary\/wp-content\/themes\/educlever",
            "previous": "Previous",
            "next": "Next",
            "smartphone_animations": "no",
            "fail_form_error": "Sorry you are an error in ajax, please contact the administrator of the website"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/uploads/siteground-optimizer-assets/educlever-theme-script.min.js' id='educlever-theme-script-js'></script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/plugins/js_composer/assets/js/dist/js_composer_front.min.js' id='wpb_composer_front_js-js'></script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/plugins/js_composer/assets/lib/vc_waypoints/vc-waypoints.min.js' id='vc_waypoints-js'></script>
    <script type='text/javascript' id='essential-grid-essential-grid-script-js-extra'>
        /* <![CDATA[ */
        var eg_ajax_var = {
            "url": "https:\/\/educlever.beplusthemes.com\/elementary\/wp-admin\/admin-ajax.php",
            "nonce": "6d586f3be3"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/plugins/essential-grid/public/assets/js/jquery.themepunch.essential.min.js' id='essential-grid-essential-grid-script-js'></script>
    <script type='text/javascript' src='https://educlever.beplusthemes.com/elementary/wp-content/plugins/js_composer/assets/lib/bower/skrollr/dist/skrollr.min.js' id='vc_jquery_skrollr_js-js'></script>
</body>

</html>