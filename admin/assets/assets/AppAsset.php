<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/datepicker.css',
        'css/datepicker.css',
        'vendor/fontawesome-free/css/all.min.css',
        'vendor/bootstrap/css/bootstrap.min.css',
        'css/ruang-admin.min.css',
        'css/eeye.css',
    ];
    public $js = [
        'js/qr.min.js',
        'js/scan.js',
        'js/nepali.datepicker.v3.2.min.js',
        'js/demo/chart-area-demo.js',
        'vendor/chart.js/Chart.min.js',
        'js/ruang-admin.min.js',
        'vendor/jquery-easing/jquery.easing.min.js',
        'vendor/bootstrap/js/bootstrap.bundle.min.js',
        'js/dynamic_form.js',
        'js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
    ];
}
