-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 29, 2022 at 09:05 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rci_rojgar`
--

-- --------------------------------------------------------

--
-- Table structure for table `about`
--

CREATE TABLE `about` (
  `id` int(11) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` varchar(5000) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `about`
--

INSERT INTO `about` (`id`, `fk_user_id`, `title`, `description`, `created_date`, `status`) VALUES
(1, 1, 'Introduction', '<p>Rupandehi district of Lumbini Zone comes under the State no. 5 of Federal Republic of Nepal and Western Development Region. This district is about 270 k.ms. south-west of Kathmandu, the capital city of Nepal. Rupandehi district has become successful to establish its own distinct industrial, commercial, historical, geographical, religious, archeological and cultural identity. The historical and cultural significance of this district is further enhanced because of being the down town of Lumbini, the holy birthplace of Lord Buddha, the apostle of peace, compassion and non-violence, location of winter palace of Mani Mukunda Sen, the past king of Palpa, presence of historical place Jitgadhi Fort built at the time of Nepal British war and also having drained by overflowing the Tinau river. Rupandehi district is situated in the panoramic lap of the Chure range and bordered by Palpa and Arghakhanchi on the north, India on the south, Kapilvastu on the west and Nawalparasi on the east. It is 100 to 1219 meter above from the sea level. The total area of this district is 1172 sq. Km. This district in extended to 83\'30\" degree longitude east and 27\'20\" degree latitude north. The total population is about 0.9 million. The whole district is having altogether seven Election Constituencies with One sub- metropolitan city, Five Municipalities and Forty-eight Village Development Committees (V.D.Cs).</p>\r\n<h4>Industrial Journey of Rupandehi District.</h4>\r\n<p>The industrial Journey of the district begins with the establishment of Mahendra Sugar Mill and Butwal Ghee Factory in 1920s. Now this District is filled with different industrial academies in course of its development as per with the pace of time. After the establishment of Butwal Technical Institute (BTI) and Butwal Industrial Estate, speedy development of industrialization is further increased. At the end of 2015, more than 9000 Cottage, Small, Medium and Large Industries were established in this district. After the full functioning of Special Economic Zone (SEZ) in Bhairahawa, Private Industrial State in Siktahan and New Industrial Estate in Motipur, the district will definitely have recognition of as the best Industrial District of Nepal.</p>\r\n<h4>Establishment of Rupandehi Chamber of Industries (RCI).</h4>\r\n<p>Rupandehi Chamber of Industries was established in 1978 A.D.( 2035, Mangsir 15)with the initiation of some visionary senior industrialists. After the establishment and slightly development of industries in the district, the industrialists realized the requirement of an Organization which can raise their voices collectively for the protection, preservation and promotion of the Industries. In spite of having very close relation between Industries and Trade, they have been facing their problems time and again. As a result of the founder industrialists thought about the significance of an independent umbrella organization to solve the problems of industries, Rupandehi Chamber of Industries (RCI) was established. Since its inception, RCI has been working for the establishment, development and betterment of Industries introducing innovative ideas and technologies as par with the national and international standard.. RCI has been envisioning a road map along with Long term visions supported by annual working plans for the industrialization and sustainable Economic development of the District and hence of the Country.</p>', '2022-06-20 10:52:18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `adminuser`
--

CREATE TABLE `adminuser` (
  `id` int(11) NOT NULL,
  `username` varchar(150) NOT NULL,
  `password` varchar(100) NOT NULL,
  `org_name` varchar(100) NOT NULL,
  `org_type` int(11) NOT NULL,
  `contact_person` varchar(50) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `role` varchar(30) NOT NULL,
  `authkey` varchar(100) NOT NULL,
  `register_date` varchar(15) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `status` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `adminuser`
--

INSERT INTO `adminuser` (`id`, `username`, `password`, `org_name`, `org_type`, `contact_person`, `phone`, `role`, `authkey`, `register_date`, `created_date`, `status`) VALUES
(1, 'rci.btl@gmail.com', 'tuki@12345', '', 0, 'RCI', '9815557002', '1', '60f52ae28be0c', '', '2021-07-19 00:00:00', 1),
(8, 'kp464784@gmail.com', 'Kamal@19783', 'Bottlers nefol pvt.ltd', 14, 'Buddhisagar pandey', '9866132080', '2', '62b953a02f50a', '2079-03-13', '2022-06-27 06:52:16', 1),
(9, 'manojkc80005@gmail.com', 'Manoj@123', 'tuki', 14, 'Manoj KC', '9800550025', '2', '62b959fcc2a28', '2079-03-13', '2022-06-27 07:19:24', 1),
(10, 'nepalbgplastic@gmail.com', 'Rojgarnebgold', 'nepal blackgold and allied ind.pvt.ltd.', 4, 'Yadav Bhandari', '9857028955', '2', '62b95df8c6c11', '2079-03-13', '2022-06-27 07:36:24', 1);

-- --------------------------------------------------------

--
-- Table structure for table `available_for`
--

CREATE TABLE `available_for` (
  `id` int(11) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `fk_org_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `available_for`
--

INSERT INTO `available_for` (`id`, `fk_user_id`, `fk_org_id`, `title`, `created_date`, `status`) VALUES
(1, 1, 1, 'Full Time', '2022-06-08 14:23:17', 1),
(2, 1, 1, 'Part Time', '2022-06-08 14:24:35', 1),
(3, 1, 1, 'Contract', '2022-06-08 14:24:46', 1),
(4, 1, 1, 'Freelance', '2022-06-08 14:24:55', 1),
(5, 1, 1, 'Banking /Finance', '2022-06-08 16:30:18', 1);

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `city_name` varchar(50) NOT NULL,
  `city_nepali` varchar(50) DEFAULT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `city_name`, `city_nepali`, `created_date`, `status`) VALUES
(1, 'Nepalgunj', NULL, '2022-06-10 17:24:49', 1),
(2, 'Butwal', NULL, '2022-06-10 17:26:55', 1),
(3, 'pokhara', NULL, '2022-06-18 15:27:02', 1),
(5, 'kathmandu', NULL, '2022-06-23 09:40:45', 1),
(6, 'dharan', NULL, '2022-06-23 09:41:03', 1);

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `email` varchar(20) NOT NULL,
  `message` varchar(250) NOT NULL,
  `created_date` date NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `name`, `phone`, `email`, `message`, `created_date`) VALUES
(13, 'manoj', '4444444444444', 'info@tuki.com', 'test\r\n', '2022-06-20'),
(14, 'test', '2222222222', 'info@tuki.com', '5test', '2022-06-20'),
(15, 'Buddhisagar Pandey', '9866132080', 'kp464784@gmail.com', 'hello ', '2022-06-24');

-- --------------------------------------------------------

--
-- Table structure for table `designation`
--

CREATE TABLE `designation` (
  `id` int(11) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `designation` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `designation`
--

INSERT INTO `designation` (`id`, `fk_user_id`, `designation`, `created_date`, `status`) VALUES
(1, 1, 'president\r\n', '2022-06-17 14:05:18', 1),
(2, 1, 'vice president', '2022-06-20 08:52:45', 1),
(3, 1, 'office secretery\r\n', '2022-06-20 08:53:14', 1),
(4, 1, 'Treasurer', '2022-06-20 09:01:52', 1),
(5, 1, 'Joint Treasurer', '2022-06-20 09:02:14', 1),
(6, 1, 'Secretary women', '2022-06-20 09:03:11', 1),
(7, 1, 'vice president 2nd', '2022-06-20 09:03:42', 1),
(8, 1, 'vice president 1nd', '2022-06-20 09:04:48', 1),
(9, 1, 'Senior Vice President ', '2022-06-20 09:05:31', 1),
(10, 1, 'Immediate Past President', '2022-06-20 09:06:15', 1),
(11, 1, 'Cheif Executive Officer(CEO)', '2022-06-20 09:06:43', 1),
(12, 1, 'Accountant', '2022-06-20 09:07:06', 1),
(13, 1, 'Computer Operator', '2022-06-20 09:07:25', 1),
(14, 1, 'Office Assistant', '2022-06-20 09:07:47', 1),
(15, 1, 'Office Helper', '2022-06-20 09:08:04', 1),
(16, 1, 'Cheif Executive Officer(CEO)', '2022-06-20 09:22:21', 1),
(17, 1, 'Secretary General', '2022-06-20 09:34:43', 1),
(18, 1, 'Secretary', '2022-06-20 09:51:00', 1),
(19, 1, 'Advisor', '2022-06-20 09:55:36', 1),
(20, 1, 'Co-ordinator\r\n', '2022-06-20 09:55:58', 1),
(21, 1, 'Founder president', '2022-06-22 05:15:14', 1),
(22, 1, 'Past president', '2022-06-22 05:15:39', 1),
(23, 1, 'IT Officer', '2022-06-24 07:16:01', 1);

-- --------------------------------------------------------

--
-- Table structure for table `district`
--

CREATE TABLE `district` (
  `id` int(11) NOT NULL,
  `district` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `district_nepali` varchar(3000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `fk_province` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `district`
--

INSERT INTO `district` (`id`, `district`, `district_nepali`, `fk_province`) VALUES
(1, 'Jhapa', 'झापा ', 1),
(2, 'Morang', 'मोरंग', 1),
(3, 'Ilam', 'इलाम', 1),
(4, 'Sunsari', 'सुनसरी', 1),
(5, 'Dhankuta', 'धनकुटा', 1),
(6, 'Pachthar', 'पांचथर', 1),
(7, 'Teharthum', 'तेह्रथुम', 1),
(8, 'Taplejung', 'ताप्लेजुंग', 1),
(9, 'Bhojpur', 'भोजपुर', 1),
(10, 'Sankhuwasabha', 'संखुवासभा', 1),
(11, 'Khotang', 'खोटाँग', 1),
(12, 'Udaipur', 'उदयपुर', 1),
(13, 'Okhaldhunga', 'ओखलढुंगा', 1),
(14, 'Solukhumbu', 'सोलुखुम्बू', 1),
(15, 'Parsa', 'पर्सा', 2),
(16, 'Bara', 'बारा', 2),
(17, 'Rautahat', 'रौतहट ', 2),
(18, 'Sarlahi', 'सर्लाही', 2),
(19, 'Mahottari', 'महोत्तरी', 2),
(20, 'Dhanusa', 'धनुषा ', 2),
(21, 'Siraha', 'सिराहा', 2),
(22, 'Saptari', 'सप्तरी', 2),
(37, 'Rasuwa', 'रसुवा ', 3),
(38, 'Chitwan', 'चितवन', 3),
(39, 'Makwanpur', 'मकवानपुर', 3),
(40, 'Sindhuli', 'सिन्दुली', 3),
(41, 'Dhading', 'धादिङ', 3),
(42, 'Nuwakot', ' नुवाकोट', 3),
(43, 'Lalitpur', 'ललितपुर', 3),
(44, 'Bhaktapur', 'भक्तपुर', 3),
(45, 'Kathmandu', 'काठमाडौँ', 3),
(46, 'Kabhre', 'काभ्रे', 3),
(47, 'Ramechhap', 'रामेछाप', 3),
(48, 'Dolakha', 'धोलाखा', 3),
(49, 'Sindhupalchok', 'सिन्धुपाल्चोक', 3),
(50, 'Manang', 'मनाङ', 4),
(51, 'Mustang', 'मुस्ताङ ', 4),
(52, 'Gorkha', 'गोर्खा', 4),
(53, 'Lamjung', 'लमजुङ', 4),
(54, 'Kaski', 'कास्की', 4),
(55, 'Tanahu', 'तनहु', 4),
(56, 'Nawalparasi East', ' नवलपरासी ', 4),
(57, 'Syanja', 'स्याङग्जा', 4),
(58, 'Parbat', 'पर्वत', 4),
(59, 'Baglung ', 'बागलुङ', 4),
(60, 'Myagdi', 'म्याग्दी ', 4),
(61, 'Bardiya', '	बर्दिया', 5),
(62, 'Banke', 'बाँके', 5),
(63, 'Dang', 'दाङ', 5),
(64, 'Rolpa', 'रोल्पा ', 5),
(65, 'Rukum East', 'पश्चिमी रूकुम ', 5),
(67, 'Pyuthan', 'प्युठान', 5),
(68, 'Gulmi', 'गुल्मी', 5),
(69, 'Arghakhanchi', 'अर्घाखाँची', 5),
(70, 'Kapilbastu', 'कपिलवस्तु ', 5),
(71, 'Palpa', 'पाल्पा', 5),
(72, 'Rupendehi', 'रुपन्देही', 5),
(73, 'Nawalparasi', 'नवलपरासी', 5),
(74, 'Humla', 'हुम्ला', 6),
(75, 'Kalikot', 'कालिकोट', 6),
(76, 'Mugu', 'मुगु', 6),
(77, 'Jumla', 'जुम्ला', 6),
(78, 'Dolpa', 'डोल्पा', 6),
(79, 'Dailekh', 'दैलेख', 6),
(80, 'Surkhet', 'सुर्खेत', 6),
(81, 'Salyan', 'सल्यान', 6),
(82, 'Rukum West', 'पश्चिमी रूकुम', 6),
(83, 'Jajarkot', '	जाजरकोट', 6),
(84, 'Darchula', 'दार्चुला', 7),
(85, 'Baitadi', 'बैतडी', 7),
(86, 'Bajhang', 'बझाङ', 7),
(87, 'Bajura', 'बाजुरा', 7),
(88, 'Dadeldhura', 'डडेलधुरा ', 7),
(89, 'Doti', 'डोटी', 7),
(90, 'Achham', 'अछाम', 7),
(91, 'Kanchanpur', 'कंचनपुर', 7),
(92, 'Kailali', 'कैलाली', 7);

-- --------------------------------------------------------

--
-- Table structure for table `employer_user`
--

CREATE TABLE `employer_user` (
  `id` int(11) NOT NULL,
  `org_name` varchar(200) NOT NULL,
  `org_type` int(11) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `contact_person` varchar(50) NOT NULL,
  `contact` varchar(15) NOT NULL,
  `register_date` date NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `employment`
--

CREATE TABLE `employment` (
  `id` int(11) NOT NULL,
  `fk_user_id` int(11) DEFAULT NULL,
  `fk_org_id` int(11) DEFAULT NULL,
  `full_name` varchar(100) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `ctzn_num` varchar(30) NOT NULL,
  `father_name` varchar(50) DEFAULT NULL,
  `mother_name` varchar(50) DEFAULT NULL,
  `fk_per_province` int(11) NOT NULL,
  `fk_per_district` int(11) NOT NULL,
  `fk_per_mun` int(11) NOT NULL,
  `fk_per_ward` int(11) NOT NULL,
  `fk_per_tole` varchar(100) NOT NULL,
  `fk_temp_province` int(11) NOT NULL,
  `fk_temp_district` int(11) NOT NULL,
  `fk_temp_mun` int(11) NOT NULL,
  `fk_temp_ward` int(11) NOT NULL,
  `fk_temp_tole` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `contact_no` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  `fk_qualification` int(11) NOT NULL,
  `is_disability` int(11) NOT NULL,
  `fk_job_sector` int(11) NOT NULL,
  `submission_date` date NOT NULL,
  `pp_image` varchar(100) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employment`
--

INSERT INTO `employment` (`id`, `fk_user_id`, `fk_org_id`, `full_name`, `gender`, `ctzn_num`, `father_name`, `mother_name`, `fk_per_province`, `fk_per_district`, `fk_per_mun`, `fk_per_ward`, `fk_per_tole`, `fk_temp_province`, `fk_temp_district`, `fk_temp_mun`, `fk_temp_ward`, `fk_temp_tole`, `dob`, `contact_no`, `email`, `fk_qualification`, `is_disability`, `fk_job_sector`, `submission_date`, `pp_image`, `created_date`, `status`) VALUES
(4, 1, 1, 'Prakash Chaudhary', 'Male', '445454', 'Dubar Tharu', 'Jagrani Tharu', 5, 62, 490, 4, 'fdsfj', 5, 62, 489, 3, 'fgjkfj', '2066-02-19', '54545454', 'brainchaudhary@gmail.com', 1, 1, 1, '2079-02-18', 'img/profile/629717d1dc9b9.jpg', '2022-06-01 13:25:01', 1),
(5, 1, 1, 'Brijmohan Bhujuwa', 'Male', '5454545', 'Father Name', 'Mother Name', 3, 38, 283, 3, '10', 5, 62, 488, 1, '02', '2079-02-26', '9848427993', 'bageshworisancharsewa@gmail.com', 1, 0, 1, '2079-02-26', 'img/profile/62a18d45c17ac.png', '2022-06-09 11:48:49', 1),
(8, 1, 1, 'manoj kc', 'Female', '12123', 'test', 'test', 5, 51, 399, 2, 'test', 5, 53, 417, 2, 'test', '2079-03-01', '989898989', 'info@tuki.com', 2, 0, 2, '2079-03-08', 'img/profile/62b2f1b7d49c8.jpg', '2022-06-22 10:40:55', 1),
(9, 1, 1, 'kamal pandey', 'Male', '42-02-73-01120', 'Bishnu pandey', 'Bindu pandey', 4, 57, 446, 4, 'birgha', 5, 72, 579, 3, 'shankarnagar', '2056-04-22', '9866132080', 'kp464784@gmail.com', 3, 0, 1, '2079-03-09', 'img/profile/62b3eccf03fb0.jpg', '2022-06-23 04:32:15', 1),
(10, 1, 1, 'kamal pandey', 'Male', '42-02-74-01190', 'Bishnu prasad pandey', 'Bindu Pandey', 4, 57, 446, 2, 'birgha', 5, 72, 579, 3, 'shankarnagar', '2056-04-22', '9812343234', 'kp464556@gmail.com', 4, 0, 1, '2079-03-12', 'img/profile/62b7fc3e8fba6.docx', '2022-06-26 06:27:10', 1),
(11, 1, 1, 'kamal pandey', 'Male', '42-02-74-01191', 'Bishnu prasad pandey', 'Bindu Pandey', 5, 72, 579, 3, 'Shankarnagar', 4, 57, 446, 4, 'Birgha', '2060-04-22', '9847218878', 'kp006@gmail.com', 5, 0, 5, '2079-03-13', 'img/profile/62b958975cd47.jpg', '2022-06-27 07:13:27', 1);

-- --------------------------------------------------------

--
-- Table structure for table `emp_and_job`
--

CREATE TABLE `emp_and_job` (
  `id` int(11) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `text_type` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` mediumtext NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `emp_and_job`
--

INSERT INTO `emp_and_job` (`id`, `fk_user_id`, `text_type`, `title`, `description`, `created_date`, `status`) VALUES
(1, 1, 1, 'कामदार', 'देशको श्रम शक्तिको समुचित परिचालन शिपयुक्त जनशक्ति उत्पादन, व्यवस्थापन र निरन्तरताका लागि राज्यका निकाय, नीजि क्षेत्र र तालिम प्रदायक निकायहरुको सहकार्य हुनु जरुरी छ । उत्पादित जनशक्तिलाई खपत गराउन, बेरोजगार समस्या समाधन गर्न र औद्योगिक उत्पादनमा टेवा पुर्याउने यो रोजगार सूचना केन्द्र को स्थापना गरिएको हो।', '2022-06-26 17:26:15', 1),
(2, 1, 2, 'रोजगार दाता ', 'रुपन्देही उद्योग संघले श्रम व्यवस्थापनका निम्ति रोजगार सूचना केन्द्रमार्फत  शिपयुक्त जनशक्तिको तथ्यांक संकलनका साथै उद्योगीहरुको आवश्यकता पहिचान गरी व्यवस्थापन गर्नेछ । युवा शक्तिलाई स्वदेशमै पसिना बगाउने वातावरण तयार हुनेछ भन्ने सोच का साथ् यस केन्द्रले रोजगारी श्रृजना, जनशक्ति उत्पादन र व्यवस्थापनमा समेत ठूलो टेवा पुर्याउन रोजगार सूचना केन्द्र को स्थापना गरिएको हो।', '2022-06-26 17:30:13', 1);

-- --------------------------------------------------------

--
-- Table structure for table `goal`
--

CREATE TABLE `goal` (
  `id` int(11) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `title` varchar(50) CHARACTER SET utf8 NOT NULL,
  `goals` varchar(2000) CHARACTER SET utf8 NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `goal`
--

INSERT INTO `goal` (`id`, `fk_user_id`, `title`, `goals`, `created_date`, `status`) VALUES
(2, 1, 'रोजगार सूचना केन्द्र संचालनको लक्ष्य,', '<p>स्वदेशी उद्योगको विकास र प्रवद्र्धनकालागी क्रियाशिल रही राष्ट्रिय अर्थतन्त्रको विकास र समुन्नत समाज निर्माणमाअग्रसर रहने ।उद्योगीको नेतृत्वदायी संस्थाको रुपमा स्थापित भई औद्योगिक विकासको पर्यायवाची संस्थाको रुपमा प्रतिष्ठित हुने । उद्योगीको संरक्षण र सम्वद्र्धनगर्ने, अवसर र संभावनाको खोजीगरी श्रोत र साधनको समुचित परिचालन गराउंदै औद्योगिक विकासका साथै सामाजिक उत्तरदायित्व वहनगर्ने ।</p>', '2022-06-19 13:26:02', 1);

-- --------------------------------------------------------

--
-- Table structure for table `header`
--

CREATE TABLE `header` (
  `id` int(11) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `job_logo` varchar(250) NOT NULL,
  `org_logo` varchar(250) NOT NULL,
  `text_slider` varchar(250) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `header`
--

INSERT INTO `header` (`id`, `fk_user_id`, `job_logo`, `org_logo`, `text_slider`, `created_date`, `status`) VALUES
(1, 1, 'img/62ba9f40b0a5d.jpeg', 'img/62ba9f40b0b1e.jpg', 'नयाँ औद्योगिक क्षेत्रको स्थापना ,रोजगार मूलक कार्यक्रम संचालन र स्वदेशी उद्योगको स्थापनामा जोड दिन को लागि स्थापना गरिएको रोजगार सूचना केन्द्र ,बुटवल ४ रुपन्देही। ', '2022-06-24 11:41:08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `introduction`
--

CREATE TABLE `introduction` (
  `id` int(11) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `title` varchar(50) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `introduction`
--

INSERT INTO `introduction` (`id`, `fk_user_id`, `title`, `description`, `created_date`, `status`) VALUES
(2, 1, 'रुपन्देहीको औद्योगिक यात्रा', '<div>बि.सं.२०१९ सालमा स्थापना भएको महेन्द्रसुगर मिल र बुटवलमा स्थापना भएको बुटवल घ्यू कारखानाबाट सुरु भएको रुपन्देही जिल्लाको औद्योगिक यात्रा समय क्रमससँगै विस्तार हुदै जाँदा पुरै जिल्ला औद्योगिक प्रतिष्ठान हरुले भरिपूर्ण भएको छ । वि.सं. २०२० सालमा बुटवल टेक्नीकल इन्ष्टिच्यूटको स्थापना र २०३२ सालमा बुटवल औद्योगिक क्षेत्र को&nbsp; निर्माण पछी रुपन्देहीमा उद्योगहरुको तिव्र विकास शुरु भएको पाइन्छ ।&nbsp; नेपालकै दोस्रो ठूलो औद्योगिक जिल्लामा करिब १४ हजार भन्दा बढि परम्परागत, घरेलु तथा साना, मझौला र ठूला ठूला उद्योगहरु समेत स्थापना भै संचालन भइरहेका छन् । यस जिल्ला औद्योगिक विकासको ठूलो सम्भावना रहेको जिल्ला हो । भैरहवामा स्थापना भएको विशेष आर्थिक षेत्र, मोतिपुरमा निर्माण हुन लागेको नयाँ औद्योगिक क्षेत्र र सहकारीका माध्ययमबाट सिक्टहनमा स्थापना गर्न लागिएको निजीस्तरको औद्योगिक क्षेत्र संचालन भए पश्चात्जिल्ला नेपाल कै उत्कृष्ट औद्योगिक जिल्ला वन्नेछ ।</div>\r\n<div>&nbsp;</div>\r\n<div>\r\n<div><strong>रुपन्देही उद्योग संघको स्थापना:</strong></div>\r\n<div>रुपन्देही उद्योगसंघको स्थापना बि.सं. २०३५ साल मंसिर १५ गते भएको थियो । रुपन्देही मातत्कालीन समयमा उद्योगहरु स्थापनाको लहर देखिय सगै उद्योग र उद्योगीको भावना बुझ्ने, समस्या समाधानका लागी पहल गर्ने र औद्योगिक विकासमा टेवा पुर्याउने पवित्र उद्देश्यले अग्रज उद्योगीहरुको सक्रियतामा संघको स्थापना भएको हो । व्यापार र उद्योग विच नङ र मासु सरह को सम्वन्ध रहँदा रहँदै पनि उद्योगका समस्या र व्यापारका समस्याविच सामन्जस्यता हुन नसकेका कारण उद्योग र व्यापार विचको समस्या समाधानका लागि छुट्टाछुट्टै संस्थाको आवश्यकताको महसुश हुन गई रुपन्देही उद्योगसंघको स्थापना भएको हो । समयक्रमसँगै जिल्लामा उद्योग स्थापनाको क्रम वढ्दै जाँदा उद्योग र उद्योगीको हितमा संघले प्रदान गर्ने सेवामा समया नुकुल परिमार्जन र परिस्कृत हुदै गएको छ । पछिल्लो समयमा समग्र जिल्लाको औद्योगिक, आर्थिक र सामाजिक विकासकालागी संघले विभिन्न खालका योजना र कार्यक्रमहरु संचालन गर्दै आइरहेको छ ।<br><br>\r\n<div>\r\n<div>\r\n<div><strong><strong>रोजगार सूचना केन्द्र्को स्थापना र संचालनः</strong></strong></div>\r\n<div><strong><strong><br></strong></strong>\r\n<div>\r\n<div>देशको श्रम शक्तिको समुचित परिचालनको अभावमा लाखौं युवाहरु विदेशी भूमिमा पसिना बगाउन बाध्य छन् । एकातर्प युवाहरु बेरोजगार बन्दै गइरहेका छन् भने अर्कातर्फ्र स्वदे शीउद्योगहरु उपयुक्त श्रम शक्तिको अभाव खेप्न बाध्य छन् । शिप युक्त जन शक्ति उत्पादन, व्यवस्थापन र निरन्तरताकालागि राज्यका निकाय, नीजि क्षेत्र र तालिम प्रदायक निकायहरुको सहकार्य हुनु जरुरी छ । उत्पादित जनशक्तिलाई खपत गराउन, बेरोजगार समस्या समाधन गर्न र औद्योगिक उत्पादन मा टेवा पुर्याउन एक पुल को खचो छ । रुपन्देही उद्योग संघले श्रम व्यवस्थापनका निम्ति रोजगार सूचना केन्द्र मार्फर्त शिपयुक्त जनशक्तिको तथ्यांक संकलनका साथै उद्योगीहरुको आवश्यकता पहिचान गरी व्यवस्थापन गर्ने छ । केन्द्रको स्थापना सँगै उद्योगीहरुले झेल्नु परेको अभाव समस्या समाधानमा टेवा पुगी राष्ट्रिय उत्पादनमा समेत बृद्धि हुनेछ भने युवा शक्तिलाई स्वदेशमै पसिना बगाउने वातावरण तयार हुनेछ । यस केन्द्रले रोजगारी श्रृजना, जनशक्ति उत्पादन र व्यवस्थापनमा समेत ठूलो टेवा पुग्नेदेखिन्छ । साथै केन्द्रले राज्यका निकायहरु, तालिम प्रदायक संस्था र उद्योग हरुलाई समेत आवश्यक सूचना प्रवाह गर्नुका साथै राय सुझावहरु समेत पेश गर्ने छ । यसकेन्द्र संचालनमा सरकारका निकायहरु र उद्योग, वाणिज्य तथा उपभोक्ता हित संरक्षण निर्देशनालयको प्रत्यक्ष एवं सक्रिय सहयोग र सहकार्यको अपेक्षा लिएका छौं ।</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>', '2022-06-19 11:12:35', 1);

-- --------------------------------------------------------

--
-- Table structure for table `job_exp`
--

CREATE TABLE `job_exp` (
  `id` int(11) NOT NULL,
  `fk_user_id` int(11) DEFAULT NULL,
  `fk_org_id` int(11) DEFAULT NULL,
  `fk_employment_id` int(11) NOT NULL,
  `exp_title` varchar(100) NOT NULL,
  `duration` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `job_level`
--

CREATE TABLE `job_level` (
  `id` int(11) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `fk_org_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_level`
--

INSERT INTO `job_level` (`id`, `fk_user_id`, `fk_org_id`, `title`, `created_date`, `status`) VALUES
(1, 1, 1, 'Top Level', '2022-06-08 13:06:48', 1),
(2, 1, 1, 'Senior Level', '2022-06-08 13:15:09', 1),
(3, 1, 1, 'Mid Level', '2022-06-08 13:15:21', 1),
(4, 1, 1, 'Entry Level', '2022-06-08 13:15:31', 1);

-- --------------------------------------------------------

--
-- Table structure for table `job_post`
--

CREATE TABLE `job_post` (
  `id` int(11) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `fk_org_id` int(11) NOT NULL,
  `job_position` varchar(50) DEFAULT NULL,
  `no_of_employees` int(11) NOT NULL,
  `fk_job_posting_option` int(11) DEFAULT NULL,
  `fk_job_level` int(11) NOT NULL,
  `fk_job_category` int(11) NOT NULL,
  `fk_avail_for` int(11) NOT NULL,
  `city` varchar(50) DEFAULT NULL,
  `job_location` varchar(50) DEFAULT NULL,
  `offered_salary_type` int(11) NOT NULL,
  `min_salary` decimal(10,2) DEFAULT NULL,
  `max_salary` decimal(10,2) DEFAULT NULL,
  `salary` decimal(10,2) DEFAULT NULL,
  `fk_salary_type` int(11) NOT NULL,
  `job_description` text NOT NULL,
  `fk_education` int(11) NOT NULL,
  `application_deadline` varchar(12) NOT NULL,
  `is_training_necessary` int(11) NOT NULL,
  `min_experience` varchar(50) DEFAULT NULL,
  `other_facility` varchar(200) NOT NULL,
  `submit_date` date NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `update_date` date DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_post`
--

INSERT INTO `job_post` (`id`, `fk_user_id`, `fk_org_id`, `job_position`, `no_of_employees`, `fk_job_posting_option`, `fk_job_level`, `fk_job_category`, `fk_avail_for`, `city`, `job_location`, `offered_salary_type`, `min_salary`, `max_salary`, `salary`, `fk_salary_type`, `job_description`, `fk_education`, `application_deadline`, `is_training_necessary`, `min_experience`, `other_facility`, `submit_date`, `created_date`, `update_date`, `status`) VALUES
(9, 8, 7, 'Sales Manager ', 20, 1, 1, 14, 1, '2', 'jesis chowk', 2, NULL, NULL, '35000.00', 1, 'Search For Professional Bio Example. Smart Results Here. SearchStartNow. Search Everything You Need. Save Time & Get Quick Results. Find Fast. Search Smarter. Find More. Multi Search. Search Efficiently. Better Results. Useful Info. Find Right Now.hurry up !!!', 2, '2079-03-23', 0, NULL, 'fuel free and rent free ', '2079-03-13', '2022-06-27 06:59:54', NULL, 1),
(10, 1, 1, 'Office Manager', 3, 2, 2, 4, 2, '1', 'newroad', 1, '20000.00', '30000.00', NULL, 1, 'new company', 5, '2079-03-20', 0, NULL, 'travel petrol', '2079-03-13', '2022-06-27 07:37:58', NULL, 1),
(11, 10, 1, 'Machine Operator', 1, 2, 3, 4, 1, '2', 'industrial Area', 2, NULL, NULL, '15000.00', 1, 'machine operator will work in the pipe making factory .', 4, '2079-03-14', 0, NULL, 'lodging', '2079-03-13', '2022-06-27 07:40:27', NULL, 1),
(12, 8, 1, 'Firm helper ', 2, 3, 3, 1, 1, '1', 'purano buspark ', 2, NULL, NULL, '21343.00', 1, 'org is made for agriculture and firm ..making various types of products .', 8, '2079-03-23', 0, NULL, 'double wages in overtime', '2079-03-13', '2022-06-27 10:58:28', NULL, 1),
(13, 1, 1, 'test', 3, 1, 2, 6, 4, '', '', 1, NULL, NULL, NULL, 3, 'test', 6, '2079-03-21', 1, NULL, 'test', '2079-03-14', '2022-06-28 17:40:55', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `job_posting`
--

CREATE TABLE `job_posting` (
  `id` int(11) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `fk_org_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_posting`
--

INSERT INTO `job_posting` (`id`, `fk_user_id`, `fk_org_id`, `title`, `created_date`, `status`) VALUES
(1, 1, 1, 'Top Job', '2022-06-08 12:39:33', 1),
(2, 1, 1, 'Hot Job', '2022-06-08 12:41:43', 1),
(3, 1, 1, 'Feature Job', '2022-06-08 12:41:54', 1);

-- --------------------------------------------------------

--
-- Table structure for table `job_sector`
--

CREATE TABLE `job_sector` (
  `id` int(11) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `fk_org_id` int(11) NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_sector`
--

INSERT INTO `job_sector` (`id`, `fk_user_id`, `fk_org_id`, `title`, `created_date`, `status`) VALUES
(1, 1, 1, 'कृषि ', '2022-06-01 10:55:09', 1),
(4, 1, 1, 'उधॊग ', '2022-06-23 10:59:23', 1),
(5, 1, 1, 'फलाम,इन्जिनियरिङ्ग ', '2022-06-27 06:11:01', 1),
(6, 1, 1, 'प्लाष्टिक', '2022-06-27 06:11:30', 1),
(7, 1, 1, 'पर्यटन', '2022-06-27 06:12:01', 1),
(8, 1, 1, 'फर्निचर', '2022-06-27 06:12:14', 1),
(9, 1, 1, 'प्रेस ', '2022-06-27 06:12:29', 1),
(10, 1, 1, 'गार्मेन्ट', '2022-06-27 06:12:44', 1),
(11, 1, 1, 'आल्मुनियम', '2022-06-27 06:12:58', 1),
(12, 1, 1, 'डेरी', '2022-06-27 06:13:10', 1),
(13, 1, 1, 'खाद्यान्ण', '2022-06-27 06:31:58', 1),
(14, 1, 1, 'अन्य', '2022-06-27 06:33:44', 1);

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `id` int(11) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `fk_member_cat` int(11) NOT NULL,
  `full_Name` varchar(80) NOT NULL,
  `fk_designation` int(11) NOT NULL,
  `contact` varchar(10) NOT NULL,
  `email` varchar(50) NOT NULL,
  `profile_img` varchar(100) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id`, `fk_user_id`, `fk_member_cat`, `full_Name`, `fk_designation`, `contact`, `email`, `profile_img`, `created_date`, `status`) VALUES
(5, 1, 1, 'Mr. Madhav Prasad Poudel', 1, '123', '123', 'img/62b0399031994.jpg', '2022-06-20 09:10:40', 1),
(6, 1, 1, 'Mr. Baburam Bohora', 10, '9857038500', '..', 'img/62b03a82551f0.jpg', '2022-06-20 09:14:42', 1),
(7, 1, 4, 'Yadav Prasad Bhandari', 16, '..', '..', 'img/62b03c6c5c2b5.jpg', '2022-06-20 09:22:52', 1),
(8, 1, 4, 'Sabin Khanal', 12, '..', '..', 'img/62b03c9d5292c.jpg', '2022-06-20 09:23:41', 1),
(9, 1, 4, 'Malati Kunwar', 13, '..', '..', 'img/62b03cc90b1f0.jpg', '2022-06-20 09:24:25', 1),
(10, 1, 4, 'Nabin Chaudhary', 14, '..', '..', 'img/62b03cf065211.jpg', '2022-06-20 09:25:04', 1),
(11, 1, 4, 'Gyan Bahadur Oli', 14, '..', '..', 'img/62b03d107a12f.jpg', '2022-06-20 09:25:36', 1),
(12, 1, 4, 'Dhan Kumari Oli', 15, '..', '..', 'img/62b03d2f4e0e2.jpg', '2022-06-20 09:26:07', 1),
(13, 1, 1, 'Mr. Dilip Sapkota', 9, '..', '..', 'img/62b03e7b4d0cd.jpg', '2022-06-20 09:31:39', 1),
(14, 1, 1, 'Mr. Krishna Prasad Parajuli', 8, '123', '123', 'img/62b03ea7bc3d5.jpg', '2022-06-20 09:32:23', 1),
(15, 1, 1, 'Mr.Jibkanta Kafle', 7, '..', '..', 'img/62b03f00cd1e1.jpg', '2022-06-20 09:33:52', 1),
(16, 1, 1, 'Mr. Hari Bahadur Khatri', 17, '..', '..', 'img/62b042b6186a2.jpg', '2022-06-20 09:49:42', 1),
(18, 1, 1, 'Mr.Hiramani Bhattarai', 4, '123', '..', 'img/62b042d3193a9.jpg', '2022-06-20 09:50:11', 1),
(19, 1, 1, 'Mr. Basanta Dhakal', 18, '..', '..', 'img/62b043223885a.jpg', '2022-06-20 09:51:30', 1),
(20, 1, 1, 'Nawadurga Food Industry', 6, '..', '..', 'img/62b04341a65b4.jpg', '2022-06-20 09:52:01', 1),
(21, 1, 1, 'Mr. Surendra Gyawali', 5, '123', '..', 'img/62b0435ecb3fc.jpg', '2022-06-20 09:52:30', 1),
(22, 1, 2, 'Mr. dinesh raj Regmi', 20, '..', '..', 'img/62b0495f1f996.jpg', '2022-06-20 10:18:07', 1),
(23, 1, 2, 'Mr. Bhupal Kumar Shrestha', 19, '..', '..', 'img/62b049b64c227.jpg', '2022-06-20 10:19:34', 1),
(24, 1, 2, 'Mr. Shiva Kumar Pathak', 19, '..', '..', 'img/62b049e8c7365.jpg', '2022-06-20 10:20:24', 1),
(25, 1, 2, 'Mr. Bidur Dhungana', 19, '123', '123', 'img/62b04a440a51f.jpg', '2022-06-20 10:21:56', 1),
(26, 1, 2, 'Mr. Raj Kumar Pokhrel', 19, '..', '..', 'img/62b04aa09e6f0.jpg', '2022-06-20 10:23:28', 1),
(27, 1, 2, 'Dr. Khagaraj Sharma', 19, '...', '........', 'img/62b04ad00e6d4.jpg', '2022-06-20 10:24:16', 1),
(28, 1, 2, 'Mr. Yamlal khanal', 19, '..', '..', 'img/62b04b00c2b61.jpg', '2022-06-20 10:25:04', 1),
(29, 1, 2, 'Er. suman shrestha', 19, '..', '..', 'img/62b04b2482447.jpg', '2022-06-20 10:25:40', 1),
(30, 1, 2, 'Mr. Suman Adhikari', 19, '123', '..', 'img/62b04b48bc850.jpg', '2022-06-20 10:26:16', 1),
(31, 1, 3, 'Mr. Rana Bahadur Shah', 21, '..', '..', 'img/62b2a5cc5a6a9.jpg', '2022-06-22 05:17:00', 1),
(32, 1, 3, 'Late. Balaram Pradhan', 22, '..', '..', 'img/62b2a5fda45c7.jpg', '2022-06-22 05:17:49', 1),
(33, 1, 3, 'Er. Padhumlal Shrestha', 22, '..', '..', 'img/62b2a9ab878e7.jpg', '2022-06-22 05:33:31', 1),
(34, 1, 3, 'Late.Chandraman Shrestha', 1, 'qq', 'qq', 'img/62b2aae351a20.jpg', '2022-06-22 05:38:43', 1),
(35, 1, 3, 'Late. Narayan Bhakta Pradhan', 22, '..', '123', 'img/62b2ab34eff47.jpg', '2022-06-22 05:40:04', 0),
(36, 1, 3, 'Mr. Damber Dev Aryal', 22, '..', '..', 'img/62b2ab97f02c6.jpg', '2022-06-22 05:41:43', 1),
(37, 1, 3, 'Mr. Benuram Pradhan', 22, '..', '..', 'img/62b2abc8aece4.jpg', '2022-06-22 05:42:32', 1),
(38, 1, 3, 'Mr. Shyam Bahadur Pathak', 22, '..', '..', 'img/62b2ac26be406.jpg', '2022-06-22 05:44:06', 1),
(39, 1, 3, 'Mr. Mahendra Narayan Shrestha', 22, '..', '..', 'img/62b2ac6228633.jpg', '2022-06-22 05:45:06', 1),
(40, 1, 3, 'Late. Pramananda Kharel', 22, '..', '..', 'img/62b2ac9338acb.jpg', '2022-06-22 05:45:55', 1),
(41, 1, 3, 'Late. Satish Chandra Kasaudhan', 22, '..', '..', 'img/62b2accd96735.jpg', '2022-06-22 05:46:53', 1),
(42, 1, 3, 'Mr. Azaj Alam', 22, '..', '..', 'img/62b2acfe6a9e7.jpg', '2022-06-22 05:47:42', 1),
(43, 1, 3, 'Mr. Tej Kumar Pathak', 22, '..', '..', 'img/62b2ad1aaf25d.jpg', '2022-06-22 05:48:10', 1),
(44, 1, 3, 'Mr. Bonin Piya', 22, '..', '..', 'img/62b2ad36ed4bc.jpg', '2022-06-22 05:48:38', 1);

-- --------------------------------------------------------

--
-- Table structure for table `member_cat`
--

CREATE TABLE `member_cat` (
  `id` int(11) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `category` varchar(100) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member_cat`
--

INSERT INTO `member_cat` (`id`, `fk_user_id`, `category`, `created_date`, `status`) VALUES
(1, 1, 'Bod of Directors', '2022-06-17 13:56:03', 1),
(2, 1, 'Advisor', '2022-06-17 17:01:20', 1),
(3, 1, 'past president', '2022-06-19 17:16:43', 1),
(4, 1, 'Staff', '2022-06-20 11:54:50', 1);

-- --------------------------------------------------------

--
-- Table structure for table `municipals`
--

CREATE TABLE `municipals` (
  `id` int(11) NOT NULL,
  `name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `municipal_nepali` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `fk_district` int(11) NOT NULL,
  `fk_province` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `municipals`
--

INSERT INTO `municipals` (`id`, `name`, `municipal_nepali`, `fk_district`, `fk_province`) VALUES
(15, 'Damak Municipality', 'दमक नगरपालिका', 1, 1),
(16, 'Kamal', 'कमल गाउँपालिका', 1, 1),
(17, 'Gauradhaha', 'गौरादह नगरपालिका', 1, 1),
(18, 'Shivasataxi', 'शिवशताक्षी नगरपालिका', 1, 1),
(19, 'Kankai', '	कन्काई नगरपालिका', 1, 1),
(20, 'Jhapa', '	झापा गाउँपालिका', 1, 1),
(21, 'Barhadashi', 'बाह्रदशी गाउँपालिका', 1, 1),
(22, 'Birtamod', 'विर्तामोड नगरपालिका', 1, 1),
(23, 'Haldibari', '	हल्दिवारी गाउँपालिका', 1, 1),
(24, 'Bhadrapur', '	भद्रपुर नगरपालिका', 1, 1),
(25, 'Kachankawal', '	कचनकवल गाउँपालिका', 1, 1),
(26, 'Arjundhara', '	अर्जुनधारा नगरपालिका', 1, 1),
(27, 'Mechinagar', '	मेचीनगर नगरपालिका', 1, 1),
(28, 'Buddhashanti', '	बुद्धशान्ति गाउँपालिका', 1, 1),
(29, 'kerabari', 'केरावारी गाउँपालिका', 2, 1),
(30, 'Letang Municipality', '	लेटाङ नगरपालिका', 2, 1),
(31, 'Miklajung', 'मिक्लाजुङ गाउँपालिका', 2, 1),
(32, 'Sundarharaicha', 'सुन्दरहरैचा नगरपालिका', 2, 1),
(33, 'Belbari', 'बेलवारी नगरपालिका', 2, 1),
(34, 'Kanepokhari', 'कानेपोखरी गाउँपालिका', 2, 1),
(35, 'Pataharishanishchare', 'पथरी शनिश्चरे नगरपालिका', 2, 1),
(36, 'Budhiganga', 'बुढीगंगा गाउँपालिका', 2, 1),
(37, 'Katahari', 'कटहरी गाउँपालिका', 2, 1),
(38, 'Rangeli Municipality', 'रंगेली नगरपालिका', 2, 1),
(39, 'Biratnagar', 'विराटनगर महानगरपालिका', 2, 1),
(40, 'Jahada', 'जहदा गाउँपालिका', 2, 1),
(41, 'Dhanpalthan', 'धनपालथान गाउँपालिका', 2, 1),
(42, 'Sunwarshi', 'सुनवर्षि नगरपालिका', 2, 1),
(43, 'RAtuwamai', 'रतुवामाई नगरपालिका', 2, 1),
(44, 'Urlabari', '	उर्लावारी नगरपालिका', 2, 1),
(45, 'Gramthan', 'ग्रामथान गाउँपालिका', 2, 1),
(46, 'Sandakpur', 'सन्दकपुर गाउँपालिका', 3, 1),
(47, 'Fakphokthum', 'फाकफोकथुम गाउँपालिका', 3, 1),
(48, 'Deumai', 'देउमाई नगरपालिका', 3, 1),
(49, 'Ilam', 'ईलाम नगरपालिका', 3, 1),
(50, 'Maijogmai', 'माईजोगमाई गाउँपालिका', 3, 1),
(51, 'Suryodaya', '	सूर्योदय नगरपालिका', 3, 1),
(52, 'Rong', 'रोङ गाउँपालिका', 3, 1),
(53, 'Mai', 'माई नगरपालिका', 3, 1),
(54, 'Chulachuli', 'चुलाचुली गाउँपालिका', 3, 1),
(55, 'Mangsebung', '	माङसेबुङ गाउँपालिका', 3, 1),
(56, 'Barahchhetra', 'बराहक्षेत्र नगरपालिका', 4, 1),
(57, 'Dharan', 'धरान उपमहानगरपालिका', 4, 1),
(58, 'Ramdhuni', 'रामधुनी नगरपालिका', 4, 1),
(59, 'Itahari', 'ईटहरी उपमहानगरपालिका', 4, 1),
(60, 'Inaruwa', 'ईनरुवा नगरपालिका', 4, 1),
(61, 'Gadhi', 'गढी गाउँपालिका', 4, 1),
(62, 'Duhabi', 'दुहवी नगरपालिका', 4, 1),
(63, 'Barju', 'बर्जु गाउँपालिका', 4, 1),
(64, 'Dewanganj', 'देवानगञ्ज गाउँपालिका', 4, 1),
(65, 'Harinagar', '	हरिनगर गाउँपालिका', 4, 1),
(66, 'Koshi', 'कोशी गाउँपालिका', 4, 1),
(67, 'Bhokraha Nursing', 'भोक्राहा नरसिंह गाउँपालिका', 4, 1),
(68, 'Mahalaxmi', 'महालक्ष्मी नगरपालिका', 5, 1),
(69, 'Pakhribas', 'पाख्रिबास नगरपालिका', 5, 1),
(70, 'Shahidbhumi', 'सहिदभूमि गाउँपालिका', 5, 1),
(71, 'Sangurigadhi', 'साँगुरीगढी गाउँपालिका', 5, 1),
(72, 'Chaubise', 'चौविसे गाउँपालिका', 5, 1),
(73, 'Dhankuta', '	धनकुटा नगरपालिका', 5, 1),
(74, 'Chhathar Jorpati', '	छथर जोरपाटी गाउँपालिका', 5, 1),
(75, 'Miklajung', 'मिक्लाजुङ गाउँपालिका', 6, 1),
(76, 'Tumbewa', 'तुम्बेवा गाउँपालिका', 6, 1),
(77, 'Kummayak', 'कुम्मायक गाउँपालिका', 6, 1),
(78, 'Falgunanda', '	फाल्गुनन्द गाउँपालिका', 6, 1),
(79, 'Falelung', 'फालेलुङ गाउँपालिका', 6, 1),
(80, 'Yangwarak', 'याङवरक गाउँपालिका', 6, 1),
(81, 'Phidim', 'फिदिम नगरपालिका', 6, 1),
(82, 'Hilihang', '	हिलिहाङ गाउँपालिका', 6, 1),
(83, 'Menchayam', 'मेन्छयायेम गाउँपालिका', 7, 1),
(84, 'Athrai', 'आठराई गाउँपालिका', 7, 1),
(85, 'Phedap', 'फेदाप गाउँपालिका', 7, 1),
(86, 'Myanglum', '	म्याङलुङ नगरपालिका', 7, 1),
(87, 'Laligurans', 'लालीगुराँस नगरपालिका', 7, 1),
(88, 'Chhathar', '	छथर गाउँपालिका', 7, 1),
(89, 'Phaktanglung', 'फक्ताङलुङ गाउँपालिका', 8, 1),
(90, 'Mikwakhola', '	मैवाखोला गाउँपालिका', 8, 1),
(91, 'Meringden', 'मेरिङदेन गाउँपालिका', 8, 1),
(92, 'Maiwakhola', '	मिक्वाखोला गाउँपालिका', 8, 1),
(93, 'Phunling', 'फुङलिङ नगरपालिका', 8, 1),
(94, 'Sirijangha', 'सिरीजङ्घा गाउँपालिका', 8, 1),
(95, 'Sidingba', '	सिदिङ्वा गाउँपालिका', 8, 1),
(96, 'Athrai Tribeni', '	आठराई त्रिवेणी गाउँपालिका', 8, 1),
(97, 'Pathibhara Yangwarak', 'पाथीभरा याङवरक गाउँपालिका', 8, 1),
(98, 'Salpasilichho', 'साल्पासिलिछो गाउँपालिका', 9, 1),
(99, 'Shadananda', '	षडानन्द नगरपालिका', 9, 1),
(100, 'Bhojpur', 'भोजपुर नगरपालिका', 9, 1),
(101, 'Temkemaiyum', '	टेम्केमैयुङ गाउँपालिका', 9, 1),
(102, 'Arun', 'अरुण गाउँपालिका', 9, 1),
(103, 'Pauwadungma', 'पौवादुङमा गाउँपालिका', 9, 1),
(104, 'Ramprasad Rai', 'रामप्रसाद राई गाउँपालिका', 9, 1),
(105, 'Hatuwagadhi', 'हतुवागढी गाउँपालिका', 9, 1),
(106, 'Aamchowk', 'आमचोक गाउँपालिका', 9, 1),
(107, 'Bhotkhola', 'भोटखोला गाउँपालिका', 10, 1),
(108, 'Makalu', 'मकालु गाउँपालिका', 10, 1),
(109, 'Silichong', 'सिलीचोङ गाउँपालिका', 10, 1),
(110, 'Chichila', 'चिचिला गाउँपालिका', 10, 1),
(111, 'Khandabari', 'खाँदवारी नगरपालिका', 10, 1),
(112, 'Sabhapokhari', 'सभापोखरी गाउँपालिका', 10, 1),
(113, 'Panchakhapan', 'पाँचखपन नगरपालिका', 10, 1),
(114, 'Chainpur', '	चैनपुर नगरपालिका', 10, 1),
(115, 'Madi', 'मादी नगरपालिका', 10, 1),
(116, 'Dharmadevi', 'धर्मदेवी नगरपालिका', 10, 1),
(117, 'Ainselukhark', 'ऐसेलुखर्क गाउँपालिका', 11, 1),
(118, 'Kepilasagadhi', 'केपिलासगढी गाउँपालिका', 11, 1),
(119, 'Rawa Besi', 'रावा बेसी गाउँपालिका', 11, 1),
(120, 'Doktel Rupkot Majhuwagadi', 'दिक्तेल रुपाकोट मझुवागढी नगरपालिका', 11, 1),
(121, 'Sakela', 'साकेला गाउँपालिका', 11, 1),
(122, 'Halesi Tuwachung', 'हलेसी तुवाचुङ नगरपालिका', 11, 1),
(123, 'Diprung Chuichumma', 'दिप्रुङ चुइचुम्मा गाउँपालिका', 11, 1),
(124, 'Khotehang', 'खोटेहाङ गाउँपालिका', 11, 1),
(125, 'Jantedhunga', 'जन्तेढुंगा गाउँपालिका', 11, 1),
(126, 'Barahpokhari', '	वराहपोखरी गाउँपालिका', 11, 1),
(127, 'Katari', 'कटारी नगरपालिका', 12, 1),
(128, 'Tapil', '	ताप्ली गाउँपालिका', 12, 1),
(129, 'Limchungbung', '	लिम्चुङ्बुङ गाउँपालिका', 12, 1),
(130, 'Udayapurgadhi', 'उदयपुरगढी गाउँपालिका', 12, 1),
(131, 'Rautamai', 'रौतामाई गाउँपालिका', 12, 1),
(132, 'Triyuga', '	त्रियुगा नगरपालिका', 12, 1),
(133, 'Chaudandigadhi', 'चौदण्डीगढी नगरपालिका', 12, 1),
(134, 'baleka', 'वेलका नगरपालिका', 12, 1),
(135, 'Khijidemba', 'खिजिदेम्बा गाउँपालिका', 13, 1),
(136, 'Likhu', '	लिखु गाउँपालिका', 13, 1),
(137, 'Molung', 'मोलुङ गाउँपालिका', 13, 1),
(138, 'Champadevi', 'चम्पादेवी गाउँपालिका', 13, 1),
(139, 'Sunkoshi', '	सुनकोशी गाउँपालिका', 13, 1),
(140, 'Siddhicharan', 'सिद्दिचरण नगरपालिका', 13, 1),
(141, 'Manebhanjyang', 'मानेभञ्याङ गाउँपालिका', 13, 1),
(142, 'Khumbupasanglahmu', 'खुम्वु पासाङल्हमु गाउँपालिका', 14, 1),
(143, 'Mahakulung', 'माहाकुलुङ गाउँपालिका', 14, 1),
(144, 'Likhupike', 'लिखु पिके गाउँपालिका', 14, 1),
(145, 'SoluDudhakunda', 'सोलुदुधकुण्ड नगरपालिका', 14, 1),
(146, 'Mapya Dudhakoshi', 'माप्य दुधकोशी गाउँपालिका', 14, 1),
(147, 'Sotang', 'सोताङ गाउँपालिका', 14, 1),
(148, 'NechaSalyan', 'नेचासल्यान गाउँपालिका', 14, 1),
(149, 'Thori', 'ठोरी गाउँपालिका', 15, 2),
(150, 'Jirabhawani', 'जिरा भवानी गाउँपालिका', 15, 2),
(151, 'Paterwasugauli', '	पटेर्वा सुगौली गाउँपालिका', 15, 2),
(152, 'SakhuwaPrasauni', 'सखुवा प्रसौनी गाउँपालिका', 15, 2),
(153, 'Parsagadhi', 'पर्सागढी नगरपालिका', 15, 2),
(154, 'Jagarnathpur', '	जगरनाथपुर गाउँपालिका', 15, 2),
(155, 'Dhobini', 'धोबीनी गाउँपालिका', 15, 2),
(156, 'Pokhariya', 'पोखरिया नगरपालिका', 15, 2),
(157, 'Bahudaramai', 'बहुदरमाई नगरपालिका', 15, 2),
(158, 'Birgunj', 'बिरगंज महानगरपालिका', 15, 2),
(159, 'Kalikamai', 'कालिकामाई गाउँपालिका', 15, 2),
(160, 'Bindabasini', 'बिन्दबासिनी गाउँपालिका', 15, 2),
(161, 'Chhipaharmai', 'छिपहरमाई गाउँपालिका', 15, 2),
(162, 'Pakahamainpur', 'पकाहा मैनपुर गाउँपालिका', 15, 2),
(163, 'Jitpur Simara', 'जीतपुर सिमरा उपमहानगरपालिका', 16, 2),
(164, 'Nigadh', 'निजगढ नगरपालिका', 16, 2),
(165, 'Kolhabi', 'कोल्हवी नगरपालिका', 16, 2),
(166, 'Kalaiya', 'कलैया उपमहानगरपालिका', 16, 2),
(167, 'Karaiyamai', '	करैयामाई गाउँपालिका', 16, 2),
(168, 'Baragadhi', 'बारागढीगाउँपालिका', 16, 2),
(169, 'AdarshKotwal', 'आदर्श कोटवाल गाउँपालिका', 16, 2),
(170, 'Mahangadhimai', 'महागढीमाई नगरपालिका', 16, 2),
(171, 'Devtal', '	देवताल गाउँपालिका', 16, 2),
(172, 'Suwarna', 'सुवर्ण गाउँपालिका', 16, 2),
(173, 'Simraungadh', 'सिम्रौनगढ नगरपालिका', 16, 2),
(174, 'Pacharauta', 'पचरौता नगरपालिका', 16, 2),
(175, 'Bishrampur', 'विश्रामपुर गाउँपालिका', 16, 2),
(176, 'Pheta', 'फेटा गाउँपालिका', 16, 2),
(177, 'Prasauni', '	प्रसौनी गाउँपालिका', 16, 2),
(178, 'Chandrapur', 'चन्द्रपुर नगरपालिका', 17, 2),
(179, 'Gujara', 'गुजरा नगरपालिका', 17, 2),
(180, 'Brindaban', 'बृन्दावन नगरपालिका', 17, 2),
(181, 'Kathariya', 'कटहरिया नगरपालिका', 17, 2),
(182, 'Phatuwa Bijayapur', 'फतुवाबिजयपुर नगरपालिका', 17, 2),
(183, 'Garuda', 'गरुडा नगरपालिका', 17, 2),
(184, 'Gadhimai', '	गढीमाई नगरपालिका', 17, 2),
(185, 'Maulapur', 'मौलापुर नगरपालिका', 17, 2),
(186, 'Debahhi Gonahi', 'देवाही गोनाही नगरपालिका', 17, 2),
(187, 'Baudhimai', 'बौधीमाई नगरपालिका', 17, 2),
(188, 'Paroha', 'परोहा नगरपालिका', 17, 2),
(189, 'Yamunamai', 'यमुनामाई गाउँपालिका', 17, 2),
(190, 'Durga Bhagawati', 'दुर्गा भगवती गाउँपालिका', 17, 2),
(191, 'Rajpur', 'राजपुर नगरपालिका', 17, 2),
(192, 'Ishanath', 'ईशनाथ नगरपालिका', 17, 2),
(193, 'Gaur', 'गौर नगरपालिका', 17, 2),
(194, 'Rajdevi', 'राजदेवी नगरपालिका', 17, 2),
(195, 'Bagmati', 'बागमती नगरपालिका', 18, 2),
(196, 'Hariwan', '	हरिवन नगरपालिका', 18, 2),
(197, 'Haripur', '	हरिपुर नगरपालिका', 18, 2),
(198, 'Ishworpur', 'ईश्वरपुर नगरपालिका', 18, 2),
(199, 'Barahathawa', 'बरहथवा नगरपालिका', 18, 2),
(200, 'Basbariya', 'बसबरीया गाउँपालिका', 18, 2),
(201, 'Kabilasi', '	कविलासी नगरपालिका', 18, 2),
(202, 'Chandranagar', 'चन्द्रनगर गाउँपालिका', 18, 2),
(203, 'Haripurwa', 'हरिपुर्वा नगरपालिका', 18, 2),
(204, 'Chakraghatta', 'चक्रघट्टा गाउँपालिका', 18, 2),
(205, 'Bramhapuri', 'ब्रह्मपुरी गाउँपालिका', 18, 2),
(206, 'Parsa', 'पर्सा गाउँपालिका', 18, 2),
(207, 'Malangawa', 'मलंगवा नगरपालिका', 18, 2),
(208, 'Kaudena', 'कौडेना गाउँपालिका', 18, 2),
(209, 'Godaita', 'गोडैटा नगरपालिका', 18, 2),
(210, 'Ramnagar', 'रामनगर गाउँपालिका', 18, 2),
(211, 'Bishnu', 'विष्णु गाउँपालिका', 18, 2),
(212, 'Balara', 'बलरा नगरपालिका', 18, 2),
(213, 'Bardibas', 'बर्दिबास नगरपालिका', 19, 2),
(214, 'Gaushala', 'गौशाला नगरपालिका', 19, 2),
(215, 'Sonama', 'सोनमा गाउँपालिका', 19, 2),
(216, 'Aurahi', 'औरही नगरपालिका', 19, 2),
(217, 'Bhangaha', 'भँगाहा नगरपालिका', 19, 2),
(218, 'Samsi', 'साम्सी गाउँपालिका', 19, 2),
(219, 'Ramgopalpur', 'रामगोपालपुर नगरपालिका', 19, 2),
(220, 'Balwa', 'बलवा नगरपालिका', 19, 2),
(221, 'Loharpatti', 'लोहरपट्टी नगरपालिका', 19, 2),
(222, 'Manra Siswa', 'मनरा शिसवा नगरपालिका', 19, 2),
(223, 'Maottari', 'महोत्तरी गाउँपालिका', 19, 2),
(224, 'Ekdanra', 'एकडारा गाउँपालिका', 19, 2),
(225, 'Jaleswor', 'जलेश्वर नगरपालिका', 19, 2),
(226, 'Matihani', 'मटिहानी नगरपालिका', 19, 2),
(227, 'Mithila', 'मिथिला नगरपालिका', 20, 2),
(228, 'Bateshwor', 'बटेश्वर गाउँपालिका', 20, 2),
(229, 'Dhanusadham', 'धनुषाधाम नगरपालिका', 20, 2),
(230, 'Ganeshman Charnath', 'गणेशमान चारनाथ नगरपालिका', 20, 2),
(231, 'Chhireshwornath', 'क्षिरेश्वरनाथ नगरपालिका', 20, 2),
(232, 'Lakshminiya', 'लक्ष्मीनिया गाउँपालिका', 20, 2),
(233, 'Mithila Bihari', 'मिथिला बिहारी नगरपालिका', 20, 2),
(234, 'Sabaila', 'सबैला नगरपालिका', 20, 2),
(235, 'Hansapur', 'हंसपुर नगरपालिका', 20, 2),
(236, 'Janakpurdham', 'जनकपुरधाम उपमहानगरपालिका', 20, 2),
(237, 'Aurahi', 'औरही गाउँपालिका', 20, 2),
(238, 'Sahidnagar', 'शहीदनगर नगरपालिका', 20, 2),
(239, 'Kamala', 'कमला नगरपालिका', 20, 2),
(240, 'Bideha', 'विदेह नगरपालिका', 20, 2),
(241, 'Janaknandani', 'जनकनन्दिनी गाउँपालिका', 20, 2),
(242, 'Dhanauji', 'धनौजी गाउँपालिका', 20, 2),
(243, 'Nagarain', 'नगराइन नगरपालिका', 20, 2),
(244, 'Mukhaiyapatti Musaharmiya', 'मुखियापट्टी मुसहरमिया गाउँपालिका', 20, 2),
(245, 'Karjanha', 'कर्जन्हा नगरपालिका', 21, 2),
(246, 'Mirchaiya', 'मिर्चैयाँ नगरपालिका', 21, 2),
(247, 'Kalyanpur', 'कल्याणपुर नगरपालिका', 21, 2),
(248, 'Bishnupur', 'विष्णुपुर गाउँपालिका', 21, 2),
(249, 'Siraha', 'सिरहा नगरपालिका', 21, 2),
(250, 'Arnama', 'अर्नमा गाउँपालिका', 21, 2),
(251, 'Lahan', 'लहान नगरपालिका', 21, 2),
(252, 'Naraha', '	नरहा गाउँपालिका', 21, 2),
(253, 'Dhangadhimai', 'धनगढीमाई नगरपालिका', 21, 2),
(254, 'Golbazar', 'गोलबजार नगरपालिका', 21, 2),
(255, 'Sukhipur', 'सुखीपुर नगरपालिका', 21, 2),
(256, 'Laxmipur', 'लक्ष्मीपुर पतारी गाउँपालिका', 21, 2),
(257, 'Aurahi', 'औरही गाउँपालिका', 21, 2),
(258, 'Bariyarpatti', 'बरियारपट्टी गाउँपालिका', 21, 2),
(259, 'Nawarajpur', 'नवराजपुर गाउँपालिका', 21, 2),
(260, 'Bhagawanpur', 'भगवानपुर गाउँपालिका', 21, 2),
(261, 'Surunga', 'सुरुङ्‍गा नगरपालिका', 22, 2),
(262, 'Khadak', 'खडक नगरपालिका', 22, 2),
(263, 'Shambhunath', 'शम्भुनाथ नगरपालिका', 22, 2),
(264, 'Rupani', 'रुपनी गाउँपालिका', 22, 2),
(265, 'Balan Bihul', 'बलान-बिहुल गाउँपालिका', 22, 2),
(266, 'Bode Barsain', 'बोदेबरसाईन नगरपालिका', 22, 2),
(267, 'Dakneshwori', 'डाक्नेश्वरी नगरपालिका', 22, 2),
(268, 'Agnisair Krishna Savaran', 'अग्निसाइर कृष्णासरवन गाउँपालिका', 22, 2),
(269, 'Saptkoshi', 'सप्तकोशी नगरपालिका', 22, 2),
(270, 'Kanchanrup', '	कञ्चनरुप नगरपालिका', 22, 2),
(271, 'Tirahut', 'तिरहुत गाउँपालिका', 22, 2),
(272, 'Mahadeva', 'महादेवा गाउँपालिका', 22, 2),
(273, 'Rajbiraj', 'राजविराज नगरपालिका', 22, 2),
(274, 'Hanumannagar Kankalini', 'हनुमाननगर कङ्‌कालिनी नगरपालिका', 22, 2),
(275, 'Tilathi Koladi', '	तिलाठी कोईलाडी गाउँपालिका', 22, 2),
(276, 'Chhinnamasta', 'छिन्नमस्ता गाउँपालिका', 22, 2),
(277, 'Rajgadh', 'राजगढ गाउँपालिका', 22, 2),
(278, 'Aamachhodingmo', 'आमाछोदिङमो गाउँपालिका', 37, 3),
(279, 'Gosainkunda', 'गोसाईकुण्ड गाउँपालिका', 37, 3),
(280, 'Uttargaya', 'उत्तरगया गाउँपालिका', 37, 3),
(281, 'Kalika', 'कालिका गाउँपालिका', 37, 3),
(282, 'Nakunda', 'नौकुण्ड गाउँपालिका', 37, 3),
(283, 'Madi', 'माडी नगरपालिका', 38, 3),
(284, 'Bharatpur', 'भरतपुर महानगरपालिका', 38, 3),
(285, 'Ratnangar', 'रत्ननगर नगरपालिका', 38, 3),
(286, 'Khairahani', 'खैरहनी नगरपालिका', 38, 3),
(287, 'Rapti', '	राप्ती नगरपालिका', 38, 3),
(288, 'Kalika', 'कालिका नगरपालिका', 38, 3),
(289, 'Ichchhyakamana', 'इच्छाकामना गाउँपालिका', 38, 3),
(290, 'Raksirang', 'राक्सिराङ्ग गाउँपालिका', 39, 3),
(291, 'Kailash', 'कैलाश गाउँपालिका', 39, 3),
(292, 'Thaha', 'थाहा नगरपालिका', 39, 3),
(293, 'Indrasarowar', 'इन्द्रसरोबर गाउँपालिका', 39, 3),
(294, 'Bhimphedi', 'भिमफेदी गाउँपालिका', 39, 3),
(295, 'Manahari', 'मनहरी गाउँपालिका', 39, 3),
(296, 'Makwanpurgadhi', '	मकवानपुरगढी गाउँपालिका', 39, 3),
(297, 'Hetauda', 'हेटौडा उपमहानगरपालिका', 39, 3),
(298, 'Bakaiya', 'बकैया गाउँपालिका', 39, 3),
(299, 'Bagmati', 'बाग्मति गाउँपालिका', 39, 3),
(300, 'Sunakoshi', 'सुनकोशी गाउँपालिका', 40, 3),
(301, 'Ganglekh', 'घ्याङलेख गाउँपालिका', 40, 3),
(302, 'Hariharpurgadhi', 'हरिहरपुरगढी गाउँपालिका', 40, 3),
(303, 'Marin', 'मरिण गाउँपालिका', 40, 3),
(304, 'Kamalamai', 'कमलामाई नगरपालिका', 40, 3),
(305, 'Golanjor', 'गोलन्जर गाउँपालिका', 40, 3),
(306, 'Phikkal', 'फिक्कल गाउँपालिका', 40, 3),
(307, 'Tinpatan', 'तीनपाटन गाउँपालिका', 40, 3),
(308, 'Dudhauli', 'दुधौली नगरपालिका', 40, 3),
(309, 'Rubi Valley', 'रुवी भ्याली गाउँपालिका', 41, 3),
(310, 'Gangajamuna', 'गङ्गाजमुना गाउँपालिका', 41, 3),
(311, 'Khaniyabash', 'खनियाबास गाउँपालिका', 41, 3),
(312, 'Tripura Sundari', 'त्रिपुरासुन्दरी गाउँपालिका', 41, 3),
(313, 'Netrawati Dabjong', 'नेत्रावती डबजोङ गाउँपालिका', 41, 3),
(314, 'Jwalamukhi', 'ज्वालामूखी गाउँपालिका', 41, 3),
(315, 'Nilakantha', 'निलकण्ठ नगरपालिका', 41, 3),
(316, 'Siddhalek', '	सिद्धलेक गाउँपालिका', 41, 3),
(317, 'Galchhi', 'गल्छी गाउँपालिका', 41, 3),
(318, 'Benighat', 'बेनीघाट रोराङ्ग गाउँपालिका', 41, 3),
(319, 'Gajuri', 'गजुरी गाउँपालिका', 41, 3),
(320, 'Thakre', '	थाक्रे गाउँपालिका', 41, 3),
(321, 'Kispang', '	किस्पाङ गाउँपालिका', 42, 3),
(322, 'Myagang', 'म्यगङ गाउँपालिका', 42, 3),
(323, 'Bidur', 'विदुर नगरपालिका', 42, 3),
(324, 'Tarkeshwar', 'तारकेश्वर गाउँपालिका', 42, 3),
(325, 'Suryagadhi', 'सुर्यगढी गाउँपालिका', 42, 3),
(326, 'Likhu', 'लिखु गाउँपालिका', 42, 3),
(327, 'Tadi', 'तादी गाउँपालिका', 42, 3),
(328, 'Panchakanya', 'पञ्चकन्या गाउँपालिका', 42, 3),
(329, 'Dupcheswar', 'दुप्चेश्वर गाउँपालिका', 42, 3),
(330, 'Shivapuri', '	शिवपुरी गाउँपालिका', 42, 3),
(331, 'Kakani', 'ककनी गाउँपालिका', 42, 3),
(332, 'Belkotgadhi', '	बेलकोटगढी नगरपालिका', 42, 3),
(333, 'Lalitpur', 'ललितपुर महानगरपालिका', 43, 3),
(334, 'Mahalaxmi', 'महालक्ष्मी नगरपालिका', 43, 3),
(335, 'Godawari', '	गोदावरी नगरपालिका', 43, 3),
(336, 'Bagmati', 'बागमती गाउँपालिका', 43, 3),
(337, 'Mahankal', 'महाङ्काल गाउँपालिका', 43, 3),
(338, 'Konjyosom', 'कोन्ज्योसोम गाउँपालिका', 43, 3),
(339, 'Madhyapur Thimi', 'मध्यपुर थिमी नगरपालिका', 44, 3),
(340, 'Changunarayan', 'चाँगुनारायण नगरपालिका', 44, 3),
(341, 'Bhaktapur', '	भक्तपुर नगरपालिका', 44, 3),
(342, 'Suryabinayak', '	सूर्यविनायक नगरपालिका', 44, 3),
(343, 'Dakshinkali', 'दक्षिणकाली नगरपालिका', 45, 3),
(344, 'Kirtipur', 'कीर्तिपुर नगरपालिका', 45, 3),
(345, 'Chandragiri', 'चन्द्रागिरी नगरपालिका', 45, 3),
(346, 'Kathmandu', 'काठमाण्डौं महानगरपालिका', 45, 3),
(347, 'Nagarjun', 'नागार्जुन नगरपालिका', 45, 3),
(348, 'Tarakeshwar', 'तारकेश्वर नगरपालिका', 45, 3),
(349, 'Budhanilkanth', 'बुढानिलकण्ठ नगरपालिका', 45, 3),
(350, 'Tokha', 'टोखा नगरपालिका', 45, 3),
(351, 'Gokarneshwar', 'गोकर्णेश्वर नगरपालिका', 45, 3),
(352, 'Shankarapur', 'शङ्खरापुर नगरपालिका', 45, 3),
(353, 'Kageshwori Manahora', 'कागेश्वरी मनोहरा नगरपालिका', 45, 3),
(354, 'Mandendeupur', 'मण्डनदेउपुर नगरपालिका', 46, 3),
(355, 'Banepa', 'बनेपा नगरपालिका', 46, 3),
(356, 'Panauti', 'पनौती नगरपालिका', 46, 3),
(357, 'Dhulikhel', 'धुलिखेल नगरपालिका', 46, 3),
(358, 'Panchkhal', 'पाँचखाल नगरपालिका', 46, 3),
(359, 'Bhumlu', 'भुम्लु गाउँपालिका', 46, 3),
(360, 'Chaurideurali', 'चौंरीदेउराली गाउँपालिका', 46, 3),
(361, 'Temal', '	तेमाल गाउँपालिका', 46, 3),
(362, 'Namobuddha', 'नमोबुद्ध नगरपालिका', 46, 3),
(363, 'Rosi', 'रोशी गाउँपालिका', 46, 3),
(364, 'Khanikhola', 'खानीखोला गाउँपालिका', 46, 3),
(365, 'Mahabharat', 'महाभारत गाँउपालिका', 46, 3),
(366, 'Doramba', 'दोरम्बा गाउँपालिका', 47, 3),
(367, 'Sunapati', 'सुनापती गाउँपालिका', 47, 3),
(368, 'Khadadevi', 'खाँडादेवी गाउँपालिका', 47, 3),
(369, 'Manthali', 'मन्थली नगरपालिका', 47, 3),
(370, 'Ramechhap', '	रामेछाप नगरपालिका', 47, 3),
(371, 'Likhu Tamakoshi', 'लिखु तामाकोशी गाउँपालिका', 47, 3),
(372, 'Gokulganga', 'गोकुलगङ्गा गाउँपालिका', 47, 3),
(373, 'Umakunda', 'उमाकुण्ड गाउँपालिका', 47, 3),
(374, 'Bigu', 'विगु गाउँपालिका', 48, 3),
(375, 'Gaurishankhar', 'गौरीशङ्कर गाउँपालिका', 48, 3),
(376, 'Kalinchok', 'कालिन्चोक गाउँपालिका', 48, 3),
(377, 'Jiri', '	जिरी नगरपालिका', 48, 3),
(378, 'Bhimeshwar', 'भिमेश्वर नगरपालिका', 48, 3),
(379, 'Baiteshwar', 'वैतेश्वर गाउँपालिका', 48, 3),
(380, 'Sailung', 'शैलुङ्ग गाउँपालिका', 48, 3),
(381, 'Melung', 'मेलुङ्ग गाउँपालिका', 48, 3),
(382, 'Tamakoshi', 'तामाकोशी गाउँपालिका', 48, 3),
(383, 'Helambu', '	हेलम्बु गाउँपालिका', 49, 3),
(384, 'Panchapokhari Thangpal', '	पाँचपोखरी थाङपाल गाउँपालिका', 49, 3),
(385, 'Jugal', 'जुगल गाउँपालिका', 49, 3),
(386, 'Bhotekoshi', 'भोटेकोशी गाउँपालिका', 49, 3),
(387, 'Melamchi', 'मेलम्ची नगरपालिका', 49, 3),
(388, 'Barhabise', 'बाह्रविसे नगरपालिका', 49, 3),
(389, 'Tripurasundari', 'त्रिपुरासुन्दरी गाउँपालिका', 49, 3),
(390, 'Sunkoshi', 'सुनकोशी गाउँपालिका', 49, 3),
(391, 'Lisangkhu Pakhar', 'लिसङ्खु पाखर गाउँपालिका', 49, 3),
(392, 'Indrawati', 'ईन्द्रावती गाउँपालिका', 49, 3),
(393, 'Balefi', 'बलेफी गाउँपालिका', 49, 3),
(394, 'Chautara Sangachokgadhi', 'चौतारा साँगाचोकगढी नगरपालिका', 49, 3),
(395, 'Narpa Bhumi', 'नार्पा भूमि गाउँपालिका', 50, 4),
(396, 'Chame', 'चामे गाउँपालिका', 50, 4),
(397, 'Nason', 'नासोँ गाउँपालिका', 50, 4),
(398, 'Manang Ngisyang', 'मनाङ ङिस्याङ गाउँपालिका', 50, 4),
(399, 'Lomanthang', 'लोमन्थाङ गाउँपालिका', 51, 4),
(400, 'Lo Gherkar Damodarkunda', 'लो-घेकर दामोदरकुण्ड गाउँपालिका', 51, 4),
(401, 'Varagung Muktichhetra', 'वारागुङ मुक्तिक्षेत्र गाउँपालिका', 51, 4),
(402, 'Gharapjhong', 'घरपझोङ गाउँपालिका', 51, 4),
(403, 'Thasang', 'थासाङ गाउँपालिका', 51, 4),
(404, 'Chum Nubri', 'चुमनुव्री गाउँपालिका', 52, 4),
(405, 'Dharche', '	धार्चे गाउँपालिका', 52, 4),
(406, 'Ajirkot', 'अजिरकोट गाउँपालिका', 52, 4),
(407, 'Barpak', 'बारपाक सुलिकोट गाउँपालिका', 52, 4),
(408, 'Gandaki Rural Municipality', 'गण्डकी गाउँपालिका', 52, 4),
(409, 'Aarughat', 'आरूघाट गाउँपालिका', 52, 4),
(410, 'Siranchok', 'सिरानचोक गाउँपालिका', 52, 4),
(411, 'Palungtar', 'पालुङटार नगरपालिका', 52, 4),
(412, 'Bhimsenthapa', 'भिमसेनथापा गाउँपालिका', 52, 4),
(413, 'Gorkha', 'गोरखा नगरपालिका', 52, 4),
(414, 'Sahid Lekhan', 'शहिद लखन गाउँपालिका', 52, 4),
(415, 'Marsyangdi', 'मर्स्याङदी गाउँपालिका', 53, 4),
(416, 'Kwholwsothar', 'क्व्होलासोथार गाउँपालिका', 53, 4),
(417, 'Besisahar', 'बेसीशहर नगरपालिका', 53, 4),
(418, 'Dordi', 'दोर्दी गाउँपालिका', 53, 4),
(419, 'Dudhpokhari', 'दूधपोखरी गाउँपालिका', 53, 4),
(420, 'Madhyanepal', 'मध्यनेपाल नगरपालिका', 53, 4),
(421, 'Sundarbazar', 'सुन्दरबजार नगरपालिका', 53, 4),
(422, 'Rainas', 'रार्इनास नगरपालिका', 53, 4),
(423, 'Annapurna', 'अन्नपूर्ण गाउँपालिका', 54, 4),
(424, 'Machhapuchchhre', 'माछापुच्छ्रे गाउँपालिका', 54, 4),
(425, 'Madi', 'मादी गाउँपालिका', 54, 4),
(426, 'Pokhara', 'पोखरा महानगरपालिका', 54, 4),
(427, 'Rupa', 'रूपा गाउँपालिका', 54, 4),
(428, 'Shuklagandaki', 'शुक्लागण्डकी नगरपालिका', 55, 4),
(429, 'Bhimad', 'भिमाद नगरपालिका', 55, 4),
(430, 'Magde', 'म्याग्दे गाउँपालिका', 55, 4),
(431, 'Vyas', 'व्यास नगरपालिका', 55, 4),
(432, 'Bhanu', 'भानु नगरपालिका', 55, 4),
(433, 'Bandipur', 'वन्दिपुर गाउँपालिका', 55, 4),
(434, 'Anbukhaireni', 'आँबुखैरेनी गाउँपालिका', 55, 4),
(435, 'Devghat', 'देवघाट गाउँपालिका', 55, 4),
(436, 'Rhising', 'ऋषिङ्ग गाउँपालिका', 55, 4),
(437, 'Ghiring', 'घिरिङ गाउँपालिका', 55, 4),
(438, 'Baudikali', 'बौदीकाली गाउँपालिका', 56, 4),
(439, 'Bulingtar', 'बुलिङटार गाउँपालिका', 56, 4),
(440, 'Hupsekot', 'हुप्सेकोट गाउँपालिका', 56, 4),
(441, 'Devchuli', 'देवचुली नगरपालिका', 56, 4),
(442, 'Gaidakot', '	गैडाकोट नगरपालिका', 56, 4),
(443, 'Kawasoti', 'कावासोती नगरपालिका', 56, 4),
(444, 'Madhyabindu', '	मध्यविन्दु नगरपालिका', 56, 4),
(445, 'Binayee Tribeni', 'विनयी त्रिवेणी गाउँपालिका', 56, 4),
(446, 'Kaligandaki', '	कालीगण्डकी गाउँपालिका', 57, 4),
(447, 'Galyang', 'गल्याङ नगरपालिका', 57, 4),
(448, 'Chapakot', 'चापाकोट नगरपालिका', 57, 4),
(449, 'Waling', '	वालिङ नगरपालिका', 57, 4),
(450, 'Biruwa', 'बिरुवा गाउँपालिका', 57, 4),
(451, 'Harinas', 'हरिनास गाउँपालिका', 57, 4),
(452, 'Bhirkot', '	भीरकोट नगरपालिका', 57, 4),
(453, 'Arjunchaupari', '	अर्जुनचौपारी गाउँपालिका', 57, 4),
(454, 'Putalibazar', 'पुतलीबजार नगरपालिका', 57, 4),
(455, 'Aandhikhola', 'आँधिखोला गाउँपालिका', 57, 4),
(456, 'Phedikhola', 'फेदीखोला गाउँपालिका', 57, 4),
(457, 'Jaljala', 'जलजला गाउँपालिका', 58, 4),
(458, 'Modi', 'मोदी गाउँपालिका', 58, 4),
(459, 'Kushma', '	कुश्मा नगरपालिका', 58, 4),
(460, 'Phalebas', 'फलेवास नगरपालिका', 58, 4),
(461, 'Mahashila', '	महाशिला गाउँपालिका', 58, 4),
(462, 'Bihadi', 'विहादी गाउँपालिका', 58, 4),
(463, 'Painyu', 'पैयूं गाउँपालिका', 58, 4),
(464, 'Nisikhola', '	निसीखोला गाउँपालिका', 59, 4),
(465, 'Dhorpatan', 'ढोरपाटन नगरपालिका', 59, 4),
(466, 'Taman khola', 'तमानखोला गाउँपालिका', 59, 4),
(467, 'Badigad', '	वडिगाड गाउँपालिका', 59, 4),
(468, 'Tara Khola', 'ताराखोला गाउँपालिका', 59, 4),
(469, 'Galkot', 'गल्कोट नगरपालिका', 59, 4),
(470, 'Kanthekhola', 'काठेखोला गाउँपालिका', 59, 4),
(471, 'Baglung', '	बागलुङ नगरपालिका', 59, 4),
(472, 'Jaimuni', '	जैमूनी नगरपालिका', 59, 4),
(473, 'Bareng', '	वरेङ गाउँपालिका', 59, 4),
(474, 'Dhaulagiri', '	धवलागिरी गाउँपालिका', 60, 4),
(475, 'Malika', 'मालिका गाउँपालिका', 60, 4),
(476, 'Raghuganga', 'रघुगंगा गाउँपालिका', 60, 4),
(477, 'Annapurna', 'अन्नपुर्ण गाउँपालिका', 60, 4),
(478, 'Mangala', '	मंगला गाउँपालिका', 60, 4),
(479, 'Beni', 'बेनी नगरपालिका', 60, 4),
(480, 'Geruwa', 'गेरुवा गाउँपालिका', 61, 5),
(481, 'Rajapur', 'राजापुर नगरपालिका', 61, 5),
(482, 'Thakurbaba', 'ठाकुरबाबा नगरपालिका', 61, 5),
(483, 'Madhuwan', '	मधुवन नगरपालिका', 61, 5),
(484, 'Barbardiya', '	बारबर्दिया नगरपालिका', 61, 5),
(485, 'Bansgadhi', 'बाँसगढी नगरपालिका', 61, 5),
(486, 'Gulariya', 'गुलरिया नगरपालिका', 61, 5),
(487, 'Badhaiyatal', 'बढैयाताल गाउँपालिका', 61, 5),
(488, 'Baijanath', 'बैजनाथ गाउँपालिका', 62, 5),
(489, 'Kohalpur', 'कोहलपुर नगरपालिका', 62, 5),
(490, 'Khajura', '	खजुरा गाउँपालिका', 62, 5),
(491, 'Janaki', '	जानकी गाउँपालिका', 62, 5),
(492, 'Nepalgunj', 'नेपालगंज उपमहानगरपालिका', 62, 5),
(493, 'Duduwa', 'डुडुवा गाउँपालिका', 62, 5),
(494, 'Rapti Sonari', 'राप्ती सोनारी गाउँपालिका', 62, 5),
(495, 'Narainapur', 'नरैनापुर गाउँपालिका', 62, 5),
(496, 'Babai', '	बबई गाउँपालिका', 63, 5),
(497, 'Shantinagar', '	शान्तिनगर गाउँपालिका', 63, 5),
(498, 'Dangisharan', 'दंगीशरण गाउँपालिका', 63, 5),
(499, 'Tulsipur', '	तुल्सीपुर उपमहानगरपालिका', 63, 5),
(500, 'Ghorahi', 'घोराही उपमहानगरपालिका', 63, 5),
(501, 'Lamahi', 'लमही नगरपालिका', 63, 5),
(502, 'Banglachuli', 'बंगलाचुली गाउँपालिका', 63, 5),
(503, 'Rapti', 'राप्ती गाउँपालिका', 63, 5),
(504, 'Rajpur', 'राजपुर गाउँपालिका', 63, 5),
(505, 'Gadhawa', 'गढवा गाउँपालिका', 63, 5),
(506, 'Gangadev', 'गंगादेव गाउँपालिका', 64, 5),
(507, 'Madi', 'माडी गाउँपालिका', 64, 5),
(508, 'Tribeni', '	त्रिवेणी गाउँपालिका', 64, 5),
(509, 'Runtigadhi', 'रुन्टीगढी गाउँपालिका', 64, 5),
(510, 'Sunil Smriti', 'सुनिल स्मृति गाउँपालिका', 64, 5),
(511, 'Lungri', '	लुङग्री गाउँपालिका', 64, 5),
(512, 'Rolpa', '	रोल्पा नगरपालिका', 64, 5),
(513, 'Sunchhahari', 'सुनछहरी गाउँपालिका', 64, 5),
(514, 'Thawang', 'थवाङ गाउँपालिका', 64, 5),
(515, 'Pariwartan', '	परिवर्तन गाउँपालिका', 64, 5),
(516, 'Sisne', '	सिस्ने गाउँपालिका', 65, 5),
(517, 'Bhume', 'भूमे गाउँपालिका', 65, 5),
(518, 'Putha Uttarganga', '	पुथा उत्तरगंगा गाउँपालिका', 65, 5),
(519, 'Naubahini', 'नौवहिनी गाउँपालिका', 67, 5),
(520, 'Gaumukhi', '	गौमुखी गाउँपालिका', 67, 5),
(521, 'Jhimruk', '	झिमरुक गाउँपालिका', 67, 5),
(522, 'Pyuthan', '	प्यूठान नगरपालिका', 67, 5),
(523, 'Mallarani', '	मल्लरानी गाउँपालिका', 67, 5),
(524, 'Swargadwari', '	स्वर्गद्वारी नगरपालिका', 67, 5),
(525, 'Mandavi', 'माण्डवी गाउँपालिका', 67, 5),
(526, 'Sarumarani', 'सरुमारानी गाउँपालिका', 67, 5),
(527, 'Ayirabati', 'ऐरावती गाउँपालिका', 67, 5),
(528, 'Malika', 'मालिका गाउँपालिका', 68, 5),
(529, 'Madane', '	मदाने गाउँपालिका', 68, 5),
(530, 'Dhurkot', 'धुर्कोट गाउँपालिका', 68, 5),
(531, 'Isma', 'ईस्मा गाउँपालिका', 68, 5),
(532, 'Musikot', '	मुसिकोट नगरपालिका', 68, 5),
(533, 'Resunga', 'रेसुङ्गा नगरपालिका', 68, 5),
(534, 'Gulmidarbar', 'गुल्मी दरबार गाउँपालिका', 68, 5),
(535, 'Chandrakot', '	चन्द्रकोट गाउँपालिका', 68, 5),
(536, 'Satyawati', 'सत्यवती गाउँपालिका', 68, 5),
(537, 'Kaligandaki', '	कालीगण्डकी गाउँपालिका', 68, 5),
(538, 'Chatrakot', '	छत्रकोट गाउँपालिका', 68, 5),
(539, 'RuruKshetra', 'रुरुक्षेत्र गाउँपालिका', 68, 5),
(540, 'Malarani', 'मालारानी गाउँपालिका', 69, 5),
(541, 'Bhumekasthan', 'भूमिकास्थान नगरपालिका', 69, 5),
(542, 'Sandhikharka', 'सन्धिखर्क नगरपालिका', 69, 5),
(543, 'Chhatradev', 'छत्रदेव गाउँपालिका', 69, 5),
(544, 'Panini', 'पाणिनी गाउँपालिका', 69, 5),
(545, 'Sitganga', 'शितगंगा नगरपालिका', 69, 5),
(546, 'Bijayanagar', 'विजयनगर गाउँपालिका', 70, 5),
(547, 'Shivaraj', 'शिवराज नगरपालिका', 70, 5),
(548, 'Budhdhabhumi', 'बुद्धभुमी नगरपालिका', 70, 5),
(549, 'Krishnanagar', '	कृष्णनगर नगरपालिका', 70, 5),
(550, 'Maharajgunj', '	महाराजगंज नगरपालिका', 70, 5),
(552, 'Banganga', '	बाणगंगा नगरपालिका', 70, 5),
(553, 'Yasodhara', '	यसोधरा गाउँपालिका', 70, 5),
(554, 'Mayadevi', 'मायादेवी गाउँपालिका', 70, 5),
(555, 'Sudhodhan', 'सुद्धोधन गाउँपालिका', 70, 5),
(556, 'Rainadevi Chhahara', 'रैनादेवी छहरा गाउँपालिका', 71, 5),
(557, 'Ridikot', 'रिब्दिकोट गाउँपालिका', 71, 5),
(558, 'Tansen', '	तानसेन नगरपालिका', 71, 5),
(559, 'Tinau', '	तिनाउ गाउँपालिका', 71, 5),
(560, 'Bagnaskali', '	बगनासकाली गाउँपालिका', 71, 5),
(561, 'Rambha', 'रम्भा गाउँपालिका', 71, 5),
(562, 'Mathagadhi', 'माथागढी गाउँपालिका', 71, 5),
(563, 'Purbakhola', '	पूर्वखोला गाउँपालिका', 71, 5),
(564, 'Rampur', '	रामपुर नगरपालिका', 71, 5),
(565, 'Nisdi', '	निस्दी गाउँपालिका', 71, 5),
(566, 'Sunwal', 'सुनवल नगरपालिका', 73, 5),
(567, 'Bardaghat', '	बर्दघाट नगरपालिका', 73, 5),
(568, 'Ramgram', 'रामग्राम नगरपालिका', 73, 5),
(569, 'Sarawal', 'सरावल गाउँपालिका', 73, 5),
(570, 'Palhi Nandan', 'पाल्हीनन्दन गाउँपालिका', 73, 5),
(571, 'Pratappur', '	प्रतापपुर गाउँपालिका', 73, 5),
(573, 'Sainamaina', '	सैनामैना नगरपालिका', 72, 5),
(574, 'Butwal', 'बुटवल उपमहानगरपालिका', 72, 5),
(575, 'Devdaha', '	देवदह नगरपालिका', 72, 5),
(576, 'Kanchan', 'कन्चन गाउँपालिका', 72, 5),
(577, 'Gaidahawa', 'गैडहवा गाउँपालिका', 72, 5),
(578, 'Sudhdhodhan', '	शुद्धोधन गाउँपालिका', 72, 5),
(579, 'Tilotam', 'तिलोत्तमा नगरपालिका', 72, 5),
(580, 'Siyari', '	सियारी गाउँपालिका', 72, 5),
(581, 'Om Satiya', 'ओमसतिया गाउँपालिका', 72, 5),
(582, 'Rohini', 'रोहिणी गाउँपालिका', 72, 5),
(583, 'Siddharthanagar', 'सिद्धार्थनगर नगरपालिका', 72, 5),
(584, 'Mayadevi', '	मायादेवी गाउँपालिका', 72, 5),
(585, 'Lumbini Sanskritik', '	लुम्बिनी सांस्कृतिक नगरपालिका', 72, 5),
(586, 'Kotahimai', 'कोटहीमाई गाउँपालिका', 72, 5),
(587, 'Sammarimai', '	सम्मरीमाई गाउँपालिका', 72, 5),
(588, 'Marchawari', 'मर्चवारी गाउँपालिका', 72, 5),
(589, 'Namkha', '	नाम्खा गाउँपालिका', 74, 6),
(590, 'Simkot', 'सिमकोट गाउँपालिका', 74, 6),
(591, 'Kharpunath', 'खार्पुनाथ गाउँपालिका', 74, 6),
(592, 'Sarkegad', 'सर्केगाड गाउँपालिका', 74, 6),
(593, 'Chankheli', '	चंखेली गाउँपालिका', 74, 6),
(594, 'Adanchuli', '	अदानचुली गाउँपालिका', 74, 6),
(595, 'Tanjakot', '	ताँजाकोट गाउँपालिका', 74, 6),
(596, 'Palata', 'पलाता गाउँपालिका', 75, 6),
(597, 'Sanni Tribeni', 'सान्नी त्रिवेणी गाउँपालिका', 75, 6),
(598, 'Raskot', 'रास्कोट नगरपालिका', 75, 6),
(599, 'Pachjharana', 'पचालझरना गाउँपालिका', 75, 6),
(600, 'Naraharinath', 'नरहरिनाथ गाउँपालिका', 75, 6),
(601, 'Khandachakra', 'खाँडाचक्र नगरपालिका', 75, 6),
(602, 'Tilugufa', '	तिलागुफा नगरपालिका', 75, 6),
(603, 'Shuva Kalika', 'शुभ कालीका गाउँपालिका', 75, 6),
(604, 'Mahawai', 'महावै गाउँपालिका', 75, 6),
(605, 'Soru', 'सोरु गाउँपालिका', 76, 6),
(606, 'Khatyad', '	खत्याड गाउँपालिका', 76, 6),
(607, 'Chhayanath Rara', 'छायाँनाथ रारा नगरपालिका', 76, 6),
(608, 'Mugum Kharmarong', 'मुगुम कार्मारोंग गाउँपालिका', 76, 6),
(609, 'Kanaka Sundari', 'कनकासुन्दरी गाउँपालिका', 77, 6),
(610, 'Sinja', 'सिंजा गाउँपालिका', 77, 6),
(611, 'Patrasi', '	पातारासी गाउँपालिका', 77, 6),
(612, 'Chandannath', 'चन्दननाथ नगरपालिका', 77, 6),
(613, 'Tila', '	तिला गाउँपालिका', 77, 6),
(614, 'Tatopani', '	तातोपानी गाउँपालिका', 77, 6),
(615, 'Guthichaur', 'गुठिचौर गाउँपालिका', 77, 6),
(616, 'Shey phoksundo', '	शे फोक्सुन्डो गाउँपालिका', 78, 6),
(617, 'Dolpo Buddha', '	डोल्पो बुद्ध गाउँपालिका', 78, 6),
(618, 'Jagadulla', 'जगदुल्ला गाउँपालिका', 78, 6),
(619, 'Mudkechula', '	मुड्केचुला गाउँपालिका', 78, 6),
(620, 'Tripurasundari', 'त्रिपुरासुन्दरी नगरपालिका', 78, 6),
(621, 'Thuli Bheri', 'ठूली भेरी नगरपालिका', 78, 6),
(622, 'Kaike', '	काईके गाउँपालिका', 78, 6),
(623, 'Chharka Tangsong', 'छार्का ताङसोङ गाउँपालिका', 78, 6),
(624, 'Aathbis', 'आठबीस नगरपालिका', 79, 6),
(625, 'Thantikandh', 'ठाँटीकाँध गाउँपालिका', 79, 6),
(626, 'Bhairabi', '	भैरवी गाउँपालिका', 79, 6),
(627, 'Mahabu', 'महावु गाउँपालिका', 79, 6),
(628, 'Chamunda Bindrasaini', 'चामुण्डा विन्द्रासैनी नगरपालिका', 79, 6),
(629, 'Dullu', 'दुल्लु नगरपालिका', 79, 6),
(630, 'Narayan', 'नारायण नगरपालिका', 79, 6),
(631, 'Naumule', 'नौमुले गाउँपालिका', 79, 6),
(632, 'Bhagawatimai', '	भगवतीमाई गाउँपालिका', 79, 6),
(633, 'Dungeshwor', 'डुंगेश्वर गाउँपालिका', 79, 6),
(634, 'Gurans', '	गुराँस गाउँपालिका', 79, 6),
(635, 'Chaukune', 'चौकुने गाउँपालिका', 80, 6),
(636, 'Panchapuri', 'पञ्चपुरी नगरपालिका', 80, 6),
(637, 'Barahtal', 'बराहताल गाउँपालिका', 80, 6),
(638, 'Birendranagar', 'बीरेन्द्रनगर नगरपालिका', 80, 6),
(639, 'Lekbeshi', 'लेकवेशी नगरपालिका', 80, 6),
(640, 'Chingad', '	चिङ्गाड गाउँपालिका', 80, 6),
(641, 'Simta', '	सिम्ता गाउँपालिका', 80, 6),
(642, 'Bheriganga', 'भेरीगंगा नगरपालिका', 80, 6),
(643, 'Gurbhakot', 'गुर्भाकोट नगरपालिका', 80, 6),
(644, 'Kalimati', 'कालिमाटी गाउँपालिका', 81, 6),
(645, 'Tribeni', '	त्रिवेणी गाउँपालिका', 81, 6),
(646, 'Kapurkot', 'कपुरकोट गाउँपालिका', 81, 6),
(647, 'Chhatraeshwori', '	छत्रेश्वरी गाउँपालिका', 81, 6),
(648, 'Sharada', '	शारदा नगरपालिका', 81, 6),
(649, 'Siddha Kumakh', '	सिद्ध कुमाख गाउँपालिका', 81, 6),
(650, 'Bagchaur', 'बागचौर नगरपालिका', 81, 6),
(651, 'Darma', '	दार्मा गाउँपालिका', 81, 6),
(652, 'Kumakh', 'कुमाख गाउँपालिका', 81, 6),
(653, 'Bangad Kupinde', 'बनगाड कुपिण्डे नगरपालिका', 81, 6),
(654, 'Athbiskot', 'आठबिसकोट नगरपालिका', 82, 6),
(655, 'Banfikot', 'बाँफिकोट गाउँपालिका', 82, 6),
(656, 'Sani Bheri', 'सानी भेरी गाउँपालिका', 82, 6),
(657, 'Musikot', '	मुसिकोट नगरपालिका', 82, 6),
(658, 'Chaurjahari', '	चौरजहारी नगरपालिका', 82, 6),
(659, 'Tribeni', 'त्रिवेणी गाउँपालिका', 82, 6),
(660, 'Barekot', 'बारेकोट गाउँपालिका', 83, 6),
(661, 'Nalgad', '	नलगाड नगरपालिका', 83, 6),
(662, 'Kuse', '	कुसे गाउँपालिका', 83, 6),
(663, 'Junichande', 'जुनीचाँदे गाउँपालिका', 83, 6),
(664, 'Chhedagad', '	छेडागाड नगरपालिका', 83, 6),
(665, 'Bheri', 'भेरी नगरपालिका', 83, 6),
(666, 'Siwalaya', '	शिवालय गाउँपालिका', 83, 6),
(667, 'Byas', 'ब्याँस गाउँपालिका', 84, 7),
(668, 'Apihimal', 'अपिहिमाल गाउँपालिका', 84, 7),
(669, 'Dunhu', 'दुहुँ गाउँपालिका', 84, 7),
(670, 'Mahakali', '	महाकाली नगरपालिका', 84, 7),
(671, 'Naugad', 'नौगाड गाउँपालिका', 84, 7),
(672, 'Marma', 'मार्मा गाउँपालिका', 84, 7),
(673, 'MalikaArjun', 'मालिकार्जुन गाउँपालिका', 84, 7),
(674, 'Shailesikhar', 'शैल्यशिखर नगरपालिका', 84, 7),
(675, 'Lekam', 'लेकम गाउँपालिका', 84, 7),
(676, 'Dilasaini', 'डीलासैनी गाउँपालिका', 85, 7),
(677, 'Purchaudi', 'पुर्चौडी नगरपालिका', 85, 7),
(678, 'Dogadakedar', 'दोगडाकेदार गाउँपालिका', 85, 7),
(679, 'Surnaya', 'सुर्नया गाउँपालिका', 85, 7),
(680, 'Patan', 'पाटन नगरपालिका', 85, 7),
(681, 'Sigas', '	सिगास गाउँपालिका', 85, 7),
(682, 'Dashrathchanda', 'दशरथचन्द नगरपालिका', 85, 7),
(683, 'Pancheshwor', 'पञ्चेश्वर गाउँपालिका', 85, 7),
(684, 'Melauli', 'मेलौली नगरपालिका', 85, 7),
(685, 'Shivanath', 'शिवनाथ गाउँपालिका', 85, 7),
(686, 'Sailpal', 'साइपाल गाउँपालिका', 86, 7),
(687, 'Surma', 'सूर्मा गाउँपालिका', 86, 7),
(688, 'Talikot', 'तलकोट गाउँपालिका', 86, 7),
(689, 'Bungal', 'बुंगल नगरपालिका', 86, 7),
(690, 'Bithadchir', '	वित्थडचिर गाउँपालिका', 86, 7),
(691, 'Kedarseu', 'केदारस्युँ गाउँपालिका', 86, 7),
(692, 'Durgathali', '	दुर्गाथली गाउँपालिका', 86, 7),
(693, 'Jayaprithivi', 'जयपृथ्वी नगरपालिका', 86, 7),
(694, 'Masta', 'मष्टा गाउँपालिका', 86, 7),
(695, 'Chabispathivera', '	छबिसपाथिभेरा गाउँपालिका', 86, 7),
(696, 'Thalara', 'थलारा गाउँपालिका', 86, 7),
(697, 'Khaptadchhanna', '	खप्तडछान्ना गाउँपालिका', 86, 7),
(698, 'Himali', 'हिमाली गाउँपालिका', 87, 7),
(699, 'Gaumul', 'गौमुल गाउँपालिका', 87, 7),
(700, 'Budhinand', '	बुढीनन्दा नगरपालिका', 87, 7),
(701, 'Badimalika', '	बडीमालिका नगरपालिका', 87, 7),
(702, 'Jagannath', 'जगन्‍नाथ गाउँपालिका', 87, 7),
(703, 'Tribeni', 'त्रिवेणी नगरपालिका', 87, 7),
(704, 'Budhiganga', 'बुढीगंगा नगरपालिका', 87, 7),
(705, 'Khapdad Chhededaha', 'खप्तड छेडेदह गाउँपालिका', 87, 7),
(706, 'Ajaymeru', 'अजयमेरु गाउँपालिका', 88, 7),
(707, 'Nawadurga', 'नवदुर्गा गाउँपालिका', 88, 7),
(708, 'Amargadhi', '	अमरगढी नगरपालिका', 88, 7),
(709, 'Bhageshwar', 'भागेश्वर गाउँपालिका', 88, 7),
(710, 'Alital', 'आलिताल गाउँपालिका', 88, 7),
(711, 'Parashuram', 'परशुराम नगरपालिका', 88, 7),
(712, 'Sayal', 'सायल गाउँपालिका', 89, 7),
(713, 'Adharsha', 'आदर्श गाउँपालिका', 89, 7),
(714, 'Purbichauki', '	पूर्वीचौकी गाउँपालिका', 89, 7),
(715, 'Dipayal silgadi', 'दिपायल सिलगढी नगरपालिका', 89, 7),
(716, 'Shikhar', 'शिखर नगरपालिका', 89, 7),
(717, 'K.I. Singh', 'के.आई.सिं. गाउँपालिका', 89, 7),
(718, 'Jorayal', 'जोरायल गाउँपालिका', 89, 7),
(719, 'Bandikedar', 'बडीकेदार गाउँपालिका', 89, 7),
(720, 'Bogatan Phudsil', 'बोगटान फुड्सिल गाउँपालिका', 89, 7),
(721, 'Sanphebagar', 'साँफेबगर नगरपालिका', 90, 7),
(722, 'Mallekh', '	मेल्लेख गाउँपालिका', 90, 7),
(723, 'Chaurapati', '	चौरपाटी गाउँपालिका', 90, 7),
(724, 'Bannigadhi Jayagadh', 'बान्निगढी जयगढ गाउँपालिका', 90, 7),
(725, 'Ramroshan', 'रामारोशन गाउँपालिका', 90, 7),
(726, 'Mangalsen', '	मंगलसेन नगरपालिका', 90, 7),
(727, 'Kamalbazar', '	कमलबजार नगरपालिका', 90, 7),
(728, 'Panchadewai Binayak', 'पन्चदेवल विनायक नगरपालिका', 90, 7),
(729, 'Dhakari', '	ढकारी गाउँपालिका', 90, 7),
(730, 'Turmakhad', '	तुर्माखाँद गाउँपालिका', 90, 7),
(731, 'Bhimadatta', 'भीमदत्त नगरपालिका', 91, 7),
(732, 'Bedkot', 'वेदकोट नगरपालिका', 91, 7),
(733, 'Shuklaphanta', 'शुक्लाफाँटा नगरपालिका', 91, 7),
(734, 'Krishnapur', 'कृष्णपुर नगरपालिका', 91, 7),
(735, 'Beldandi', '	बेलडाडी गाउँपालिका', 91, 7),
(736, 'Lajhadi', 'लालझाडी गाउँपालिका', 91, 7),
(737, 'Belauri', '	बेलौरी नगरपालिका', 91, 7),
(738, 'Purnabas', 'पुर्नवास नगरपालिका', 91, 7),
(739, 'Chure', '	चुरे गाउँपालिका', 92, 7),
(740, 'Godawari', 'गोदावरी नगरपालिका', 92, 7),
(741, 'Dhangadi', 'धनगढी उपमहानगरपालिका', 92, 7),
(742, 'Gauriganga', 'गौरीगंगा नगरपालिका', 92, 7),
(743, 'Kailari', 'कैलारी गाउँपालिका', 92, 7),
(744, 'Bhajani', '	भजनी नगरपालिका', 92, 7),
(745, 'Tikapur', '	टिकापुर नगरपालिका', 92, 7),
(746, 'Janaki', 'जानकी गाउँपालिका', 92, 7),
(747, 'Joshipur', 'जोशीपुर गाउँपालिका', 92, 7),
(748, 'Ghodaghodi', 'घोडाघोडी नगरपालिका', 92, 7),
(749, 'Bardagoriya', 'बर्दगोरिया गाउँपालिका', 92, 7),
(750, 'Lamkichuha', 'लम्कीचुहा नगरपालिका', 92, 7),
(751, 'Mohanyal', 'मोहन्याल गाउँपालिका', 92, 7),
(752, 'Chishankhugadhi Rural Municipal\r\n', 'चिशंखुगढी गाउँपालिका', 13, 1),
(753, 'Thulung Dudhkoshi Rural Municipal\r\n', 'थुलुङ दुधकोशी गाउँपालिका', 14, 1),
(754, 'Parwanipur Rural Municipality\r\n', 'परवानीपुर गाउँपालिका', 16, 2),
(755, 'Madhav Narayan Municipality', 'माधव नारायण नगरपालिका', 17, 2),
(756, 'Dhankaul Rural Municipality', 'धनकौल गाउँपालिका', 18, 2),
(757, 'Lalbandi Municipality', 'लालबन्दी नगरपालिका', 18, 2),
(758, 'Pipara Rural Municipality', 'पिपरा गाउँपालिका', 19, 2),
(759, 'Sakhuwanankarkatti Ruralmunicipality', 'सखुवानान्कारकट्टी गाउँपालिका', 21, 2),
(760, 'Bishnupur Ruralmunicipality', 'बिष्णुपुर गाउँपालिका', 22, 2),
(761, 'DhurniBesi Municipality', 'धुनीबेंशी नगरपालिका', 41, 3),
(762, 'Bethanchowk Ruralmunicipality', 'बेथानचोक गाउँपालिका', 46, 3),
(763, 'Kapilvastu Municipality', 'कपिलवस्तु नगरपालिका', 70, 5),
(764, 'Susta Rural Municipality', 'सुस्ता गाउँपालिका', 73, 5),
(765, 'HIma Rural Municipality', '	हिमा गाउँपालिका', 77, 6),
(766, 'Swamikartik khapar Rural Municipality', '	स्वामीकार्तिक खापर गाउँपालिका', 87, 7),
(767, 'GanyaPadhura Rural Municipality', 'गन्यापधुरा गाउँपालिका', 88, 7),
(768, 'Mahakali Municipality', 'महाकाली नगरपालिका', 91, 7);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `date` varchar(20) NOT NULL,
  `title` varchar(100) NOT NULL,
  `image` varchar(250) NOT NULL,
  `description` mediumtext NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `fk_user_id`, `date`, `title`, `image`, `description`, `created_date`, `status`) VALUES
(1, 1, '२०७९-०२-२२', 'रुपन्देही उद्योग संघद्वारा संघसंस्था अवलोकन र आन्तरिक पर्यटन प्रवद्र्धन भ्रमण सम्पन्न औद्योगिक र पर्', 'img/62b809cd050ad.jpg', 'बुटवल, २२ जेठ\r\n                रुपन्देही उद्योग संघको एक टोलीले बार्षिक कार्यक्रम अन्तरगत जेष्ठ २० र २१ गते संघसंस्था अवलोकन भ्रमण सम्पन्न गरेको छ ।\r\n                संघका अध्यक्ष माधव प्रसाद पौडेलको नेतृत्वमा गएको टोलीले छिमेकी जिल्ला कपिलवस्तु र अर्घाखाँचीको संघसंस्था, धार्मिक एर्व पर्यटकीय क्षेत्रहरुको तथा पर्यटन अवलोकन भ्रमण गरेको हो । भ्रमणका क्रममा त्यहाँ रहेका उद्योगी व्यवसायीहरुसँग सामुहिक छलफल तथा अन्तरक्रिया गरिएको संघका महासचिव हरि बहादुर खत्रीले बताउनु भयो ।\r\n                भ्रमणको पहिलो दिन कपिलबस्तु उद्योग वाणिज्य संघ जितपुरको आयोजनामा अन्तरक्रियात्मक कार्यक्रम गरिएको थियो । संघको सचिवालयमा आयोजित स्वागत तथा सम्मान कार्यक्रममा बोल्ने वक्ताहरुले संस्थागत रुपमा गरिने भ्रमण तथा अवलोकन कार्यक्रमले व्यवसायिक अनुभव साटासाट गर्न तथा सहयोग आदान प्रदान गर्न महत्वपूर्ण भूमिका खेल्ने विश्वास व्यक्त गरेका थिए । बक्ताहरुले यस क्षेत्रको आर्थिक विकासका लागि औद्योगिक विकास हुनु जरुरी भएको कुरामा जोड दिदै कपिलवस्तुमा औद्योगिक ग्राम सञ्चालन गर्न र पर्यटन उद्योगलाई बलियो बनाउन पहल थाल्न आग्रह पनि गरिएको थियो ।\r\n                सो अवसरमा बोल्दै रुपन्देही उद्योग संघका अध्यक्ष माधव प्रसाद पौडेलले सरकारले औद्योगिक विकासका लागि नीति र कार्यक्रम तथा वजेटमा समावेश गरेका सहुलियतहरु कार्यान्वयनमा ल्याउन सकेमा औद्योगिक बातावरण बन्ने हुँदा त्यसमा ध्यान दिन आग्रह गर्नु भयो । उहाँले रुपन्देही र कपिलवस्तु पछिल्लो समय मुलुक कै आर्थिक केन्द्रका रुपमा विकसित हँुदै गएको हुँदा यस क्षेत्रमा उद्योगको स्थापना र पर्यटनको विकासमा स्थानीय सरकारहरुलाई मिलेर काम गर्ने वातावरण तयार गर्न सुझाव दिनु भयो ।\r\n                संघका अध्यक्ष परिषद्का संयोजक एवं लुम्बिनी प्रदेश उद्योग परिसंघका अध्यक्ष एजाज आलमले स्थानीय आवश्यकता र उपभोक्ताहरुको चाहनालाई ध्यानमा राखेर आर्थिक गतिविधि गर्न सकियो भने संमृद्धिको लक्ष्य प्राप्त हुने विश्वास व्यक्त गर्नु भयो । उहाँले काम गर्ने दौरानमा खुट्टा तान्ने र विरोध गर्ने प्रवृति हाम्रो समाजमा धेरै छ तर इमानदारिताका साथ काम गरिएको खण्डमा त्यस्ता प्रवृतिहरु पछाडी पर्ने बताउनु भयो ।\r\n                कार्यक्रममा कपिलवस्तु उद्योग वाणिज्य संघका निवर्तमान अध्यक्ष उमानाथ पौडेलले संस्थागत परिचय दिदै कपिलवस्तुमा आर्थिक विकासका धेरै सम्भावना रहेको उल्लेख गर्दै त्यसका लागि सहकार्य, सहमती र कार्यक्रम जरुरी रहेको हुँदा रुपन्देही उद्योग संघसंग सहकार्यको प्रस्ताव राख्नु भयो ।\r\n                कपिलबस्तु उद्योग वाणिज्य संघका अध्यक्ष डोलकराम घिमिरेले संघले स्थापनाको छोटो समयमा केही महत्वपूर्ण कामहरु गरेको जानकारी गराउँदै आर्थिक विकास र औद्योगिकिकरणका लागि आफुहरुले दिन सक्ने योगदान दिन पछाडी नपर्ने बताउनु भयो ।\r\n                कार्यक्रममा रुपन्देही उद्योग संघका निवर्तमान अध्यक्ष बाबुराम बोहोरा, पूर्व अध्यक्ष प्रद्युम्मलाल श्रेष्ठ, वोनिन पिया, सल्लाहाकार भुपाल कुमार श्रेष्ठ र ऋषि भण्डारी अवलोकन भ्रमण कार्यक्रमका संयोजक तथा संघका वरिष्ठ उपाध्यक्ष दिलिप सापकोटा, कपिलबस्तु उद्योग वाणिज्य संघका उद्योग उपाध्यक्ष तुल्सीराम घिमिरे लगायतले बोल्नु भएको थियो ।\r\n                कपिलवस्तुको छलफल कार्यक्रम पछि भ्रमण टोली अर्घाखाँचीको धार्मिक पर्यटकिय स्थल नरपानी तथा सुपादेउरालीको अवलोकन गर्न गएको थियो । त्यस अवसरमा नरपानी क्षेत्रको पर्यटकिय विकाससँगै त्यहाँ रहेको चुनढुङा खानीको अवस्थाका बारेमा स्थानियहरुसँग जानकारी लिएको संघका प्रमुख कार्यकारी अधिकृत यादव प्रसाद भण्डारीले जानकारी गराउनु भयो ।', '2022-06-24 16:17:21', 1),
(3, 1, '2079-03-12', 'रुपन्देही उद्योग संघले बुझायो विद्युत प्राधिकारणलाई ज्ञापन पत्र विद्युत अनियमितता र ट्रिपिङको समस्या', 'img/62b80f98a848a.jfif', 'मितिः २०७९/१/१४\r\n\r\nबुटवल, १४ बैशाख\r\nरुपन्देही उद्योग संघले विगत केही दिनदेखि उद्योग क्षेत्रमा विद्युत आपूर्तिमा देखिएको अनियमितता र ट्रिपिङको समस्या समाधान गर्न आग्रह गर्र्दै बुधवार नेपाल विद्युत प्राधिकारण वितरण तथा ग्राहक सेवा निर्देशानालय, लुम्बिनी प्रदेश कार्यालय बुटवलमा ज्ञापन पत्र बुझाएको छ ।\r\nसंघका अध्यक्ष माधव प्रसाद पौडेलको नेतृत्वमा गएको संघका पदाधिकारी तथा लुम्बिनी कोरीडोर क्षेत्रका उद्योगीहरु सहितको प्रतिनिधि मण्डलले प्राधिकरणका प्रदेश प्रमुख निर्देशक ई.नवराज ओझालाई ज्ञापनपत्र बुझाउदै विशेष गरी लुम्बिनी कोरीडोर क्षेत्रमा रहेका उद्योग र जिल्ला भरिका उ्रद्योगहरुमा देखिएको विद्युत आपूर्तिको समस्या तत्काल समाधान गर्न अनुरोध गरेको छ ।\r\nपत्र बुझाउँदै अध्यक्ष पौडेलले प्राधिकरणले विना सुचना विद्युत आपूर्ति बन्द गर्ने र नियमित विद्युत नदिएका कारण उद्योगहरुले ठूलो क्षति व्यहोर्नु परिरेहको जनाउँदै विद्युत वितरणमा भैरहेको समस्यालाई तत्काल समाधान गर्न आग्रह गर्दै उद्योग धराशाही बनाउने काम नगर्न सुझाव दिनु भयो । उहाँले विद्युत प्रधिकरणले समय तालिका बनाएर उद्योग क्षेत्रको विद्युत आपूर्ति नियमित गर्नुपर्ने माग राख्दै समस्या सामाधानमा छिटै कदम नचालिए संघ चरणबद्ध आन्दोलन गर्न बाध्य हुने चेतावनी पनि दिनुभयो ।\r\nसो अवसरमा संघका निवर्तमान अध्यक्ष बाबुराम बोहोराले प्राधिकरणको नेतृत्वको असक्षमताका कारण पटकपटक उद्योगीहरुले पिडा भोग्नु परेको बताउँदै हाल विद्युत आपूर्तिमा देखिएको समस्या नियोजित भएको बताउनु भयो ।\r\nलुम्बिनी कोरीडोर स्थित जगदम्बा सिमेन्ट उद्योगका महाप्रवन्धक विदुर ढुङ्गानाले नेपाल विद्यूत प्राधिकारण सधै उद्योगलाई सताउने र धाराशाही बनाउने कुरामा उद्दत हँुदै आएको अनुभूति आफुहरुलाई भएको बताउनु भयो । उहाले विद्युत आपूर्तिको समस्या समाधान गर्न ढिलाई गरिए आफुहरु उद्योगमा ताला लगाएर सडकमा आउने चेतावनी दिनु भयो ।\r\nज्ञापन पत्र बुझ्नुहुँदै प्राधिकरणका प्रदेश प्रमुख निर्देशक ई.नवराज ओझाले विद्युतको उत्पादन र आयातमा आएको समस्याका कारण वितरणमा समस्या देखिएको बताउँदै समस्या समाधान गर्न आफुले सक्दो प्रयास गर्ने प्रतिवद्धता व्यक्त गर्नु भएको थियो । उहाँले समय तालिका बनाएर विद्युत वितरण गर्न वा उद्योगमा विद्युत प्रवाह गर्न प्राविधिक टोलीसँग परामर्श गरी तत्काल उपाय खोज्ने आश्वासन दिनु भएको थियो ।  \r\nप्रतिनिधि मण्डलमा संघका वरिष्ठ उपाध्यक्ष दिलिप सापकोटा, प्रथम उपाध्यक्ष कृष्ण प्रसाद पराजुली, महासचिव हरि बहादुर खत्री, कोषाध्यक्ष हिरामणी भट्टराई, महिला सचिव मञ्जु भुसाल, सहकोषाध्यक्ष सुरेन्द्र ज्ञवाली, कासस लाल प्रसाद वस्याल, अशोक कुँवर, संघका प्रमुख कार्यकारी अधिकृत यादव प्रसाद भण्डारी, उद्योगीहरु जगदम्बा स्पेनिङ्गका चिरञ्जिवी रायमाझी, वृज सिमेन्टका रबि रञ्जित गुप्ता, अम्बुजा सिमेन्टका राजकुमार गुप्ता, गोयनका स्टिलका अमित गोयनका लगायतको सहभागिता रहेको थियो ।', '2022-06-26 07:49:44', 1),
(4, 1, '2079-03-11', '५ दिने विजनेश स्टार्टअप तालिम समापन स्वरोजगारमुलक उद्यमको विकास गर्न आग्रह', 'img/62b824abdc1c9.jfif', 'मितिः २०७८/१२/२१\r\n\r\nबुटवल, २५ फागुन–\r\nरुपन्देही उद्योग संघको आयोजनामा सञ्चालित ५ दिने विजनेश स्टार्टअप तालिम शुक्रवार एक समारोहकावीच समापन गरिएको छ ।\r\nतालिमको समापन गर्दै कार्यक्रमका प्रमुख अतिथि बुटवल उपमहानगरपालिकाका प्रमुख शिवराज सुवेदीले राजनीतिक परिवर्तनको लागि अब युवा पुस्ताले समय खर्चने बेला नरहेको उल्लेख गर्दै आर्थिक समृद्धीका लागि उद्यमको विकास गर्न आग्रह गर्नु भयो । उहाँले कुनै पनी मुलुकको आर्थिक विकास त्यसको लघु तथा स्वरोजगारको विकासले मात्र सम्भव हुने हँुदा त्यसका लागि स्थानीय सरकार र नीजि क्षेत्रवीचको सहकार्यमा जोड दिनु भयो ।\r\nप्रमुख अतिथि सुवेदीले स्थानिय उत्पादन र श्रमको सहि उपयोग हुने गरी गरिने उद्यमले समग्र राष्ट्रको उन्नतीका लागि आधार तयार गर्ने हु्ँदा त्यसतर्पm तालिममा सहभागीहरुको ध्यान जानु पर्ने बताउनु भयो । उहाँले स्थानीय सरकारहरु उद्यमको विकास र सिप क्षमता अभिबृद्धि गर्न सधै सहकार्य गर्दै जाने प्रतिवद्धता व्यक्त गर्नु भयो ।', '2022-06-26 09:19:39', 1),
(5, 1, '2079-03-15', 'कर दाखिला सम्बन्धी रुपन्देही उद्योग संघको प्रेस विज्ञप्ती', 'img/62b95acbac6a3.jpg', 'मितिः२०७९ असार १३\r\n\r\nकोरोना भाइरसको विश्वव्यापी असरले आक्रान्त बनेको नेपालको अर्थतन्त्र र विशेषगरी औद्योगिक क्षेत्र दिनानुदिन धरासायी बन्दै गइरहेको छ । लामो समयको लकडाउनले उद्योगी व्यवसायी थिलो परिसकेका छन् । सरकारले कुनै ठोस कार्यविधि र संचालनको वातावरण तयारी विनै ४२ खालका उद्योगहरु संचालन गर्ने निर्णय गरिरहँदा उद्योगीहरुको वास्तविक अवस्थाबारे कुनै ध्यानकर्षण गर्न सकेन । बैंकको ब्याज, बजार बन्द र श्रम समस्याका बीच उद्योग संचालन गर्ने घोषणा गरेर सोही बहानामा राज्यले कर असुल्ने नीति अवलम्वन गरेको हो भने सो निर्णय अत्यन्तै घातक भएको रुपन्देही रुपन्देही उद्योग संघको ठहर छ । यस्तो विषम परिस्थतिमा सरकारले कर बुझाउन निर्देशन गर्ने,  बैंकले ब्याज ताकेता गर्ने र विद्युत प्राधिकरणले महसुल असुल्न खोज्नुले सरकारको निजी क्षेत्र प्रतिको धारणा अत्यन्त नकारात्मक रहेको स्पष्ट भएको छ । उद्योगी व्यवसायीका समस्या सन्दर्भमा ठोस राहत प्याकेज आउन नसकेको सन्दर्भमा सरकारको बजेट मार्फत केही गुनासाहरु सम्बोधन भएता पनि सोको कार्यान्वयन प्रति विश्वस्त हुने आधार अझैसम्म तय हुन सकेको छैन । तसर्थः कर दाखिला गर्ने समय लकडाउन समाप्ती पछिको कम्तिमा ३ महिना थप गर्न र आयकर, मूल्य अभिबृद्धि कर, अन्तशुल्क विवरण बुझाउने समय लक डाउन खुलेको एक महिना थप गर्नुहुन सम्बन्धित निकायहरुसँग रुपन्देही उद्योग संघ जोडदार माग गर्दछ । ', '2022-06-27 07:22:51', 1);

-- --------------------------------------------------------

--
-- Table structure for table `objective`
--

CREATE TABLE `objective` (
  `id` int(11) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `title` varchar(50) CHARACTER SET utf8 NOT NULL,
  `objective` varchar(2000) CHARACTER SET utf8 NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `objective`
--

INSERT INTO `objective` (`id`, `fk_user_id`, `title`, `objective`, `created_date`, `status`) VALUES
(1, 1, 'रोजगार सूचना केन्द्र संचालनको उद्देश्य:', '<div>\r\n<div>उद्योगहरुको व्यवस्थापन सम्बन्धीसिपको विकास र विस्तार ।&nbsp;</div>\r\n<div>तालिमगोष्ठी, कार्यशालाको आयोजना ।&nbsp;</div>\r\n<div>विभिन्न राष्ट्रिय तथा अन्तर्राष्ट्रिय संघसंस्थासँग आपसी सम्बन्ध स्थापना ।&nbsp;</div>\r\n<div>नयाँ औद्योगिकक्षेत्रको स्थापना ।&nbsp;</div>\r\n<div>रोजगार मूलक कार्यक्रम संचालन ।&nbsp;</div>\r\n<div>स्वदेशी उद्योगको स्थापनामा जोड ।&nbsp;</div>\r\n<div>स्थानीय स्तरमा उपलब्ध श्रोत र साधनकोपहिचान ।&nbsp;</div>\r\n<div>संघ र उद्योगीहरुको संस्थागत विकासमा जोड ।&nbsp;</div>\r\n<div>उद्योगिहरुको आर्थिक स्तरमा बृद्धिगर्ने र राष्ट्रिय अर्थतन्त्रमा टेवापुरयाउने ।&nbsp;</div>\r\n<div>सरकारी एवं गैरसरकारी निकायहरुसँग सहकार्य ।&nbsp;</div>\r\n<div>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>\r\n</div>', '2022-06-17 13:34:58', 1);

-- --------------------------------------------------------

--
-- Table structure for table `organization`
--

CREATE TABLE `organization` (
  `id` int(11) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `org_name` varchar(100) NOT NULL,
  `pan_no` varchar(20) NOT NULL,
  `ownership` int(11) NOT NULL,
  `size_of_org` int(11) NOT NULL,
  `reg_no` varchar(20) NOT NULL,
  `working_capital` decimal(20,2) DEFAULT NULL,
  `fixed_capital` decimal(20,2) DEFAULT NULL,
  `main_production` varchar(100) DEFAULT NULL,
  `fk_province` int(11) NOT NULL,
  `fk_district` int(11) NOT NULL,
  `fk_mun` int(11) NOT NULL,
  `fk_ward` int(11) NOT NULL,
  `tole` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `org_email` varchar(50) NOT NULL,
  `telephone` varchar(15) DEFAULT NULL,
  `org_contact` varchar(15) NOT NULL,
  `contact_person` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `contact` varchar(15) NOT NULL,
  `facebook` varchar(100) DEFAULT NULL,
  `twitter` varchar(100) DEFAULT NULL,
  `about` text NOT NULL,
  `submit_date` date DEFAULT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `update_date` date DEFAULT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organization`
--

INSERT INTO `organization` (`id`, `fk_user_id`, `org_name`, `pan_no`, `ownership`, `size_of_org`, `reg_no`, `working_capital`, `fixed_capital`, `main_production`, `fk_province`, `fk_district`, `fk_mun`, `fk_ward`, `tole`, `city`, `org_email`, `telephone`, `org_contact`, `contact_person`, `email`, `contact`, `facebook`, `twitter`, `about`, `submit_date`, `created_date`, `update_date`, `status`) VALUES
(7, 8, 'Bottlers nepal', '560123', 1, 2, '1234', '10000000.00', '1000000.00', 'Plastic Bottles ', 5, 72, 574, 4, 'Jesis chowk', 'Butwal', 'Bottlersnefol12@gmail.com', '07123432', '07123432', 'kamal ', 'Bottlersnefol123@gmail.com', '9866132080', 'kamal pandey', 'kamal pandey', 'Search For Professional Bio Example. Smart Results Here. SearchStartNow. Search Everything You Need. Save Time & Get Quick Results. Find Fast. Search Smarter. Find More. Multi Search. Search Efficiently. Better Results. Useful Info. Find Right Now.', '2079-03-13', '2022-06-27 06:57:02', NULL, 1),
(8, 8, 'Mahalaxmi agro firm', '10001', 2, 1, '12366', '100000000.00', '10000000.00', 'Agriculture products', 5, 73, 764, 4, 'narayani ', 'aarunkhola ', 'mahalaxmi123@gmail.com', '071574657', '071234212', 'Laxman pandey', 'laxmanpandey145@gmail.com', '9861378682', 'Laxman pandey', 'laxu003', 'this is our firm .', '2079-03-13', '2022-06-27 10:56:10', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `org_type`
--

CREATE TABLE `org_type` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(200) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `fk_org_id` int(11) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ownership`
--

CREATE TABLE `ownership` (
  `id` int(11) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ownership`
--

INSERT INTO `ownership` (`id`, `fk_user_id`, `title`, `created_date`, `status`) VALUES
(1, 1, 'Government', '2022-06-07 11:03:39', 1),
(2, 1, 'Private', '2022-06-07 11:05:44', 1),
(3, 1, 'Public', '2022-06-07 11:05:55', 1),
(4, 1, 'Non-Profit', '2022-06-07 11:06:07', 1);

-- --------------------------------------------------------

--
-- Table structure for table `province`
--

CREATE TABLE `province` (
  `id` int(11) NOT NULL,
  `province` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `province_nepali` varchar(250) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `province`
--

INSERT INTO `province` (`id`, `province`, `province_nepali`) VALUES
(1, 'Province 1', 'प्रदेश नं १'),
(2, 'Province 2', 'प्रदेश नं २'),
(3, 'Province 3', 'बाग्मती प्रदेश'),
(4, 'Province 4', 'गण्डकी प्रदेश'),
(5, 'Province 5', 'लुम्बिनी प्रदेश '),
(6, 'Province 6', 'कर्णाली प्रदेश'),
(7, 'Province 7', ' सुदुरपश्चिम प्रदेश');

-- --------------------------------------------------------

--
-- Table structure for table `qualification`
--

CREATE TABLE `qualification` (
  `id` int(11) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `fk_org_id` int(11) NOT NULL,
  `qualification` varchar(50) NOT NULL,
  `university_name` varchar(100) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `qualification`
--

INSERT INTO `qualification` (`id`, `fk_user_id`, `fk_org_id`, `qualification`, `university_name`, `created_date`, `status`) VALUES
(2, 1, 1, 'Higher secondary', 'TU', '2022-06-19 07:44:31', 1),
(4, 1, 1, 'Slc', 'school', '2022-06-23 09:35:13', 1),
(5, 1, 1, 'Bachelor degree', 'Pu , Tu ', '2022-06-23 09:36:02', 1),
(6, 1, 1, 'Master degree', '..', '2022-06-26 06:37:55', 1),
(7, 1, 1, 'PHD', 'Pokhara university', '2022-06-26 06:38:32', 1),
(8, 1, 1, 'Simple reading', '..', '2022-06-26 06:41:22', 1),
(9, 1, 1, 'Above 8 ', '..', '2022-06-26 06:41:57', 1),
(10, 1, 1, 'Below 8', '..', '2022-06-26 06:42:16', 1);

-- --------------------------------------------------------

--
-- Table structure for table `salary_type`
--

CREATE TABLE `salary_type` (
  `id` int(11) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `fk_org_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salary_type`
--

INSERT INTO `salary_type` (`id`, `fk_user_id`, `fk_org_id`, `title`, `created_date`, `status`) VALUES
(1, 1, 1, 'Monthly', '2022-06-09 14:57:53', 1),
(2, 1, 1, 'Weekly', '2022-06-09 15:00:42', 1),
(3, 1, 1, 'Daily Basis', '2022-06-12 16:31:42', 1),
(4, 1, 1, 'Hourly', '2022-06-20 05:24:55', 1);

-- --------------------------------------------------------

--
-- Table structure for table `size_of_org`
--

CREATE TABLE `size_of_org` (
  `id` int(11) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `size_of_org`
--

INSERT INTO `size_of_org` (`id`, `fk_user_id`, `title`, `created_date`, `status`) VALUES
(1, 1, '1 - 10 Employee', '2022-06-07 11:25:19', 1),
(2, 1, '10 - 50 Employees', '2022-06-07 11:34:37', 1);

-- --------------------------------------------------------

--
-- Table structure for table `slider`
--

CREATE TABLE `slider` (
  `id` int(11) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `image` varchar(250) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `slider`
--

INSERT INTO `slider` (`id`, `fk_user_id`, `title`, `image`, `created_date`, `status`) VALUES
(5, 1, 'रोजगार सूचना केन्द्र रुपन्देही ', 'img/62b56cda47685.jpg', '2022-06-24 07:50:50', 1),
(6, 1, 'रोजगार सूचना केन्द्र  बुटवल ४ रुपन्देही। ', 'img/62b56da491194.jpg', '2022-06-24 07:54:12', 1),
(12, 1, '...', 'img/62b825763c6de.jpg', '2022-06-26 09:23:02', 1);

-- --------------------------------------------------------

--
-- Table structure for table `traning`
--

CREATE TABLE `traning` (
  `id` int(11) NOT NULL,
  `fk_user_id` int(11) DEFAULT NULL,
  `fk_org_id` int(11) DEFAULT NULL,
  `fk_employment_id` int(11) NOT NULL,
  `training_title` varchar(100) NOT NULL,
  `training_duration` varchar(100) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT current_timestamp(),
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `traning`
--

INSERT INTO `traning` (`id`, `fk_user_id`, `fk_org_id`, `fk_employment_id`, `training_title`, `training_duration`, `created_date`, `status`) VALUES
(3, 1, 1, 4, 'Basic Computer', '3 Month', '2022-06-01 13:25:01', 1),
(4, 1, 1, 4, 'Advance Computer', '6 Month', '2022-06-01 13:25:01', 1),
(5, 1, 1, 5, 'Nepalgunj', '12 Days ', '2022-06-09 11:48:49', 1),
(6, NULL, NULL, 8, '', '', '2022-06-22 10:40:55', 1),
(7, NULL, NULL, 9, '', '', '2022-06-23 04:32:15', 1),
(8, NULL, NULL, 10, '', '', '2022-06-26 06:27:10', 1),
(9, NULL, NULL, 11, 'TELLY advance ', '4 months ', '2022-06-27 07:13:27', 1),
(10, NULL, NULL, 11, '', '', '2022-06-27 07:13:27', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ward`
--

CREATE TABLE `ward` (
  `id` int(11) NOT NULL,
  `ward` int(11) NOT NULL,
  `fk_organization_id` int(11) NOT NULL,
  `fk_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ward`
--

INSERT INTO `ward` (`id`, `ward`, `fk_organization_id`, `fk_user_id`) VALUES
(1, 1, 2, 2),
(2, 2, 2, 2),
(3, 3, 2, 2),
(4, 4, 2, 2),
(5, 5, 2, 2),
(6, 6, 2, 2),
(7, 7, 2, 2),
(8, 8, 2, 2),
(9, 9, 2, 2),
(10, 10, 2, 2),
(11, 11, 2, 2),
(12, 12, 2, 2),
(13, 13, 2, 2),
(14, 14, 2, 2),
(15, 15, 2, 2),
(16, 16, 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about`
--
ALTER TABLE `about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `adminuser`
--
ALTER TABLE `adminuser`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `available_for`
--
ALTER TABLE `available_for`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designation`
--
ALTER TABLE `designation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `district`
--
ALTER TABLE `district`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employer_user`
--
ALTER TABLE `employer_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employment`
--
ALTER TABLE `employment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `emp_and_job`
--
ALTER TABLE `emp_and_job`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `goal`
--
ALTER TABLE `goal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `header`
--
ALTER TABLE `header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `introduction`
--
ALTER TABLE `introduction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_exp`
--
ALTER TABLE `job_exp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_level`
--
ALTER TABLE `job_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_post`
--
ALTER TABLE `job_post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_posting`
--
ALTER TABLE `job_posting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_sector`
--
ALTER TABLE `job_sector`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member_cat`
--
ALTER TABLE `member_cat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `municipals`
--
ALTER TABLE `municipals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `objective`
--
ALTER TABLE `objective`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organization`
--
ALTER TABLE `organization`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `org_type`
--
ALTER TABLE `org_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ownership`
--
ALTER TABLE `ownership`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `province`
--
ALTER TABLE `province`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `qualification`
--
ALTER TABLE `qualification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `salary_type`
--
ALTER TABLE `salary_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `size_of_org`
--
ALTER TABLE `size_of_org`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider`
--
ALTER TABLE `slider`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `traning`
--
ALTER TABLE `traning`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ward`
--
ALTER TABLE `ward`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about`
--
ALTER TABLE `about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `adminuser`
--
ALTER TABLE `adminuser`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `available_for`
--
ALTER TABLE `available_for`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `designation`
--
ALTER TABLE `designation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `district`
--
ALTER TABLE `district`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT for table `employer_user`
--
ALTER TABLE `employer_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `employment`
--
ALTER TABLE `employment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `emp_and_job`
--
ALTER TABLE `emp_and_job`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `goal`
--
ALTER TABLE `goal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `header`
--
ALTER TABLE `header`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `introduction`
--
ALTER TABLE `introduction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `job_exp`
--
ALTER TABLE `job_exp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `job_level`
--
ALTER TABLE `job_level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `job_post`
--
ALTER TABLE `job_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `job_posting`
--
ALTER TABLE `job_posting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `job_sector`
--
ALTER TABLE `job_sector`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `member_cat`
--
ALTER TABLE `member_cat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `municipals`
--
ALTER TABLE `municipals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=769;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `objective`
--
ALTER TABLE `objective`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `organization`
--
ALTER TABLE `organization`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `org_type`
--
ALTER TABLE `org_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ownership`
--
ALTER TABLE `ownership`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `province`
--
ALTER TABLE `province`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `qualification`
--
ALTER TABLE `qualification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `salary_type`
--
ALTER TABLE `salary_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `size_of_org`
--
ALTER TABLE `size_of_org`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `slider`
--
ALTER TABLE `slider`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `traning`
--
ALTER TABLE `traning`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `ward`
--
ALTER TABLE `ward`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
