<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RecentBlogs;

/**
 * RecentBlogsSearch represents the model behind the search form of `app\models\RecentBlogs`.
 */
class RecentBlogsSearch extends RecentBlogs
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'image_date'], 'integer'],
            [['short_description', 'image', 'img_heading', 'img_description', 'posted_by', 'status'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RecentBlogs::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'image_date' => $this->image_date,
        ]);

        $query->andFilterWhere(['like', 'short_description', $this->short_description])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'img_heading', $this->img_heading])
            ->andFilterWhere(['like', 'img_description', $this->img_description])
            ->andFilterWhere(['like', 'posted_by', $this->posted_by])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
