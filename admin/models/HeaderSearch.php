<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Header;

/**
 * OrganizationdetailSearch represents the model behind the search form of `app\models\Organizationdetail`.
 */
class HeaderSearch extends Header
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'title', 'contact'], 'integer'],
            [['logo', 'email', 'twitterlink', 'fblink', 'instalink', 'skypelink', 'linkedinlink'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Header::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'title' => $this->title,
            'contact' => $this->contact,
        ]);

        $query->andFilterWhere(['like', 'logo', $this->logo])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'twitterlink', $this->twitterlink])
            ->andFilterWhere(['like', 'fblink', $this->fblink])
            ->andFilterWhere(['like', 'instalink', $this->instalink])
            ->andFilterWhere(['like', 'skypelink', $this->skypelink])
            ->andFilterWhere(['like', 'linkedinlink', $this->linkedinlink]);

        return $dataProvider;
    }
}
