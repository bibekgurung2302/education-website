<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productdetail".
 *
 * @property int $id
 * @property string $description
 */
class Partner extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'partner';
    }

    /**
     * {@inheritdoc}
     */
    public $productImage;
    public function rules()
    {
        return [
            [['description'], 'required'],
            [['description', 'image'], 'string'],
            [['productImage'], 'file','extensions' => 'png, jpg'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Description',
            'image' => 'Image',
        ];
    }
}
