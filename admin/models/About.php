<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "details".
 *
 * @property int $id
 * @property string $title
 * @property string $discription
 * @property string $image
 */
class About extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'About';
    }

    /**
     * {@inheritdoc}
     */
    public $detailsImage;
    public $aboutImage;
    public function rules()
    {
        return [
            [['title', 'discription'], 'required'],
            [['title', 'discription', 'image1','image2','location'], 'string', 'max' => 5000],
            [['detailsImage'], 'file', 'extensions' => 'png, jpg'],
            [['aboutImage'],'file','extensions' => 'png, jpg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'discription' => 'Discription',
            // 'discription2' => 'Discription Second',
            'image1' => 'First Image',
            'image2' => 'Second Image'
        ];
    }
}
