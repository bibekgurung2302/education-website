<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productdetail".
 *
 * @property int $id
 * @property string $description
 */
class Photo extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'photo';
    }

    /**
     * {@inheritdoc}
     */
    public $productImage;
    public function rules()
    {
        return [
            [['image', 'name'], 'required'],
            [['name', 'link', 'image'], 'string'],
            [['productImage'], 'file','extensions' => 'png, jpg'],

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'name',
            'link' => 'link',
            'image' => 'Image',
        ];
    }
}
