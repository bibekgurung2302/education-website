<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "banner".
 *
 * @property int|null $id
 * @property string $title
 * @property string $discription
 */
class Banner extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'banner';
    }

    /**
     * {@inheritdoc}
     */
    public $bannerImage;
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['title1','title2','discription'], 'required'],
            [['title1','title2', 'discription', 'Image'], 'string', 'max' => 900],
            [['bannerImage'], 'file', 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title1' => 'Title first',
            'title2' => 'Title Second',
            'discription' => 'Discription',
            'Image'=> 'Image',
        ];
    }
}
