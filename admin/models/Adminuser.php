<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "adminuser".
 *
 * @property int $id
 * @property string $username
 * @property string $password
 * @property string $org_name
 * @property int $org_type
 * @property string $contact_person
 * @property string $phone
 * @property string $role
 * @property string $authkey
 * @property string $register_date
 * @property string $created_date
 * @property int $status
 */
class Adminuser extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'adminuser';
    }

    /**
     * {@inheritdoc}
     */
     // User Role
     const USER_ADMIN = 1;
     const USER_EMPLOYER = 2;
     // User Status
     const INACTIVE = 0;
     const REGISTERED = 1;
     const VERIFIED = 3;
     public $captcha;

    public function rules()
    {
        return [
            [['username', 'password', 'org_name', 'org_type', 'contact_person', 'phone', 'role', 'authkey', 'created_date', 'status'], 'required'],
            [['org_type', 'status'], 'integer'],
            [['username'], 'string', 'max' => 150],
            [['username'], 'unique', 'message' => 'Your Email is already exist.'],
            [['org_name','password', 'authkey'], 'string', 'max' => 100],
            [['contact_person'], 'string', 'max' => 50],
            [['register_date', 'phone'], 'string', 'max' => 15],
            [['role'], 'string', 'max' => 30],
            [['captcha'], 'captcha', 'message' => 'Captcha does not matched'],
            [['created_date'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Email',
            'password' => 'Password',
            'org_name' => 'Organization Name',
            'org_type' => 'Organization Type',
            'contact_person' => 'Contact Person',
            'phone' => 'Phone',
            'role' => 'Role',
            'authkey' => 'Authkey',
            'register_date' => 'Register Date',
            'created_date' => 'Created Date',
            'status' => 'Status',
        ];
    }
    
    public static function primaryKey() {
        return ['id'];
    }

    public function getAuthKey(): string {
        return $this->authkey;
    }

    public function getId() {
        return $this->getPrimaryKey();
    }

    public function validateAuthKey($authKey): bool {
        return $this->authkey === $authKey;
    }

    public static function findIdentity($id) {
        return self::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        throw new NotSupportedException();
    }

    public static function findByUsername($username) {
        return self::findOne(['username' => $username]);
    }

    public function validatePassword($password) {
        return $this->password === $password;
    }

    public function getOrgType(){
        return $this->hasOne(JobSector::className(), ['id' => 'org_type']);
    }
}
