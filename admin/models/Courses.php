<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "courses".
 *
 * @property int $id
 * @property string $heading
 * @property string $short_description
 * @property string $image
 * @property string $img_heading
 * @property string $lesson
 * @property string $week
 * @property string $course_description
 * @property string $price
 */
class Courses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'courses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['heading', 'short_description', 'image', 'img_heading', 'lesson', 'week', 'course_description', 'price'], 'required'],
            [['heading'], 'string', 'max' => 50],
            [['short_description', 'course_description'], 'string', 'max' => 255],
            [['image', 'img_heading', 'lesson', 'week', 'price'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'heading' => 'Heading',
            'short_description' => 'Short Description',
            'image' => 'Image',
            'img_heading' => 'Img Heading',
            'lesson' => 'Lesson',
            'week' => 'Week',
            'course_description' => 'Course Description',
            'price' => 'Price',
        ];
    }
}
