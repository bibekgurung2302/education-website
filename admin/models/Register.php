<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "register".
 *
 * @property int $id
 * @property string $full_name
 * @property string $contact
 * @property string $email
 * @property int $fk_province
 * @property int $fk_district
 * @property int $fk_local_level
 * @property string $service_sector
 * @property string $org_name
 * @property string $org_sector_from
 * @property string|null $designation
 * @property string $info_source
 * @property string $meal_type
 * @property string $qr_code
 * @property string $otp
 * @property string $created_date
 * @property int $status
 */
class Register extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'register';
    }
    
    public $captcha;

    /**
     * {@inheritdoc}
     */
    const SUB_METROPOLITAN = 1;
    const MUNICIPALITY = 2;
    const RURAL_MUNICIPALITY = 3;
    const SAHAKARI = 4;
    const PABSON = 5;
    const NPABSON = 6;
    const OTHER = 7;
    const VEG = 7;
    const NON_VEG = 8;
    const SUBMITTED = 1;
    const VERIFIED = 2;
    const APPROVED = 3;
    const SOCIAL_MEDIA = 1;
    const NEWS_SITE = 2;
    const FRIENDS = 3;

    public function rules() {
        return [
            [['full_name', 'contact', 'email', 'fk_province', 'fk_district', 'fk_local_level', 'service_sector', 'org_name', 'org_sector_from', 'info_source', 'meal_type', 'otp', 'created_date', 'status'], 'required'],
            [['fk_province', 'fk_district', 'fk_local_level', 'status'], 'integer'],
            [['created_date', 'info_source', 'qr_code'], 'safe'],
            [['full_name', 'org_name', 'org_sector_from', 'designation'], 'string', 'max' => 100],
            [['contact'], 'number'],
            [['email'], 'email'],
            [['contact'],'unique', 'message' => 'Your Contact Number is already exist.'],
            [['email'], 'string', 'max' => 50],
            [['captcha'], 'captcha', 'message' => 'Captcha does not matched'],
            [['service_sector', 'meal_type'], 'string', 'max' => 20],
            [['otp'], 'number'],
           // [['info_source'], 'string', 'max' => 200],
           // [['qr_code'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'full_name' => 'Full Name',
            'contact' => 'Contact',
            'email' => 'Email',
            'fk_province' => 'Province',
            'fk_district' => 'District',
            'fk_local_level' => 'Local Level',
            'service_sector' => 'Service Sector',
            'org_name' => 'Organization Name',
            'org_sector_from' => 'Organization Sector From',
            'designation' => 'Designation',
            'info_source' => 'From where did you know about Event?',
            'meal_type' => 'Meal Type',
            'qr_code' => 'Qr Code',
            'otp' => 'Otp',
            'created_date' => 'Created Date',
            'status' => 'Status',
        ];
    }

    public static function getSectorFrom() {
        return [
            self::SUB_METROPOLITAN => 'Sub Metropolitan',
            self::MUNICIPALITY => 'Municipality',
            self::RURAL_MUNICIPALITY => 'Rural Municpality',
            self::SAHAKARI => 'Sahakari',
            self::PABSON => 'PABSON',
            self::NPABSON => 'NPABSON',
            self::OTHER => 'Other'
        ];
    }
    
    public function getProvince(){
        return $this->hasOne(Province::className(), ['id'  =>'fk_province']);
    }
    
    public function getDistrict(){
        return $this->hasOne(District::className(), ['id' => 'fk_district']);
    }
    
    public function getMunicipals(){
        return $this->hasOne(Municipals::className(), ['id' => 'fk_local_level']);
    }
}
