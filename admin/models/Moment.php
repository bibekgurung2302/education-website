<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;                    

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $icon
 * @property string $title
 * @property string $discription
 */
class Moment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'moment';
    }

    /**
     * {@inheritdoc}
     */
   
    public function rules()
    {
        return [
            [[ 'title', 'discription'], 'required'],
            [['icon', 'title', 'logo', 'discription'], 'string', 'max' => 255],
            // [['imageFile'], 'file', 'extensions' => 'png, jpg'],

        ];
    }

    /**
     * {@inheritdoc}
     */
 
    
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'icon' => 'Icon',
            'title' => 'Title',
            ' logo' => 'logo',
            'discription' => 'Number',
        ];
    }
    
        public function upload_logo(){
        $file='images/'.uniqid().'.'.$this->logo->extension;
        if($this->logo->saveAs($file)){
            $this->logo=$file;
            return true;
        }
        else{
            return false;
        }
    }

}
