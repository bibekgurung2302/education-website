<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $heading
 * @property string|null $discription
 * @property string|null $image
 */
class Products extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * {@inheritdoc}
     */
    public $productImage;
    public function rules()
    {
        return [
            [['title', 'heading', 'discription', 'image'], 'string', 'max' => 255],
            [['image'], 'string'],
            [['productImage'], 'file','extensions' => 'png, jpg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'heading' => 'Heading',
            'discription' => 'Discription',
            'image' => 'Image',
            'productImage' => 'Image'
        ];
    }
}
