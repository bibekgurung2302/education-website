<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "organizationdetail".
 *
 * @property int $id
 * @property int $name
 * @property string $logo
 * @property string $email
 * @property int $contact
 * @property string $twitterlink
 * @property string $fblink
 * @property string $instalink
 * @property string $skypelink
 * @property string $linkedinlink
 */
class Header extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'header';
    }

    /**
     * {@inheritdoc}
     */
    public $logoImage;
    public function rules()
    {
        return [
            [['title', 'email', 'contact'], 'required'],
            // [['contact'], 'integer'],
            [['logo', 'email','title', 'contact','twitterlink', 'fblink', 'instalink', 'skypelink', 'linkedinlink'], 'string', 'max' => 255],
            [['logoImage'], 'file', 'extensions' => 'png, jpeg, jpg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'logo' => 'Logo',
            'email' => 'Email',
            'contact' => 'Contact',
            'twitterlink' => 'Twitterlink',
            'fblink' => 'Fblink',
            'instalink' => 'Instalink',
            'skypelink' => 'Skypelink',
            'linkedinlink' => 'Linkedinlink',
            'logoImage'=>'Logo'
        ];
    }
}
