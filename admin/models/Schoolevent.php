<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "schoolevent".
 *
 * @property int $id
 * @property string $main_title
 * @property string $description
 * @property string $image
 * @property string $img_heading
 * @property string $img_description
 */
class Schoolevent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'schoolevent';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['main_title', 'description', 'image', 'img_heading', 'img_description'], 'required'],
            [['main_title', 'image'], 'string', 'max' => 80],
            [['description', 'img_description'], 'string', 'max' => 255],
            [['img_heading'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'main_title' => 'Main Title',
            'description' => 'Description',
            'image' => 'Image',
            'img_heading' => 'Img Heading',
            'img_description' => 'Img Description',
        ];
    }
}
