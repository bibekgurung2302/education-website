<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "recent_blogs".
 *
 * @property int $id
 * @property string $short_description
 * @property string $image
 * @property int $image_date
 * @property string $img_heading
 * @property string $img_description
 * @property string $posted_by
 * @property string $status
 */
class RecentBlogs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'recent_blogs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['short_description', 'image', 'image_date', 'img_heading', 'img_description', 'posted_by', 'status'], 'required'],
            [['image_date'], 'integer'],
            [['short_description', 'img_description'], 'string', 'max' => 255],
            [['image', 'status'], 'string', 'max' => 20],
            [['img_heading', 'posted_by'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'short_description' => 'Short Description',
            'image' => 'Image',
            'image_date' => 'Image Date',
            'img_heading' => 'Img Heading',
            'img_description' => 'Img Description',
            'posted_by' => 'Posted By',
            'status' => 'Status',
        ];
    }
}
