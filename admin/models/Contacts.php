<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contacts".
 *
 * @property int $id
 * @property string $location
 * @property string $email
 * @property string $callus
 * @property string $googlemap
 */
class Contacts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contacts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['location', 'email', 'callus', 'googlemap'], 'required'],
            [['location', 'email', 'callus', 'googlemap','image'], 'string'],
            [['image'],'file','extensions'=>'jpg,png'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'location' => 'Location',
            'email' => 'Email',
            'callus' => 'Callus',
            'googlemap' => 'Googlemap',
            'image' => 'Image',
        ];
    }
}
