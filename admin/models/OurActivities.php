<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "our_activities".
 *
 * @property int $id
 * @property string $short_description
 * @property string $video_link
 * @property string $status
 */
class OurActivities extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'our_activities';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['short_description', 'video_link', 'status'], 'required'],
            [['short_description', 'video_link'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'short_description' => 'Short Description',
            'video_link' => 'Video Link',
            'status' => 'Status',
        ];
    }
}
