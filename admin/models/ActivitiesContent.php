<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "activities_content".
 *
 * @property int $id
 * @property string $title
 * @property string $icon
 * @property string $description
 * @property string $status
 */
class ActivitiesContent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'activities_content';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'icon', 'description', 'status'], 'required'],
            [['title'], 'string', 'max' => 30],
            [['icon'], 'string', 'max' => 80],
            [['description'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'icon' => 'Icon',
            'description' => 'Description',
            'status' => 'Status',
        ];
    }
}
