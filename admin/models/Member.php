<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "member".
 *
 * @property int $id
 * @property int $fk_user_id
 * @property int $fk_member_cat
 * @property string $full_Name
 * @property int $fk_designation
 * @property string $contact
 * @property string $email
 * @property string $profile_img
 * @property string $created_date
 * @property int $status
 */
class Member extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'member';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'fk_member_cat', 'full_Name', 'position', 'contact', 'email', 'status'], 'required'],
            [[ 'fk_member_cat', 'status'], 'integer'],
            [['created_date'], 'safe'],
            [['full_Name'], 'string', 'max' => 80],
            [['contact'], 'string', 'max' => 10],
            [['position', 'email'], 'string', 'max' => 50],
            [['profile_img'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
           
            'fk_member_cat' => ' Member Category',
            'full_Name' => 'Full Name',
            'position' => 'Position',
            'contact' => 'Contact',
            'email' => 'Email',
            'profile_img' => 'Profile Img',
            'created_date' => 'Created Date',
            'status' => 'Status',
        ];
    }
    
    
     public function upload_image(){
    
        $file='img/'.uniqid().'.'.$this->profile_img->extension;
        if($this->profile_img->saveAs($file)){
            $this->profile_img=$file;
            return true;
        }
        else{
            return false;
        }
    }
   public function getMemberCat(){
        return $this->hasOne(MemberCat::className(), ['id' => 'fk_member_cat']);
    }

}
