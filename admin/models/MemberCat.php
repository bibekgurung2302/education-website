<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "member_cat".
 *
 * @property int $id
 * @property int $fk_user_id
 * @property int $category
 * @property string $created_date
 * @property int $status
 */
class MemberCat extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'member_cat';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [[ 'category', 'status'], 'required'],
            [['status'], 'integer'],
            [['created_date', 'category'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
          
            'category' => 'Category',
            'created_date' => 'Created Date',
            'status' => 'Status',
        ];
    }
}
