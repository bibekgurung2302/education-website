<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "team".
 *
 * @property int $id
 * @property string $image
 * @property string $name
 * @property string $designation
 * @property string $qualification
 * @property string $twitter_link
 * @property string $fb_link
 * @property string $insta_link
 * @property string $linkedin _ink
 */
class Team extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'team';
    }

    /**
     * {@inheritdoc}
     */
    public $teamImage;
    public function rules()
    {
        return [
            [[ 'name', 'designation', 'qualification'], 'required'],
            [['qualification'], 'string'],
            [['image', 'name', 'designation', 'twitter_link', 'fb_link', 'insta_link','web_link', 'linkedin_ink'], 'string', 'max' => 255],
            [['teamImage'], 'file', 'extensions' => 'png, jpeg, jpg'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Image',
            'name' => 'Name',
            'designation' => 'Designation',
            'qualification' => 'Qualification',
            'twitter_link' => 'Twitter Link',
            'fb_link' => 'Fb Link',
            'insta_link' => 'Insta Link',
            'linkedin_ink' => 'Linkedin Ink',
            'web_link' => 'web_link',
        ];
    }
}
