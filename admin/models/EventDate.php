<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "event_date".
 *
 * @property int $id
 * @property string $event_title
 * @property string $event_date
 * @property string $status
 */
class EventDate extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_date';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['event_title', 'event_date', 'status'], 'required'],
            [['event_title'], 'string', 'max' => 80],
            [['event_date'], 'string', 'max' => 50],
            [['status'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'event_title' => 'Event Title',
            'event_date' => 'Event Date',
            'status' => 'Status',
        ];
    }
}
