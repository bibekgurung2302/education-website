<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\controllers;

/**
 * Description of Helper
 *
 * @author hiramani
 */
class Helper {

    //put your code here
    public function actionNepaliDate() {
        date_default_timezone_set('Asia/Kathmandu');
        $nepdate = new NepaliCalender();
        $year = date('Y');
        $month = date('m');
        $day = date('d');
        $today = $nepdate->eng_to_nep($year, $month, $day);
//print_r($cal->nep_to_eng(2065,8,8));
        //$nepali_date1 = $today['year'] . '-' . $today['month'] . '-' . $today['date'];

        $nepali_date2 = strlen($today['month']);
        $nepali_date1 = strlen($today['date']);
        if ($nepali_date2 == 1) {
            $month1 = '0' . $today['month'];
        } else {
            $month1 = $today['month'];
        }
        if ($nepali_date1 == 1) {
            $date1 = '0' . $today['date'];
        } else {
            $date1 = $today['date'];
        }
        $nepali_today = $today['year'] . '-' . $month1 . '-' . $date1;
        return $nepali_today;
    }

    public function getOrganization() {
        $user_id = \Yii::$app->user->id;
        $user = \app\models\Organization::findOne(['fk_user_id' => $user_id]);
        return $user->id;
    }

    public function getUserId() {
        return \Yii::$app->user->id;
    }

    public static function createMultiple($modelClass, $multipleModels = []) {
        $model = new $modelClass;
        $formName = $model->formName();
        $post = \Yii::$app->request->post($formName);
        $models = [];

        if (!empty($multipleModels)) {
            $keys = array_keys(\yii\helpers\ArrayHelper::map($multipleModels, 'id', 'id'));
            $multipleModels = array_combine($keys, $multipleModels);
        }

        if ($post && is_array($post)) {
            foreach ($post as $i => $item) {
                if (isset($item['id']) && !empty($item['id']) && isset($multipleModels[$item['id']])) {
                    $models[] = $multipleModels[$item['id']];
                } else {
                    $models[] = new $modelClass;
                }
            }
        }

        unset($model, $formName, $post);

        return $models;
    }

    public static function loadMultiple($models, $data, $formName = null) {
        if ($formName === null) {
            /* @var $first Model|false */
            $first = reset($models);
            if ($first === false) {
                return false;
            }
            $formName = $first->formName();
        }

        $success = false;
        foreach ($models as $i => $model) {
            /* @var $model Model */
            if ($formName == '') {
                if (!empty($data[$i]) && $model->load($data[$i], '')) {
                    $success = true;
                }
            } elseif (!empty($data[$formName][$i]) && $model->load($data[$formName][$i], '')) {
                $success = true;
            }
        }

        return $success;
    }

    public static function getBsTpAd($date) {
        // var_dump($date);die;
        if (is_numeric($date)) {
            $miliseconds = ($date - (25567 + 2)) * 86400 * 1000;
            $seconds = $miliseconds / 1000;
            $date = date("Y-m-d", $seconds);
            // return $date;
        }
        $dateExplode = explode('-', $date);
        $dateModel = new NepaliCalender();
        $count_dates = count($dateExplode);
        //var_dump($dateExplode);die;
        if ($count_dates == 3) {
            //var_dump($date);die;
            $adDate = $dateModel->nep_to_eng($dateExplode[0], $dateExplode[1], $dateExplode[2]);
        } else {
            return date("Y-m-d");
        }
        //var_dump($adDate);die;
        $month = '';
        $day = '';
        if (strlen($adDate['month']) == 1) {
            $month = '0' . $adDate['month'];
        } else {
            $month = $adDate['month'];
        }
        if (strlen($adDate['day']) == 1) {
            $day = '0' . $adDate['date'];
        } else {
            $day = $adDate['date'];
        }
        $en_date = $adDate['year'] . '-' . $month . '-' . $day;
        //var_dump($np_date);die;
        // var_dump($en_date);die;
        return $en_date;
        // return $adDate;
    }

    public static function getAdToBs($date) {
        // var_dump($date);die;
        if (is_numeric($date)) {
            $miliseconds = ($date - (25567 + 2)) * 86400 * 1000;
            $seconds = $miliseconds / 1000;
            $date = date("Y-m-d", $seconds);
            // return $date;
        }
        $dateExplode = explode('-', $date);
        $dateModel = new NepaliCalender();
        $count_dates = count($dateExplode);
        if ($count_dates == 3) {
            //var_dump($date);die;
            $adDate = $dateModel->eng_to_nep($dateExplode[0], $dateExplode[1], $dateExplode[2]);
        } else {
            return date("Y-m-d");
        }
        $month = '';
        $day = '';
        if (strlen($adDate['month']) == 1) {
            $month = '0' . $adDate['month'];
        } else {
            $month = $adDate['month'];
        }
        if (strlen($adDate['day']) == 1) {
            $day = '0' . $adDate['date'];
        } else {
            $day = $adDate['date'];
        }
        $en_date = $adDate['year'] . '-' . $month . '-' . $day;
        //var_dump($np_date);die;
        return $en_date;
        // return $adDate;
    }

    public function getEngToNepaliNumbers($number) {
        $eng_number = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');
        $nep_number = array('०', '१', '२', '३', '४', '५', '६', '७', '८', '९');
        $nep_number = str_replace($eng_number, $nep_number, $number);
        return $nep_number;
    }

    // public function getOrganizationType() {
    //     $user = \app\models\Users::findOne(['id' => $this->getUserId()]);
    //     $organization = \app\models\Organizations::findOne(['id' => $user->fk_organization_id]);
    //     return $organization->type;
    // }

    // public function getPartyId() {
    //     $user = \app\models\Users::findOne(['id' => $this->getUserId()]);
    //     return $user->fk_party_id;
    // }

    // public function getOrganizationTypeName($id) {
    //     if (!$id) {
    //         return false;
    //     }
    //     $type = '';
    //     if ($id == \app\models\Organization::TUKI) {
    //         $type = 'TUKI';
    //     } else if ($id == \app\models\Organization::DISTRIBUTOR) {

    //         $type = 'Distributor';
    //     } else if ($id == \app\models\Organization::RESELLER) {
    //         $type = 'Reseller';
    //     }

    //     return $type;
    // }

    public function getEconomicYear() {
        $model = \app\models\EconomicYear::find()->where(['fk_organization_id' => $this->getOrganization()])->andWhere(['status'=> \app\models\EconomicYear::ACTIVE])->one();
        return $model->id;
    }

    public function getDistrictName($id) {
        $model = \app\models\District::findOne(['id' => $id]);
        return $model->district;
    }

    public function getMunicipalName($id) {
        $model = \app\models\Municipals::findOne(['id' => $id]);
        return $model->municipal_nepali;
    }

    public function getProvinceName($id) {
        $model = \app\models\Province::findOne(['id' => $id]);
        return $model->province_nepali;
    }

    public function getWardNo($id) {
        $model = \app\models\Ward::findOne(['id' => $id]);
        return $model->ward;
    }

    public function getUserRole() {
        $userId = $this->getUserId();
        $model = \app\models\Adminuser::findOne(['id' => $userId]);
        return $model->role;
    }

    function nepaliCurrencyFormat($number) {
        $split_number = @explode(".", $number);
        $rupee = $split_number[0];
        $paise = @$split_number[1];
        $thousands = '';
        $lakhs = '';
        if (@strlen($rupee) > 3) {
            $hundreds = substr($rupee, strlen($rupee) - 3);
            $thousands_in_reverse = strrev(substr($rupee, 0, strlen($rupee) - 3));
            $length = (strlen($thousands_in_reverse));
            for ($i = 0; $i < (strlen($thousands_in_reverse)); $i++) {
                $thousands .= $thousands_in_reverse[$i];
                if ($i % 2) {
                    $thousands .= ',';
                }
            } $thousands = strrev(trim($thousands, ","));
            $formatted_rupee = $thousands . "," . $hundreds;
        } else {
            $formatted_rupee = $rupee;
        } if ((int) $paise > 0) {
            $formatted_paise = "." . substr($paise, 0, 2);
        } else {
            $formatted_paise = "." . substr("00", 0, 2);
        }
        return $formatted_rupee . $formatted_paise;
    }


    public function getUserName($id) {
        $model = \app\models\Users::findOne(['id' => $id]);
        return $model->name;
    }

}
