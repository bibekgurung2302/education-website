<?php

namespace app\controllers;

use Yii;
use app\models\About;
use app\models\AboutSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\UploadForm;
use yii\web\UploadedFile;
/**
 * DetailsController implements the CRUD actions for Details model.
 */
class AboutController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Details models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new AboutSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Details model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Details model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new About();
        if ($model->load(Yii::$app->request->post())){
            $model->image1 = UploadedFile::getInstance($model, 'image1');
            $model->image2 = UploadedFile::getInstance($model, 'image2');
            //var_dump($model);die;
            if(empty($model->image1)){
                yii::$app->session->setFlash('message','Please insert image');
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
            if(empty($model->image2)){
                yii::$app->session->setFlash('message','Please insert image');
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
             //var_dump($model->detailsImage);die;
           // echo uniqid();die;
            $unique_id=uniqid();
            //var_dump($unique_id);die;
            if(!empty($model->image1)){
            $model->image1->saveAs('img/'.$unique_id . '.' .$model->image1->extension);//for folder save 
            $model->image1='img/' . $unique_id . '.' . $model->image1->extension;//for database
            }
            if(!empty($model->image2)){
                $model->image2->saveAs('img/'.$unique_id . '.' .$model->image2->extension);//for folder save 
                $model->image2='img/' . $unique_id . '.' . $model->image2->extension;//for database
                }
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }
    /**
     * Updates an existing Details model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())){

            $model->image1 = UploadedFile::getInstance($model, 'image1');
            $model->image2 = UploadedFile::getInstance($model, 'image2');
            //var_dump($model);die;
            // if(empty($model->image1)){
            //     yii::$app->session->setFlash('message','Please insert image');
            //     return $this->render('update', [
            //         'model' => $model,
            //     ]);
            // }
            // if(empty($model->image2)){
            //     yii::$app->session->setFlash('message','Please insert image');
            //     return $this->render('update', [
            //         'model' => $model,
            //     ]);
            // }
             //var_dump($model->detailsImage);die;
           // echo uniqid();die;
            $unique_id=uniqid();
            //var_dump($unique_id);die;
            if(!empty($model->image1)){
            $model->image1->saveAs('img/'.$unique_id . '.' .$model->image1->extension);//for folder save 
            $model->image1='img/' . $unique_id . '.' . $model->image1->extension;//for database
            }
            if(!empty($model->image2)){
                $model->image2->saveAs('img/'.$unique_id . '.' .$model->image2->extension);//for folder save 
                $model->image2='img/' . $unique_id . '.' . $model->image2->extension;//for database
                }
            $model->save();
    
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Details model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Details model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Details the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = About::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
