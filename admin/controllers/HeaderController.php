<?php

namespace app\controllers;

use Yii;
use app\models\Header;
use app\models\HeaderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * OrganizationdetailController implements the CRUD actions for Organizationdetail model.
 */
class HeaderController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Organizationdetail models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new HeaderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Organizationdetail model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Organizationdetail model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Header();

        if ($model->load(Yii::$app->request->post())){

            $model->logoImage = UploadedFile::getInstance($model, 'logoImage');
            //  var_dump($model->detailsImage);die;
           // echo uniqid();die;
            $unique_id=uniqid();
            //var_dump($unique_id);die;
            if(!empty($model->logoImage)){
            $model->logoImage->saveAs('logo/'.$unique_id . '.' .$model->logoImage->extension,false);//for folder save 
            $model->logo='logo/' . $unique_id . '.' . $model->logoImage->extension;//for database
            }
            if($model->save()){
                return $this->redirect(['view', 'id' => $model->id]);
            }
    
           
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Organizationdetail model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) ) {
            $model->logoImage = UploadedFile::getInstance($model, 'logoImage');
           // var_dump($model->logoImage);die;

            if(!empty($model->logoImage)){

                
                $path = 'logo/'.uniqid() . '.' .$model->logoImage->extension;
                $model->logoImage->saveAs($path,false);//for folder save 
                $model->logo=$path;
               // var_dump($model->logo);die;
                }
                
                if($model->Save()){
                    return $this->redirect(['view', 'id' => $model->id]);
                }
           
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Organizationdetail model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Organizationdetail model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Organizationdetail the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Header::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
