<?php

namespace app\controllers;

use app\models\Register;
use yii\filters\AccessControl;
use app\models\RegisterSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use Da\QrCode\QrCode;
use yii\web\ForbiddenHttpException;
use Yii;

/**
 * RegisterController implements the CRUD actions for Register model.
 */
class RegisterController extends Controller {

    /**
     * @inheritDoc
     */
    public function behaviors() {
        return array_merge(
                parent::behaviors(),
                [   
                    'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'update', 'view', 'delete', 'scan', 'print-all'],
                'rules' => [
                    [
                       // 'actions' => ['index', 'update', 'view', 'delete'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all Register models.
     *
     * @return string
     */
    public function actionIndex() {
        $searchModel = new RegisterSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Register model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id) {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Register model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        $model = new Register();

        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                $model->created_date = date('Y-m-d');
                $model->status = Register::SUBMITTED;
                $model->otp = rand(100000, 999999);
                $model->info_source = implode(",", $model->info_source); 
                throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));die;
                $val = Register::find()->where(['contact' => $model->contact])->One();
                if ($val) {

                    if ($val->status == 1) {
                         Yii::$app->db->createCommand()
                                ->update('register', ['otp' => $model->otp])
                                ->execute();
                      $message = $model->otp . " This is your OTP for Registration for Digital Summit 2022 \n Powered By TUKI";
                      $to = $model->contact;
                      $ch = curl_init();
                      curl_setopt($ch, CURLOPT_URL, 'http://app.easy.com.np/easyApi');
                      curl_setopt($ch, CURLOPT_HEADER, 0);
                      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                      curl_setopt($ch, CURLOPT_POST, 1);
                      $data = array(
                      'key' => 'EASY5c406d30cb33d0.12577082',
                      'source' => 'none', // for default sender ID
                      'message' => $message,
                      'destination' => $to, // with or without country code
                      'type' => 1,
                      );

                      curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                      $contents = curl_exec($ch);
                        Yii::$app->session->setFlash('error', "Your Registration Submitted but not Verified.");
                        return $this->redirect(['verify', 'id' => $val->id]);
                    } else if ($val->status == 2  OR $val->status == 3) {
                        Yii::$app->session->setFlash('error', "Sorry !!! Your contact number is already exist.");
                        // return $this->redirect(['create']); 
                        $model->loadDefaultValues();
                    }
                    return $this->render('create', [
                                'model' => $model,
                    ]);
                }
             /*   $qrCode = (new QrCode('Guest Name: ' . $model->full_name . 'Contact:' . $model->contact . '<br> Organization Name: ' . $model->org_name))
                        ->setSize(250)
                        ->setMargin(5)
                        ->useForegroundColor(0, 0, 0); */
                
// now we can display the qrcode in many ways
// saving the result to a file:
            /*    $image_name = uniqid();
                $qrCode->writeFile('img/' . $image_name . '.png'); // writer defaults to PNG when none is specified

             * // display directly to the browser 
             */
//  header('Content-Type: ' . $qrCode->getContentType());
//echo $qrCode->writeString();
// echo '<img src="' . $qrCode->writeDataUri() . '">';
            //    $model->qr_code = 'img/' . $image_name . '.png';
                if ($model->save(false)) {
                    $message = $model->otp . " This is your OTP for Registration for Digital Summit 2022 \n Powered By TUKI";
                    $to = $model->contact;
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, 'http://app.easy.com.np/easyApi');
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    $data = array(
                        'key' => 'EASY5c406d30cb33d0.12577082',
                        'source' => 'none', // for default sender ID
                        'message' => $message,
                        'destination' => $to, // with or without country code
                        'type' => 1,
                    );

                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                    $contents = curl_exec($ch); 
                    return $this->redirect(['verify', 'id' => $model->id]);
                }
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Register model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    public function actionVerify($id) {
        $model = $this->findModel($id);
        $otp = $model->otp;
        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
                $verification_otp = $model->otp;
                if ($otp === $verification_otp) {
                    $model->status = 2;
                    $model->update(false);
                    Yii::$app->session->setFlash('success', "Your Registration Successfully Verified.");
                    return $this->redirect(['create']);
                } else {
                    Yii::$app->session->setFlash('error', 'Verification code does not matched');
                    return $this->redirect(['verify', 'id' => $id]);
                }
            }
        }
        return $this->render('verify');
    }
    
    public function actionOtpVerify($id){
        $model = $this->findModel($id);
        $model->status = 2;
        $model->update(false);
        Yii::$app->session->setFlash('success', 'Successfully otp verified by Admin');
        return $this->redirect(['index']);
    }

    public function actionApprove($id){
        $model = $this->findModel($id);
        $model->status = 3;
        $model->update(false);
        $message = "Your participation has been accepted in TUKI-Digital Summit in association with Ninja Infosys.
Program Details:
Date: 2078-12-28
Time: 9:00 am
Venue: Hotel Siddhartha Karnando
Please collect your ID card at registration desk at venue.
For more details:
9855047033
9858023662
SMS Powered By TUKI";
                    $to = $model->contact;
                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, 'http://app.easy.com.np/easyApi');
                    curl_setopt($ch, CURLOPT_HEADER, 0);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_POST, 1);
                    $data = array(
                        'key' => 'EASY5c406d30cb33d0.12577082',
                        'source' => 'none', // for default sender ID
                        'message' => $message,
                        'destination' => $to, // with or without country code
                        'type' => 1,
                    );

                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                    $contents = curl_exec($ch); 
        Yii::$app->session->setFlash('success', 'Successfully Approved for Digital Summit');
        return $this->redirect(['index']);
    }
    
     public function actionPrint($id){
        $model = $this->findModel($id);
        return $this->render('print', [
            'model' => $model,
        ]);
    }
    
    public function actionProgram(){
        return $this->render('program');
    }

    public function actionScan(){
        $model = new Register();
        if ($this->request->isPost) {
            if ($model->load($this->request->post())) {
        $query = (new yii\db\Query())
                ->select('register.full_name, register.contact, register.email, register.org_name, register.status')
                ->from('register')
                ->where(['register.contact' => $model->contact])
                ->all();
        if($query){
        return $this->render('approved', [
            'query' => $query,
        ]);
    }else{
        Yii::$app->session->setFlash('error', 'Sorry!!! You are not Registered');
    }

        }
    }
        return $this->render('qr', [
                    'model' => $model,
        ]);
            }

    public function actionPrintAll(){
        $query = (new yii\db\Query())
                ->select('register.full_name, register.contact, register.email, register.org_name, register.status')
                ->from('register')
                ->where(['register.status' => Register::APPROVED])
                ->all();
        return $this->render('print_all', [
            'query' => $query
        ]);
    }

    public function actionDistrict() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $cat_id = $parents[0];
// var_dump($cat_id);die;
                $out = \app\models\District::find()
                        ->select('id , district as name')
                        ->where(['fk_province' => $cat_id])
                        ->asArray()
                        ->all();

// the getSubCatList function will query the database based on the
// cat_id and return an array like below:
// [
//    ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
//    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
// ]
                return ['output' => $out, 'selected' => ''];
            }
        }
        return ['output' => '', 'selected' => ''];
    }

    public function actionMunicipals() {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $out = [];
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if ($parents != null) {
                $cat_id = $parents[0];
// var_dump($cat_id);die;
                $out = \app\models\Municipals::find()
                        ->select('id , name')
                        ->where(['fk_district' => $cat_id])
                        ->asArray()
                        ->all();

// the getSubCatList function will query the database based on the
// cat_id and return an array like below:
// [
//    ['id'=>'<sub-cat-id-1>', 'name'=>'<sub-cat-name1>'],
//    ['id'=>'<sub-cat_id_2>', 'name'=>'<sub-cat-name2>']
// ]
                return ['output' => $out, 'selected' => ''];
            }
        }
        return ['output' => '', 'selected' => ''];
    }

    /**
     * Deletes an existing Register model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id) {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Register model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Register the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Register::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
