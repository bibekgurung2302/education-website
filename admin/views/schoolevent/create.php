<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Schoolevent */

$this->title = 'Create Schoolevent';
$this->params['breadcrumbs'][] = ['label' => 'Schoolevents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="schoolevent-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
