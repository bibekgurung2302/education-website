<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RecentBlogs */

$this->title = 'Create Recent Blogs';
$this->params['breadcrumbs'][] = ['label' => 'Recent Blogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recent-blogs-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
