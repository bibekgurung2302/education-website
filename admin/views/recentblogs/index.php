<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RecentBlogsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Recent Blogs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="recent-blogs-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Recent Blogs', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'short_description',
            'image',
            'image_date',
            'img_heading',
            //'img_description',
            //'posted_by',
            //'status',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, RecentBlogs $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
