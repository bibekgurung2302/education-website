<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RecentBlogs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="recent-blogs-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'short_description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image_date')->textInput() ?>

    <?= $form->field($model, 'img_heading')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'img_description')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'posted_by')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
