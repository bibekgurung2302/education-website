<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RecentBlogs */

$this->title = 'Update Recent Blogs: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Recent Blogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="recent-blogs-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
