<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RecentBlogsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="recent-blogs-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'short_description') ?>

    <?= $form->field($model, 'image') ?>

    <?= $form->field($model, 'image_date') ?>

    <?= $form->field($model, 'img_heading') ?>

    <?php // echo $form->field($model, 'img_description') ?>

    <?php // echo $form->field($model, 'posted_by') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
