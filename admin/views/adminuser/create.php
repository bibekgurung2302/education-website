<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Adminuser */

$this->title = 'Register Employer';
if(Yii::$app->user->id){
$this->params['breadcrumbs'][] = ['label' => 'Adminusers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
}
?>
<div class="adminuser-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
