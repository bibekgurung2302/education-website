<?php

use app\models\Adminuser;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AdminuserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Adminusers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="adminuser-index">
    <div class="container">
    <h4><?= Html::encode($this->title) ?></h4>

<p>
    <?= Html::a('Create Adminuser', ['create'], ['class' => 'btn btn-success']) ?>
</p>

<?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

     //   'id',
        'username',
        //'password',
        'org_name',
        [
            'attribute' => 'org_type',
            'value' => 'orgType.title'
        ],
      //  'contact_person',
        'phone',
        [
            'attribute' => 'role',
            'value' => function($model){
                if($model->role == Adminuser::USER_ADMIN){
                    return 'Admin';
                }else if($model->role == Adminuser::USER_EMPLOYER){
                    return 'Employer';
                }
            }
        ],
        //'authkey',
        //'register_date',
        //'created_date',
        [
            'attribute' => 'status',
            'value' => function($model){
                if($model->status == Adminuser::REGISTERED){
                    return 'Registered';
                }else if($model->status == Adminuser::VERIFIED){
                    return 'Verified';
                }
                else if($model->status == Adminuser::INACTIVE){
                    return 'Inactive';
                }
            }
        ],
        [
            'class' => ActionColumn::className(),
           'template' => '{view}'
        ],
    ],
]); ?>


    </div>
</div>
