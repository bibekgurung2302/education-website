<?php

use app\models\Adminuser;
use yii\helpers\Html;
use yii\widgets\DetailView;
use app\controllers\Helper;

/* @var $this yii\web\View */
/* @var $model app\models\Adminuser */

$this->title = $model->id;
if(Yii::$app->user->id){
$this->params['breadcrumbs'][] = ['label' => 'Adminusers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
}
\yii\web\YiiAsset::register($this);
?>
<div class="adminuser-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
    <?php
$helper = new Helper ();
if($helper->getUserId() == $model->id) {
     echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']);
     } ?>
        <!-- <= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?> -->
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
         //   'password',
            'org_name',
            [
                'attribute' => 'org_type',
                'value' => $model->orgType->title
            ],
            'contact_person',
            'phone',
            [
                'attribute' => 'role',
                'value' => function($model){
                    if($model->role == Adminuser::USER_ADMIN){
                        return 'Admin';
                    }else if($model->role == Adminuser::USER_EMPLOYER){
                        return 'Employer';
                    }
                }
            ],
           // 'authkey',
            'register_date',
            'created_date',
            [
                'attribute' => 'status',
                'value' => function($model){
                    if($model->status == Adminuser::REGISTERED){
                        return 'Registered';
                    }else if($model->status == Adminuser::VERIFIED){
                        return 'Verified';
                    }
                    else if($model->status == Adminuser::INACTIVE){
                        return 'Inactive';
                    }
                }
            ],
        ],
    ]) ?>

</div>
