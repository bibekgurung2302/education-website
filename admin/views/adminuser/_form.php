<?php

use app\models\Adminuser;
use app\models\JobSector;
use kartik\select2\Select2;
use yii\captcha\Captcha;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Adminuser */
/* @var $form yii\widgets\ActiveForm */
$job_sector = ArrayHelper::map(JobSector::find()->where(['status' => JobSector::ACTIVE])->all(), 'id', 'title');
Yii::$app->params['bsDependencyEnabled'] = false;
?>

<div class="adminuser-form">
    <div class="container">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-md-11">
                    <h4><?= Html::encode($this->title) ?></h4>
                    </div>
                    <div class="col-md-1">
                        <?=Html::a('Login','index.php?r=site/login', ['class' => 'btn btn-primary'])?>
                    </div>
                </div>
                
            </div>
            <div class="card-body">
                <?php $form = ActiveForm::begin(); ?>
                <div class="row">
                    <div class="col-md-4">
                        <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'password')->passwordInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'org_name')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <?= $form->field($model, 'org_type')->widget(Select2::className(), [
                        'data' => $job_sector,
                        'options' => ['placeholder' => 'Select Organization type'],
                        'pluginOptions' => [
                            'alloClear' => true,
                        ]
                    ]) ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'contact_person')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-md-4">
                        <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <?=
            $form->field($model, 'captcha')->widget(Captcha::className(), [
                'imageOptions' => [
                    'id' => 'my-captcha-image'
                ]
            ]);
            ?>
                        <?php echo Html::button('Refresh Captcha', ['id' => 'refresh-captcha', 'class' => 'btn btn-sm btn-primary']); ?>
                    </div>
                </div>
                <br>

                <div class="form-group">
                    <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
        </div>
    </div>

</div>
<?php
$this->registerJs("
$('#refresh-captcha').on('click', function(e){
    e.preventDefault();

    $('#my-captcha-image').yiiCaptcha('refresh');
})
"); 
?>