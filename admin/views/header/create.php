<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Organizationdetail */

$this->title = 'Create Organizationdetail';
$this->params['breadcrumbs'][] = ['label' => 'Header', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="Header-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
