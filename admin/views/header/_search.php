<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\OrganizationdetailSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="organizationdetail-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'title') ?>

    <?= $form->field($model, 'logo') ?>

    <?= $form->field($model, 'email') ?>

    <?= $form->field($model, 'contact') ?>

    <?php // echo $form->field($model, 'twitterlink') ?>

    <?php // echo $form->field($model, 'fblink') ?>

    <?php // echo $form->field($model, 'instalink') ?>

    <?php // echo $form->field($model, 'skypelink') ?>

    <?php // echo $form->field($model, 'linkedinlink') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
