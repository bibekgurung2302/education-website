<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Organizationdetail */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Header', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="Header-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            'title',
            ['attribute'=>'logo',
            'format'=>'html',
            'value'=>function($data){
                // $url=$data['logo'];
                return Html::img($data->logo,['width'=>'50','height'=>'50']);
            }
            ],
            'email:email',
            'contact',
            'twitterlink',
            'fblink',
            'instalink',
            'skypelink',
            'linkedinlink',
        ],
    ]) ?>

</div>
