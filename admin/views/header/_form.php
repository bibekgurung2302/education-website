<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Organizationdetail */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="Header-form">

    <?php $form = ActiveForm::begin(['options'=>['enctype'=>'multipart/form-data']]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'logoImage')->fileInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'contact')->textInput() ?>

    <?= $form->field($model, 'twitterlink')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'fblink')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'instalink')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'skypelink')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'linkedinlink')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
