<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model app\models\Productdetail */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="photo-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

     <?= $form->field($model, 'name')->textinput(['rows' => 6]) ?> 
<!--    //<= $form->field($model, 'nam')->widget(TinyMce::className(), [
//    'options' => ['rows' => 6],
//    'language' => 'en',
//    'clientOptions' => [
//        'plugins' => [
//            "advlist autolink lists link charmap print preview anchor",
//            "searchreplace visualblocks code fullscreen",
//            "insertdatetime media table contextmenu paste"
//        ],
//        'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
//    ]
//]);?>-->
    
          <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>
    
    
    <?= $form->field($model, 'productImage')->fileInput(['maxlength' => true]) ?>
    


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
