<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Productdetail */

$this->title = 'Create Photo';
$this->params['breadcrumbs'][] = ['label' => 'Photo', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="Photo-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
