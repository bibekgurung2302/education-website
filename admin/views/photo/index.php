<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductdetailSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Photo';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="Photo-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Photo', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'name',
            'link',
            ['attribute'=>'image',
            'format'=>'html',
            'value'=>function($data){
                $url=$data['image'];
                return Html::img($url,['width'=>'100','height'=>'100']);
            }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
