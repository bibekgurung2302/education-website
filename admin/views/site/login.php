<?php

/** @var yii\web\View $this */
/** @var yii\bootstrap4\ActiveForm $form */
/** @var app\models\LoginForm $model */

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;

//$this->title = 'Login';
//$this->params['breadcrumbs'][] = $this->title;
?>

<style>
.h-custom {
    height: calc(100% - 73px);
}

@media (max-width: 450px) {
    .h-custom {
        height: 100%;
    }
}
</style>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css">
<div class="site-login" style="margin-top:70px;">
    <section class="vh-30">
        <div class="container-fluid h-custom" style="min-height:415px;">
            <div class="row d-flex justify-content-center align-items-center">
                <div class="col-md-9 col-lg-6 col-xl-5">
                    <img src="img/logo/login.webp" class="img-fluid" alt="Sample image">
                </div>
                <div class="card">
                    <div class="card-header" style="text-align:center;">
                        <img src="img/ .jpg" width="60" height="60">
                        <h4>GSMI LOGIN</h4>
                    </div>
                    <div class="card-body">
                        <div class="col-md-12 col-lg-6 col-xl-12" style="margin-top: -50px;">
                            <div class="divider d-flex align-items-center my-4">
                                <p class="text-center fw-bold mx-3 mb-0"></p>
                            </div>
                            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

                            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                            <?= $form->field($model, 'password')->passwordInput() ?>
                            <div class="row">
                                <div class="col-md-6">
                                <?= $form->field($model, 'rememberMe')->checkbox([]) ?>
                                </div>
                                <div class="col-md-6">
                                <a href="index.php" style="font-size:14px;">Forget Password</a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                <div class="form-group">
                                <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                                <?php ActiveForm::end(); ?>
                            </div>
                                </div>
                                <div class="col-md-6">
                                <!--<a href="index.php?r=adminuser/create">Register</a>-->
                                </div>
                            </div>
                            
                            
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </section>
</div>