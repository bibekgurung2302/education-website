<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\OurActivities */

$this->title = 'Create Our Activities';
$this->params['breadcrumbs'][] = ['label' => 'Our Activities', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="our-activities-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
