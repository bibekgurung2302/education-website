
<?php
/** @var yii\web\View $this */
/** @var string $content */
use app\assets\AppAsset;
use app\models\Adminuser;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <?php
        $this->registerCsrfMetaTags();
        $this->title = 'EDUSITE';
        ?>
        <title><?= $this->title ?></title>

        <?php $this->head() ?>
        <style>
            .breadcrumb{
                background-color: #E7E7E7;

            }
        </style>
    </head>
    <body id="page-top">
        <div id="wrapper">
            <?php
            $this->beginBody();
              $helper = new \app\controllers\Helper();
              if ($helper->getUserId()) {
            
            ?>

            <!-- Sidebar -->
            <ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar">
                <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php?r=site/index">
                    <div class="sidebar-brand-icon">
<!--                            <img src="img/logo/jaban.jpg">-->
                    </div>
                    <div class="sidebar-brand-text mx-3">EDUSITE </div>
                </a>
                <hr class="sidebar-divider my-0">
                <li class="nav-item active">
                    <a class="nav-link" href="index.php?r=site/index">
                        <i class="fas fa-fw fa-tachometer-alt"></i>
                        <span>Dashboard</span></a>
                </li>
                <hr class="sidebar-divider">
                <?php
                if ($helper->getUserRole() == Adminuser::USER_ADMIN) {
                        echo $this->render('admin_side_items');
                }else if($helper->getUserRole() == Adminuser::USER_EMPLOYER){
                    echo $this->render("employer_side_items");
                }
               // echo $this->render('admin_side_items');
                ?>
                <div class="version" id="version-ruangadmin"></div>
            </ul>
            <!-- Sidebar -->
            <?php } ?>
            <div id="content-wrapper" class="d-flex flex-column">
                <div id="content">
                    <!-- TopBar -->
                    <nav class="navbar navbar-expand navbar-light bg-navbar topbar mb-4 static-top">
                        <button id="sidebarToggleTop" class="btn btn-link rounded-circle mr-3">
                            <i class="fa fa-bars"></i>
                        </button>
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item no-arrow">
                            <a style="float:left;" class="nav-link" href="#">EDUSITE </span></a> 
                            </li>
                            <?php
                              if (!Yii::$app->user->isGuest) {
                            ?>
                            <li class="nav-item dropdown no-arrow mx-1">
                                <a class="nav-link dropdown-toggle" href="#" id="messagesDropdown" role="button" data-toggle="dropdown">
                                    <!--                                        Expires in-->
                                </a>

                            </li>
                            <?php  }   ?>
                            <div class="topbar-divider d-none d-sm-block"></div>
                            <li class="nav-item dropdown no-arrow">
                                <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown"
                                   aria-haspopup="true" aria-expanded="false">
                                    <img class="img-profile rounded-circle" src="img/boy.png" style="max-width: 60px">
                                    <span class="ml-2 d-none d-lg-inline text-white small"><?= Yii::$app->user->isGuest ? '' : Yii::$app->user->identity->username ?></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                                    <a class="dropdown-item" href="#">
                                        <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                        Profile
                                    </a>
                                    <a class="dropdown-item" href="#">
                                        <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                                        Settings
                                    </a>
                                    <a class="dropdown-item" href="#">
                                        <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                                        Activity Log
                                    </a>
                                    <div class="dropdown-divider"></div>
                                    <?php
                                    if (Yii::$app->user->isGuest) {
                                    echo \yii\helpers\Html::a('<i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>Login', ['site/login'], ['class' => 'dropdown-item']);
                                    ?>

                                    <?php
                                    } else {

                                    echo \yii\helpers\Html::a('Log Out', ['site/logout'], ['data' => ['confirm' => 'Log Out'], 'data-method' => 'POST', 'class' => 'dropdown-item']);
                                    }
                                    ?>

                                </div>
                            </li>
                        </ul>
                    </nav>

                    <div class="container-fluid" id="container-wrapper">
                        <?=
                        Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        ])
                        ?>
                        <?= Alert::widget() ?>
                        <?= $content ?>
                    </div>
                    <!-- Topbar -->
                </div>
                <!-- Footer -->
                <footer class="sticky-footer bg-white">
                    <div class="container my-auto">
                        <div class="copyright text-center my-auto">
                            <span>copyright &copy; <script> document.write(new Date().getFullYear());</script> - All Rights Reserved
                            </span>
                        </div>
                    </div>
                </footer>
                <!-- Footer -->
            </div>

        </div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>


