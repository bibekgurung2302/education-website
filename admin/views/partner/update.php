<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Productdetail */

$this->title = 'Update Partner: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Partner', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="Partner-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
