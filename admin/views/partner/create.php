<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Productdetail */

$this->title = 'Create Partner';
$this->params['breadcrumbs'][] = ['label' => 'Partner', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="Partner-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
