<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Member */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Members', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="member-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
//         
         
            'full_Name',
             'position',
            'contact',
            'email:email',
            // 'profile_img',
            [
                'attribute' =>'profile_img',
                'format' => 'html',
                'label' => 'profile_img',
                'value' => function($data){
                return Html :: img($data['profile_img'],
                        [
                    'width' => '80px',
                    'height' => '80px',
                ]);
                }
            ],
            'created_date',
            // 'status',
            [
                'attribute'=>'status',
                'value'=>function($data){
        if($data->status==1){
            return('Active');
        }else{
            return('Inactive');
        }
        
    }
            ],
        ],
    ]) ?>

</div>
