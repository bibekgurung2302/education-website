<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
// use yii\models\designation;


/* @var $this yii\web\View */
/* @var $searchModel app\models\MemberSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Members';
$this->params['breadcrumbs'][] = $this->title;
// $designation=designation::find()->where(['id'=>$model['designation']])->one();
?>
<div class="member-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Member', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
//           
//            'fk_member_cat',
            [
                'attribute'=>'fk_member_cat',
                'value' =>'memberCat.category'
            ],
  
            'full_Name',
           'position',
        // '$designation->designation',
            'contact',
            'email:email',
            // 'profile_img',
            [
                'attribute' =>'profile_img',
                'format' => 'html',
                'label' => 'profile_img',
                'value' => function($data){
                return Html :: img($data['profile_img'],
                        [
                    'width' => '80px',
                    'height' => '80px',
                ]);
                }
            ],
            //'created_date',
            //'status',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action,  $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
