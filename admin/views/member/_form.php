<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
//use app\models\MemberCat;

/* @var $this yii\web\View */
/* @var $model app\models\Member */
/* @var $form yii\widgets\ActiveForm */

$mem_category= ArrayHelper::map(app\models\MemberCat::find()->all(),'id','category');
?>

<div class="member-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="card">
        <div class="card-header">
              <h1><?= Html::encode($this->title) ?></h1>
        </div>
        <div class="card-body">
<div class="row">
   
 <div class="col-md-4">
    <?= $form->field($model, 'full_Name')->textInput(['maxlength' => true]) ?>
 </div>
   
 <div class="col-md-4">
    <?= $form->field($model, 'position')->textInput(['maxlength' => true]) ?>
      
     </div>
 <div class="col-md-4">
     <?= $form->field($model, 'fk_member_cat')->dropDownList($mem_category,['pormpt'=>'Choose Category...']) ?>
 </div>
 <div class="col-md-4">
    <?= $form->field($model, 'contact')->textInput(['maxlength' => true]) ?>
 </div>
 <div class="col-md-4">
    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

   </div>
    
 <div class="col-md-4">

    <!--<= $form->field($model, 'created_date')->textInput() ?>-->

    <?= $form->field($model, 'status')->dropDownList(['1'=>'Active','0'=>'Inactive'],['prompt'=>'Select Status']) ?>
   </div>
    <div class="col-md-12">
      <?= $form->field($model, 'profile_img')->fileInput() ?>
        </div>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
   
 </div>
</div></div>
        <div>
    <?php ActiveForm::end(); ?>

</div>
