<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $query app\models\Register */
$this->title = 'Digital Summit';
//$this->params['breadcrumbs'][] = ['label' => 'Registers', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<style>
#w1{
padding-left: 750px;
}
.program {
width:100%;
}
/*@media only screen and (max-width: 750px) {*/
/*          ul li {*/
/*               padding: 0px;*/
/*           }*/
/*       }*/

</style>
<div class="container">
    <div class="col-md-6">
        <?php foreach($query as $value){ ?>
        <div class="card" style="width: 428px; height: 150px;">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        
                        <h6><strong>Name :</strong> <?= $value['full_name']; ?> </h6>
                        <h6><strong>Contact : </strong><?= $value['contact']; ?></h6>
                        <h6><strong>Organization : </strong><?= $value['org_name']; ?></h6>
                        <h6><strong>Status : </strong>
                            <?php if($value['status'] == 3){ 
                                echo "<label class='text-success'>Approved</label>";
                                 }else if($value['status'] == 2){ 
                                echo "<label class='text-primary'>Verified</label>";
                            } else if($value['status'] == 1){ 
                                echo "<label class='text-danger'>Submitted</label>";
                            }
                    ?></h6>
                   
                    </div>
                    
                </div><br>
                
            </div>
        </div>
        <br>
         <?php } ?>
    </div>
    
</div>