<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model app\models\Register */
/* @var $form yii\widgets\ActiveForm */

Yii::$app->params['bsDependencyEnabled'] = false;
?>
<style>
    #w2{
                padding-left: 750px;
            }
            @media only screen and (max-width: 750px) {
               ul li {
                    padding: 0px;
                }
    }
    form div.required label.control-label:after {

        content:" * ";

        color:red;

    }
    .help-block{
        color: red;
    }
</style>
<div class="register-form">
   
    <?php 
    $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'contact')->textInput(['maxlength' => true]) ?>
        </div>

    </div>
   
    <br>
    <div class="form-group">
        <?= Html::submitButton('Validate Guest', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
<img style="width:100%;" src="img/partners.JPG" class="pull-left img-responsive">
</div>