<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Register */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Registers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<style>
    h1, h2, h3{
        font-size: 14px;
    }
    table, th, td{
        font-size: 13px;
        height: 10px;
    }


    tr{
        padding: 0;
        height: 10px;
        margin: 0;
        line-height: 1;
    }
    p{
        font-size: 12px;

    }

        #w1{
                padding-left: 750px;
            }
            @media only screen and (max-width: 750px) {
                li {
                    padding: 0px;
                }
            }
    

</style>
<div class="register-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php if($model->status == 2){ 
            echo Html::a('Approve', ['approve', 'id' => $model->id], [
            'class' => 'btn btn-success',
            'data' => [
                'confirm' => 'Are you sure to approve for Digital Summit?'
            ]
        ]); } else if($model->status==1){ 
            echo Html::a('Otp Verify', ['otp-verify', 'id' => $model->id], [
            'class' => 'btn btn-success',
            'data' => [
                'confirm' => 'Are you sure to verify otp?'
            ]
        ]); 
            
        } ?>
            <?php /* echo Html::a('Delete', ['delete', 'id' => $model->id], [
          'class' => 'btn btn-danger',
          'data' => [
          'confirm' => 'Are you sure you want to delete this item?',
          'method' => 'post',
          ],
          ]) */ ?>
    </p>
    <table class="table table-striped">
        <tr>
            <th>Full Name</th>
            <th>Contact</th>
            <th>Email</th>
            <th>Province</th>
            <th>District</th>
            <th>Local Level</th>
            <th>Service Sector</th>
        </tr>
        <tr>
            <td><?= $model->full_name; ?></td>
            <td><?= $model->contact; ?></td>
            <td><?= $model->email; ?></td>
            <td><?= $model->province->province; ?></td>
            <td><?= $model->district->district; ?></td>
            <td><?= $model->municipals->name; ?></td>
            <td><?= $model->service_sector; ?>
            </td>
        </tr>
        <tr>
            <th>Organization Name</th>
            <th>Organization Sector From</th>
            <th>Designation</th>
            <th>From where did you know about Event?</th>
            <th>Meal Type</th>
            <th>Created Date</th>
            <th>Status</th>
        </tr>
        <tr>
            <td><?= $model->org_name; ?></td>
            <td><?php
                if ($model->org_sector_from == 1) {
                    echo 'Sub Metroplitan';
                } else if ($model->org_sector_from == 2) {
                    echo 'Municipality';
                } else if ($model->org_sector_from == 3) {
                    echo 'Rural Municipality';
                } else if ($model->org_sector_from == 4) {
                    echo 'Sahakari';
                } else if ($model->org_sector_from == 5) {
                    echo 'PABSON';
                } else if ($model->org_sector_from == 6) {
                    echo 'NPABSON';
                } else if ($model->org_sector_from == 7) {
                    echo 'Other';
                }
                ?>
            </td>
            <td><?= $model->designation; ?></td>
            <td><?php
                $info_src = [];
                $info_src = explode(",", $model->info_source);
                $count = count($info_src);
                $social = "";
                $news = "";
                $frnds = "";
                $output = "";
                if ($count == 3) {
                    if ($info_src[0] == 1) {
                        $social = "Social Media";
                    }
                    if ($info_src[1] == 2) {
                        $news = "News Portal";
                    }
                    if ($info_src[2] == 3) {
                        $frnds = "Friends";
                    }
                    echo $social . " , " . $news . " , " . $frnds;
                } else if ($count == 2) {
                    if ($info_src[0] == 1) {
                        $social = "Social Media";
                    } else if ($info_src[0] == 2) {
                        $social = "News Portal";
                    } else if ($info_src[0] == 3) {
                        $social = "Friends";
                    }
                    if ($info_src[1] == 1) {
                        $news = "Social Media";
                    } else if ($info_src[1] == 2) {
                        $news = "News Portal";
                    } else if ($info_src[1] == 3) {
                        $news = "Friends";
                    }
                    echo $social . " , " . $news;
                } else if ($count == 1) {
                    if ($info_src[0] == 1) {
                        $output = "Social Media";
                    } else if ($info_src[0] == 2) {
                        $output = "News Portal";
                    } else if ($info_src[0] == 3) {
                        $output = "Friends";
                    }
                    echo $output;
                }
                ?>
            </td>
            <td><?php
                if ($model->meal_type == 7) {
                    echo "Veg";
                } else if ($model->meal_type == 8) {
                    echo "Non Veg";
                }
                ?>
            </td>
            <td><?=$model->created_date; ?></td>
            <td><?php
                if ($model->status == 1) {
                    echo '<label class="text-danger">Submitted</label>';
                } else if ($model->status == 2) {
                    echo '<label class="text-primary">Verified</label>';
                } else if ($model->status == 3) {
                    echo '<label class="text-success">Approved</label>';
                }
                ?></td>
        </tr>
    </table>
    
</div>
