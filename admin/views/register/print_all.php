<?php
use yii\helpers\Html;
use Da\QrCode\QrCode;
/* @var $this yii\web\View */
/* @var $query app\models\Register */
$this->title = 'Digital Summit';
//$this->params['breadcrumbs'][] = ['label' => 'Registers', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<style>
#w1{
padding-left: 750px;
}
.program {
width:100%;
}
/*@media only screen and (max-width: 750px) {*/
/*          ul li {*/
/*               padding: 0px;*/
/*           }*/
/*       }*/

</style>
<input class="printPagebooking btn btn-primary" style="float: right;" name="b_print" type="button" class="ipt" onClick="printdivbooking('div_printbooking');" value=" Print ">
<div class="container">
	<br><br>
	<div id="div_printbooking">
		<div class="row">
		<?php foreach($query as $value){ ?>
                    <div class="col-md-6">
                        <div class="card" style="width: 530px; height: 271px;">
                            <div class="card-body">
                                
                                    <div class="col-md-8">
                                        <h6><strong>Name :</strong> <?= $value['full_name']; ?> </h6>
                                        <h6><strong>Contact : </strong><?= $value['contact']; ?></h6>
                                        <h6><strong>Organization : </strong><?= $value['org_name']; ?></h6> 
                                    </div>

                                    <div class="col-md-2">
                                        <?php
                                        $qrCode = (new QrCode($value['contact']))
                                                ->setSize(100)
                                                ->setMargin(5)
                                                ->useForegroundColor(0, 0, 0);
                                        header('Content-Type: ' . $qrCode->getContentType());
                                        $qrCode->writeString();
                                        echo '<img src="' . $qrCode->writeDataUri() . '">';
                                        ?>
                                    </div>
                                

                            </div>
                        </div>
                    </div>
                    
                     <?php } ?>
                     </div><br>
                </div>

                <script language="javascript">
    function printdivbooking(printpagebooking) {
        var headstr = "<html><head><title></title><style type='text/css'>body{height:270px;width:428px;}</style></head><body>";
        var bodystr = "";
        var footstr = "</body>";
        var newstr = document.all.item(printpagebooking).innerHTML;
        var oldstr = document.body.innerHTML;
        document.body.innerHTML = headstr + bodystr + newstr + footstr;
        window.print();
        document.body.innerHTML = oldstr;
        return false;
    }
</script>
</div>