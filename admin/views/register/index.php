<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
//use yii\grid\GridView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RegisterSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Registers';
$this->params['breadcrumbs'][] = $this->title;
Yii::$app->params['bsDependencyEnabled'] = false;
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<style>
    h1, h2, h3{
        font-size: 14px;
    }
    table, th, td{
        font-size: 13px;
        height: 10px;
    }
    
    tr{
        padding: 0;
        height: 10px;
        margin: 0;
        line-height: 1;
    }
    p{
        font-size: 12px;
        
    }
    input[type="text"]{
        height: 30px;
        font-size: 13px;
    }
    input[name="RegisterSearch[contact]"]{
        width: 80px;
    }
    input[name="RegisterSearch[org_sector_from]"]{
        width: 160px;
    }
    
    select[name="RegisterSearch[status]"]{
        height: 30px;
    }
    table thead th th{
        width: 30px;
    }
    .action-column{
        width: 60px;
    }
     #w2{
                padding-left: 750px;
            }
            @media only screen and (max-width: 750px) {
               ul li {
                    padding: 0px;
                }
    }
    .navbar-brand{
        font-size: 20px;
        padding-top: 10px;
    }
    #w3-collapse{
        padding-left: 750px;
    }
    .logout{
        font-size: 16px;
    }
    ol li{
        font-size: 16px;
    }
    .btn{
        font-size: 16px;
    }
       
</style>
<div class="register-index">


    <p>
        <?= Html::a('Register', ['create'], ['class' => 'btn btn-success']) ?>
        <?= Html::a('Qr Scan', ['scan'], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Print All', ['print-all'], ['class' => 'btn btn-warning'])?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'id'=>'grid-view',
        'bordered' => true,
        'striped' => true,
        // 'selectedColumns'=> [1, 2, 3, 4, 5], 
        'condensed' => false,
        'responsive' => true,
        'hover' => true,
        'floatHeader' => true,
      //  'showPageSummary' => true,
        'toolbar'=>['{toggleData}'],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<b style="font-weight:bold;margin-left:45%;">Register Report</b>',
        ],
        
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'full_name',
            'contact',
            'email:email',
           // 'fk_province',
            //'fk_district',
            //'fk_local_level',
            //'service_sector',
            'org_name',
            [
                'attribute' => 'org_sector_from',
                'value' => function($model){
        if($model->org_sector_from==1){
            return 'Sub Metroplitan';
                }else if($model->org_sector_from==2){
                  return 'Municipality';  
                }else if($model->org_sector_from==3){
                   return 'Rural Municipality'; 
                }
                else if($model->org_sector_from==4){
                    return 'Sahakari';
                }
                else if($model->org_sector_from==5){
                    return 'PABSON';
                }
                else if($model->org_sector_from==6){
                    return 'NPABSON';
                }
                else if($model->org_sector_from==7){
                    return 'Other';
                }
                }
            ],
            //'designation',
            //'info_source',
            //'meal_type',
          //  'qr_code',
            //'otp',
            //'created_date',
            [
                'attribute' => 'status',
                'format' => 'raw',
                'filter' => [1 => 'Submitted', 2 =>'Verified', 3 => 'Approved'],
                'value' => function($model){
                if($model->status==1){
                    return '<label class="text-danger">Submitted</label>';
                }else if($model->status==2){
                    return '<label class="text-primary">Verified</label>';
                }else if($model->status==3){
                    return '<label class="text-success">Approved</label>';
                }
                }
            ],
            [
                'class' => ActionColumn::className(),
                'template' => '{view} {print}',
                'buttons' => [
                    'print' => function ($url, $model, $key) {
                        return Html::a('<i class="fa fa-print"></i>', ['register/print', 'id' => $model->id]);
                    }
                ],
            ],
        ],
    ]); ?>


</div>
