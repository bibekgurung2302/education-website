<?php

use Da\QrCode\QrCode;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Register */

$this->title = 'Digital Summit';
//$this->params['breadcrumbs'][] = ['label' => 'Registers', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    #w1{
                padding-left: 750px;
            }
            @media only screen and (max-width: 750px) {
               ul li {
                    padding: 0px;
                }
    }
</style>
<div class="pt-30 pb-30">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">

                <input class="printPagebooking btn btn-primary" style="float: right;" name="b_print" type="button" class="ipt" onClick="printdivbooking('div_printbooking');" value=" Print ">
                <!--========================Print Div Start ==================================-->
                <div id="div_printbooking">
                    <div class="col-md-6">
                        <div class="card" style="width: 530px; height: 271px;">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-8">
                                        <h6><strong>Name :</strong> <?= $model->full_name; ?> </h6>
                                        <h6><strong>Contact : </strong><?= $model->contact; ?></h6>
                                        <h6><strong>Organization : </strong><?= $model->org_name; ?></h6> 
                                    </div>

                                    <div class="col-md-2">
                                        <?php
                                        $qrCode = (new QrCode($model->contact))
                                                ->setSize(100)
                                                ->setMargin(5)
                                                ->useForegroundColor(0, 0, 0);
                                        header('Content-Type: ' . $qrCode->getContentType());
                                        $qrCode->writeString();
                                        echo '<img src="' . $qrCode->writeDataUri() . '">';
                                        ?>
                                    </div>
                                </div><br>
                              <!--  <div class="row">
                                    <div class="col-md-3">
                                        <img src="img/logo/tuki_logo.jpeg" width="80" alt="TUKI"/>
                                    </div>
                                    <div class="col-md-3">
                                        <img src="img/logo/CAN_banke.webp" width="80" alt="CAN Banke"/>
                                    </div>
                                    <div class="col-md-3">
                                        <img src="img/logo/ninja_logo.png" width="80" alt="Ninja Infosys"/>
                                    </div>
                                    <div class="col-md-3">
                                        <img src="img/logo/icash.jpeg" width="80" alt="Ninja Infosys"/>
                                    </div>
                                </div><br>
                                <div class="row">
                                    <div class="col-md-12">
                                        <h5 class="text-center">~~~~~~ Digital Summit 2022 ~~~~~~</h5>
                                    </div>
                                    
                                </div> -->

                            </div>
                        </div>
                    </div>

                </div>
                <!--========================Print Div Closing ==================================-->

            </div>
        </div>
    </div>
</div>

<script language="javascript">
    function printdivbooking(printpagebooking) {
        var headstr = "<html><head><title></title><style type='text/css'>body{height:270px;width:428px;}</style></head><body>";
        var bodystr = "";
        var footstr = "</body>";
        var newstr = document.all.item(printpagebooking).innerHTML;
        var oldstr = document.body.innerHTML;
        document.body.innerHTML = headstr + bodystr + newstr + footstr;
        window.print();
        document.body.innerHTML = oldstr;
        return false;
    }
</script>