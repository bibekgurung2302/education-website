<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Province;
use app\models\District;
use app\models\Municipals;
use app\models\Register;


/* @var $this yii\web\View */
/* @var $model app\models\Register */
/* @var $form yii\widgets\ActiveForm */
$province = ArrayHelper::map(Province::find()->all(), 'id', 'province');
$district = ArrayHelper::map(District::find()->all(), 'id', 'district');
$local_level = ArrayHelper::map(Municipals::find()->all(), 'id', 'municipals');
Yii::$app->params['bsDependencyEnabled'] = false;
?>
<style>
    form div.required label.control-label:after {

        content:" * ";

        color:red;

    }
    .help-block{
        color: red;
    }
</style>
<div class="register-form">
   
    <?php 
    $model = new Register();
    $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'otp')->textInput(['placeholder' => 'Please enter your 6-digit otp code']) ?>
        </div>
        

    </div>
   
    <br>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
