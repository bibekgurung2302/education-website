<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Register */

$this->title = 'Digital Summit';
//$this->params['breadcrumbs'][] = ['label' => 'Registers', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="register-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
