<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Province;
use app\models\District;
use app\models\Municipals;
use kartik\depdrop\DepDrop;
use kartik\widgets\Select2;
use yii\helpers\Url;
use app\models\Register;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $model app\models\Register */
/* @var $form yii\widgets\ActiveForm */
$province = ArrayHelper::map(Province::find()->all(), 'id', 'province');
$district = ArrayHelper::map(District::find()->all(), 'id', 'district');
$local_level = ArrayHelper::map(Municipals::find()->all(), 'id', 'municipals');
Yii::$app->params['bsDependencyEnabled'] = false;
?>
<style>
    form div.required label.control-label:after {

        content:" * ";

        color:red;

    }
    .help-block{
        color: red;
    }
   
         
           ul li{
                padding-left: 750px;
            }
            @media only screen and (max-width: 750px) {
               ul li {
                    padding: 0px;
                }
            }

        
</style>
<div class="register-form">
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-6">
                <img style="float: right" class="img-responsive" src="img/logo/tuki_logo.jpeg" width="150" height="60" title="TUKI Complete EMIS" alt="TUKI"/>
            </div><br>
            <div class="col-md-6">
                <img src="img/logo/CAN_banke.webp" width="120" height="60" alt="alt"/>
                <h6 style="margin-top:-10px;padding-left: 70px;color:darkblue"><strong>Banke</strong></h6>
                <h5 style="margin-top: -35px;color: darkblue;" class="text-right"><strong>Presents </strong></h5>
            </div>

            <h3 class="text-center bg-primary text-white"><strong>DIGITAL SUMMIT</strong></h3>
            <div class="col-md-8">
                <h5 style="color:darkblue;" class="text-right"><strong>In association with</strong></h5>

            </div>
            <div class="col-md-9 text-right">
                <img  src="img/logo/ninja_logo.png" width="180" height="60" alt="alt"/>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-md-12">
        <?php echo Html::a('Click here for program details', ['program'], ['class' => 'btn btn-primary'])?>
        </div>
        </div>
    <br>
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'contact')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
        </div>

    </div>
    <div class="row">
        <div class="col-md-4">
            <?=
            $form->field($model, 'fk_province')->widget(Select2::className(), [
                'data' => $province,
                'options' => ['placeholder' => 'Select Province', 'id' => 'province'],
                'pluginOptions' => [
                    'allowClear' => true
                ]
            ]);
            ?>
        </div>
        <div class="col-md-4">
            <?=
            $form->field($model, 'fk_district')->widget(DepDrop::className(), [
                'type' => DepDrop::TYPE_SELECT2,
                'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                'options' => ['id' => 'district'],
                'pluginOptions' => [
                    'initialize' => $model->isNewRecord ? false : true,
                    'depends' => ['province'],
                    'placeholder' => 'Select District',
                    'url' => Url::to(['register/district'])
                ]
            ])
            ?>
        </div>
        <div class="col-md-4">
            <?=
            $form->field($model, 'fk_local_level')->widget(DepDrop::className(), [
                'type' => DepDrop::TYPE_SELECT2,
                'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                'options' => ['id' => 'municipals'],
                'pluginOptions' => [
                    'initialize' => $model->isNewRecord ? false : true,
                    'depends' => ['district'],
                    'placeholder' => 'Select Local Level',
                    'url' => Url::to(['register/municipals'])
                ]
            ])
            ?>
        </div>

    </div>
    <div class="row">
        <div class="col-md-4">
            <?=
            $form->field($model, 'service_sector')->widget(Select2::className(), [
                'data' => [
                    'Education' => 'Education',
                    'Krishi' => 'Krishi'
                ],
                'options' => ['placeholder' => 'Select Service Sector'],
                'pluginOptions' => [
                    'allowClear' => true,
                ]
            ])
            ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'org_name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?=
            $form->field($model, 'org_sector_from')->widget(Select2::className(), [
                'data' => Register::getSectorFrom(),
            ])
            ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'designation')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'info_source')->checkboxList([
                Register::SOCIAL_MEDIA => 'Social Media',
                Register::NEWS_SITE => 'News Portal',
                Register::FRIENDS => 'Friends'
                ]) ?>
        </div>
        <div class="col-md-4">
            <?=
            $form->field($model, 'meal_type')->widget(Select2::className(), [
                'data' => [
                    Register::NON_VEG => 'Non Veg',
                    Register::VEG => 'VEG'
                ],
                'options' => ['placeholder' => 'Select Meal Type'],
                'pluginOptions' => [
                    'allowClear' => true,
                ]
            ])
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <?=
            $form->field($model, 'captcha')->widget(Captcha::className(), [
                'imageOptions' => [
                    'id' => 'my-captcha-image'
                ]
            ]);
            ?>
            <?php echo Html::button('Refresh captcha', ['id' => 'refresh-captcha', 'class' => 'btn btn-primary']); ?>
        </div>
        
    </div>
    <br>
    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    <img style="width:100%;" src="img/partners.JPG" class="pull-left img-responsive">
</div>
<?php $this->registerJs("
    $('#refresh-captcha').on('click', function(e){
        e.preventDefault();

        $('#my-captcha-image').yiiCaptcha('refresh');
    })
"); ?>