<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DetailsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="About-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create About', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            'title',
            'discription',
            // 'discription2',
            ['attribute'=>'image1',
            'format'=>'html',
            'value'=>function($data){
                $url=$data['image1'];
                return Html::img($url,['width'=>'50','height'=>'50']);
            }
            ],
            ['attribute'=>'image2',
            'format'=>'html',
            'value' => function($data){
                $url=$data['image2'];
                return Html::img($url,['width'=>'50','height'=>'50']);
            }
        ],
            'location',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
