<?php

use Codeception\PHPUnit\ResultPrinter\HTML as ResultPrinterHTML;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Details */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'About', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="details-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            // 'id',
            ['attribute'=>'image1',
            'format'=>'html',
            'value'=>function($data){
                $url=$data['image1'];
                return Html::img($url,['width'=>'100','height'=>'100']);
            }
            ],
            ['attribute'=>'image2',
            'format'=> 'html',
            'value'=>function($data){
                $url=$data['image2'];
                return Html::img($url,['width'=>'100','height'=>'100']);
            }
        ],
            'title',
            'discription',
            'location',
            // 'discription2',

           
        ],
    ]) ?>

</div>
