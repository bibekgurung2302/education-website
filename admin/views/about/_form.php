<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model app\models\Details */
/* @var $form yii\widgets\ActiveForm */
$message=yii::$app->session->getFlash('message');
?>

<div class="About-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'discription')->widget(TinyMce::className(), [
    'options' => ['rows' => 15],
    'language' => 'en',
    'clientOptions' => [
        'plugins' => [
            "advlist autolink lists link charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
        ],
        'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
    ]
]);?>
    <!-- <= $form->field($model, 'discription1')->textarea(['rows'=>5]) ?>
    <= $form->field($model, 'discription2')->textarea(['rows'=>10]) ?> -->


    <?= $form->field($model, 'image1')->fileInput(['maxlength' => true]) ?>
    <?php if($message){ ?>
        <h5 style="color:red;"><?= $message ?></h5>
    <?php } ?>
    <?= $form->field($model, 'image2')->fileInput(['maxlength' => true]) ?>
    <?php if($message){ ?>
        <h5 style="color:red;"><?= $message ?></h5>
    <?php } ?>
    <?= $form->field($model, 'location')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
