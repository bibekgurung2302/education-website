<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MemberCatSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Member Cats';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="member-cat-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Member Cat', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
//          
            'category',
//            'created_date',
//            'status',
              [
                'attribute'=>'status',
                'value'=>function($data){
        if ($data->status==1){
            return('Active');
        }else{
            return('Inactive');
        }
                }
            ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action,  $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                 }
            ],
        ],
    ]); ?>


</div>
