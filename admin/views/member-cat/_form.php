<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Introduction */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="introduction-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="container">
        <div class="card">
            <div class="card-header">  <h1><?= Html::encode($this->title) ?></h1>
            </div>
            
            <div class="card-body">
        <div class="row">
               <div class="col-md-12">
                    <?= $form->field($model, 'category')->textarea(['rows' => 6]) ?>
                
            </div>
             <div class="col-md-12">
                
                <?= $form->field($model, 'status')->dropDownList(['1'=>'Active','0'=>'Inactive'],['prompt'=>'Select Status']) ?>

            </div>
           
              
        </div>  
                  <div class="form-group" style="text-align: center" >
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
        </div>
        
        </div>
    </div>
    <!--<= $form->field($model, 'fk_user_id')->textInput() ?>-->

    



    <!--<= $form->field($model, 'created_date')->textInput() ?>-->

    
  

    <?php ActiveForm::end(); ?>

</div>
