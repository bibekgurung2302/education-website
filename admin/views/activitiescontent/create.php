<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ActivitiesContent */

$this->title = 'Create Activities Content';
$this->params['breadcrumbs'][] = ['label' => 'Activities Contents', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="activities-content-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
