<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ActivitiesContent */

$this->title = 'Update Activities Content: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Activities Contents', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="activities-content-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
