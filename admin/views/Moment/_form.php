<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="Moment-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?= $form->field($model, 'icon')->dropDownList(['bi bi-emoji-smile' =>'Smile','bi bi-journal-richtext'=>'image','bi bi-headset'=>'Headphone','bi bi-people'=>' people'],['prompt'=>'select icon']) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
    
     <!--<= $form->field($model, 'logo')->fileInput(   ) ?>-->

    <?= $form->field($model, 'discription')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
