<?php include "connect_db.php" ?>
<?php include "header.php" ?>
<!-- slider section -->
<section class="slider_section ">
  <div id="customCarousel1" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
      <?php
      $count = 1;
      $sql = "SELECT * FROM banner";
     // var_dump($sql);die;
      $a = mysqli_query($con, $sql);
      foreach($a as $row){
      //  var_dump($row['title1']);die;

      ?>
      <div class="carousel-item <?php if($count == 1){ echo 'active'; } ?>">
        <div class="container">
          <div class="row">
            <div class="col-md-6">

              <div class="detail-box">
                <h5>
                  01
                </h5>
                <h1>
                  <?= $row['title1'] ?><br />
                  <?= $row['title2'] ?>
                </h1>
                <a href="" class="">
                  buy Now
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <?php $count++; } ?>
      <!-- <div class="carousel-item ">
        <div class="container">
          <div class="row">
            <div class="col-md-6">
              <div class="detail-box">
                <h5>
                  02
                </h5>
                <h1>
                  Fresh <br />
                  Vegetables
                </h1>
                <a href="" class="">
                  buy Now
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> -->
      <ol class="carousel-indicators">
        <li data-target="#customCarousel1" data-slide-to="0" class="active"></li>
        <li data-target="#customCarousel1" data-slide-to="1"></li>
      </ol>
    </div>
</section>
<!-- end slider section -->
</div>

<!-- veg section -->

<section class="veg_section layout_padding">
  <div class="container">
    <div class="heading_container heading_center">
      <h2>
        Our Vegetables
      </h2>
      <p>
        which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't an
      </p>
    </div>
    <div class="row">
      <?php
      $sql = "SELECT * FROM products";
      $a = mysqli_query($con, $sql);
      while ($row = mysqli_fetch_assoc($a)) {
      ?>
        <div class="col-md-6 col-lg-4">
          <div class="box">
            <div class="img-box">
              <img src="admin/web/<?= $row['image'] ?>" alt="">
            </div>
            <div class="detail-box">
              <a href="">
                <?= $row['title'] ?>
              </a>
              <div class="price_box">
                <h6 class="price_heading">
                  <span><?= $row['heading'] ?></span> <?= $row['discription'] ?>
                </h6>
              </div>
            </div>
          </div>
        </div>
      <?php } ?>
    </div>
    <div class="btn-box">
      <a href="">
        View More
      </a>
    </div>
  </div>
</section>

<!-- end veg section -->

<!-- about section -->

<section class="about_section ">
  <?php
  $sql = "SELECT * FROM about";
  $a = mysqli_query($con, $sql);
  $row = mysqli_fetch_assoc($a);

  ?>
  <div class="about_bg_box">
    <img src="admin/web/<?= $row['image'] ?>" alt="">
  </div>
  <div class="container ">
    <div class="row">
      <div class="col-md-6 ml-auto ">
        <div class="detail-box">
          <div class="heading_container">
            <h2>
              <?= $row['title'] ?> <br>
            </h2>
          </div>
          <p>
            <?= $row['discription'] ?>
          </p>
          <a href="" class="mt_20">
            Read More
          </a>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- end about section -->

<!-- contact section -->
<section class="contact_section layout_padding">
  <div class="container">
    <div class="heading_container">
      <h2>
        Contact <span>Us</span>
      </h2>
    </div>
    <div class="row">
      <div class="col-md-6">
        <div class="form_container contact-form">
          <form action="">
            <div>
              <input type="text" placeholder="Your Name" />
            </div>
            <div>
              <input type="text" placeholder="Phone Number" />
            </div>
            <div>
              <input type="email" placeholder="Email" />
            </div>
            <div>
              <input type="text" class="message-box" placeholder="Message" />
            </div>
            <div class="btn_box">
              <button>
                SEND
              </button>
            </div>
          </form>
        </div>
      </div>
      <div class="col-md-6">
        <div class="map_container">
          <div class="map">
            <div id="googleMap"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- end contact section -->

<!-- client section -->

<section class="client_section layout_padding-bottom">
  <div class="container">
    <div class="heading_container heading_center">
      <h2>
        What Says Our Customers
      </h2>
    </div>
    <div class="col-md-9 col-lg-7 mx-auto px-0">
      <?php
      $sql = "SELECT * FROM team";
      $a = mysqli_query($con, $sql);
      while ($row = mysqli_fetch_assoc($a)) {

      ?>
        <div class="box">
          <div class="client_id">
            <div class="img-box">
              <img src="admin/web/<?= $row['image'] ?>" alt="">
            </div>
            <div class="name">
              <h5>
                <?= $row['name'] ?>
              </h5>
              <h6>
                <?= $row['designation'] ?>
              </h6>
            </div>
          </div>
          <div class="client_detail">
            <p>
              <?= $row['qualification'] ?>
            </p>
          </div>
        </div>
      <?php } ?>
    </div>
    <div class="btn-box">
      <a href="">
        View More
      </a>
    </div>
  </div>
</section>

<!-- end client section -->

<?php include "footer.php" ?>