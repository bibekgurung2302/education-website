<?php include "connect_db.php" ?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="keywords" content="" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title> Eatveg </title>
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css" />
    <link href="css/font-awesome.min.css" rel="stylesheet" />
    <link href="https://fonts.googleapis.com/css?family=Poppins:400,700|Raleway:400,700&display=swap" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet" />
    <link href="css/responsive.css" rel="stylesheet" />
</head>

<body>
    <div class="hero_area">
        <div class="hero_bg_box">
            <?php
            $sql = "SELECT * FROM banner";
            $a = mysqli_query($con, $sql);
            $row = mysqli_fetch_assoc($a);

            ?>
            <img src="admin/web/<?php  echo $row['Image'] ?>" alt="">
            <h1>sjalj</h1>
        </div>
        <!-- header section strats -->
        <header class="header_section">
            <div class="container-fluid">
                <nav class="navbar navbar-expand-lg custom_nav-container">
                    <a class="navbar-brand" href="index.php">
                        <?php
                        $sql = "SELECT * FROM header";
                        $a = mysqli_query($con, $sql);
                        $row = mysqli_fetch_assoc($a);

                        ?>
                        <span>
                        <?= $row['name'] ?>
                        </span>
                    </a>
                    <div class="" id="">
                        <div class="container">
                            <div class=" mr-auto flex-column flex-lg-row align-items-center">
                                <ul class="navbar-nav justify-content-between ">
                                    <div class="User_option">
                                        <li class="">
                                            <a class="" href="">
                                                <i class="fa fa-user" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                        <form class="form-inline ">
                                            <button class="btn   nav_search-btn" type="submit">
                                                <i class="fa fa-search" aria-hidden="true"></i>
                                            </button>
                                        </form>
                                    </div>
                                </ul>
                            </div>
                        </div>

                        <div class="custom_menu-btn">
                            <button onclick="openNav()">
                                <span class="s-1"> </span>
                                <span class="s-2"> </span>
                                <span class="s-3"> </span>
                            </button>
                        </div>
                        <div id="myNav" class="overlay">
                            <div class="overlay-content">
                                <a href="index.php">HOME</a>
                                <a href="about.php">ABOUT</a>
                                <a href="vegetables.php">VEGETABLES</a>
                                <a href="contact.php">CONTACT US</a>
                            </div>
                        </div>
                    </div>
                </nav>
            </div>
        </header>