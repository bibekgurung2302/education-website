<?php include "header.php" ?>

<!-- about section -->

<section class="about_section ">
    <?php
    $sql = "SELECT * FROM about";
    $a = mysqli_query($con, $sql);
    $row = mysqli_fetch_assoc($a);

    ?>
    <div class="about_bg_box">
        <img src="admin/web/<?= $row['image'] ?>" alt="">
    </div>
    <div class="container ">
        <div class="row">
            <div class="col-md-6 ml-auto ">
                <div class="detail-box">
                    <div class="heading_container">
                        <h2>
                            <?= $row['title'] ?> <br>
                        </h2>
                    </div>
                    <p>
                        <?= $row['discription'] ?>
                    </p>
                    <a href="" class="mt_20">
                        Read More
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- end about section -->

<?php include "footer.php" ?>