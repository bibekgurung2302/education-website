<!-- footer section -->
<?php
$sql = "SELECT * FROM contacts";
$a = mysqli_query($con, $sql);
$row = mysqli_fetch_assoc($a);

?>
<section class="container-fluid footer_section">
    <div class="container">
        <div class="row ">
            <div class="col-sm-6 col-md-4 col-lg-3 footer-col">
                <div class="footer_detail">
                    <a href="index.php">
                        <h4>
                            Eatveg
                        </h4>
                    </a>
                    <p>
                        Soluta odit exercitationem rerum aperiam eos consectetur impedit delectus qui reiciendis, distinctio, asperiores fuga labore a? Magni natus.
                    </p>
                    <div class="social_box">
                        <a href="">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                        </a>
                        <a href="">
                            <i class="fa fa-twitter" aria-hidden="true"></i>
                        </a>
                        <a href="">
                            <i class="fa fa-linkedin" aria-hidden="true"></i>
                        </a>
                        <a href="">
                            <i class="fa fa-instagram" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3 mx-auto footer-col">
                <h4>
                    Contact us
                </h4>
                <p>
                    Lorem ipsum dolor sit amet consectetur adipisicing elit
                </p>
                <div class="contact_nav">
                    <a href="">
                        <span>
                            <?= $row['location'] ?>
                        </span>
                    </a>
                    <a href=""> 
                        <span>
                            <?= $row['callus'] ?>
                        </span>
                    </a>
                    <a href="">
                        <span>
                            <?= $row['email'] ?>
                        </span>
                    </a>
                </div>
            </div>
            <div class="col-md-4 footer-col">
                <div class="footer_form">
                    <h4>
                        SIGN UP TO OUR NEWSLETTER
                    </h4>
                    <form action="">
                        <input type="text" placeholder="Enter Your Email" />
                        <button type="submit">
                            Subscribe
                        </button>
                    </form>
                </div>
            </div>
        </div>
        <div class="footer-info">
            <p>
                &copy; <span id="displayYear"></span> All Rights Reserved By
                <a href="#">Eagle Eye IT Service Pvt Ltd</a>
            </p>
        </div>
    </div>
</section>
<!-- end  footer section -->

<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCh39n5U-4IoWpsVGUHWdqB6puEkhRLdmI&callback=myMap"></script>
</body>

</html>