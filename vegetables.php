<?php include "header.php" ?>

<!-- veg section -->

<section class="veg_section layout_padding">
    <div class="container">
        <div class="heading_container heading_center">
            <h2>
                Our Vegetables
            </h2>
            <p>
                which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't an
            </p>
        </div>
        <div class="row">
            <?php
            $sql = "SELECT * FROM products";
            $a = mysqli_query($con, $sql);
            while ($row = mysqli_fetch_assoc($a)) {
            ?>
                <div class="col-md-6 col-lg-4">
                    <div class="box">
                        <div class="img-box">
                            <img src="admin/web/<?= $row['image'] ?>" alt="">
                        </div>
                        <div class="detail-box">
                            <a href="">
                                <?= $row['title'] ?>
                            </a>
                            <div class="price_box">
                                <h6 class="price_heading">
                                    <span><?= $row['heading'] ?></span> <?= $row['discription'] ?>
                                </h6>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <div class="btn-box">
            <a href="">
                View More
            </a>
        </div>
    </div>
</section>

<!-- end veg section -->

<?php include "footer.php" ?>